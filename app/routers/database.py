from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app.internal.resources.sqlite import crud, models
from app.internal import models as schemas
from app.internal.resources.sqlite.database import SessionLocal, engine


router = APIRouter()

#Dependency
def get_db():
    db = SessionLocal()
    try: 
        yield db
    finally: 
        db.close()

#Not yet attached a Route b/c not sure if I want to allow ability to create medical summaries via API@router.get("/medical_summaries/", response_model=List[schemas.MedicalSummary], tags=['database'])
@router.post("/medical_summaries/", response_model=schemas.MedicalSummary, tags=['database'])
def create_medical_summary(medical_summary: schemas.MedicalSummaryCreate, db: Session = Depends(get_db)):
    print(f"\nDatabase Session: {db}\n")
    print(f"\nSummary @ database.py: {medical_summary}\n")
    return crud.create_medical_summary(db=db, medical_summary=medical_summary)

@router.get("/medical_summaries/", response_model=List[schemas.MedicalSummary], tags=['database'])
def get_medical_summaries(skip: int = 0, limit: int = 50, db: Session = Depends(get_db)):
    medical_summaries = crud.get_medical_summaries(db, skip=skip, limit=limit)
    if medical_summaries is None:
        raise HTTPException(status_code=404, detail="No medical summaries found")
    return medical_summaries
