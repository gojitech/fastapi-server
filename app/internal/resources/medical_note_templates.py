DAP_TEMPLATE = """
Symptoms:
Psychotropic Meds:
Medical:
Alcohol/Drugs:
Past Counselling:
Family:
Family of origin:
Income Source:
MSE:- presented as: 

Goals of Counselling:
Formulation/Treatment Plan:

Mental Health                          Progress Note
Client reported:
D:- 
A:-
P: 
"""

DEAR_TEMPLATE = """
Registered Dietitian Initial Appointment Note (In Person/Phone/Virtual) 
*Verbal consent to treatment obtained. 

Referral Source: Dr./NP or IHP from referral form.
Referral Reason: From referral form.
Date of Referral: From referral form
Patient’s Reason or Concern: What the patient says they want to focus on today

D/ - Medical Hx: Mostly from PCP Server, or what conditions are discussed in session.
- Recent Hospitalizations: Mostly from PCP Server, or what is stated in session.
- Labs: From PCP server.
- Meds: From PCP server and and medication confirmed by patient.
- Vit/Min/Supp: Any vitamin, mineral or other supplement patient mentions.
- Anthropometrics: ht: wt: from PCP server or if estimated or taken in session.
- Wt Hx: how the patient’s weight has changed or remained stable over time
- Food Allergies/Intolerances: any thing the patient states as an allergy or intolerance or ‘doesn’t sit well with them’ or have symptoms after eating.
- Bowel Habits: Any diarrhea or constipation or gas. If nothing irregular then recorded as ‘Regular 1x/day’ for example.
- Swallowing/Dentition: Any issues with chewing, swallowing or dysphagia and aspiration of food or fluids.
- Lifestyle and Social Hx: Lives with: whomever they live with ie. Wife, mother etc. Work:.what they do for work, full time, part time, if no work if they are retired then what they used to do. Groceries/cooking/food prep done by themselves, partner/parent or both/all. This can also be separated if the same person doesn’t do the groceries and cooking. Dining out/take out: How often they get food from any restaurant, can include what type of restaurant but not necessary.
- MH/Stress: How stressed they state that they are and sources of stress. Any mental health diagnoses. Stress/mood management strategies/techniques.
- Physical Activity: What they do for physical activity, for how long and how regularly. Ie walks 30mins per day 4 days a week.
- Previous Nutrition Education: state if they have seen an RD before (may be in chart or referral source) or if they state they have seen a nutritionist/naturopath before or been a part of an in person diet program such as weight watchers.
- Previous/Current Diet Changes: Any recent changes to their diet.
- Meal Routine: when in the day do they eat, do they skip meals, do they have regular snacks. Ie. Eats Lunch and dinner, skips breakfast, has evening snack.
- Intake: Breakfast – food and drinks listed of first meal of day/breakfast, as much detail as they give, if skipped, indicate here. 
Lunch – food and drinks listed of second meal of day/lunch, as much detail as they give, if skipped, indicate here.
Dinner – same as previous meals; usually listed as: protein AND carbohydrate AND veg with examples.
Snacks – Any listed foods outside of 3 previous meals, can list with the time consumed.
Beverages – Everything they drink in the day, listed as litres for water ie. 6 cups water written as Water 1.5L. Alcohol also listed here if reported.
Added fats/oils – Type of oil, butter or margarine they use for cooking.
- Other: Anything that doesn’t fit into the above categories such as likes and dislikes, symptoms and relevant life circumstances.

E/ - Assessment of nutrition and hydration. Usually as a Nutrition Diagnosis. This does not need to be completed by the scribe. 
Can be listed as general notes such as: excessive intake of alcohol OR generally well balanced diet. 

A/ - Action – what did the practitioner do. List out topics discussed and information provided. 
Also list the patient’s goals here.
List the materials/handouts provided (not possible to be completed by scribe).

R/ - Follow Up: When the next appointment is scheduled for. If no appointment scheduled, note that the contact info was provided for them to rebook if appropriate.
- Topics to Discuss: Notes from the practitioner of what to discuss at next visit. (Not able to be completed by scribe)

--------------------------------------------------------------------------------------------------------------------------------------------------

Registered Dietitian Follow Up Note (Appt #, In Person/Phone)

D/ - Nutrition Follow Up Re: Reason for referral/patient’s reason from previous appt. Last Appt: Previous appointment date from EMR.
- Patient Reports: Most of what the patient talks about goes here in point or sentence form. 
- Labs: If any updated labs, they go here, from PCP server. If no changes/not mentioned/ delete line.
- Meds: If any changes, reported here. If no changes/not mentioned/delete line.
- Vit/Min/Supp: If any changes, reported here. If no changes/not mentioned/delete line.
- Symptoms: Any nutrition specific symptoms mentioned. Ex. Satiety/ hunger, bowel issues. 
- Anthropometric: Any weight changes reported. If no changes/not mentioned/delete line.
- Physical Activity: If any changes, reported here. If no changes/not mentioned/delete line.
- Recent Diet Changes: Any changes to their diet since last appointment ex. If they have changed anything as a result of the previous appt or not.
- Diet/Intake: Anything the patient reports eating or drinking. Organized as Breakfast, lunch and dinner if a full diet recall is asked for. 

E/ - Improved/continued/worsened from previous assessment. As with the initial, this does not need to be completed by scribe. Additional assessments can be added.

A/ - Action – what did the practitioner do. List out topics discussed and information provided. 
Also list the patient’s goals here.
List the materials/handouts provided (not possible to be completed by scribe).
(Same as in initial note)

R/ - Follow Up: When the next appointment is scheduled for. If no appointment scheduled, note that the contact info was provided for them to rebook if appropriate.
- Topics to Discuss: Notes from the practitioner of what to discuss at next visit. (Not able to be completed by scribe).
"""

SOAP_TEMPLATE = """
Subjective [what the patient reports, use patient's words]

Reason for Visit / Chief Complaint: [the problem(s) that are the reason for the encounter]

History of Presenting Illness: [story the patient is sharing about the problem(s) and symptom(s)*] 

Medical History: [other health information shared by the patient]

Surgical History: [history of surgical procedures]

Medications: [medications the patient reports taking]

Allergies: [allergies, sensitivities]

Family History: [family structure, relationships, family's health issues]

Social History:  [home and environment, education, employment, eating, activities; alcohol/drug intake; sexuality; and mental health]

Objective [measurable findings]

Physical Examination:	[objectively observed findings* from visual observation and the physical examination]

[* Symptom vs. Sign: For example, a patient stating he has “stomach pain,” is a symptom, documented under the subjective heading; versus “abdominal tenderness to palpation,” an objective sign under the objective heading.

Investigations: [any results from investigations such as laboratory, diagnostic imaging or consultation reports] 

Assessment
[any clinical judgment of what is going on with the patient]

Plan
[investigations, therapies, referrals to consultants, patient education and action, follow-up appointments]
"""