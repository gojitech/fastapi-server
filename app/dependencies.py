# import nltk
# import stanza

# #utils dependencies:
# nltk.download('punkt')

# #Stanza dependencies:
# stanza.download('en', package='mimic', processors={'ner':'i2b2'})
# stanza.download('en', package='mimic', processors={'ner':'ncbi_disease'})
# stanza.download('en', package='mimic', processors={'ner':'anatem'})
# stanza.download('en', processors='tokenize,ner')

# #XGB Model API dependencies:
# nltk.download('wordnet')
# nltk.download('omw-1.4')
# nltk.download('brown')