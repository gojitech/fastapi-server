# from string import ascii_lowercase
# from ..internal.models import SymStatus, ConditionStatus
# import httpx
# import os

# #fetch environment variable macaw URL else default to standard
# macaw_largemodel_fallbackurl = "http://107.22.64.3:8084/api"

# try:
#     fetched_fastapi_environment = os.environ["FASTAPI_ENV"]
    
#     if len(fetched_fastapi_environment) <= 1:
#         macaw_url = macaw_largemodel_fallbackurl
#         print(f"\nERROR: env variable for FASTAPI_ENV, is only len(1) or less, reverting to using fallback URLs >> macaw:{macaw_url}")    
#     elif fetched_fastapi_environment == "DEV":
#         macaw_url = "http://107.22.64.3:8082/api"
#         print(f"\nSuccess: FASTAPI env = {fetched_fastapi_environment}, using smaller macaw model @ {macaw_url}")
#     elif fetched_fastapi_environment == "STAGING":
#         macaw_url = "http://107.22.64.3:8084/api"
#         print(f"\nSuccess: FASTAPI env = {fetched_fastapi_environment}, using larger macaw model @ {macaw_url}")
#     else:
#         macaw_url = macaw_largemodel_fallbackurl
#         print(f"\nERROR: env variable for FASTAPI_ENV = {fetched_fastapi_environment}, does not match DEV or STAGING, reverting to using larger macaw model >> macaw:{macaw_url}")    
# except KeyError as e:
#     macaw_url = macaw_largemodel_fallbackurl
#     print(f"\nERROR: No such env variable for FASTAPI_ENV, reverting to fallback URL >> {macaw_url}\n")

# #Symptom-related
# def extract_symptom_change_status_macaw(full_text: str, symptom: str) -> str:
#     """
#     Extract change in symptom status from the full text of a note.
#     """
#     change_status = ""
#     context = full_text.lower()
#     context = context.strip('\n\t\r')


#     q1 = "What is the status of the following symptom, {}?".format(symptom)
#     options = "(A) improving (B) worsening (C) unchanged (D) unknown"
    
#     query_string_q1 = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q1, options, context)
#     params = {'input': query_string_q1}
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q1 = r.json()['output_slots_list']
    
#     if 'answer' in r_json_output_q1[0]:
#         change_status = r_json_output_q1[0]['answer']

#     return change_status
    
# def extract_symptom_status_macaw(full_text: str, symptom: str) -> str:
#     context = full_text.lower()
#     context = context.strip('\n\t\r')

#     experience_hypothetical = ''
#     experiencing = ''
#     experience_by_someone_else = ''

#     print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>context:\n', context, '\n')
#      #macaw server

#     #q1 = "What is the status of the following symptom, {}, by the patient? ; $mcoptions$=(A) Experienced (B) Not Experienced (C) Experienced, Theoretical (D) Other’s Experience".format(symptom)
#     q2 = "Is the patient experiencing the following symptom, {}?".format(symptom)
#     q3 = "Is the following symptom, {}, being experienced by someone other than the patient?".format(symptom)
    
#     q4 = "Is the following symptom, {}, only mentioned as a hypothetical or theoretical statement?".format(symptom)
#     #Theoretical/Hypothetical
#     q4 = "Is the following symptom, {}, only mentioned as a hypothetical or theoretical statement?".format(symptom)
#     q4b = "Is the following symptom, {}, discussed as a hypothetical?".format(symptom)
#     q4c = "Is the following symptom, {}, mentioned as a side effect only?".format(symptom)
#     q4d = "Is the following symptom, {}, mentioned as a theoretical benefit only?".format(symptom)
    
#     # query_string = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {}".format(q1, context)
#     # params = {'input': query_string}
#     #  #macaw server
#     # r = httpx.get(macaw_url, params=params, timeout=600.0)
#     # r_json_output_q1 = r.json()

#     query_string_q2 = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$=(A) yes (B) no (C) unknown ; $context$= {}".format(q2, context)
#     params = {'input': query_string_q2}
#      #macaw server
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q2 = r.json()

#     query_string_q3 = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q3, context)
#     params = {'input': query_string_q3}
#      #macaw server
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q3 = r.json()

#     query_string_q4 = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q4, context)
#     params = {'input': query_string_q4}
#      #macaw server
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q4 = r.json()

#     #print('Q4 REQ!!!!>>>>>>>>>>',r_json_output_q4)

#     query_string_q4b = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q4b, context)
#     params = {'input': query_string_q4b}
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q4b = r.json()

#     query_string_q4c = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q4c, context)
#     params = {'input': query_string_q4c}
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q4c = r.json()

#     query_string_q4d = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q4d, context)
#     params = {'input': query_string_q4d}
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q4d = r.json()
    
#     #enabled for debugging
#     # print('q2\n',r_json_output_q2)
#     # print('q3\n',r_json_output_q3)
#     # print('q4\n',r_json_output_q4)
#     # print('q4b\n',r_json_output_q4b)
#     # print('q4c\n',r_json_output_q4c)
#     # print('q4d\n',r_json_output_q4d)


#     print('\n statments about the following symptom: {}: \n'.format(symptom))
#     #print('What is the status of the symptom? ', r_json_output_q1['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q2['output_slots_list'][0]:
#         print('Is the patient experiencing the symptom? ', r_json_output_q2['output_slots_list'][0]['answer'])
    
#     if 'answer' in r_json_output_q3['output_slots_list'][0]:
#         print('Is the symptom being experienced by someone other than the patient? ', r_json_output_q3['output_slots_list'][0]['answer'])

#     if 'answer' in r_json_output_q4['output_slots_list'][0]:
#         print('Is the symptom mentioned in a theoretical/hypothetical sense? ', r_json_output_q4['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q4b['output_slots_list'][0]:
#         print('Is the symptom discussed as a hypothetical? ', r_json_output_q4b['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q4c['output_slots_list'][0]:
#         print('Is the symptom mentioned as a side effect only? ', r_json_output_q4c['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q4d['output_slots_list'][0]:
#         print('Is the symptom mentioned as a theoretical benefit only? ', r_json_output_q4d['output_slots_list'][0]['answer'])
    
#     #building response:
#     if 'answer' in r_json_output_q2['output_slots_list'][0]:
#         experiencing = r_json_output_q2['output_slots_list'][0]['answer']
    
#     if 'answer' in r_json_output_q3['output_slots_list'][0]:
#         experience_by_someone_else = r_json_output_q3['output_slots_list'][0]['answer']
    
#     if 'answer' in r_json_output_q4['output_slots_list'][0] and 'answer' in r_json_output_q4b['output_slots_list'][0] and 'answer' in r_json_output_q4c['output_slots_list'][0] and 'answer' in r_json_output_q4d['output_slots_list'][0]:
#         if r_json_output_q4['output_slots_list'][0]['answer'].lower() == 'yes' or r_json_output_q4b['output_slots_list'][0]['answer'].lower() == 'yes' or r_json_output_q4c['output_slots_list'][0]['answer'].lower() == 'yes' or r_json_output_q4d['output_slots_list'][0]['answer'].lower() == 'yes': 
#             experience_hypothetical = 'yes'
#         else:
#             experience_hypothetical = 'no'

#     if experience_hypothetical.lower() == 'yes':
#         return [{'label': SymStatus.EXPERIENCE_THEOR.value, 'value': 1}]
#     elif experience_by_someone_else.lower() == "yes" and experiencing.lower() == "no":
#         return [{'label': SymStatus.EXPERIENCE_OTHERS.value, 'value': 1}]
#     elif experiencing.lower() == 'yes' and experience_hypothetical.lower() == 'no' and experience_by_someone_else.lower() == 'no':
#         return [{'label': SymStatus.EXPERIENCED.value, 'value': 1}]
#     else:
#         return [{'label': SymStatus.EXPERIENCE_UNK.value, 'value': 1}] #unknown    

# #Condition-related
# def extract_condition_change_status_macaw(full_text: str, condition: str) -> str:      
#       change_status = ""
#       context = full_text.lower()
#       context = context.strip('\n\t\r')
      

#       q1 = "What is the status of the following condition, {}?".format(condition)
#       options = "(A) improving (B) worsening (C) unchanged (D) unknown"
      
#       query_string_q1 = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q1, options, context)
#       params = {'input': query_string_q1}
#       r = httpx.get(macaw_url, params=params, timeout=600.0)
#       r_json_output_q1 = r.json()['output_slots_list']
      
#       if 'answer' in r_json_output_q1[0]:
#         change_status = r_json_output_q1[0]['answer']

#       return change_status

# def extract_condition_status_macaw(full_text: str, condition: str) -> str:
#     context = full_text.lower()
#     context = context.strip('\n\t\r')
#     is_educational = ''
#     is_affirmed = ''
#     is_denied = ''
#     has_priorhx = ''
#     is_atrisk = ''
    
#     print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>context:\n', context, '\n')
#      #macaw server

#     q1 = "Is the patient the one experiencing, {}?".format(condition)
#     q2 = "Has the patient denied the presence of the following condition, {}, for themselves?".format(condition)
#     q3 = "Has the patient communicated a prior history of the following condition, {}?".format(condition)
#     q4 = "Is the patient at risk of the following condition, {}?".format(condition)
#     q5 = "Is the following condition, {}, only discussed in an educational context?".format(condition)

#     query_string_q1 = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q1, context)
#     params = {'input': query_string_q1}
#      #macaw server
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q1 = r.json()

#     query_string_q2 = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q2, context)
#     params = {'input': query_string_q2}
#      #macaw server
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q2 = r.json()

#     query_string_q3 = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q3, context)
#     params = {'input': query_string_q3}
#      #macaw server
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q3 = r.json()

#     query_string_q4 = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q4, context)
#     params = {'input': query_string_q4}
#      #macaw server
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q4 = r.json()

#     query_string_q5 = "$answer$ ; $explanation$ ; $question$ = {} ; $context$= {} ; $mcoptions$=(A) yes (B) no (C) unknown".format(q5, context)
#     params = {'input': query_string_q5}
#     r = httpx.get(macaw_url, params=params, timeout=600.0)
#     r_json_output_q5 = r.json()

#     #enable for debugging
#     # print('q1\n',r_json_output_q1)
#     # print('q2\n',r_json_output_q2)
#     # print('q3\n',r_json_output_q3)
#     # print('q4\n',r_json_output_q4)
#     # print('q5\n',r_json_output_q5)

#     # q2 = "Is this condition, {}, denied by the subject?".format(condition)
#     # q3 = "Has there been prior history of the following condition, {}, communicated".format(condition)
#     # q4 = "Is the patient at risk of, {}?".format(condition)
#     # q5 = "Is the condition, {}, discussed in an educational context".format(condition)

#     if 'answer' in r_json_output_q1['output_slots_list'][0]:
#         print('Is this condition affirmed to be present for the subject in question? ', r_json_output_q1['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q2['output_slots_list'][0]:
#         print("Is this condition denied by the subject?", r_json_output_q2['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q3['output_slots_list'][0]:
#         print("Has there been prior history of the following condition communicated?", r_json_output_q3['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q4['output_slots_list'][0]:
#         print("Is the patient at risk of?".format(condition), r_json_output_q4['output_slots_list'][0]['answer'])
#     if 'answer' in r_json_output_q5['output_slots_list'][0]:
#         print("Is the condition discussed in an educational context?".format(condition), r_json_output_q5['output_slots_list'][0]['answer'])

#     #building response:
#     if 'answer' in r_json_output_q1['output_slots_list'][0]:
#         is_affirmed = r_json_output_q1['output_slots_list'][0]['answer']
#     if 'answer' in r_json_output_q2['output_slots_list'][0]:
#         is_denied = r_json_output_q2['output_slots_list'][0]['answer']
#     if 'answer' in r_json_output_q3['output_slots_list'][0]:
#         has_priorhx = r_json_output_q3['output_slots_list'][0]['answer']
#     if 'answer' in r_json_output_q4['output_slots_list'][0]:
#         is_atrisk = r_json_output_q4['output_slots_list'][0]['answer']
#     if 'answer' in r_json_output_q5['output_slots_list'][0]:
#         is_educational = r_json_output_q5['output_slots_list'][0]['answer']

#     if (is_affirmed.lower() == 'yes' or has_priorhx.lower() == 'yes') and is_denied.lower() == 'no':
#         return [{"label": ConditionStatus.PRESENT.value, 'value': 1}]
#     elif (is_denied.lower() == 'yes' or has_priorhx.lower() == 'no') and is_affirmed.lower() == 'no' and is_atrisk.lower() == 'no':
#         return [{"label": ConditionStatus.ABSENT.value, 'value': 1}]
#     elif is_educational.lower() == 'yes' and is_affirmed.lower() == 'no' and is_denied.lower() == 'no' and has_priorhx.lower() == 'no':
#         return [{"label": ConditionStatus.EDUCATIONAL.value, 'value': 1}]
#     elif is_atrisk.lower() == 'yes' and is_affirmed.lower() == 'no' and is_denied.lower() == 'no' and has_priorhx.lower() == 'no' and is_educational.lower() == 'no':
#         return [{"label": ConditionStatus.UNKNOWN.value, 'value': 1}]
#     else:
#         return [{"label": ConditionStatus.UNKNOWN.value, 'value': 1}]

# #Procedure or Test-related
# def extract_values_and_findings_tests_macaw(transcription: str, test: str):
#     most_recent_num_value = -1.0
#     macaw_validated_most_recent_num_value = -1.0
#     physician_commentary_recent_num_value = ""
#     macaw_validated_physician_commentary_recent_num_value = ""

#     prior_num_value = -1.0
#     macaw_validated_prior_num_value = -1.0
#     physician_commentary_prior_num_value = ""
#     macaw_validated_physician_commentary_prior_num_value = ""    


#     context = transcription.lower()
#     context = transcription.strip('\n\t\r')

    

#     #Write logic for prior lab values?

#     #Since we cannot rely on our mcoptions approach, we'll first probe macaw for the value
#     #and then ask a validating question using the answer from the proving question
#     q1 = f"What was the most recent numerical value of the following medical test, {test}?"
#     query_string_q1 = f"$answer$ ; $question$ = {q1} ; $context$={context}"
#     params_q1 = {'input': query_string_q1}

#     try:
#         res_1 = httpx.get(macaw_url, params=params_q1)
#         #print(">",res_1)
#         res_1_json_output = res_1.json()['output_slots_list']
#     except Exception as e:
#         print(f'Error making API call to macaw re: test findings. Error: {e}')     

#     if "res_1_json_output" in locals(): 
#         if 'answer' in res_1_json_output[0]:
#             print(f"{query_string_q1}\n")
#             print(f">macaw response for probing question: \n{res_1_json_output[0]['answer']}\n")
#             try:
#                 most_recent_num_value = float(res_1_json_output[0]['answer'])
#                 print(f">>most recent numerical value: {most_recent_num_value}")
#             except ValueError:
#                 print(f"Error cannot convert the following resp to float:{res_1_json_output[0]['answer']}")
#                 most_recent_num_value = 0.0

#     #Only check answer if we have one
#     if most_recent_num_value != 0.0:
#         q1_check = f"Is the most recent numerical value of {test}, {most_recent_num_value}?"
#         query_string_q1_check = f"$answer$ ; $question$ = {q1_check} ; $mcoptions$=(A) yes (B) no (C) unknown ; $context$={context}"
#         params_q1_check = {'input': query_string_q1_check}

#         try:
#             res_1_check = httpx.get(macaw_url, params=params_q1_check, timeout=600)
#             #print(">",res_1_check)
#             #print(f"macaw response: \n{res_1_check.json()}\n")
#             res_1_check_json_output = res_1_check.json()['output_slots_list']
#         except Exception as e:
#             print(f"Error making API call to macaw re: test findings. Error: {e}")

#         if "res_1_check_json_output" in locals():
#             print(f"{query_string_q1_check}\n")
#             print(f"macaw response for confirming question: \n{res_1_check_json_output[0]['answer']}\n")
#             if 'answer' in res_1_check_json_output[0] and res_1_check_json_output[0]['answer'].lower() == 'yes':
#                 macaw_validated_most_recent_num_value = most_recent_num_value

#     #What was the physician’s commentary on the most recent results for the following medical test?
#     q2 = f"What was the physician's commentary of the most recent numerical value of the following medical test, {test}?"
#     query_string_q2 = f"$answer$ ; $question$ = {q2} ; $context$={context}"
#     params_q2 = {'input': query_string_q2}

#     try:
#         res_2 = httpx.get(macaw_url, params=params_q2, timeout=600)
#         #print(">", res_2)
#         res_2_json_output = res_2.json()['output_slots_list']
#     except Exception as e:
#         print(f"Error making API call to macaw re: physician commentary on findings. Error: {e}")

#     if "res_2_json_output" in locals():
#         if "answer" in res_2_json_output[0]:
#             print(f"{query_string_q2}\n")
#             print(f">macaw response for probing question: \n{res_2_json_output[0]['answer']}\n")
#             physician_commentary_recent_num_value = res_2_json_output[0]['answer']

#     #Only validate answer if we got one
#     if physician_commentary_recent_num_value:
#         q2_check = f"Did the physician comment that the most recent numerical value of, {test}, was '{physician_commentary_recent_num_value}'?"
#         query_string_q2_check = f"$answer$ ; $question$ = {q2_check} ; $mcoptions$=(A) yes (B) no (C) unknown ; $context$={context}"
#         params_q2_check = {'input': query_string_q2_check}

#         try:
#             res_2_check = httpx.get(macaw_url, params=params_q2_check, timeout=600)
#             #print(">", res_2_check)
#             #print(f"macaw response: \n {res_2_check.json()}")
#             res_2_check_json_output = res_2_check.json()['output_slots_list']
#         except Exception as e:
#             print(f"Error making API call to macaw re: physician commentary on findings. Error: {e}")

#         if "res_2_check_json_output" in locals():
#             print(query_string_q2_check)
#             print(f">macaw response for confirming question: \n{res_2_check_json_output[0]}")
#             if 'answer' in res_2_check_json_output[0] and res_2_check_json_output[0]['answer'].lower() == 'yes':
#                 macaw_validated_physician_commentary_recent_num_value = physician_commentary_recent_num_value
#     #---------------------------------------Prior------------------------------------
#     q3 = f"What was the prior numerical value of the following medical test, {test}?"
#     query_string_q3 = f"$answer$ ; $question$ = {q3} ; $context$={context}"
#     params_q3 = {'input': query_string_q1}

#     try:
#         res_3 = httpx.get(macaw_url, params=params_q3, timeout=600)
#         #print(">",res_1)
#         res_3_json_output = res_3.json()['output_slots_list']
#     except Exception as e:
#         print(f'Error making API call to macaw re: test findings. Error: {e}')     

#     if "res_3_json_output" in locals(): 
#         if 'answer' in res_3_json_output[0]:
#             print(f"{query_string_q3}\n")
#             print(f">macaw response for probing question: \n{res_3_json_output[0]['answer']}\n")
#             try:
#                 prior_num_value = float(res_3_json_output[0]['answer'])
#             except ValueError:
#                 print(f"Error cannot convert the following resp to float:{res_3_json_output[0]['answer']}")
#                 prior_num_value = 0.0

#     #Only check answer if we have one
#     if prior_num_value != 0.0:
#         q3_check = f"Was the prior numerical value of {test}, {prior_num_value}?"
#         query_string_q3_check = f"$answer$ ; $question$ = {q3_check} ; $mcoptions$=(A) yes (B) no (C) unknown ; $context$={context}"
#         params_q3_check = {'input': query_string_q3_check}

#         try:
#             res_3_check = httpx.get(macaw_url, params=params_q3_check, timeout=600)
#             #print(">",res_3_check)
#             #print(f"macaw response: \n{res_3_check.json()}\n")
#             res_3_check_json_output = res_3_check.json()['output_slots_list']
#         except Exception as e:
#             print(f"Error making API call to macaw re: test findings. Error: {e}")

#         if "res_3_check_json_output" in locals():
#             print(f"{query_string_q3_check}\n")
#             print(f"macaw response for confirming question: \n{res_3_check_json_output[0]['answer']}\n")
#             if 'answer' in res_3_check_json_output[0] and res_3_check_json_output[0]['answer'].lower() == 'yes':
#                 macaw_validated_prior_num_value = prior_num_value

#     #What was the physician’s commentary on the most recent results for the following medical test?
#     q4 = f"What is the physician's commentary of the prior numerical value of the following medical test, {test}?"
#     query_string_q4 = f"$answer$ ; $question$ = {q4} ; $context$={context}"
#     params_q4 = {'input': query_string_q4}

#     try:
#         res_4 = httpx.get(macaw_url, params=params_q4, timeout=600)
#         #print(">", res_2)
#         res_4_json_output = res_4.json()['output_slots_list']
#     except Exception as e:
#         print(f"Error making API call to macaw re: physician commentary on findings. Error: {e}")

#     if "res_4_json_output" in locals():
#         if "answer" in res_4_json_output[0]:
#             print(f"{query_string_q4}\n")
#             print(f">macaw response for probing question: \n{res_4_json_output[0]['answer']}\n")
#             physician_commentary_prior_num_value = res_4_json_output[0]['answer']

#     #Only validate answer if we got one
#     if physician_commentary_prior_num_value:
#         q4_check = f"Did the physician comment that the prior numerical value of, {test}, was '{physician_commentary_prior_num_value}'?"
#         query_string_q4_check = f"$answer$ ; $question$ = {q4_check} ; $mcoptions$=(A) yes (B) no (C) unknown ; $context$={context}"
#         params_q4_check = {'input': query_string_q4_check}

#         try:
#             res_4_check = httpx.get(macaw_url, params=params_q4_check, timeout=600)
#             #print(">", res_2_check)
#             #print(f"macaw response: \n {res_2_check.json()}")
#             res_4_check_json_output = res_4_check.json()['output_slots_list']
#         except Exception as e:
#             print(f"Error making API call to macaw re: physician commentary on findings. Error: {e}")

#         if "res_4_check_json_output" in locals():
#             print(query_string_q4_check)
#             print(f">macaw response for confirming question: \n{res_4_check_json_output[0]}")
#             if 'answer' in res_4_check_json_output[0] and res_4_check_json_output[0]['answer'].lower() == 'yes':
#                 macaw_validated_physician_commentary_prior_num_value = physician_commentary_prior_num_value

#     return {'recent': {"value": macaw_validated_most_recent_num_value, "comments": macaw_validated_physician_commentary_recent_num_value},"prior": {"value": macaw_validated_prior_num_value, "comments": macaw_validated_physician_commentary_prior_num_value}}
# #Misc
# def extract_timebased_entities_macaw(entity, segment, time_based_entities, type_of_entity):
#     if type_of_entity == 'symptom':
#         temp_time_entities = []
#         ascii_lowercase_count = 0
#         time_of_onset = ""
#         frequency_tempo = ""
#         duration_alltime = ""
#         duration_episodic = ""

        

#         context = segment.lower()
#         context = context.strip('\n\t\r')

#         for idx, item in enumerate(time_based_entities):
#             temp_time_entities.append("({})".format(ascii_lowercase[idx].upper()) + " {}".format(item))
#             ascii_lowercase_count += 1
        
#         temp_time_entities.append("({}) unknown".format(ascii_lowercase[ascii_lowercase_count].upper()))
#         options = " ".join(temp_time_entities)
        
#         q1_onset = "When did the following symptom begin for the patient, {}?".format(entity)
#         q2_frequency = "How often does the following symptom occur for the patient, {}?".format(entity)
#         q3a_duration_alltime = "How long has the patient been with the following symptom, {}?".format(entity)
#         q3b_duration_episodic = "How long does an episode of the following symptom last, {}?".format(entity)

#         #q1 = "When did the patient experience the following symptom, {}?".format(symptom)
#         #q1 = "How long has the following symptom been occuring for the patient, {}?".format(symptom)
        
#         query_string_q1_onset = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q1_onset, options, context)
#         params = {'input': query_string_q1_onset}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q1 = r.json()['output_slots_list']

#         query_string_q2_frequency = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q2_frequency, options, context)
#         params = {'input': query_string_q2_frequency}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q2 = r.json()['output_slots_list']

#         query_string_q3a_duration_alltime = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q3a_duration_alltime, options, context)
#         params = {'input': query_string_q3a_duration_alltime}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q3a = r.json()['output_slots_list']

#         query_string_q3b_duration_episodic = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q3b_duration_episodic, options, context)
#         params = {'input': query_string_q3b_duration_episodic}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q3b = r.json()['output_slots_list']

#         if 'answer' in r_json_output_q1[0]:
#             time_of_onset = r_json_output_q1[0]['answer']
#         if 'answer' in r_json_output_q2[0]:
#             frequency_tempo = r_json_output_q2[0]['answer']
#         if 'answer' in r_json_output_q3a[0]:
#             duration_alltime = r_json_output_q3a[0]['answer']
#         if 'answer' in r_json_output_q3b[0]:
#             duration_episodic = r_json_output_q3b[0]['answer']
    
#         print(query_string_q1_onset, '\n', time_of_onset)
#         print(query_string_q2_frequency, '\n', frequency_tempo)
#         print(query_string_q3a_duration_alltime, '\n', duration_alltime)
#         print(query_string_q3b_duration_episodic, '\n', duration_episodic)
    
#         return {"time_of_onset": time_of_onset, "frequency_tempo": frequency_tempo, "duration_alltime": duration_alltime, "duration_episodic": duration_episodic}
    
#     if type_of_entity == 'condition':
#         temp_time_entities = []
#         ascii_lowercase_count = 0
        
#         time_of_onset = ""
#         duration = ""
#         duration_flareup = ""
#         frequency = ""

        

#         context = segment.lower()
#         context = context.strip('\n\t\r')

#         for idx, item in enumerate(time_based_entities):
#             temp_time_entities.append("({})".format(ascii_lowercase[idx].upper()) + " {}".format(item))
#             ascii_lowercase_count += 1
        
#         temp_time_entities.append("({}) unknown".format(ascii_lowercase[ascii_lowercase_count].upper()))
#         options = " ".join(temp_time_entities)
        
#         q1_onset = "When was the patient diagnosed with the following condition, {}?".format(entity)
#         q2a_duration = "How long the patient has had the following condition, {}?".format(entity)
#         q2b_duration_flareup = "How long the patient has been experiencing a flare-up for the following condition, {}?".format(entity)
#         q3_frequency = "How often does the following condition present itself, {}?".format(entity)

#         #q1 = "When did the patient experience the following symptom, {}?".format(symptom)
#         #q1 = "How long has the following symptom been occuring for the patient, {}?".format(symptom)
        
#         query_string_q1_onset = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q1_onset, options, context)
#         params = {'input': query_string_q1_onset}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q1 = r.json()['output_slots_list']

#         query_string_q2a_duration = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q2a_duration, options, context)
#         params = {'input': query_string_q2a_duration}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q2a = r.json()['output_slots_list']

#         query_string_q2b_duration_alltime = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q2b_duration_flareup, options, context)
#         params = {'input': query_string_q2b_duration_alltime}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q2b = r.json()['output_slots_list']

#         query_string_q3_frequency = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q3_frequency, options, context)
#         params = {'input': query_string_q3_frequency}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q3 = r.json()['output_slots_list']

#         if 'answer' in r_json_output_q1[0]:
#             time_of_onset = r_json_output_q1[0]['answer']
#         if 'answer' in r_json_output_q2a[0]:
#             duration = r_json_output_q2a[0]['answer']
#         if 'answer' in r_json_output_q2b[0]:
#             duration_flareup = r_json_output_q2b[0]['answer']
#         if 'answer' in r_json_output_q3[0]:
#             frequency = r_json_output_q3[0]['answer']
        
#         print(query_string_q1_onset, '\n', time_of_onset)
#         print(query_string_q2a_duration, '\n', duration)
#         print(query_string_q2b_duration_alltime, '\n', duration_flareup)
#         print(query_string_q3_frequency, '\n', frequency)

#         return {"time_of_onset": time_of_onset, "frequency": frequency, "duration": duration, "duration_flareup": duration_flareup}

#     if type_of_entity == 'procedure':
#         temp_time_entities = []
#         ascii_lowercase_count = 0
        
#         frequency = ""
#         time_of_study = ""

        

#         context = segment.lower()
#         context = context.strip('\n\t\r')

#         for idx, item in enumerate(time_based_entities):
#             temp_time_entities.append("({})".format(ascii_lowercase[idx].upper()) + " {}".format(item))
#             ascii_lowercase_count += 1
        
#         temp_time_entities.append("({}) unknown".format(ascii_lowercase[ascii_lowercase_count].upper()))
#         options = " ".join(temp_time_entities)
        
#         q1_frequency = "How frequently is the following medical test performed or will be performed, {}?".format(entity)
#         q2_time_of_study = "When was the following medical test conducted or will be conducted, {}?".format(entity)

#         #q1 = "When did the patient experience the following symptom, {}?".format(symptom)
#         #q1 = "How long has the following symptom been occuring for the patient, {}?".format(symptom)
        
#         query_string_q1_frequency = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q1_frequency, options, context)
#         params = {'input': query_string_q1_frequency}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q1 = r.json()['output_slots_list']

#         query_string_q2_time_of_study = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q2_time_of_study, options, context)
#         params = {'input': query_string_q2_time_of_study}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q2 = r.json()['output_slots_list']

#         if 'answer' in r_json_output_q1[0]:
#           frequency = r_json_output_q1[0]['answer']
#         if 'answer' in r_json_output_q2[0]:
#           time_of_study = r_json_output_q2[0]['answer']
        
        
#         print(query_string_q1_frequency, '\n', frequency)
#         print(query_string_q2_time_of_study, '\n', time_of_study)
        
#         return {"frequency":frequency, "time_of_study":time_of_study}
    
#     if type_of_entity == 'medication':
#         print('medication: ', entity)
#         print('segment: ', segment)

#         temp_time_entities = []
#         ascii_lowercase_count = 0
        
#         frequency = ""
#         duration_how_long_pt_taking = ""
#         duration_as_per_physician_instructions = ""

        

#         context = segment.lower()
#         context = context.strip('\n\t\r')

#         for idx, item in enumerate(time_based_entities):
#             temp_time_entities.append("({})".format(ascii_lowercase[idx].upper()) + " {}".format(item))
#             ascii_lowercase_count += 1
        
#         temp_time_entities.append("({}) unknown".format(ascii_lowercase[ascii_lowercase_count].upper()))
#         options = " ".join(temp_time_entities)
        
#         q1_frequency = "What is the frequency at which the patient is taking the following medication, {}?".format(entity)
#         q2a_duration_pt = "What is the total time that the patient has been taking the following medication, {}?".format(entity)
#         q2b_duration_instructions = "What is the length of time that the doctor wants the patient to take the following medication, {}?".format(entity)

#         query_string_q1_frequency = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q1_frequency, options, context)
#         params = {'input': query_string_q1_frequency}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q1 = r.json()['output_slots_list']
        
#         query_string_q2a_duration_pt = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q2a_duration_pt, options, context)
#         params = {'input': query_string_q2a_duration_pt}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q2a = r.json()['output_slots_list']

#         query_string_q2b_duration_instructions = "$answer$ ; $explanation$ ; $question$ = {} ; $mcoptions$={} ; $context$={}".format(q2b_duration_instructions, options, context)
#         params = {'input': query_string_q2b_duration_instructions}
#         r = httpx.get(macaw_url, params=params, timeout=600.0)
#         r_json_output_q2b = r.json()['output_slots_list']

#         if 'answer' in r_json_output_q1[0]:
#           frequency = r_json_output_q1[0]['answer']
#         if 'answer' in r_json_output_q2a[0]:
#           duration_how_long_pt_taking = r_json_output_q2a[0]['answer']
#         if 'answer' in r_json_output_q2b[0]:
#           duration_as_per_physician_instructions = r_json_output_q2b[0]['answer']
        
#         print(query_string_q1_frequency, '\n', frequency)
#         print(query_string_q2a_duration_pt, '\n', duration_how_long_pt_taking)
#         print(query_string_q2b_duration_instructions, '\n', duration_as_per_physician_instructions)
        
#         return {"frequency":frequency, "duration_how_long_pt_taking": duration_how_long_pt_taking, "duration_as_per_physician_instructions": duration_as_per_physician_instructions}