1) Create a virtual environment
2) Use pip to install requirements.txt
3) OPTIONAL: If you get an error with QuickUMLS trying to call Spacy's English model:
    a) replace the existing core.py with the file in this directory into where your quickumls is installed for example:"/usr/local/lib/python3.8/dist-packages/quickumls"
4) In app/internals/utils.py, make sure line41 onward points to correct QuickUMLS path on your local