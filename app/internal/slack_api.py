import httpx 

def report_error_via_slack(payload_text):
  try:
    slack_payload = {
                          "blocks": [
                            {
                              "type": "section",
                              "text": {
                                "type": "mrkdwn",
                                "text": "Hey <@U02BCRQ5GP3>, looks like there's a Failed process on the ML-server!"
                              }
                            },
                            {
                              "type": "section",
                              "text": {
                                "type": "mrkdwn",
                                "text": payload_text
                              }
                            }
                          ]
                        }
    r = httpx.post("https://hooks.slack.com/services/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", json=slack_payload)
    print(f"\nResult of Slack notification: {r.status_code}\n{r}\n")
  except Exception as e:
    print(f"\n\U0001F480 Error notifying Slack channel re: FAILED status: {e}\n")
    