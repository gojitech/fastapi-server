import time
from fastapi import APIRouter
from app.internal.models import AssemblyFullResponse, AssemblyTopics, Extra
from typing import List
# from ..internal.assembly_full_response import assembly_full_response

router = APIRouter()


@router.post('/', response_model=AssemblyTopics, response_model_exclude_unset=True, tags=['assembly'])
def extract_assembly_topics(assembly_full_response: AssemblyFullResponse):
    return {}
    # id: str = assembly_full_response.id
    # query: str = assembly_full_response.text
    # source: str = 'assembly/topics'
    # topics = []
    # api_version: int = 1

    # extra: List[Extra] = []
    # error_code: int = 0
    # error_messages: List[str] = []

    # time_of_req = time.time()
    # if assembly_full_response.status == "completed":
    #     if assembly_full_response.iab_categories:
    #         results = assembly_full_response.iab_categories_result

    #         for result in results.results:
    #             topics.append(
    #                 {"text": result.text, "score": result.labels[0].relevance, "label": result.labels[0].label})
    #         end_time = time.time()
    #         query_runtime = end_time - time_of_req
    #         return {"id": id, "query": query,  "topics": topics, "extra": extra, "error_code": error_code, "error_messages": error_messages, "run_time_in_secs": query_runtime, 'api_version': api_version, "source": source, }
