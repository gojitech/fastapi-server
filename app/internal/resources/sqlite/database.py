from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"

engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
#by default SQLite only allows one thread to communicate, built to prevent accidently sharing the same connection for different requests
#FastAPI deals with this so we can disable see: https://fastapi.tiangolo.com/tutorial/sql-databases/
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
