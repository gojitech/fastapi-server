assembly_full_response = {
    "id": "ogmtxqejgp-48f9-4bb5-9174-64d9d67e76ae",
    "language_model": "assemblyai_default",
    "acoustic_model": "assemblyai_default",
    "language_code": "en_us",
    "status": "completed",
    "audio_url": "https://storage.googleapis.com/goji-voice-transcription/goji-voice-audio-6477be72.wav",
    "text": "Quilt was developed using data from the Surveillance, Epidemiology and End Results program, or Steer for short and compared to nine other prognostic models that are currently used in the clinic. Joining me now to discuss this further are two ####### of the paper, ####### ### ### #### and ####### ##### #########, both primarily based at the ########## ## #########. ####### is a ######### of ####### ########, ########## ############, and ########, and ####### is both a ###### in ######### and a ########## ########### specializing in ######## ######. So just kind of going back to the start, can you give us a bit of background on this particular study and what perhaps was your inspiration for starting and what clinical problem were you hoping to address? ######## ###### is extremely common, as we all know, and the numbers are increasing. And it's a funny disease because on the one hand, we know that it kills many men, but on the other hand, we also know that many men will have it and live with it and may not even know about. So trying to decide if you find a ######## ######, whether you need to treat it or not, is actually quite a complex problem. For many years, the mantra has been if you find a ######## ######, you must treat it. But multiple studies have shown that actually survival may not be different from doing nothing to even doing something as radical as #######. So the question that we had in our group was really, how do you make that difference? And how do you actually find out which of the men would benefit and which would not? And of course, this is not new. There's been many pieces of work around that, but they tend to be fairly small, ad hoc with rather poor endpoints. And most importantly, very few of these things were being used in a standard approach. And even now, every country has its own particular set of guidelines, rules. And ultimately, the final arbitrator is who the ######### is when they see a patient and how they conveyed an information. So our primary interests driving the development of use from elastic tools was not only how do you bring together all the known variables, but also how do you transmit that into a way that ######### and a patient can understand and how that information can be given in a standardized way. So it doesn't really matter where being is diagnosed in the world. Potentially they will get the same information, same guidance. So that was the underpinning reason for looking at this, and there are different ways of doing it. One of them is tiered, which means you put people into brackets of groups and you look at how the groups perform. But of particular interest to us was how you get into a personalized level. And how do we actually use this new emerging signs of ########## ############ and ####### ######## to the application? And that's where we came from. It from our Echo fantastic. So delving into the method. ########, can you tell us a bit about how the Survival Quilt based model was developed? Survival Quilt is a ####### ######## method that we have developed two years ago, and it appeared in 2019, which is one of our main conferences in ########## ############ and ####### ########. We know that there are numerous survival models, and a key question is how to select among these numerous survival models for a particular dataset, such as the Seer data set. And Survival Quilts is the first automated ####### ######## method that is able to do so for survival analysis and is learning on the basis of the underlying data, how to weigh the different variables to achieve the best trade off between discriminatory performance and calibration. This is important because the usefulness of a survival model should be assessed both by how well the model discriminates among predictive risk and by what is calibrated. And while it's important to correctly discriminate and prioritize patients on the basis of risk, there is prediction of a model also to be well calibrated in order to be really valuable and provide prognostic value to ##########. So survival quiz is able to learn on the basis of whatever data is available. The best model is learning the best model and the best model across the different Horizons of survival analysis. What is the best integration of the variables and how the variables interact in order to issue a personalized prediction for survival? So it is learning personalized predictions and how to best combine these different existing models to issue this particular prediction for the patient at hand. That's interesting. And what were the key findings then of this study? Well, the first thing to say is that to our knowledge, it's the first application of the ####### ######## algorithm and ########## ############ method to such a big data set. To answer this question, and one of the key things that we knew, the Survival Quilts model, like other machine models, can actually take new information and process it very quickly, and it took us many years to develop our previous models. But I think working with #######, one of your ########, with #######, we were able to produce this model very rapidly because the technique was already there. And even in this first situation where we had fairly standard variables, I was amazed to find that the final Quilts model was able to come up with a performance characteristic which was as good as the best that we have from a clinical standpoint and actually a little bit better as well. So that is to me an illustration that the direction of travel or what we do in future has to be using this kind of technology. That's a great summary. You've mentioned in the paper that the Seared data set doesn't include data on comorbidity, ########, ###, nor treatment, although potentially this could improve the prognostic capabilities of the model. So how easy would it be to incorporate these variables in the future and what effect do you think that could have on the model output? One advantage of survival quiz is that it's an automated ####### ######## method. It is able to take whatever data we throw at it and craft a model with good discrimination and calibration performance. So it is very easy to push off a button if this data becomes available to integrate additional variables into the mobile to issue this prediction. So in terms of building a new model on the basis of new variables, that is, I think, very easy. What is important about these methods such as survival quilts, is that not only we are able to assess the advantage and the value of what I call the value of modeling, identifying which methods are best for what type of data and what type of patients, but also the value of information. So we are going to learn in a data driven way on the basis of the data, what variables.",
    "words": [
        {
            "text": "Quilt",
            "start": 310,
            "end": 730,
            "confidence": 0.69927,
            "speaker": "A"
        },
        {
            "text": "was",
            "start": 850,
            "end": 1134,
            "confidence": 0.99832,
            "speaker": "A"
        },
        {
            "text": "developed",
            "start": 1172,
            "end": 1618,
            "confidence": 0.99907,
            "speaker": "A"
        },
        {
            "text": "using",
            "start": 1654,
            "end": 1890,
            "confidence": 0.99992,
            "speaker": "A"
        },
        {
            "text": "data",
            "start": 1940,
            "end": 2190,
            "confidence": 0.99975,
            "speaker": "A"
        },
        {
            "text": "from",
            "start": 2240,
            "end": 2382,
            "confidence": 0.99956,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 2396,
            "end": 2502,
            "confidence": 0.97752,
            "speaker": "A"
        },
        {
            "text": "Surveillance,",
            "start": 2516,
            "end": 3214,
            "confidence": 0.52397,
            "speaker": "A"
        },
        {
            "text": "Epidemiology",
            "start": 3322,
            "end": 4258,
            "confidence": 0.62908,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 4294,
            "end": 4422,
            "confidence": 0.98907,
            "speaker": "A"
        },
        {
            "text": "End",
            "start": 4436,
            "end": 4614,
            "confidence": 0.70329,
            "speaker": "A"
        },
        {
            "text": "Results",
            "start": 4652,
            "end": 5062,
            "confidence": 0.73267,
            "speaker": "A"
        },
        {
            "text": "program,",
            "start": 5086,
            "end": 5490,
            "confidence": 0.8934,
            "speaker": "A"
        },
        {
            "text": "or",
            "start": 5600,
            "end": 5874,
            "confidence": 0.73536,
            "speaker": "A"
        },
        {
            "text": "Steer",
            "start": 5912,
            "end": 6214,
            "confidence": 0.18716,
            "speaker": "A"
        },
        {
            "text": "for",
            "start": 6262,
            "end": 6438,
            "confidence": 0.95862,
            "speaker": "A"
        },
        {
            "text": "short",
            "start": 6464,
            "end": 6906,
            "confidence": 0.99557,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 7028,
            "end": 7314,
            "confidence": 0.96928,
            "speaker": "A"
        },
        {
            "text": "compared",
            "start": 7352,
            "end": 7654,
            "confidence": 0.91613,
            "speaker": "A"
        },
        {
            "text": "to",
            "start": 7702,
            "end": 7878,
            "confidence": 0.99904,
            "speaker": "A"
        },
        {
            "text": "nine",
            "start": 7904,
            "end": 8094,
            "confidence": 0.99874,
            "speaker": "A"
        },
        {
            "text": "other",
            "start": 8132,
            "end": 8334,
            "confidence": 0.91394,
            "speaker": "A"
        },
        {
            "text": "prognostic",
            "start": 8372,
            "end": 8938,
            "confidence": 0.92154,
            "speaker": "A"
        },
        {
            "text": "models",
            "start": 8974,
            "end": 9322,
            "confidence": 0.99572,
            "speaker": "A"
        },
        {
            "text": "that",
            "start": 9346,
            "end": 9462,
            "confidence": 0.98799,
            "speaker": "A"
        },
        {
            "text": "are",
            "start": 9476,
            "end": 9582,
            "confidence": 0.98714,
            "speaker": "A"
        },
        {
            "text": "currently",
            "start": 9596,
            "end": 9918,
            "confidence": 0.99898,
            "speaker": "A"
        },
        {
            "text": "used",
            "start": 10004,
            "end": 10290,
            "confidence": 0.99933,
            "speaker": "A"
        },
        {
            "text": "in",
            "start": 10340,
            "end": 10446,
            "confidence": 0.9991,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 10448,
            "end": 10542,
            "confidence": 0.90357,
            "speaker": "A"
        },
        {
            "text": "clinic.",
            "start": 10556,
            "end": 11254,
            "confidence": 0.72917,
            "speaker": "A"
        },
        {
            "text": "Joining",
            "start": 11422,
            "end": 11842,
            "confidence": 0.96994,
            "speaker": "A"
        },
        {
            "text": "me",
            "start": 11866,
            "end": 12018,
            "confidence": 0.99934,
            "speaker": "A"
        },
        {
            "text": "now",
            "start": 12044,
            "end": 12198,
            "confidence": 0.99859,
            "speaker": "A"
        },
        {
            "text": "to",
            "start": 12224,
            "end": 12378,
            "confidence": 0.99907,
            "speaker": "A"
        },
        {
            "text": "discuss",
            "start": 12404,
            "end": 12702,
            "confidence": 0.99991,
            "speaker": "A"
        },
        {
            "text": "this",
            "start": 12776,
            "end": 13014,
            "confidence": 0.99254,
            "speaker": "A"
        },
        {
            "text": "further",
            "start": 13052,
            "end": 13318,
            "confidence": 0.82422,
            "speaker": "A"
        },
        {
            "text": "are",
            "start": 13354,
            "end": 13518,
            "confidence": 0.62892,
            "speaker": "A"
        },
        {
            "text": "two",
            "start": 13544,
            "end": 13734,
            "confidence": 0.99854,
            "speaker": "A"
        },
        {
            "text": "#######",
            "start": 13772,
            "end": 14038,
            "confidence": 0.98482,
            "speaker": "A"
        },
        {
            "text": "of",
            "start": 14074,
            "end": 14202,
            "confidence": 0.74494,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 14216,
            "end": 14358,
            "confidence": 0.8382,
            "speaker": "A"
        },
        {
            "text": "paper,",
            "start": 14384,
            "end": 14898,
            "confidence": 0.99965,
            "speaker": "A"
        },
        {
            "text": "#######",
            "start": 15044,
            "end": 15622,
            "confidence": 0.22702,
            "speaker": "A"
        },
        {
            "text": "###",
            "start": 15646,
            "end": 15826,
            "confidence": 0.47779,
            "speaker": "A"
        },
        {
            "text": "###",
            "start": 15838,
            "end": 16006,
            "confidence": 0.69774,
            "speaker": "A"
        },
        {
            "text": "####",
            "start": 16018,
            "end": 16366,
            "confidence": 0.41543,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 16438,
            "end": 16638,
            "confidence": 0.9939,
            "speaker": "A"
        },
        {
            "text": "#######",
            "start": 16664,
            "end": 17074,
            "confidence": 0.92238,
            "speaker": "A"
        },
        {
            "text": "#####",
            "start": 17122,
            "end": 17662,
            "confidence": 0.4407,
            "speaker": "A"
        },
        {
            "text": "#########,",
            "start": 17686,
            "end": 18550,
            "confidence": 0.11477,
            "speaker": "A"
        },
        {
            "text": "both",
            "start": 18670,
            "end": 18990,
            "confidence": 0.99976,
            "speaker": "A"
        },
        {
            "text": "primarily",
            "start": 19040,
            "end": 19474,
            "confidence": 0.99972,
            "speaker": "A"
        },
        {
            "text": "based",
            "start": 19522,
            "end": 19698,
            "confidence": 0.99975,
            "speaker": "A"
        },
        {
            "text": "at",
            "start": 19724,
            "end": 19842,
            "confidence": 0.69104,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 19856,
            "end": 19962,
            "confidence": 0.99785,
            "speaker": "A"
        },
        {
            "text": "##########",
            "start": 19976,
            "end": 20398,
            "confidence": 0.99957,
            "speaker": "A"
        },
        {
            "text": "##",
            "start": 20434,
            "end": 20598,
            "confidence": 0.95252,
            "speaker": "A"
        },
        {
            "text": "#########.",
            "start": 20624,
            "end": 21526,
            "confidence": 0.99606,
            "speaker": "A"
        },
        {
            "text": "#######",
            "start": 21718,
            "end": 22342,
            "confidence": 0.25135,
            "speaker": "A"
        },
        {
            "text": "is",
            "start": 22366,
            "end": 22482,
            "confidence": 0.99689,
            "speaker": "A"
        },
        {
            "text": "a",
            "start": 22496,
            "end": 22602,
            "confidence": 0.9897,
            "speaker": "A"
        },
        {
            "text": "#########",
            "start": 22616,
            "end": 22942,
            "confidence": 0.99708,
            "speaker": "A"
        },
        {
            "text": "of",
            "start": 22966,
            "end": 23118,
            "confidence": 0.98527,
            "speaker": "A"
        },
        {
            "text": "#######",
            "start": 23144,
            "end": 23494,
            "confidence": 0.998,
            "speaker": "A"
        },
        {
            "text": "########,",
            "start": 23542,
            "end": 24006,
            "confidence": 0.99986,
            "speaker": "A"
        },
        {
            "text": "##########",
            "start": 24128,
            "end": 24754,
            "confidence": 0.52405,
            "speaker": "A"
        },
        {
            "text": "############,",
            "start": 24802,
            "end": 25354,
            "confidence": 0.99944,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 25402,
            "end": 25578,
            "confidence": 0.97893,
            "speaker": "A"
        },
        {
            "text": "########,",
            "start": 25604,
            "end": 26254,
            "confidence": 0.99944,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 26362,
            "end": 26598,
            "confidence": 0.6844,
            "speaker": "A"
        },
        {
            "text": "#######",
            "start": 26624,
            "end": 26998,
            "confidence": 0.56444,
            "speaker": "A"
        },
        {
            "text": "is",
            "start": 27034,
            "end": 27198,
            "confidence": 0.65183,
            "speaker": "A"
        },
        {
            "text": "both",
            "start": 27224,
            "end": 27414,
            "confidence": 0.98485,
            "speaker": "A"
        },
        {
            "text": "a",
            "start": 27452,
            "end": 27582,
            "confidence": 0.97713,
            "speaker": "A"
        },
        {
            "text": "######",
            "start": 27596,
            "end": 27826,
            "confidence": 0.9837,
            "speaker": "A"
        },
        {
            "text": "in",
            "start": 27838,
            "end": 27942,
            "confidence": 0.97799,
            "speaker": "A"
        },
        {
            "text": "#########",
            "start": 27956,
            "end": 28534,
            "confidence": 0.16386,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 28582,
            "end": 28758,
            "confidence": 0.99349,
            "speaker": "A"
        },
        {
            "text": "a",
            "start": 28784,
            "end": 28902,
            "confidence": 0.94049,
            "speaker": "A"
        },
        {
            "text": "##########",
            "start": 28916,
            "end": 29362,
            "confidence": 0.99703,
            "speaker": "A"
        },
        {
            "text": "###########",
            "start": 29386,
            "end": 30106,
            "confidence": 0.09974,
            "speaker": "A"
        },
        {
            "text": "specializing",
            "start": 30178,
            "end": 30802,
            "confidence": 0.97327,
            "speaker": "A"
        },
        {
            "text": "in",
            "start": 30826,
            "end": 30978,
            "confidence": 0.76944,
            "speaker": "A"
        },
        {
            "text": "########",
            "start": 31004,
            "end": 31378,
            "confidence": 0.99981,
            "speaker": "A"
        },
        {
            "text": "######.",
            "start": 31414,
            "end": 32194,
            "confidence": 0.61877,
            "speaker": "A"
        },
        {
            "text": "So",
            "start": 32362,
            "end": 32658,
            "confidence": 0.79018,
            "speaker": "A"
        },
        {
            "text": "just",
            "start": 32684,
            "end": 32838,
            "confidence": 0.99276,
            "speaker": "A"
        },
        {
            "text": "kind",
            "start": 32864,
            "end": 32982,
            "confidence": 0.83375,
            "speaker": "A"
        },
        {
            "text": "of",
            "start": 32996,
            "end": 33066,
            "confidence": 0.99448,
            "speaker": "A"
        },
        {
            "text": "going",
            "start": 33068,
            "end": 33198,
            "confidence": 0.99269,
            "speaker": "A"
        },
        {
            "text": "back",
            "start": 33224,
            "end": 33378,
            "confidence": 0.84063,
            "speaker": "A"
        },
        {
            "text": "to",
            "start": 33404,
            "end": 33558,
            "confidence": 0.99639,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 33584,
            "end": 33738,
            "confidence": 0.99787,
            "speaker": "A"
        },
        {
            "text": "start,",
            "start": 33764,
            "end": 34242,
            "confidence": 0.99786,
            "speaker": "A"
        },
        {
            "text": "can",
            "start": 34376,
            "end": 34638,
            "confidence": 0.99794,
            "speaker": "A"
        },
        {
            "text": "you",
            "start": 34664,
            "end": 34782,
            "confidence": 0.99931,
            "speaker": "A"
        },
        {
            "text": "give",
            "start": 34796,
            "end": 34902,
            "confidence": 0.99886,
            "speaker": "A"
        },
        {
            "text": "us",
            "start": 34916,
            "end": 35058,
            "confidence": 0.99877,
            "speaker": "A"
        },
        {
            "text": "a",
            "start": 35084,
            "end": 35166,
            "confidence": 0.99841,
            "speaker": "A"
        },
        {
            "text": "bit",
            "start": 35168,
            "end": 35298,
            "confidence": 0.95783,
            "speaker": "A"
        },
        {
            "text": "of",
            "start": 35324,
            "end": 35442,
            "confidence": 0.99777,
            "speaker": "A"
        },
        {
            "text": "background",
            "start": 35456,
            "end": 35950,
            "confidence": 0.80026,
            "speaker": "A"
        },
        {
            "text": "on",
            "start": 36010,
            "end": 36234,
            "confidence": 0.99915,
            "speaker": "A"
        },
        {
            "text": "this",
            "start": 36272,
            "end": 36474,
            "confidence": 0.99904,
            "speaker": "A"
        },
        {
            "text": "particular",
            "start": 36512,
            "end": 36894,
            "confidence": 0.99998,
            "speaker": "A"
        },
        {
            "text": "study",
            "start": 36992,
            "end": 37362,
            "confidence": 0.59012,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 37436,
            "end": 37674,
            "confidence": 0.93494,
            "speaker": "A"
        },
        {
            "text": "what",
            "start": 37712,
            "end": 37950,
            "confidence": 0.80508,
            "speaker": "A"
        },
        {
            "text": "perhaps",
            "start": 38000,
            "end": 38302,
            "confidence": 0.98829,
            "speaker": "A"
        },
        {
            "text": "was",
            "start": 38326,
            "end": 38478,
            "confidence": 0.9743,
            "speaker": "A"
        },
        {
            "text": "your",
            "start": 38504,
            "end": 38658,
            "confidence": 0.99562,
            "speaker": "A"
        },
        {
            "text": "inspiration",
            "start": 38684,
            "end": 39190,
            "confidence": 0.5765,
            "speaker": "A"
        },
        {
            "text": "for",
            "start": 39250,
            "end": 39510,
            "confidence": 0.49729,
            "speaker": "A"
        },
        {
            "text": "starting",
            "start": 39560,
            "end": 39954,
            "confidence": 0.99665,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 40052,
            "end": 40422,
            "confidence": 0.9322,
            "speaker": "A"
        },
        {
            "text": "what",
            "start": 40496,
            "end": 40734,
            "confidence": 0.99932,
            "speaker": "A"
        },
        {
            "text": "clinical",
            "start": 40772,
            "end": 41170,
            "confidence": 0.94514,
            "speaker": "A"
        },
        {
            "text": "problem",
            "start": 41230,
            "end": 41490,
            "confidence": 0.98475,
            "speaker": "A"
        },
        {
            "text": "were",
            "start": 41540,
            "end": 41682,
            "confidence": 0.93682,
            "speaker": "A"
        },
        {
            "text": "you",
            "start": 41696,
            "end": 41874,
            "confidence": 0.58788,
            "speaker": "A"
        },
        {
            "text": "hoping",
            "start": 41912,
            "end": 42202,
            "confidence": 0.99667,
            "speaker": "A"
        },
        {
            "text": "to",
            "start": 42226,
            "end": 42378,
            "confidence": 0.99877,
            "speaker": "A"
        },
        {
            "text": "address?",
            "start": 42404,
            "end": 42980,
            "confidence": 0.99924,
            "speaker": "A"
        },
        {
            "text": "########",
            "start": 44090,
            "end": 44638,
            "confidence": 0.99585,
            "speaker": "B"
        },
        {
            "text": "######",
            "start": 44674,
            "end": 44962,
            "confidence": 0.69254,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 44986,
            "end": 45174,
            "confidence": 0.65663,
            "speaker": "B"
        },
        {
            "text": "extremely",
            "start": 45212,
            "end": 45730,
            "confidence": 0.99876,
            "speaker": "B"
        },
        {
            "text": "common,",
            "start": 45790,
            "end": 46050,
            "confidence": 0.99955,
            "speaker": "B"
        },
        {
            "text": "as",
            "start": 46100,
            "end": 46350,
            "confidence": 0.42912,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 46400,
            "end": 46578,
            "confidence": 0.98998,
            "speaker": "B"
        },
        {
            "text": "all",
            "start": 46604,
            "end": 46758,
            "confidence": 0.99138,
            "speaker": "B"
        },
        {
            "text": "know,",
            "start": 46784,
            "end": 47190,
            "confidence": 0.56269,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 47300,
            "end": 47682,
            "confidence": 0.95863,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 47756,
            "end": 47958,
            "confidence": 0.97506,
            "speaker": "B"
        },
        {
            "text": "numbers",
            "start": 47984,
            "end": 48210,
            "confidence": 0.99576,
            "speaker": "B"
        },
        {
            "text": "are",
            "start": 48260,
            "end": 48402,
            "confidence": 0.99073,
            "speaker": "B"
        },
        {
            "text": "increasing.",
            "start": 48416,
            "end": 49258,
            "confidence": 0.99153,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 49414,
            "end": 49698,
            "confidence": 0.79031,
            "speaker": "B"
        },
        {
            "text": "it's",
            "start": 49724,
            "end": 49882,
            "confidence": 0.94509,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 49906,
            "end": 49986,
            "confidence": 0.94421,
            "speaker": "B"
        },
        {
            "text": "funny",
            "start": 49988,
            "end": 50182,
            "confidence": 0.99352,
            "speaker": "B"
        },
        {
            "text": "disease",
            "start": 50206,
            "end": 50614,
            "confidence": 0.84462,
            "speaker": "B"
        },
        {
            "text": "because",
            "start": 50662,
            "end": 50982,
            "confidence": 0.99765,
            "speaker": "B"
        },
        {
            "text": "on",
            "start": 51056,
            "end": 51222,
            "confidence": 0.96823,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 51236,
            "end": 51342,
            "confidence": 0.98238,
            "speaker": "B"
        },
        {
            "text": "one",
            "start": 51356,
            "end": 51534,
            "confidence": 0.66007,
            "speaker": "B"
        },
        {
            "text": "hand,",
            "start": 51572,
            "end": 51774,
            "confidence": 0.99806,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 51812,
            "end": 52014,
            "confidence": 0.9552,
            "speaker": "B"
        },
        {
            "text": "know",
            "start": 52052,
            "end": 52254,
            "confidence": 0.99938,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 52292,
            "end": 52458,
            "confidence": 0.99171,
            "speaker": "B"
        },
        {
            "text": "it",
            "start": 52484,
            "end": 52638,
            "confidence": 0.9928,
            "speaker": "B"
        },
        {
            "text": "kills",
            "start": 52664,
            "end": 52978,
            "confidence": 0.98864,
            "speaker": "B"
        },
        {
            "text": "many",
            "start": 53014,
            "end": 53502,
            "confidence": 0.9173,
            "speaker": "B"
        },
        {
            "text": "men,",
            "start": 53636,
            "end": 54006,
            "confidence": 0.9977,
            "speaker": "B"
        },
        {
            "text": "but",
            "start": 54068,
            "end": 54294,
            "confidence": 0.91221,
            "speaker": "B"
        },
        {
            "text": "on",
            "start": 54332,
            "end": 54462,
            "confidence": 0.99067,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 54476,
            "end": 54582,
            "confidence": 0.99963,
            "speaker": "B"
        },
        {
            "text": "other",
            "start": 54596,
            "end": 54702,
            "confidence": 0.99939,
            "speaker": "B"
        },
        {
            "text": "hand,",
            "start": 54716,
            "end": 54858,
            "confidence": 0.84091,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 54884,
            "end": 55038,
            "confidence": 0.95993,
            "speaker": "B"
        },
        {
            "text": "also",
            "start": 55064,
            "end": 55218,
            "confidence": 0.98112,
            "speaker": "B"
        },
        {
            "text": "know",
            "start": 55244,
            "end": 55362,
            "confidence": 0.64513,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 55376,
            "end": 55518,
            "confidence": 0.94955,
            "speaker": "B"
        },
        {
            "text": "many",
            "start": 55544,
            "end": 55734,
            "confidence": 0.99908,
            "speaker": "B"
        },
        {
            "text": "men",
            "start": 55772,
            "end": 56190,
            "confidence": 0.9944,
            "speaker": "B"
        },
        {
            "text": "will",
            "start": 56300,
            "end": 56574,
            "confidence": 0.99802,
            "speaker": "B"
        },
        {
            "text": "have",
            "start": 56612,
            "end": 56814,
            "confidence": 0.99852,
            "speaker": "B"
        },
        {
            "text": "it",
            "start": 56852,
            "end": 57018,
            "confidence": 0.97714,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 57044,
            "end": 57198,
            "confidence": 0.98349,
            "speaker": "B"
        },
        {
            "text": "live",
            "start": 57224,
            "end": 57378,
            "confidence": 0.993,
            "speaker": "B"
        },
        {
            "text": "with",
            "start": 57404,
            "end": 57522,
            "confidence": 0.98621,
            "speaker": "B"
        },
        {
            "text": "it",
            "start": 57536,
            "end": 57642,
            "confidence": 0.97518,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 57656,
            "end": 57762,
            "confidence": 0.50467,
            "speaker": "B"
        },
        {
            "text": "may",
            "start": 57776,
            "end": 57882,
            "confidence": 0.98656,
            "speaker": "B"
        },
        {
            "text": "not",
            "start": 57896,
            "end": 58038,
            "confidence": 0.9894,
            "speaker": "B"
        },
        {
            "text": "even",
            "start": 58064,
            "end": 58254,
            "confidence": 0.99815,
            "speaker": "B"
        },
        {
            "text": "know",
            "start": 58292,
            "end": 58458,
            "confidence": 0.99967,
            "speaker": "B"
        },
        {
            "text": "about.",
            "start": 58484,
            "end": 58998,
            "confidence": 0.97831,
            "speaker": "B"
        },
        {
            "text": "So",
            "start": 59144,
            "end": 59454,
            "confidence": 0.82462,
            "speaker": "B"
        },
        {
            "text": "trying",
            "start": 59492,
            "end": 59658,
            "confidence": 0.96603,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 59684,
            "end": 59838,
            "confidence": 0.96831,
            "speaker": "B"
        },
        {
            "text": "decide",
            "start": 59864,
            "end": 60250,
            "confidence": 0.99959,
            "speaker": "B"
        },
        {
            "text": "if",
            "start": 60310,
            "end": 60498,
            "confidence": 0.98573,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 60524,
            "end": 60642,
            "confidence": 0.99822,
            "speaker": "B"
        },
        {
            "text": "find",
            "start": 60656,
            "end": 60798,
            "confidence": 0.99774,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 60824,
            "end": 60906,
            "confidence": 0.86905,
            "speaker": "B"
        },
        {
            "text": "########",
            "start": 60908,
            "end": 61294,
            "confidence": 0.69493,
            "speaker": "B"
        },
        {
            "text": "######,",
            "start": 61342,
            "end": 61942,
            "confidence": 0.7371,
            "speaker": "B"
        },
        {
            "text": "whether",
            "start": 62026,
            "end": 62274,
            "confidence": 0.99939,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 62312,
            "end": 62442,
            "confidence": 0.99962,
            "speaker": "B"
        },
        {
            "text": "need",
            "start": 62456,
            "end": 62562,
            "confidence": 0.99956,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 62576,
            "end": 62682,
            "confidence": 0.99816,
            "speaker": "B"
        },
        {
            "text": "treat",
            "start": 62696,
            "end": 62838,
            "confidence": 0.99915,
            "speaker": "B"
        },
        {
            "text": "it",
            "start": 62864,
            "end": 62982,
            "confidence": 0.99415,
            "speaker": "B"
        },
        {
            "text": "or",
            "start": 62996,
            "end": 63102,
            "confidence": 0.74196,
            "speaker": "B"
        },
        {
            "text": "not,",
            "start": 63116,
            "end": 63258,
            "confidence": 0.99891,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 63284,
            "end": 63438,
            "confidence": 0.53984,
            "speaker": "B"
        },
        {
            "text": "actually",
            "start": 63464,
            "end": 63654,
            "confidence": 0.99312,
            "speaker": "B"
        },
        {
            "text": "quite",
            "start": 63692,
            "end": 63858,
            "confidence": 0.9998,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 63884,
            "end": 64002,
            "confidence": 0.98488,
            "speaker": "B"
        },
        {
            "text": "complex",
            "start": 64016,
            "end": 64498,
            "confidence": 0.991,
            "speaker": "B"
        },
        {
            "text": "problem.",
            "start": 64594,
            "end": 65178,
            "confidence": 0.99931,
            "speaker": "B"
        },
        {
            "text": "For",
            "start": 65324,
            "end": 65742,
            "confidence": 0.56779,
            "speaker": "B"
        },
        {
            "text": "many",
            "start": 65816,
            "end": 66054,
            "confidence": 0.99938,
            "speaker": "B"
        },
        {
            "text": "years,",
            "start": 66092,
            "end": 66330,
            "confidence": 0.99985,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 66380,
            "end": 66522,
            "confidence": 0.94551,
            "speaker": "B"
        },
        {
            "text": "mantra",
            "start": 66536,
            "end": 66898,
            "confidence": 0.76456,
            "speaker": "B"
        },
        {
            "text": "has",
            "start": 66934,
            "end": 67134,
            "confidence": 0.71033,
            "speaker": "B"
        },
        {
            "text": "been",
            "start": 67172,
            "end": 67374,
            "confidence": 0.8437,
            "speaker": "B"
        },
        {
            "text": "if",
            "start": 67412,
            "end": 67578,
            "confidence": 0.89664,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 67604,
            "end": 67722,
            "confidence": 0.99495,
            "speaker": "B"
        },
        {
            "text": "find",
            "start": 67736,
            "end": 67878,
            "confidence": 0.99719,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 67904,
            "end": 68022,
            "confidence": 0.41763,
            "speaker": "B"
        },
        {
            "text": "########",
            "start": 68036,
            "end": 68398,
            "confidence": 0.88151,
            "speaker": "B"
        },
        {
            "text": "######,",
            "start": 68434,
            "end": 68782,
            "confidence": 0.92248,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 68806,
            "end": 68922,
            "confidence": 0.99819,
            "speaker": "B"
        },
        {
            "text": "must",
            "start": 68936,
            "end": 69114,
            "confidence": 0.50989,
            "speaker": "B"
        },
        {
            "text": "treat",
            "start": 69152,
            "end": 69354,
            "confidence": 0.85312,
            "speaker": "B"
        },
        {
            "text": "it.",
            "start": 69392,
            "end": 69558,
            "confidence": 0.99584,
            "speaker": "B"
        },
        {
            "text": "But",
            "start": 69584,
            "end": 69774,
            "confidence": 0.98758,
            "speaker": "B"
        },
        {
            "text": "multiple",
            "start": 69812,
            "end": 70306,
            "confidence": 0.89287,
            "speaker": "B"
        },
        {
            "text": "studies",
            "start": 70378,
            "end": 70774,
            "confidence": 0.8489,
            "speaker": "B"
        },
        {
            "text": "have",
            "start": 70822,
            "end": 70998,
            "confidence": 0.99575,
            "speaker": "B"
        },
        {
            "text": "shown",
            "start": 71024,
            "end": 71314,
            "confidence": 0.98606,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 71362,
            "end": 71538,
            "confidence": 0.99911,
            "speaker": "B"
        },
        {
            "text": "actually",
            "start": 71564,
            "end": 71826,
            "confidence": 0.98761,
            "speaker": "B"
        },
        {
            "text": "survival",
            "start": 71888,
            "end": 72382,
            "confidence": 0.90804,
            "speaker": "B"
        },
        {
            "text": "may",
            "start": 72406,
            "end": 72558,
            "confidence": 0.81822,
            "speaker": "B"
        },
        {
            "text": "not",
            "start": 72584,
            "end": 72738,
            "confidence": 0.99915,
            "speaker": "B"
        },
        {
            "text": "be",
            "start": 72764,
            "end": 72882,
            "confidence": 0.9989,
            "speaker": "B"
        },
        {
            "text": "different",
            "start": 72896,
            "end": 73146,
            "confidence": 0.99662,
            "speaker": "B"
        },
        {
            "text": "from",
            "start": 73208,
            "end": 73506,
            "confidence": 0.9979,
            "speaker": "B"
        },
        {
            "text": "doing",
            "start": 73568,
            "end": 73830,
            "confidence": 0.99847,
            "speaker": "B"
        },
        {
            "text": "nothing",
            "start": 73880,
            "end": 74238,
            "confidence": 0.99951,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 74324,
            "end": 74538,
            "confidence": 0.97076,
            "speaker": "B"
        },
        {
            "text": "even",
            "start": 74564,
            "end": 74862,
            "confidence": 0.99906,
            "speaker": "B"
        },
        {
            "text": "doing",
            "start": 74936,
            "end": 75138,
            "confidence": 0.69695,
            "speaker": "B"
        },
        {
            "text": "something",
            "start": 75164,
            "end": 75390,
            "confidence": 0.84118,
            "speaker": "B"
        },
        {
            "text": "as",
            "start": 75440,
            "end": 75618,
            "confidence": 0.97994,
            "speaker": "B"
        },
        {
            "text": "radical",
            "start": 75644,
            "end": 75994,
            "confidence": 0.39609,
            "speaker": "B"
        },
        {
            "text": "as",
            "start": 76042,
            "end": 76254,
            "confidence": 0.66891,
            "speaker": "B"
        },
        {
            "text": "#######.",
            "start": 76292,
            "end": 76870,
            "confidence": 0.59162,
            "speaker": "B"
        },
        {
            "text": "So",
            "start": 76990,
            "end": 77346,
            "confidence": 0.74897,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 77408,
            "end": 77598,
            "confidence": 0.99929,
            "speaker": "B"
        },
        {
            "text": "question",
            "start": 77624,
            "end": 77886,
            "confidence": 0.99615,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 77948,
            "end": 78282,
            "confidence": 0.99287,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 78356,
            "end": 78594,
            "confidence": 0.9982,
            "speaker": "B"
        },
        {
            "text": "had",
            "start": 78632,
            "end": 79122,
            "confidence": 0.98725,
            "speaker": "B"
        },
        {
            "text": "in",
            "start": 79256,
            "end": 79482,
            "confidence": 0.96567,
            "speaker": "B"
        },
        {
            "text": "our",
            "start": 79496,
            "end": 79638,
            "confidence": 0.97244,
            "speaker": "B"
        },
        {
            "text": "group",
            "start": 79664,
            "end": 79854,
            "confidence": 0.99694,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 79892,
            "end": 80094,
            "confidence": 0.92031,
            "speaker": "B"
        },
        {
            "text": "really,",
            "start": 80132,
            "end": 80406,
            "confidence": 0.96512,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 80468,
            "end": 80658,
            "confidence": 0.99967,
            "speaker": "B"
        },
        {
            "text": "do",
            "start": 80684,
            "end": 80802,
            "confidence": 0.99729,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 80816,
            "end": 81174,
            "confidence": 0.97915,
            "speaker": "B"
        },
        {
            "text": "make",
            "start": 81272,
            "end": 81534,
            "confidence": 0.99958,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 81572,
            "end": 81846,
            "confidence": 0.99013,
            "speaker": "B"
        },
        {
            "text": "difference?",
            "start": 81908,
            "end": 82314,
            "confidence": 0.99507,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 82412,
            "end": 82602,
            "confidence": 0.62417,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 82616,
            "end": 82758,
            "confidence": 0.99959,
            "speaker": "B"
        },
        {
            "text": "do",
            "start": 82784,
            "end": 82902,
            "confidence": 0.99443,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 82916,
            "end": 83058,
            "confidence": 0.99366,
            "speaker": "B"
        },
        {
            "text": "actually",
            "start": 83084,
            "end": 83346,
            "confidence": 0.997,
            "speaker": "B"
        },
        {
            "text": "find",
            "start": 83408,
            "end": 83670,
            "confidence": 0.99965,
            "speaker": "B"
        },
        {
            "text": "out",
            "start": 83720,
            "end": 84222,
            "confidence": 0.99933,
            "speaker": "B"
        },
        {
            "text": "which",
            "start": 84356,
            "end": 84654,
            "confidence": 0.99812,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 84692,
            "end": 84786,
            "confidence": 0.42045,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 84788,
            "end": 84882,
            "confidence": 0.96712,
            "speaker": "B"
        },
        {
            "text": "men",
            "start": 84896,
            "end": 85110,
            "confidence": 0.85295,
            "speaker": "B"
        },
        {
            "text": "would",
            "start": 85160,
            "end": 85374,
            "confidence": 0.94547,
            "speaker": "B"
        },
        {
            "text": "benefit",
            "start": 85412,
            "end": 85774,
            "confidence": 0.66487,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 85822,
            "end": 85962,
            "confidence": 0.77391,
            "speaker": "B"
        },
        {
            "text": "which",
            "start": 85976,
            "end": 86226,
            "confidence": 0.92022,
            "speaker": "B"
        },
        {
            "text": "would",
            "start": 86288,
            "end": 86478,
            "confidence": 0.86635,
            "speaker": "B"
        },
        {
            "text": "not?",
            "start": 86504,
            "end": 86802,
            "confidence": 0.98736,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 86876,
            "end": 87078,
            "confidence": 0.85352,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 87104,
            "end": 87186,
            "confidence": 0.99685,
            "speaker": "B"
        },
        {
            "text": "course,",
            "start": 87188,
            "end": 87354,
            "confidence": 0.99276,
            "speaker": "B"
        },
        {
            "text": "this",
            "start": 87392,
            "end": 87522,
            "confidence": 0.83639,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 87536,
            "end": 87642,
            "confidence": 0.98359,
            "speaker": "B"
        },
        {
            "text": "not",
            "start": 87656,
            "end": 87834,
            "confidence": 0.9902,
            "speaker": "B"
        },
        {
            "text": "new.",
            "start": 87872,
            "end": 88146,
            "confidence": 0.98386,
            "speaker": "B"
        },
        {
            "text": "There's",
            "start": 88208,
            "end": 88426,
            "confidence": 0.83015,
            "speaker": "B"
        },
        {
            "text": "been",
            "start": 88438,
            "end": 88614,
            "confidence": 0.87494,
            "speaker": "B"
        },
        {
            "text": "many",
            "start": 88652,
            "end": 89286,
            "confidence": 0.90954,
            "speaker": "B"
        },
        {
            "text": "pieces",
            "start": 89468,
            "end": 89866,
            "confidence": 0.30342,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 89878,
            "end": 90018,
            "confidence": 0.70134,
            "speaker": "B"
        },
        {
            "text": "work",
            "start": 90044,
            "end": 90198,
            "confidence": 0.99412,
            "speaker": "B"
        },
        {
            "text": "around",
            "start": 90224,
            "end": 90486,
            "confidence": 0.92584,
            "speaker": "B"
        },
        {
            "text": "that,",
            "start": 90548,
            "end": 90738,
            "confidence": 0.98998,
            "speaker": "B"
        },
        {
            "text": "but",
            "start": 90764,
            "end": 91278,
            "confidence": 0.93771,
            "speaker": "B"
        },
        {
            "text": "they",
            "start": 91424,
            "end": 91662,
            "confidence": 0.97281,
            "speaker": "B"
        },
        {
            "text": "tend",
            "start": 91676,
            "end": 91918,
            "confidence": 0.18487,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 91954,
            "end": 92082,
            "confidence": 0.99975,
            "speaker": "B"
        },
        {
            "text": "be",
            "start": 92096,
            "end": 92238,
            "confidence": 0.99899,
            "speaker": "B"
        },
        {
            "text": "fairly",
            "start": 92264,
            "end": 92554,
            "confidence": 0.98818,
            "speaker": "B"
        },
        {
            "text": "small,",
            "start": 92602,
            "end": 92922,
            "confidence": 0.99978,
            "speaker": "B"
        },
        {
            "text": "ad",
            "start": 92996,
            "end": 93162,
            "confidence": 0.51455,
            "speaker": "B"
        },
        {
            "text": "hoc",
            "start": 93176,
            "end": 93514,
            "confidence": 0.76248,
            "speaker": "B"
        },
        {
            "text": "with",
            "start": 93562,
            "end": 93774,
            "confidence": 0.88954,
            "speaker": "B"
        },
        {
            "text": "rather",
            "start": 93812,
            "end": 94266,
            "confidence": 0.39127,
            "speaker": "B"
        },
        {
            "text": "poor",
            "start": 94388,
            "end": 94714,
            "confidence": 0.93676,
            "speaker": "B"
        },
        {
            "text": "endpoints.",
            "start": 94762,
            "end": 95626,
            "confidence": 0.39444,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 95758,
            "end": 96054,
            "confidence": 0.90897,
            "speaker": "B"
        },
        {
            "text": "most",
            "start": 96092,
            "end": 96294,
            "confidence": 0.97456,
            "speaker": "B"
        },
        {
            "text": "importantly,",
            "start": 96332,
            "end": 97054,
            "confidence": 0.98268,
            "speaker": "B"
        },
        {
            "text": "very",
            "start": 97162,
            "end": 97398,
            "confidence": 0.99893,
            "speaker": "B"
        },
        {
            "text": "few",
            "start": 97424,
            "end": 97614,
            "confidence": 0.68925,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 97652,
            "end": 97782,
            "confidence": 0.70525,
            "speaker": "B"
        },
        {
            "text": "these",
            "start": 97796,
            "end": 97938,
            "confidence": 0.99234,
            "speaker": "B"
        },
        {
            "text": "things",
            "start": 97964,
            "end": 98154,
            "confidence": 0.99596,
            "speaker": "B"
        },
        {
            "text": "were",
            "start": 98192,
            "end": 98358,
            "confidence": 0.49604,
            "speaker": "B"
        },
        {
            "text": "being",
            "start": 98384,
            "end": 98574,
            "confidence": 0.99693,
            "speaker": "B"
        },
        {
            "text": "used",
            "start": 98612,
            "end": 99102,
            "confidence": 0.99842,
            "speaker": "B"
        },
        {
            "text": "in",
            "start": 99236,
            "end": 99462,
            "confidence": 0.84754,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 99476,
            "end": 99582,
            "confidence": 0.86362,
            "speaker": "B"
        },
        {
            "text": "standard",
            "start": 99596,
            "end": 99882,
            "confidence": 0.99897,
            "speaker": "B"
        },
        {
            "text": "approach.",
            "start": 99956,
            "end": 100294,
            "confidence": 0.76385,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 100342,
            "end": 100482,
            "confidence": 0.8245,
            "speaker": "B"
        },
        {
            "text": "even",
            "start": 100496,
            "end": 100710,
            "confidence": 0.98386,
            "speaker": "B"
        },
        {
            "text": "now,",
            "start": 100760,
            "end": 101118,
            "confidence": 0.99735,
            "speaker": "B"
        },
        {
            "text": "every",
            "start": 101204,
            "end": 101490,
            "confidence": 0.99871,
            "speaker": "B"
        },
        {
            "text": "country",
            "start": 101540,
            "end": 101790,
            "confidence": 0.99483,
            "speaker": "B"
        },
        {
            "text": "has",
            "start": 101840,
            "end": 102054,
            "confidence": 0.51626,
            "speaker": "B"
        },
        {
            "text": "its",
            "start": 102092,
            "end": 102222,
            "confidence": 0.97009,
            "speaker": "B"
        },
        {
            "text": "own",
            "start": 102236,
            "end": 102414,
            "confidence": 0.99979,
            "speaker": "B"
        },
        {
            "text": "particular",
            "start": 102452,
            "end": 102726,
            "confidence": 0.98798,
            "speaker": "B"
        },
        {
            "text": "set",
            "start": 102788,
            "end": 103014,
            "confidence": 0.53597,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 103052,
            "end": 103218,
            "confidence": 0.66545,
            "speaker": "B"
        },
        {
            "text": "guidelines,",
            "start": 103244,
            "end": 103738,
            "confidence": 0.94603,
            "speaker": "B"
        },
        {
            "text": "rules.",
            "start": 103774,
            "end": 104062,
            "confidence": 0.85345,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 104086,
            "end": 104310,
            "confidence": 0.76893,
            "speaker": "B"
        },
        {
            "text": "ultimately,",
            "start": 104360,
            "end": 104926,
            "confidence": 0.81461,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 104998,
            "end": 105126,
            "confidence": 0.73132,
            "speaker": "B"
        },
        {
            "text": "final",
            "start": 105128,
            "end": 105330,
            "confidence": 0.87228,
            "speaker": "B"
        },
        {
            "text": "arbitrator",
            "start": 105380,
            "end": 105958,
            "confidence": 0.83315,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 105994,
            "end": 106158,
            "confidence": 0.93308,
            "speaker": "B"
        },
        {
            "text": "who",
            "start": 106184,
            "end": 106338,
            "confidence": 0.99036,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 106364,
            "end": 106482,
            "confidence": 0.99346,
            "speaker": "B"
        },
        {
            "text": "#########",
            "start": 106496,
            "end": 106894,
            "confidence": 0.69952,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 106942,
            "end": 107262,
            "confidence": 0.98618,
            "speaker": "B"
        },
        {
            "text": "when",
            "start": 107336,
            "end": 107502,
            "confidence": 0.99532,
            "speaker": "B"
        },
        {
            "text": "they",
            "start": 107516,
            "end": 107622,
            "confidence": 0.9735,
            "speaker": "B"
        },
        {
            "text": "see",
            "start": 107636,
            "end": 107778,
            "confidence": 0.99952,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 107804,
            "end": 107922,
            "confidence": 0.98359,
            "speaker": "B"
        },
        {
            "text": "patient",
            "start": 107936,
            "end": 108430,
            "confidence": 0.83709,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 108550,
            "end": 108798,
            "confidence": 0.98237,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 108824,
            "end": 108978,
            "confidence": 0.99934,
            "speaker": "B"
        },
        {
            "text": "they",
            "start": 109004,
            "end": 109158,
            "confidence": 0.9658,
            "speaker": "B"
        },
        {
            "text": "conveyed",
            "start": 109184,
            "end": 109582,
            "confidence": 0.37144,
            "speaker": "B"
        },
        {
            "text": "an",
            "start": 109606,
            "end": 109722,
            "confidence": 0.67021,
            "speaker": "B"
        },
        {
            "text": "information.",
            "start": 109736,
            "end": 110300,
            "confidence": 0.93526,
            "speaker": "B"
        },
        {
            "text": "So",
            "start": 110630,
            "end": 111174,
            "confidence": 0.71397,
            "speaker": "B"
        },
        {
            "text": "our",
            "start": 111272,
            "end": 111570,
            "confidence": 0.99315,
            "speaker": "B"
        },
        {
            "text": "primary",
            "start": 111620,
            "end": 111970,
            "confidence": 0.69835,
            "speaker": "B"
        },
        {
            "text": "interests",
            "start": 112030,
            "end": 112630,
            "confidence": 0.39886,
            "speaker": "B"
        },
        {
            "text": "driving",
            "start": 112690,
            "end": 113086,
            "confidence": 0.97878,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 113158,
            "end": 113322,
            "confidence": 0.99064,
            "speaker": "B"
        },
        {
            "text": "development",
            "start": 113336,
            "end": 113722,
            "confidence": 0.99458,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 113746,
            "end": 113898,
            "confidence": 0.85472,
            "speaker": "B"
        },
        {
            "text": "use",
            "start": 113924,
            "end": 114222,
            "confidence": 0.02046,
            "speaker": "B"
        },
        {
            "text": "from",
            "start": 114296,
            "end": 114498,
            "confidence": 0.66342,
            "speaker": "B"
        },
        {
            "text": "elastic",
            "start": 114524,
            "end": 114922,
            "confidence": 0.56265,
            "speaker": "B"
        },
        {
            "text": "tools",
            "start": 114946,
            "end": 115282,
            "confidence": 0.45971,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 115306,
            "end": 115674,
            "confidence": 0.78749,
            "speaker": "B"
        },
        {
            "text": "not",
            "start": 115772,
            "end": 115998,
            "confidence": 0.9995,
            "speaker": "B"
        },
        {
            "text": "only",
            "start": 116024,
            "end": 116502,
            "confidence": 0.99972,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 116636,
            "end": 117114,
            "confidence": 0.98873,
            "speaker": "B"
        },
        {
            "text": "do",
            "start": 117212,
            "end": 117402,
            "confidence": 0.98219,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 117416,
            "end": 117522,
            "confidence": 0.98531,
            "speaker": "B"
        },
        {
            "text": "bring",
            "start": 117536,
            "end": 117714,
            "confidence": 0.99895,
            "speaker": "B"
        },
        {
            "text": "together",
            "start": 117752,
            "end": 118026,
            "confidence": 0.99943,
            "speaker": "B"
        },
        {
            "text": "all",
            "start": 118088,
            "end": 118242,
            "confidence": 0.99488,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 118256,
            "end": 118362,
            "confidence": 0.98324,
            "speaker": "B"
        },
        {
            "text": "known",
            "start": 118376,
            "end": 118618,
            "confidence": 0.96286,
            "speaker": "B"
        },
        {
            "text": "variables,",
            "start": 118654,
            "end": 119254,
            "confidence": 0.45504,
            "speaker": "B"
        },
        {
            "text": "but",
            "start": 119302,
            "end": 119514,
            "confidence": 0.98794,
            "speaker": "B"
        },
        {
            "text": "also",
            "start": 119552,
            "end": 119862,
            "confidence": 0.9963,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 119936,
            "end": 120066,
            "confidence": 0.88983,
            "speaker": "B"
        },
        {
            "text": "do",
            "start": 120068,
            "end": 120162,
            "confidence": 0.85492,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 120176,
            "end": 120426,
            "confidence": 0.90751,
            "speaker": "B"
        },
        {
            "text": "transmit",
            "start": 120488,
            "end": 121054,
            "confidence": 0.51724,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 121102,
            "end": 121278,
            "confidence": 0.97755,
            "speaker": "B"
        },
        {
            "text": "into",
            "start": 121304,
            "end": 121530,
            "confidence": 0.46188,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 121580,
            "end": 121722,
            "confidence": 0.88105,
            "speaker": "B"
        },
        {
            "text": "way",
            "start": 121736,
            "end": 121842,
            "confidence": 0.99389,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 121856,
            "end": 122106,
            "confidence": 0.95127,
            "speaker": "B"
        },
        {
            "text": "#########",
            "start": 122168,
            "end": 122614,
            "confidence": 0.54458,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 122662,
            "end": 122838,
            "confidence": 0.81585,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 122864,
            "end": 122982,
            "confidence": 0.82235,
            "speaker": "B"
        },
        {
            "text": "patient",
            "start": 122996,
            "end": 123274,
            "confidence": 0.99574,
            "speaker": "B"
        },
        {
            "text": "can",
            "start": 123322,
            "end": 123498,
            "confidence": 0.99865,
            "speaker": "B"
        },
        {
            "text": "understand",
            "start": 123524,
            "end": 124110,
            "confidence": 0.99992,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 124280,
            "end": 124578,
            "confidence": 0.90357,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 124604,
            "end": 124794,
            "confidence": 0.99532,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 124832,
            "end": 125034,
            "confidence": 0.98757,
            "speaker": "B"
        },
        {
            "text": "information",
            "start": 125072,
            "end": 125418,
            "confidence": 0.99919,
            "speaker": "B"
        },
        {
            "text": "can",
            "start": 125504,
            "end": 125682,
            "confidence": 0.99919,
            "speaker": "B"
        },
        {
            "text": "be",
            "start": 125696,
            "end": 125838,
            "confidence": 0.99915,
            "speaker": "B"
        },
        {
            "text": "given",
            "start": 125864,
            "end": 126054,
            "confidence": 0.99986,
            "speaker": "B"
        },
        {
            "text": "in",
            "start": 126092,
            "end": 126222,
            "confidence": 0.9852,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 126236,
            "end": 126342,
            "confidence": 0.96848,
            "speaker": "B"
        },
        {
            "text": "standardized",
            "start": 126356,
            "end": 126850,
            "confidence": 0.97627,
            "speaker": "B"
        },
        {
            "text": "way.",
            "start": 126910,
            "end": 127098,
            "confidence": 0.86281,
            "speaker": "B"
        },
        {
            "text": "So",
            "start": 127124,
            "end": 127242,
            "confidence": 0.78259,
            "speaker": "B"
        },
        {
            "text": "it",
            "start": 127256,
            "end": 127362,
            "confidence": 0.99091,
            "speaker": "B"
        },
        {
            "text": "doesn't",
            "start": 127376,
            "end": 127522,
            "confidence": 0.99518,
            "speaker": "B"
        },
        {
            "text": "really",
            "start": 127546,
            "end": 127734,
            "confidence": 0.9936,
            "speaker": "B"
        },
        {
            "text": "matter",
            "start": 127772,
            "end": 128046,
            "confidence": 0.99998,
            "speaker": "B"
        },
        {
            "text": "where",
            "start": 128108,
            "end": 128442,
            "confidence": 0.98532,
            "speaker": "B"
        },
        {
            "text": "being",
            "start": 128516,
            "end": 128718,
            "confidence": 0.0198,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 128744,
            "end": 128898,
            "confidence": 0.08351,
            "speaker": "B"
        },
        {
            "text": "diagnosed",
            "start": 128924,
            "end": 129562,
            "confidence": 0.63256,
            "speaker": "B"
        },
        {
            "text": "in",
            "start": 129646,
            "end": 129822,
            "confidence": 0.90578,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 129836,
            "end": 129978,
            "confidence": 0.94984,
            "speaker": "B"
        },
        {
            "text": "world.",
            "start": 130004,
            "end": 130194,
            "confidence": 0.99211,
            "speaker": "B"
        },
        {
            "text": "Potentially",
            "start": 130232,
            "end": 130786,
            "confidence": 0.5739,
            "speaker": "B"
        },
        {
            "text": "they",
            "start": 130918,
            "end": 131142,
            "confidence": 0.98286,
            "speaker": "B"
        },
        {
            "text": "will",
            "start": 131156,
            "end": 131298,
            "confidence": 0.81773,
            "speaker": "B"
        },
        {
            "text": "get",
            "start": 131324,
            "end": 131442,
            "confidence": 0.99815,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 131456,
            "end": 131598,
            "confidence": 0.99101,
            "speaker": "B"
        },
        {
            "text": "same",
            "start": 131624,
            "end": 131814,
            "confidence": 0.99975,
            "speaker": "B"
        },
        {
            "text": "information,",
            "start": 131852,
            "end": 132306,
            "confidence": 0.99964,
            "speaker": "B"
        },
        {
            "text": "same",
            "start": 132428,
            "end": 132714,
            "confidence": 0.57847,
            "speaker": "B"
        },
        {
            "text": "guidance.",
            "start": 132752,
            "end": 133354,
            "confidence": 0.8113,
            "speaker": "B"
        },
        {
            "text": "So",
            "start": 133462,
            "end": 133662,
            "confidence": 0.77664,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 133676,
            "end": 133818,
            "confidence": 0.98528,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 133844,
            "end": 134142,
            "confidence": 0.99314,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 134216,
            "end": 134418,
            "confidence": 0.55147,
            "speaker": "B"
        },
        {
            "text": "underpinning",
            "start": 134444,
            "end": 134998,
            "confidence": 0.85228,
            "speaker": "B"
        },
        {
            "text": "reason",
            "start": 135034,
            "end": 135378,
            "confidence": 0.99952,
            "speaker": "B"
        },
        {
            "text": "for",
            "start": 135464,
            "end": 135678,
            "confidence": 0.99536,
            "speaker": "B"
        },
        {
            "text": "looking",
            "start": 135704,
            "end": 135930,
            "confidence": 0.99949,
            "speaker": "B"
        },
        {
            "text": "at",
            "start": 135980,
            "end": 136158,
            "confidence": 0.99877,
            "speaker": "B"
        },
        {
            "text": "this,",
            "start": 136184,
            "end": 136626,
            "confidence": 0.96023,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 136748,
            "end": 137142,
            "confidence": 0.74235,
            "speaker": "B"
        },
        {
            "text": "there",
            "start": 137216,
            "end": 137382,
            "confidence": 0.99651,
            "speaker": "B"
        },
        {
            "text": "are",
            "start": 137396,
            "end": 137538,
            "confidence": 0.73437,
            "speaker": "B"
        },
        {
            "text": "different",
            "start": 137564,
            "end": 137790,
            "confidence": 0.99948,
            "speaker": "B"
        },
        {
            "text": "ways",
            "start": 137840,
            "end": 138018,
            "confidence": 0.99959,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 138044,
            "end": 138198,
            "confidence": 0.93849,
            "speaker": "B"
        },
        {
            "text": "doing",
            "start": 138224,
            "end": 138378,
            "confidence": 0.99997,
            "speaker": "B"
        },
        {
            "text": "it.",
            "start": 138404,
            "end": 138558,
            "confidence": 0.96951,
            "speaker": "B"
        },
        {
            "text": "One",
            "start": 138584,
            "end": 138702,
            "confidence": 0.99624,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 138716,
            "end": 138822,
            "confidence": 0.54147,
            "speaker": "B"
        },
        {
            "text": "them",
            "start": 138836,
            "end": 138942,
            "confidence": 0.9978,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 138956,
            "end": 139098,
            "confidence": 0.90581,
            "speaker": "B"
        },
        {
            "text": "tiered,",
            "start": 139124,
            "end": 139498,
            "confidence": 0.89417,
            "speaker": "B"
        },
        {
            "text": "which",
            "start": 139534,
            "end": 139662,
            "confidence": 0.99916,
            "speaker": "B"
        },
        {
            "text": "means",
            "start": 139676,
            "end": 139818,
            "confidence": 0.99885,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 139844,
            "end": 139962,
            "confidence": 0.97369,
            "speaker": "B"
        },
        {
            "text": "put",
            "start": 139976,
            "end": 140154,
            "confidence": 0.52861,
            "speaker": "B"
        },
        {
            "text": "people",
            "start": 140192,
            "end": 140394,
            "confidence": 0.99978,
            "speaker": "B"
        },
        {
            "text": "into",
            "start": 140432,
            "end": 140670,
            "confidence": 0.52262,
            "speaker": "B"
        },
        {
            "text": "brackets",
            "start": 140720,
            "end": 141118,
            "confidence": 0.57734,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 141154,
            "end": 141318,
            "confidence": 0.4612,
            "speaker": "B"
        },
        {
            "text": "groups",
            "start": 141344,
            "end": 141622,
            "confidence": 0.97072,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 141646,
            "end": 141762,
            "confidence": 0.94722,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 141776,
            "end": 141882,
            "confidence": 0.96961,
            "speaker": "B"
        },
        {
            "text": "look",
            "start": 141896,
            "end": 142002,
            "confidence": 0.99698,
            "speaker": "B"
        },
        {
            "text": "at",
            "start": 142016,
            "end": 142122,
            "confidence": 0.99609,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 142136,
            "end": 142242,
            "confidence": 0.99368,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 142256,
            "end": 142434,
            "confidence": 0.96972,
            "speaker": "B"
        },
        {
            "text": "groups",
            "start": 142472,
            "end": 142738,
            "confidence": 0.43815,
            "speaker": "B"
        },
        {
            "text": "perform.",
            "start": 142774,
            "end": 143226,
            "confidence": 0.54814,
            "speaker": "B"
        },
        {
            "text": "But",
            "start": 143348,
            "end": 143742,
            "confidence": 0.95498,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 143816,
            "end": 144054,
            "confidence": 0.96109,
            "speaker": "B"
        },
        {
            "text": "particular",
            "start": 144092,
            "end": 144438,
            "confidence": 0.9987,
            "speaker": "B"
        },
        {
            "text": "interest",
            "start": 144524,
            "end": 144846,
            "confidence": 0.99936,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 144908,
            "end": 145098,
            "confidence": 0.97078,
            "speaker": "B"
        },
        {
            "text": "us",
            "start": 145124,
            "end": 145314,
            "confidence": 0.99498,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 145352,
            "end": 145518,
            "confidence": 0.75998,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 145544,
            "end": 145698,
            "confidence": 0.99692,
            "speaker": "B"
        },
        {
            "text": "you",
            "start": 145724,
            "end": 145842,
            "confidence": 0.94238,
            "speaker": "B"
        },
        {
            "text": "get",
            "start": 145856,
            "end": 145962,
            "confidence": 0.92007,
            "speaker": "B"
        },
        {
            "text": "into",
            "start": 145976,
            "end": 146190,
            "confidence": 0.54739,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 146240,
            "end": 146418,
            "confidence": 0.88259,
            "speaker": "B"
        },
        {
            "text": "personalized",
            "start": 146444,
            "end": 146974,
            "confidence": 0.96566,
            "speaker": "B"
        },
        {
            "text": "level.",
            "start": 147022,
            "end": 147414,
            "confidence": 0.90132,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 147512,
            "end": 147774,
            "confidence": 0.94773,
            "speaker": "B"
        },
        {
            "text": "how",
            "start": 147812,
            "end": 147942,
            "confidence": 0.98228,
            "speaker": "B"
        },
        {
            "text": "do",
            "start": 147956,
            "end": 148062,
            "confidence": 0.90079,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 148076,
            "end": 148218,
            "confidence": 0.9888,
            "speaker": "B"
        },
        {
            "text": "actually",
            "start": 148244,
            "end": 148506,
            "confidence": 0.99468,
            "speaker": "B"
        },
        {
            "text": "use",
            "start": 148568,
            "end": 148758,
            "confidence": 0.9952,
            "speaker": "B"
        },
        {
            "text": "this",
            "start": 148784,
            "end": 148938,
            "confidence": 0.63775,
            "speaker": "B"
        },
        {
            "text": "new",
            "start": 148964,
            "end": 149154,
            "confidence": 0.32661,
            "speaker": "B"
        },
        {
            "text": "emerging",
            "start": 149192,
            "end": 149746,
            "confidence": 0.59978,
            "speaker": "B"
        },
        {
            "text": "signs",
            "start": 149818,
            "end": 150178,
            "confidence": 0.34957,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 150214,
            "end": 150342,
            "confidence": 0.99319,
            "speaker": "B"
        },
        {
            "text": "##########",
            "start": 150356,
            "end": 150838,
            "confidence": 0.63013,
            "speaker": "B"
        },
        {
            "text": "############",
            "start": 150874,
            "end": 151378,
            "confidence": 0.50557,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 151414,
            "end": 151578,
            "confidence": 0.97266,
            "speaker": "B"
        },
        {
            "text": "#######",
            "start": 151604,
            "end": 151894,
            "confidence": 0.80972,
            "speaker": "B"
        },
        {
            "text": "########",
            "start": 151942,
            "end": 152334,
            "confidence": 0.99993,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 152432,
            "end": 152658,
            "confidence": 0.79295,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 152684,
            "end": 152838,
            "confidence": 0.99388,
            "speaker": "B"
        },
        {
            "text": "application?",
            "start": 152864,
            "end": 153234,
            "confidence": 0.99977,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 153332,
            "end": 153522,
            "confidence": 0.64373,
            "speaker": "B"
        },
        {
            "text": "that's",
            "start": 153536,
            "end": 153778,
            "confidence": 0.48868,
            "speaker": "B"
        },
        {
            "text": "where",
            "start": 153814,
            "end": 153978,
            "confidence": 0.99655,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 154004,
            "end": 154158,
            "confidence": 0.99596,
            "speaker": "B"
        },
        {
            "text": "came",
            "start": 154184,
            "end": 154446,
            "confidence": 0.59093,
            "speaker": "B"
        },
        {
            "text": "from.",
            "start": 154508,
            "end": 154698,
            "confidence": 0.6681,
            "speaker": "B"
        },
        {
            "text": "It",
            "start": 154724,
            "end": 155022,
            "confidence": 0.6957,
            "speaker": "A"
        },
        {
            "text": "from",
            "start": 155096,
            "end": 155298,
            "confidence": 0.92647,
            "speaker": "A"
        },
        {
            "text": "our",
            "start": 155324,
            "end": 155478,
            "confidence": 0.96972,
            "speaker": "A"
        },
        {
            "text": "Echo",
            "start": 155504,
            "end": 156210,
            "confidence": 0.31284,
            "speaker": "A"
        },
        {
            "text": "fantastic.",
            "start": 156890,
            "end": 158070,
            "confidence": 0.94803,
            "speaker": "A"
        },
        {
            "text": "So",
            "start": 158510,
            "end": 158838,
            "confidence": 0.74614,
            "speaker": "A"
        },
        {
            "text": "delving",
            "start": 158864,
            "end": 159178,
            "confidence": 0.69377,
            "speaker": "A"
        },
        {
            "text": "into",
            "start": 159214,
            "end": 159414,
            "confidence": 0.99566,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 159452,
            "end": 159654,
            "confidence": 0.99821,
            "speaker": "A"
        },
        {
            "text": "method.",
            "start": 159692,
            "end": 160318,
            "confidence": 0.99607,
            "speaker": "A"
        },
        {
            "text": "########,",
            "start": 160474,
            "end": 161026,
            "confidence": 0.00352,
            "speaker": "A"
        },
        {
            "text": "can",
            "start": 161038,
            "end": 161178,
            "confidence": 0.99812,
            "speaker": "A"
        },
        {
            "text": "you",
            "start": 161204,
            "end": 161358,
            "confidence": 0.99954,
            "speaker": "A"
        },
        {
            "text": "tell",
            "start": 161384,
            "end": 161538,
            "confidence": 0.99968,
            "speaker": "A"
        },
        {
            "text": "us",
            "start": 161564,
            "end": 161718,
            "confidence": 0.99969,
            "speaker": "A"
        },
        {
            "text": "a",
            "start": 161744,
            "end": 161862,
            "confidence": 0.98949,
            "speaker": "A"
        },
        {
            "text": "bit",
            "start": 161876,
            "end": 162054,
            "confidence": 0.99935,
            "speaker": "A"
        },
        {
            "text": "about",
            "start": 162092,
            "end": 162402,
            "confidence": 0.99982,
            "speaker": "A"
        },
        {
            "text": "how",
            "start": 162476,
            "end": 162894,
            "confidence": 0.99954,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 162992,
            "end": 163218,
            "confidence": 0.99598,
            "speaker": "A"
        },
        {
            "text": "Survival",
            "start": 163244,
            "end": 163810,
            "confidence": 0.98974,
            "speaker": "A"
        },
        {
            "text": "Quilt",
            "start": 163870,
            "end": 164242,
            "confidence": 0.81425,
            "speaker": "A"
        },
        {
            "text": "based",
            "start": 164326,
            "end": 164718,
            "confidence": 0.49514,
            "speaker": "A"
        },
        {
            "text": "model",
            "start": 164804,
            "end": 165198,
            "confidence": 0.99978,
            "speaker": "A"
        },
        {
            "text": "was",
            "start": 165284,
            "end": 165570,
            "confidence": 0.9987,
            "speaker": "A"
        },
        {
            "text": "developed?",
            "start": 165620,
            "end": 166470,
            "confidence": 0.88245,
            "speaker": "A"
        },
        {
            "text": "Survival",
            "start": 167450,
            "end": 168094,
            "confidence": 0.99236,
            "speaker": "C"
        },
        {
            "text": "Quilt",
            "start": 168142,
            "end": 168562,
            "confidence": 0.93575,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 168646,
            "end": 168858,
            "confidence": 0.98729,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 168884,
            "end": 169002,
            "confidence": 0.98045,
            "speaker": "C"
        },
        {
            "text": "#######",
            "start": 169016,
            "end": 169282,
            "confidence": 0.99805,
            "speaker": "C"
        },
        {
            "text": "########",
            "start": 169306,
            "end": 169638,
            "confidence": 0.53208,
            "speaker": "C"
        },
        {
            "text": "method",
            "start": 169724,
            "end": 170350,
            "confidence": 0.99889,
            "speaker": "C"
        },
        {
            "text": "that",
            "start": 170470,
            "end": 170718,
            "confidence": 0.99114,
            "speaker": "C"
        },
        {
            "text": "we",
            "start": 170744,
            "end": 170898,
            "confidence": 0.99766,
            "speaker": "C"
        },
        {
            "text": "have",
            "start": 170924,
            "end": 171114,
            "confidence": 0.96534,
            "speaker": "C"
        },
        {
            "text": "developed",
            "start": 171152,
            "end": 171898,
            "confidence": 0.98264,
            "speaker": "C"
        },
        {
            "text": "two",
            "start": 171994,
            "end": 172218,
            "confidence": 0.99908,
            "speaker": "C"
        },
        {
            "text": "years",
            "start": 172244,
            "end": 172470,
            "confidence": 0.85945,
            "speaker": "C"
        },
        {
            "text": "ago,",
            "start": 172520,
            "end": 172806,
            "confidence": 0.71452,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 172868,
            "end": 173058,
            "confidence": 0.90622,
            "speaker": "C"
        },
        {
            "text": "it",
            "start": 173084,
            "end": 173238,
            "confidence": 0.97303,
            "speaker": "C"
        },
        {
            "text": "appeared",
            "start": 173264,
            "end": 173902,
            "confidence": 0.83352,
            "speaker": "C"
        },
        {
            "text": "in",
            "start": 173986,
            "end": 174620,
            "confidence": 0.8317,
            "speaker": "C"
        },
        {
            "text": "2019,",
            "start": 175310,
            "end": 176410,
            "confidence": 0.94,
            "speaker": "C"
        },
        {
            "text": "which",
            "start": 176470,
            "end": 176658,
            "confidence": 0.99905,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 176684,
            "end": 176910,
            "confidence": 0.99647,
            "speaker": "C"
        },
        {
            "text": "one",
            "start": 176960,
            "end": 177174,
            "confidence": 0.99928,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 177212,
            "end": 177414,
            "confidence": 0.99817,
            "speaker": "C"
        },
        {
            "text": "our",
            "start": 177452,
            "end": 178050,
            "confidence": 0.99802,
            "speaker": "C"
        },
        {
            "text": "main",
            "start": 178220,
            "end": 178662,
            "confidence": 0.99899,
            "speaker": "C"
        },
        {
            "text": "conferences",
            "start": 178736,
            "end": 179398,
            "confidence": 0.95548,
            "speaker": "C"
        },
        {
            "text": "in",
            "start": 179494,
            "end": 179862,
            "confidence": 0.96642,
            "speaker": "C"
        },
        {
            "text": "##########",
            "start": 179936,
            "end": 180298,
            "confidence": 0.45054,
            "speaker": "C"
        },
        {
            "text": "############",
            "start": 180334,
            "end": 180814,
            "confidence": 0.98364,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 180862,
            "end": 181038,
            "confidence": 0.98628,
            "speaker": "C"
        },
        {
            "text": "#######",
            "start": 181064,
            "end": 181354,
            "confidence": 0.67226,
            "speaker": "C"
        },
        {
            "text": "########.",
            "start": 181402,
            "end": 182000,
            "confidence": 0.99995,
            "speaker": "C"
        },
        {
            "text": "We",
            "start": 182390,
            "end": 182718,
            "confidence": 0.99864,
            "speaker": "C"
        },
        {
            "text": "know",
            "start": 182744,
            "end": 182898,
            "confidence": 0.999,
            "speaker": "C"
        },
        {
            "text": "that",
            "start": 182924,
            "end": 183078,
            "confidence": 0.92343,
            "speaker": "C"
        },
        {
            "text": "there",
            "start": 183104,
            "end": 183222,
            "confidence": 0.99683,
            "speaker": "C"
        },
        {
            "text": "are",
            "start": 183236,
            "end": 183378,
            "confidence": 0.99152,
            "speaker": "C"
        },
        {
            "text": "numerous",
            "start": 183404,
            "end": 183910,
            "confidence": 0.99754,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 183970,
            "end": 184534,
            "confidence": 0.92837,
            "speaker": "C"
        },
        {
            "text": "models,",
            "start": 184582,
            "end": 185182,
            "confidence": 0.98562,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 185266,
            "end": 185586,
            "confidence": 0.86099,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 185648,
            "end": 185838,
            "confidence": 0.95343,
            "speaker": "C"
        },
        {
            "text": "key",
            "start": 185864,
            "end": 186090,
            "confidence": 0.9954,
            "speaker": "C"
        },
        {
            "text": "question",
            "start": 186140,
            "end": 186822,
            "confidence": 0.99815,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 187016,
            "end": 187446,
            "confidence": 0.98745,
            "speaker": "C"
        },
        {
            "text": "how",
            "start": 187508,
            "end": 187698,
            "confidence": 0.5132,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 187724,
            "end": 187950,
            "confidence": 0.93752,
            "speaker": "C"
        },
        {
            "text": "select",
            "start": 188000,
            "end": 188358,
            "confidence": 0.99947,
            "speaker": "C"
        },
        {
            "text": "among",
            "start": 188444,
            "end": 188794,
            "confidence": 0.99335,
            "speaker": "C"
        },
        {
            "text": "these",
            "start": 188842,
            "end": 189270,
            "confidence": 0.84501,
            "speaker": "C"
        },
        {
            "text": "numerous",
            "start": 189380,
            "end": 190054,
            "confidence": 0.80803,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 190162,
            "end": 190774,
            "confidence": 0.75758,
            "speaker": "C"
        },
        {
            "text": "models",
            "start": 190822,
            "end": 191218,
            "confidence": 0.91287,
            "speaker": "C"
        },
        {
            "text": "for",
            "start": 191254,
            "end": 191382,
            "confidence": 0.99773,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 191396,
            "end": 191538,
            "confidence": 0.73141,
            "speaker": "C"
        },
        {
            "text": "particular",
            "start": 191564,
            "end": 191970,
            "confidence": 0.99994,
            "speaker": "C"
        },
        {
            "text": "dataset,",
            "start": 192080,
            "end": 193078,
            "confidence": 0.21422,
            "speaker": "C"
        },
        {
            "text": "such",
            "start": 193234,
            "end": 193518,
            "confidence": 0.9996,
            "speaker": "C"
        },
        {
            "text": "as",
            "start": 193544,
            "end": 193734,
            "confidence": 0.85,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 193772,
            "end": 193938,
            "confidence": 0.97303,
            "speaker": "C"
        },
        {
            "text": "Seer",
            "start": 193964,
            "end": 194278,
            "confidence": 0.31236,
            "speaker": "C"
        },
        {
            "text": "data",
            "start": 194314,
            "end": 194550,
            "confidence": 0.99556,
            "speaker": "C"
        },
        {
            "text": "set.",
            "start": 194600,
            "end": 195246,
            "confidence": 0.52082,
            "speaker": "C"
        },
        {
            "text": "And",
            "start": 195428,
            "end": 195990,
            "confidence": 0.59226,
            "speaker": "C"
        },
        {
            "text": "Survival",
            "start": 196100,
            "end": 196654,
            "confidence": 0.67108,
            "speaker": "C"
        },
        {
            "text": "Quilts",
            "start": 196702,
            "end": 197242,
            "confidence": 0.53531,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 197326,
            "end": 197718,
            "confidence": 0.94437,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 197804,
            "end": 198090,
            "confidence": 0.99612,
            "speaker": "C"
        },
        {
            "text": "first",
            "start": 198140,
            "end": 198534,
            "confidence": 0.99855,
            "speaker": "C"
        },
        {
            "text": "automated",
            "start": 198632,
            "end": 199342,
            "confidence": 0.99715,
            "speaker": "C"
        },
        {
            "text": "#######",
            "start": 199426,
            "end": 199774,
            "confidence": 0.91809,
            "speaker": "C"
        },
        {
            "text": "########",
            "start": 199822,
            "end": 200286,
            "confidence": 0.99933,
            "speaker": "C"
        },
        {
            "text": "method",
            "start": 200408,
            "end": 201070,
            "confidence": 0.9987,
            "speaker": "C"
        },
        {
            "text": "that",
            "start": 201190,
            "end": 201474,
            "confidence": 0.99743,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 201512,
            "end": 201930,
            "confidence": 0.97787,
            "speaker": "C"
        },
        {
            "text": "able",
            "start": 202040,
            "end": 202422,
            "confidence": 0.99914,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 202496,
            "end": 202698,
            "confidence": 0.99828,
            "speaker": "C"
        },
        {
            "text": "do",
            "start": 202724,
            "end": 202914,
            "confidence": 0.99575,
            "speaker": "C"
        },
        {
            "text": "so",
            "start": 202952,
            "end": 203262,
            "confidence": 0.97625,
            "speaker": "C"
        },
        {
            "text": "for",
            "start": 203336,
            "end": 203574,
            "confidence": 0.9985,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 203612,
            "end": 204094,
            "confidence": 0.95305,
            "speaker": "C"
        },
        {
            "text": "analysis",
            "start": 204142,
            "end": 205054,
            "confidence": 0.97656,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 205222,
            "end": 205518,
            "confidence": 0.8414,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 205544,
            "end": 205770,
            "confidence": 0.58074,
            "speaker": "C"
        },
        {
            "text": "learning",
            "start": 205820,
            "end": 206286,
            "confidence": 0.85609,
            "speaker": "C"
        },
        {
            "text": "on",
            "start": 206408,
            "end": 206658,
            "confidence": 0.99683,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 206684,
            "end": 206838,
            "confidence": 0.99865,
            "speaker": "C"
        },
        {
            "text": "basis",
            "start": 206864,
            "end": 207346,
            "confidence": 0.63328,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 207418,
            "end": 207618,
            "confidence": 0.99897,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 207644,
            "end": 207798,
            "confidence": 0.98543,
            "speaker": "C"
        },
        {
            "text": "underlying",
            "start": 207824,
            "end": 208486,
            "confidence": 0.96216,
            "speaker": "C"
        },
        {
            "text": "data,",
            "start": 208558,
            "end": 209190,
            "confidence": 0.99067,
            "speaker": "C"
        },
        {
            "text": "how",
            "start": 209360,
            "end": 209694,
            "confidence": 0.99821,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 209732,
            "end": 209970,
            "confidence": 0.99388,
            "speaker": "C"
        },
        {
            "text": "weigh",
            "start": 210020,
            "end": 210202,
            "confidence": 0.35918,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 210226,
            "end": 210414,
            "confidence": 0.93869,
            "speaker": "C"
        },
        {
            "text": "different",
            "start": 210452,
            "end": 210798,
            "confidence": 0.99589,
            "speaker": "C"
        },
        {
            "text": "variables",
            "start": 210884,
            "end": 211738,
            "confidence": 0.57905,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 211834,
            "end": 212058,
            "confidence": 0.99781,
            "speaker": "C"
        },
        {
            "text": "achieve",
            "start": 212084,
            "end": 212434,
            "confidence": 0.51298,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 212482,
            "end": 212658,
            "confidence": 0.9919,
            "speaker": "C"
        },
        {
            "text": "best",
            "start": 212684,
            "end": 212910,
            "confidence": 0.99943,
            "speaker": "C"
        },
        {
            "text": "trade",
            "start": 212960,
            "end": 213202,
            "confidence": 0.93026,
            "speaker": "C"
        },
        {
            "text": "off",
            "start": 213226,
            "end": 213486,
            "confidence": 0.63819,
            "speaker": "C"
        },
        {
            "text": "between",
            "start": 213548,
            "end": 214062,
            "confidence": 0.99829,
            "speaker": "C"
        },
        {
            "text": "discriminatory",
            "start": 214196,
            "end": 215098,
            "confidence": 0.23367,
            "speaker": "C"
        },
        {
            "text": "performance",
            "start": 215134,
            "end": 215986,
            "confidence": 0.97506,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 216118,
            "end": 216630,
            "confidence": 0.99848,
            "speaker": "C"
        },
        {
            "text": "calibration.",
            "start": 216740,
            "end": 217726,
            "confidence": 0.80953,
            "speaker": "C"
        },
        {
            "text": "This",
            "start": 217918,
            "end": 218238,
            "confidence": 0.96415,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 218264,
            "end": 218490,
            "confidence": 0.50065,
            "speaker": "C"
        },
        {
            "text": "important",
            "start": 218540,
            "end": 219006,
            "confidence": 0.99966,
            "speaker": "C"
        },
        {
            "text": "because",
            "start": 219128,
            "end": 219486,
            "confidence": 0.9995,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 219548,
            "end": 219774,
            "confidence": 0.99176,
            "speaker": "C"
        },
        {
            "text": "usefulness",
            "start": 219812,
            "end": 220330,
            "confidence": 0.29592,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 220390,
            "end": 220542,
            "confidence": 0.99767,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 220556,
            "end": 220662,
            "confidence": 0.92904,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 220676,
            "end": 221254,
            "confidence": 0.42071,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 221302,
            "end": 221622,
            "confidence": 0.99765,
            "speaker": "C"
        },
        {
            "text": "should",
            "start": 221696,
            "end": 221898,
            "confidence": 0.9994,
            "speaker": "C"
        },
        {
            "text": "be",
            "start": 221924,
            "end": 222078,
            "confidence": 0.99749,
            "speaker": "C"
        },
        {
            "text": "assessed",
            "start": 222104,
            "end": 222514,
            "confidence": 0.95189,
            "speaker": "C"
        },
        {
            "text": "both",
            "start": 222562,
            "end": 222774,
            "confidence": 0.99307,
            "speaker": "C"
        },
        {
            "text": "by",
            "start": 222812,
            "end": 222978,
            "confidence": 0.99862,
            "speaker": "C"
        },
        {
            "text": "how",
            "start": 223004,
            "end": 223158,
            "confidence": 0.9805,
            "speaker": "C"
        },
        {
            "text": "well",
            "start": 223184,
            "end": 223338,
            "confidence": 0.99088,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 223364,
            "end": 223518,
            "confidence": 0.97347,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 223544,
            "end": 223806,
            "confidence": 0.9822,
            "speaker": "C"
        },
        {
            "text": "discriminates",
            "start": 223868,
            "end": 224590,
            "confidence": 0.97086,
            "speaker": "C"
        },
        {
            "text": "among",
            "start": 224650,
            "end": 224974,
            "confidence": 0.56109,
            "speaker": "C"
        },
        {
            "text": "predictive",
            "start": 225022,
            "end": 225514,
            "confidence": 0.95933,
            "speaker": "C"
        },
        {
            "text": "risk",
            "start": 225562,
            "end": 226006,
            "confidence": 0.53512,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 226078,
            "end": 226314,
            "confidence": 0.99692,
            "speaker": "C"
        },
        {
            "text": "by",
            "start": 226352,
            "end": 226518,
            "confidence": 0.88588,
            "speaker": "C"
        },
        {
            "text": "what",
            "start": 226544,
            "end": 226698,
            "confidence": 0.38208,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 226724,
            "end": 226878,
            "confidence": 0.2937,
            "speaker": "C"
        },
        {
            "text": "calibrated.",
            "start": 226904,
            "end": 228046,
            "confidence": 0.71557,
            "speaker": "C"
        },
        {
            "text": "And",
            "start": 228238,
            "end": 228630,
            "confidence": 0.72533,
            "speaker": "C"
        },
        {
            "text": "while",
            "start": 228680,
            "end": 228858,
            "confidence": 0.93577,
            "speaker": "C"
        },
        {
            "text": "it's",
            "start": 228884,
            "end": 229102,
            "confidence": 0.56147,
            "speaker": "C"
        },
        {
            "text": "important",
            "start": 229126,
            "end": 229494,
            "confidence": 0.99994,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 229592,
            "end": 229854,
            "confidence": 0.99832,
            "speaker": "C"
        },
        {
            "text": "correctly",
            "start": 229892,
            "end": 230350,
            "confidence": 0.84219,
            "speaker": "C"
        },
        {
            "text": "discriminate",
            "start": 230410,
            "end": 231058,
            "confidence": 0.58798,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 231094,
            "end": 231294,
            "confidence": 0.52696,
            "speaker": "C"
        },
        {
            "text": "prioritize",
            "start": 231332,
            "end": 231922,
            "confidence": 0.79533,
            "speaker": "C"
        },
        {
            "text": "patients",
            "start": 232006,
            "end": 232390,
            "confidence": 0.90265,
            "speaker": "C"
        },
        {
            "text": "on",
            "start": 232450,
            "end": 232602,
            "confidence": 0.97023,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 232616,
            "end": 232722,
            "confidence": 0.99448,
            "speaker": "C"
        },
        {
            "text": "basis",
            "start": 232736,
            "end": 233038,
            "confidence": 0.99945,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 233074,
            "end": 233310,
            "confidence": 0.50189,
            "speaker": "C"
        },
        {
            "text": "risk,",
            "start": 233360,
            "end": 233842,
            "confidence": 0.95304,
            "speaker": "C"
        },
        {
            "text": "there",
            "start": 233926,
            "end": 234138,
            "confidence": 0.92207,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 234164,
            "end": 234318,
            "confidence": 0.97794,
            "speaker": "C"
        },
        {
            "text": "prediction",
            "start": 234344,
            "end": 234874,
            "confidence": 0.36497,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 234922,
            "end": 235098,
            "confidence": 0.97733,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 235124,
            "end": 235242,
            "confidence": 0.85715,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 235256,
            "end": 235614,
            "confidence": 0.95251,
            "speaker": "C"
        },
        {
            "text": "also",
            "start": 235712,
            "end": 235974,
            "confidence": 0.57173,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 236012,
            "end": 236178,
            "confidence": 0.99458,
            "speaker": "C"
        },
        {
            "text": "be",
            "start": 236204,
            "end": 236358,
            "confidence": 0.99025,
            "speaker": "C"
        },
        {
            "text": "well",
            "start": 236384,
            "end": 236574,
            "confidence": 0.98907,
            "speaker": "C"
        },
        {
            "text": "calibrated",
            "start": 236612,
            "end": 237418,
            "confidence": 0.90105,
            "speaker": "C"
        },
        {
            "text": "in",
            "start": 237514,
            "end": 237702,
            "confidence": 0.99901,
            "speaker": "C"
        },
        {
            "text": "order",
            "start": 237716,
            "end": 237966,
            "confidence": 0.63316,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 238028,
            "end": 238218,
            "confidence": 0.99917,
            "speaker": "C"
        },
        {
            "text": "be",
            "start": 238244,
            "end": 238434,
            "confidence": 0.99861,
            "speaker": "C"
        },
        {
            "text": "really",
            "start": 238472,
            "end": 238854,
            "confidence": 0.98104,
            "speaker": "C"
        },
        {
            "text": "valuable",
            "start": 238952,
            "end": 239590,
            "confidence": 0.97534,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 239710,
            "end": 239994,
            "confidence": 0.99194,
            "speaker": "C"
        },
        {
            "text": "provide",
            "start": 240032,
            "end": 240450,
            "confidence": 0.99333,
            "speaker": "C"
        },
        {
            "text": "prognostic",
            "start": 240560,
            "end": 241138,
            "confidence": 0.97924,
            "speaker": "C"
        },
        {
            "text": "value",
            "start": 241174,
            "end": 241374,
            "confidence": 0.99955,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 241412,
            "end": 241542,
            "confidence": 0.99533,
            "speaker": "C"
        },
        {
            "text": "##########.",
            "start": 241556,
            "end": 242230,
            "confidence": 0.96575,
            "speaker": "C"
        },
        {
            "text": "So",
            "start": 242290,
            "end": 242550,
            "confidence": 0.82377,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 242600,
            "end": 243118,
            "confidence": 0.83647,
            "speaker": "C"
        },
        {
            "text": "quiz",
            "start": 243154,
            "end": 243598,
            "confidence": 0.43988,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 243694,
            "end": 243990,
            "confidence": 0.99414,
            "speaker": "C"
        },
        {
            "text": "able",
            "start": 244040,
            "end": 244326,
            "confidence": 0.99639,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 244388,
            "end": 244614,
            "confidence": 0.99882,
            "speaker": "C"
        },
        {
            "text": "learn",
            "start": 244652,
            "end": 244890,
            "confidence": 0.99949,
            "speaker": "C"
        },
        {
            "text": "on",
            "start": 244940,
            "end": 245118,
            "confidence": 0.98488,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 245144,
            "end": 245298,
            "confidence": 0.99646,
            "speaker": "C"
        },
        {
            "text": "basis",
            "start": 245324,
            "end": 245710,
            "confidence": 0.99759,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 245770,
            "end": 246030,
            "confidence": 0.99342,
            "speaker": "C"
        },
        {
            "text": "whatever",
            "start": 246080,
            "end": 246510,
            "confidence": 0.96414,
            "speaker": "C"
        },
        {
            "text": "data",
            "start": 246620,
            "end": 246930,
            "confidence": 0.99198,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 246980,
            "end": 247194,
            "confidence": 0.98986,
            "speaker": "C"
        },
        {
            "text": "available.",
            "start": 247232,
            "end": 247820,
            "confidence": 0.99984,
            "speaker": "C"
        },
        {
            "text": "The",
            "start": 248270,
            "end": 248634,
            "confidence": 0.94281,
            "speaker": "C"
        },
        {
            "text": "best",
            "start": 248672,
            "end": 249270,
            "confidence": 0.99796,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 249440,
            "end": 249882,
            "confidence": 0.99653,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 249956,
            "end": 250158,
            "confidence": 0.96421,
            "speaker": "C"
        },
        {
            "text": "learning",
            "start": 250184,
            "end": 250446,
            "confidence": 0.99655,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 250508,
            "end": 250698,
            "confidence": 0.9946,
            "speaker": "C"
        },
        {
            "text": "best",
            "start": 250724,
            "end": 250950,
            "confidence": 0.999,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 251000,
            "end": 251250,
            "confidence": 0.99069,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 251300,
            "end": 251442,
            "confidence": 0.98504,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 251456,
            "end": 251598,
            "confidence": 0.66126,
            "speaker": "C"
        },
        {
            "text": "best",
            "start": 251624,
            "end": 251850,
            "confidence": 0.99958,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 251900,
            "end": 252474,
            "confidence": 0.65968,
            "speaker": "C"
        },
        {
            "text": "across",
            "start": 252632,
            "end": 253062,
            "confidence": 0.99909,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 253136,
            "end": 253374,
            "confidence": 0.99205,
            "speaker": "C"
        },
        {
            "text": "different",
            "start": 253412,
            "end": 253722,
            "confidence": 0.99834,
            "speaker": "C"
        },
        {
            "text": "Horizons",
            "start": 253796,
            "end": 254590,
            "confidence": 0.42077,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 254710,
            "end": 254994,
            "confidence": 0.98966,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 255032,
            "end": 255574,
            "confidence": 0.6973,
            "speaker": "C"
        },
        {
            "text": "analysis.",
            "start": 255622,
            "end": 256570,
            "confidence": 0.99524,
            "speaker": "C"
        },
        {
            "text": "What",
            "start": 256750,
            "end": 257130,
            "confidence": 0.98572,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 257180,
            "end": 257358,
            "confidence": 0.99769,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 257384,
            "end": 257574,
            "confidence": 0.99851,
            "speaker": "C"
        },
        {
            "text": "best",
            "start": 257612,
            "end": 258210,
            "confidence": 0.99952,
            "speaker": "C"
        },
        {
            "text": "integration",
            "start": 258380,
            "end": 259162,
            "confidence": 0.62359,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 259246,
            "end": 259422,
            "confidence": 0.99871,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 259436,
            "end": 259614,
            "confidence": 0.66656,
            "speaker": "C"
        },
        {
            "text": "variables",
            "start": 259652,
            "end": 260182,
            "confidence": 0.95683,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 260206,
            "end": 260358,
            "confidence": 0.97568,
            "speaker": "C"
        },
        {
            "text": "how",
            "start": 260384,
            "end": 260538,
            "confidence": 0.99559,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 260564,
            "end": 260718,
            "confidence": 0.96899,
            "speaker": "C"
        },
        {
            "text": "variables",
            "start": 260744,
            "end": 261370,
            "confidence": 0.73179,
            "speaker": "C"
        },
        {
            "text": "interact",
            "start": 261430,
            "end": 262258,
            "confidence": 0.99941,
            "speaker": "C"
        },
        {
            "text": "in",
            "start": 262414,
            "end": 262698,
            "confidence": 0.99796,
            "speaker": "C"
        },
        {
            "text": "order",
            "start": 262724,
            "end": 263022,
            "confidence": 0.99867,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 263096,
            "end": 263298,
            "confidence": 0.99844,
            "speaker": "C"
        },
        {
            "text": "issue",
            "start": 263324,
            "end": 263550,
            "confidence": 0.98656,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 263600,
            "end": 263742,
            "confidence": 0.66878,
            "speaker": "C"
        },
        {
            "text": "personalized",
            "start": 263756,
            "end": 264370,
            "confidence": 0.63125,
            "speaker": "C"
        },
        {
            "text": "prediction",
            "start": 264430,
            "end": 265114,
            "confidence": 0.97819,
            "speaker": "C"
        },
        {
            "text": "for",
            "start": 265222,
            "end": 265494,
            "confidence": 0.99939,
            "speaker": "C"
        },
        {
            "text": "survival?",
            "start": 265532,
            "end": 266290,
            "confidence": 0.90002,
            "speaker": "C"
        },
        {
            "text": "So",
            "start": 266410,
            "end": 266730,
            "confidence": 0.82109,
            "speaker": "C"
        },
        {
            "text": "it",
            "start": 266780,
            "end": 266958,
            "confidence": 0.99747,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 266984,
            "end": 267210,
            "confidence": 0.9888,
            "speaker": "C"
        },
        {
            "text": "learning",
            "start": 267260,
            "end": 267726,
            "confidence": 0.99755,
            "speaker": "C"
        },
        {
            "text": "personalized",
            "start": 267848,
            "end": 268630,
            "confidence": 0.96313,
            "speaker": "C"
        },
        {
            "text": "predictions",
            "start": 268690,
            "end": 269230,
            "confidence": 0.978,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 269290,
            "end": 269478,
            "confidence": 0.90092,
            "speaker": "C"
        },
        {
            "text": "how",
            "start": 269504,
            "end": 269658,
            "confidence": 0.99931,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 269684,
            "end": 269910,
            "confidence": 0.99304,
            "speaker": "C"
        },
        {
            "text": "best",
            "start": 269960,
            "end": 270534,
            "confidence": 0.9984,
            "speaker": "C"
        },
        {
            "text": "combine",
            "start": 270692,
            "end": 271234,
            "confidence": 0.50974,
            "speaker": "C"
        },
        {
            "text": "these",
            "start": 271282,
            "end": 271494,
            "confidence": 0.7122,
            "speaker": "C"
        },
        {
            "text": "different",
            "start": 271532,
            "end": 271842,
            "confidence": 0.99954,
            "speaker": "C"
        },
        {
            "text": "existing",
            "start": 271916,
            "end": 272434,
            "confidence": 0.99847,
            "speaker": "C"
        },
        {
            "text": "models",
            "start": 272482,
            "end": 273154,
            "confidence": 0.87075,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 273262,
            "end": 273534,
            "confidence": 0.99842,
            "speaker": "C"
        },
        {
            "text": "issue",
            "start": 273572,
            "end": 273774,
            "confidence": 0.9964,
            "speaker": "C"
        },
        {
            "text": "this",
            "start": 273812,
            "end": 274050,
            "confidence": 0.91459,
            "speaker": "C"
        },
        {
            "text": "particular",
            "start": 274100,
            "end": 274494,
            "confidence": 0.99982,
            "speaker": "C"
        },
        {
            "text": "prediction",
            "start": 274592,
            "end": 275134,
            "confidence": 0.97495,
            "speaker": "C"
        },
        {
            "text": "for",
            "start": 275182,
            "end": 275358,
            "confidence": 0.99906,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 275384,
            "end": 275502,
            "confidence": 0.99717,
            "speaker": "C"
        },
        {
            "text": "patient",
            "start": 275516,
            "end": 275818,
            "confidence": 0.9747,
            "speaker": "C"
        },
        {
            "text": "at",
            "start": 275854,
            "end": 276018,
            "confidence": 0.98896,
            "speaker": "C"
        },
        {
            "text": "hand.",
            "start": 276044,
            "end": 276620,
            "confidence": 0.99291,
            "speaker": "C"
        },
        {
            "text": "That's",
            "start": 277370,
            "end": 277870,
            "confidence": 0.68102,
            "speaker": "A"
        },
        {
            "text": "interesting.",
            "start": 277930,
            "end": 278262,
            "confidence": 0.9888,
            "speaker": "A"
        },
        {
            "text": "And",
            "start": 278336,
            "end": 278646,
            "confidence": 0.79903,
            "speaker": "A"
        },
        {
            "text": "what",
            "start": 278708,
            "end": 278898,
            "confidence": 0.99493,
            "speaker": "A"
        },
        {
            "text": "were",
            "start": 278924,
            "end": 279042,
            "confidence": 0.54894,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 279056,
            "end": 279306,
            "confidence": 0.93682,
            "speaker": "A"
        },
        {
            "text": "key",
            "start": 279368,
            "end": 279594,
            "confidence": 0.99981,
            "speaker": "A"
        },
        {
            "text": "findings",
            "start": 279632,
            "end": 280150,
            "confidence": 0.58332,
            "speaker": "A"
        },
        {
            "text": "then",
            "start": 280210,
            "end": 280398,
            "confidence": 0.60101,
            "speaker": "A"
        },
        {
            "text": "of",
            "start": 280424,
            "end": 280578,
            "confidence": 0.79025,
            "speaker": "A"
        },
        {
            "text": "this",
            "start": 280604,
            "end": 280794,
            "confidence": 0.98144,
            "speaker": "A"
        },
        {
            "text": "study?",
            "start": 280832,
            "end": 281420,
            "confidence": 0.99635,
            "speaker": "A"
        },
        {
            "text": "Well,",
            "start": 282410,
            "end": 282990,
            "confidence": 0.84933,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 283100,
            "end": 283302,
            "confidence": 0.99428,
            "speaker": "B"
        },
        {
            "text": "first",
            "start": 283316,
            "end": 283458,
            "confidence": 0.9992,
            "speaker": "B"
        },
        {
            "text": "thing",
            "start": 283484,
            "end": 283602,
            "confidence": 0.99741,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 283616,
            "end": 283758,
            "confidence": 0.99225,
            "speaker": "B"
        },
        {
            "text": "say",
            "start": 283784,
            "end": 284046,
            "confidence": 0.99926,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 284108,
            "end": 284262,
            "confidence": 0.99411,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 284276,
            "end": 284418,
            "confidence": 0.96335,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 284444,
            "end": 284562,
            "confidence": 0.90915,
            "speaker": "B"
        },
        {
            "text": "our",
            "start": 284576,
            "end": 284754,
            "confidence": 0.51742,
            "speaker": "B"
        },
        {
            "text": "knowledge,",
            "start": 284792,
            "end": 285094,
            "confidence": 0.97222,
            "speaker": "B"
        },
        {
            "text": "it's",
            "start": 285142,
            "end": 285286,
            "confidence": 0.43867,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 285298,
            "end": 285438,
            "confidence": 0.64434,
            "speaker": "B"
        },
        {
            "text": "first",
            "start": 285464,
            "end": 285690,
            "confidence": 0.99959,
            "speaker": "B"
        },
        {
            "text": "application",
            "start": 285740,
            "end": 286350,
            "confidence": 0.99938,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 286520,
            "end": 286890,
            "confidence": 0.99131,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 286940,
            "end": 287118,
            "confidence": 0.96501,
            "speaker": "B"
        },
        {
            "text": "#######",
            "start": 287144,
            "end": 287398,
            "confidence": 0.99961,
            "speaker": "B"
        },
        {
            "text": "########",
            "start": 287434,
            "end": 287706,
            "confidence": 0.90056,
            "speaker": "B"
        },
        {
            "text": "algorithm",
            "start": 287768,
            "end": 288334,
            "confidence": 0.92266,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 288382,
            "end": 288558,
            "confidence": 0.80794,
            "speaker": "B"
        },
        {
            "text": "##########",
            "start": 288584,
            "end": 288982,
            "confidence": 0.99925,
            "speaker": "B"
        },
        {
            "text": "############",
            "start": 289006,
            "end": 289534,
            "confidence": 0.66297,
            "speaker": "B"
        },
        {
            "text": "method",
            "start": 289582,
            "end": 290002,
            "confidence": 0.9856,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 290086,
            "end": 290298,
            "confidence": 0.626,
            "speaker": "B"
        },
        {
            "text": "such",
            "start": 290324,
            "end": 290514,
            "confidence": 0.99644,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 290552,
            "end": 290682,
            "confidence": 0.99323,
            "speaker": "B"
        },
        {
            "text": "big",
            "start": 290696,
            "end": 290874,
            "confidence": 0.9993,
            "speaker": "B"
        },
        {
            "text": "data",
            "start": 290912,
            "end": 291114,
            "confidence": 0.99957,
            "speaker": "B"
        },
        {
            "text": "set.",
            "start": 291152,
            "end": 291354,
            "confidence": 0.72616,
            "speaker": "B"
        },
        {
            "text": "To",
            "start": 291392,
            "end": 291558,
            "confidence": 0.98006,
            "speaker": "B"
        },
        {
            "text": "answer",
            "start": 291584,
            "end": 291810,
            "confidence": 0.99866,
            "speaker": "B"
        },
        {
            "text": "this",
            "start": 291860,
            "end": 292074,
            "confidence": 0.96456,
            "speaker": "B"
        },
        {
            "text": "question,",
            "start": 292112,
            "end": 292710,
            "confidence": 0.99686,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 292880,
            "end": 293250,
            "confidence": 0.52093,
            "speaker": "B"
        },
        {
            "text": "one",
            "start": 293300,
            "end": 293442,
            "confidence": 0.96504,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 293456,
            "end": 293562,
            "confidence": 0.99873,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 293576,
            "end": 293718,
            "confidence": 0.99684,
            "speaker": "B"
        },
        {
            "text": "key",
            "start": 293744,
            "end": 293934,
            "confidence": 0.99951,
            "speaker": "B"
        },
        {
            "text": "things",
            "start": 293972,
            "end": 294210,
            "confidence": 0.99653,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 294260,
            "end": 294474,
            "confidence": 0.9888,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 294512,
            "end": 294642,
            "confidence": 0.9719,
            "speaker": "B"
        },
        {
            "text": "knew,",
            "start": 294656,
            "end": 295194,
            "confidence": 0.93984,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 295352,
            "end": 295710,
            "confidence": 0.97083,
            "speaker": "B"
        },
        {
            "text": "Survival",
            "start": 295760,
            "end": 296182,
            "confidence": 0.94872,
            "speaker": "B"
        },
        {
            "text": "Quilts",
            "start": 296206,
            "end": 296782,
            "confidence": 0.34928,
            "speaker": "B"
        },
        {
            "text": "model,",
            "start": 296866,
            "end": 297222,
            "confidence": 0.9688,
            "speaker": "B"
        },
        {
            "text": "like",
            "start": 297296,
            "end": 297786,
            "confidence": 0.8967,
            "speaker": "B"
        },
        {
            "text": "other",
            "start": 297908,
            "end": 298158,
            "confidence": 0.99476,
            "speaker": "B"
        },
        {
            "text": "machine",
            "start": 298184,
            "end": 298462,
            "confidence": 0.98186,
            "speaker": "B"
        },
        {
            "text": "models,",
            "start": 298486,
            "end": 298798,
            "confidence": 0.732,
            "speaker": "B"
        },
        {
            "text": "can",
            "start": 298834,
            "end": 298998,
            "confidence": 0.99794,
            "speaker": "B"
        },
        {
            "text": "actually",
            "start": 299024,
            "end": 299394,
            "confidence": 0.99679,
            "speaker": "B"
        },
        {
            "text": "take",
            "start": 299492,
            "end": 299754,
            "confidence": 0.99917,
            "speaker": "B"
        },
        {
            "text": "new",
            "start": 299792,
            "end": 300030,
            "confidence": 0.99906,
            "speaker": "B"
        },
        {
            "text": "information",
            "start": 300080,
            "end": 300438,
            "confidence": 0.99976,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 300524,
            "end": 300774,
            "confidence": 0.72961,
            "speaker": "B"
        },
        {
            "text": "process",
            "start": 300812,
            "end": 301050,
            "confidence": 0.99986,
            "speaker": "B"
        },
        {
            "text": "it",
            "start": 301100,
            "end": 301278,
            "confidence": 0.90016,
            "speaker": "B"
        },
        {
            "text": "very",
            "start": 301304,
            "end": 301494,
            "confidence": 0.99783,
            "speaker": "B"
        },
        {
            "text": "quickly,",
            "start": 301532,
            "end": 301878,
            "confidence": 0.99934,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 301964,
            "end": 302142,
            "confidence": 0.69573,
            "speaker": "B"
        },
        {
            "text": "it",
            "start": 302156,
            "end": 302262,
            "confidence": 0.98501,
            "speaker": "B"
        },
        {
            "text": "took",
            "start": 302276,
            "end": 302418,
            "confidence": 0.9992,
            "speaker": "B"
        },
        {
            "text": "us",
            "start": 302444,
            "end": 302598,
            "confidence": 0.98691,
            "speaker": "B"
        },
        {
            "text": "many",
            "start": 302624,
            "end": 302850,
            "confidence": 0.99892,
            "speaker": "B"
        },
        {
            "text": "years",
            "start": 302900,
            "end": 303330,
            "confidence": 0.99978,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 303440,
            "end": 303678,
            "confidence": 0.99817,
            "speaker": "B"
        },
        {
            "text": "develop",
            "start": 303704,
            "end": 303966,
            "confidence": 0.99977,
            "speaker": "B"
        },
        {
            "text": "our",
            "start": 304028,
            "end": 304182,
            "confidence": 0.71809,
            "speaker": "B"
        },
        {
            "text": "previous",
            "start": 304196,
            "end": 304510,
            "confidence": 0.96224,
            "speaker": "B"
        },
        {
            "text": "models.",
            "start": 304570,
            "end": 304942,
            "confidence": 0.95804,
            "speaker": "B"
        },
        {
            "text": "But",
            "start": 304966,
            "end": 305118,
            "confidence": 0.93998,
            "speaker": "B"
        },
        {
            "text": "I",
            "start": 305144,
            "end": 305262,
            "confidence": 0.99384,
            "speaker": "B"
        },
        {
            "text": "think",
            "start": 305276,
            "end": 305634,
            "confidence": 0.99952,
            "speaker": "B"
        },
        {
            "text": "working",
            "start": 305732,
            "end": 306066,
            "confidence": 0.99954,
            "speaker": "B"
        },
        {
            "text": "with",
            "start": 306128,
            "end": 306462,
            "confidence": 0.99925,
            "speaker": "B"
        },
        {
            "text": "#######,",
            "start": 306536,
            "end": 307330,
            "confidence": 0.07732,
            "speaker": "B"
        },
        {
            "text": "one",
            "start": 307450,
            "end": 307662,
            "confidence": 0.99574,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 307676,
            "end": 307782,
            "confidence": 0.99931,
            "speaker": "B"
        },
        {
            "text": "your",
            "start": 307796,
            "end": 307974,
            "confidence": 0.99045,
            "speaker": "B"
        },
        {
            "text": "########,",
            "start": 308012,
            "end": 308358,
            "confidence": 0.54271,
            "speaker": "B"
        },
        {
            "text": "with",
            "start": 308444,
            "end": 308622,
            "confidence": 0.48333,
            "speaker": "B"
        },
        {
            "text": "#######,",
            "start": 308636,
            "end": 309034,
            "confidence": 0.11646,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 309082,
            "end": 309258,
            "confidence": 0.98807,
            "speaker": "B"
        },
        {
            "text": "were",
            "start": 309284,
            "end": 309438,
            "confidence": 0.99197,
            "speaker": "B"
        },
        {
            "text": "able",
            "start": 309464,
            "end": 309618,
            "confidence": 0.99998,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 309644,
            "end": 309762,
            "confidence": 0.99895,
            "speaker": "B"
        },
        {
            "text": "produce",
            "start": 309776,
            "end": 310078,
            "confidence": 0.90505,
            "speaker": "B"
        },
        {
            "text": "this",
            "start": 310114,
            "end": 310278,
            "confidence": 0.97821,
            "speaker": "B"
        },
        {
            "text": "model",
            "start": 310304,
            "end": 310530,
            "confidence": 0.99689,
            "speaker": "B"
        },
        {
            "text": "very",
            "start": 310580,
            "end": 310794,
            "confidence": 0.99883,
            "speaker": "B"
        },
        {
            "text": "rapidly",
            "start": 310832,
            "end": 311278,
            "confidence": 0.90232,
            "speaker": "B"
        },
        {
            "text": "because",
            "start": 311314,
            "end": 311550,
            "confidence": 0.99583,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 311600,
            "end": 311742,
            "confidence": 0.88246,
            "speaker": "B"
        },
        {
            "text": "technique",
            "start": 311756,
            "end": 312094,
            "confidence": 0.54176,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 312142,
            "end": 312318,
            "confidence": 0.98359,
            "speaker": "B"
        },
        {
            "text": "already",
            "start": 312344,
            "end": 312606,
            "confidence": 0.98674,
            "speaker": "B"
        },
        {
            "text": "there.",
            "start": 312668,
            "end": 313110,
            "confidence": 0.96946,
            "speaker": "B"
        },
        {
            "text": "And",
            "start": 313220,
            "end": 313458,
            "confidence": 0.76545,
            "speaker": "B"
        },
        {
            "text": "even",
            "start": 313484,
            "end": 313710,
            "confidence": 0.99938,
            "speaker": "B"
        },
        {
            "text": "in",
            "start": 313760,
            "end": 313902,
            "confidence": 0.99714,
            "speaker": "B"
        },
        {
            "text": "this",
            "start": 313916,
            "end": 314058,
            "confidence": 0.93763,
            "speaker": "B"
        },
        {
            "text": "first",
            "start": 314084,
            "end": 314274,
            "confidence": 0.79629,
            "speaker": "B"
        },
        {
            "text": "situation",
            "start": 314312,
            "end": 314802,
            "confidence": 0.9808,
            "speaker": "B"
        },
        {
            "text": "where",
            "start": 314936,
            "end": 315198,
            "confidence": 0.98955,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 315224,
            "end": 315378,
            "confidence": 0.99163,
            "speaker": "B"
        },
        {
            "text": "had",
            "start": 315404,
            "end": 315630,
            "confidence": 0.82924,
            "speaker": "B"
        },
        {
            "text": "fairly",
            "start": 315680,
            "end": 315958,
            "confidence": 0.28681,
            "speaker": "B"
        },
        {
            "text": "standard",
            "start": 315994,
            "end": 316338,
            "confidence": 0.99243,
            "speaker": "B"
        },
        {
            "text": "variables,",
            "start": 316424,
            "end": 317362,
            "confidence": 0.23817,
            "speaker": "B"
        },
        {
            "text": "I",
            "start": 317506,
            "end": 317742,
            "confidence": 0.99693,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 317756,
            "end": 317898,
            "confidence": 0.99879,
            "speaker": "B"
        },
        {
            "text": "amazed",
            "start": 317924,
            "end": 318262,
            "confidence": 0.94929,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 318286,
            "end": 318366,
            "confidence": 0.96271,
            "speaker": "B"
        },
        {
            "text": "find",
            "start": 318368,
            "end": 318570,
            "confidence": 0.5457,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 318620,
            "end": 318834,
            "confidence": 0.58303,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 318872,
            "end": 319460,
            "confidence": 0.93129,
            "speaker": "B"
        },
        {
            "text": "final",
            "start": 319850,
            "end": 320250,
            "confidence": 0.63178,
            "speaker": "B"
        },
        {
            "text": "Quilts",
            "start": 320300,
            "end": 320602,
            "confidence": 0.3245,
            "speaker": "B"
        },
        {
            "text": "model",
            "start": 320626,
            "end": 320850,
            "confidence": 0.99607,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 320900,
            "end": 321078,
            "confidence": 0.99387,
            "speaker": "B"
        },
        {
            "text": "able",
            "start": 321104,
            "end": 321294,
            "confidence": 0.99975,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 321332,
            "end": 321534,
            "confidence": 0.99145,
            "speaker": "B"
        },
        {
            "text": "come",
            "start": 321572,
            "end": 321702,
            "confidence": 0.99929,
            "speaker": "B"
        },
        {
            "text": "up",
            "start": 321716,
            "end": 321822,
            "confidence": 0.68176,
            "speaker": "B"
        },
        {
            "text": "with",
            "start": 321836,
            "end": 321978,
            "confidence": 0.56416,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 322004,
            "end": 322122,
            "confidence": 0.93016,
            "speaker": "B"
        },
        {
            "text": "performance",
            "start": 322136,
            "end": 322534,
            "confidence": 0.99913,
            "speaker": "B"
        },
        {
            "text": "characteristic",
            "start": 322582,
            "end": 323506,
            "confidence": 0.61165,
            "speaker": "B"
        },
        {
            "text": "which",
            "start": 323638,
            "end": 323898,
            "confidence": 0.99927,
            "speaker": "B"
        },
        {
            "text": "was",
            "start": 323924,
            "end": 324078,
            "confidence": 0.99485,
            "speaker": "B"
        },
        {
            "text": "as",
            "start": 324104,
            "end": 324258,
            "confidence": 0.98651,
            "speaker": "B"
        },
        {
            "text": "good",
            "start": 324284,
            "end": 324474,
            "confidence": 0.99883,
            "speaker": "B"
        },
        {
            "text": "as",
            "start": 324512,
            "end": 324714,
            "confidence": 0.57197,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 324752,
            "end": 324918,
            "confidence": 0.98522,
            "speaker": "B"
        },
        {
            "text": "best",
            "start": 324944,
            "end": 325098,
            "confidence": 0.99654,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 325124,
            "end": 325278,
            "confidence": 0.9669,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 325304,
            "end": 325458,
            "confidence": 0.99619,
            "speaker": "B"
        },
        {
            "text": "have",
            "start": 325484,
            "end": 325710,
            "confidence": 0.12181,
            "speaker": "B"
        },
        {
            "text": "from",
            "start": 325760,
            "end": 325902,
            "confidence": 0.99363,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 325916,
            "end": 326022,
            "confidence": 0.68503,
            "speaker": "B"
        },
        {
            "text": "clinical",
            "start": 326036,
            "end": 326410,
            "confidence": 0.72577,
            "speaker": "B"
        },
        {
            "text": "standpoint",
            "start": 326470,
            "end": 327058,
            "confidence": 0.5579,
            "speaker": "B"
        },
        {
            "text": "and",
            "start": 327154,
            "end": 327378,
            "confidence": 0.90144,
            "speaker": "B"
        },
        {
            "text": "actually",
            "start": 327404,
            "end": 327594,
            "confidence": 0.98338,
            "speaker": "B"
        },
        {
            "text": "a",
            "start": 327632,
            "end": 327726,
            "confidence": 0.99313,
            "speaker": "B"
        },
        {
            "text": "little",
            "start": 327728,
            "end": 327858,
            "confidence": 0.98413,
            "speaker": "B"
        },
        {
            "text": "bit",
            "start": 327884,
            "end": 328038,
            "confidence": 0.99854,
            "speaker": "B"
        },
        {
            "text": "better",
            "start": 328064,
            "end": 328326,
            "confidence": 0.99965,
            "speaker": "B"
        },
        {
            "text": "as",
            "start": 328388,
            "end": 328542,
            "confidence": 0.99692,
            "speaker": "B"
        },
        {
            "text": "well.",
            "start": 328556,
            "end": 328914,
            "confidence": 0.99407,
            "speaker": "B"
        },
        {
            "text": "So",
            "start": 329012,
            "end": 329454,
            "confidence": 0.7025,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 329552,
            "end": 329778,
            "confidence": 0.99646,
            "speaker": "B"
        },
        {
            "text": "is",
            "start": 329804,
            "end": 330030,
            "confidence": 0.98744,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 330080,
            "end": 330222,
            "confidence": 0.93588,
            "speaker": "B"
        },
        {
            "text": "me",
            "start": 330236,
            "end": 330486,
            "confidence": 0.9944,
            "speaker": "B"
        },
        {
            "text": "an",
            "start": 330548,
            "end": 330702,
            "confidence": 0.81149,
            "speaker": "B"
        },
        {
            "text": "illustration",
            "start": 330716,
            "end": 331222,
            "confidence": 0.69155,
            "speaker": "B"
        },
        {
            "text": "that",
            "start": 331306,
            "end": 331482,
            "confidence": 0.78467,
            "speaker": "B"
        },
        {
            "text": "the",
            "start": 331496,
            "end": 331602,
            "confidence": 0.98378,
            "speaker": "B"
        },
        {
            "text": "direction",
            "start": 331616,
            "end": 332002,
            "confidence": 0.55285,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 332026,
            "end": 332178,
            "confidence": 0.98446,
            "speaker": "B"
        },
        {
            "text": "travel",
            "start": 332204,
            "end": 332502,
            "confidence": 0.99922,
            "speaker": "B"
        },
        {
            "text": "or",
            "start": 332576,
            "end": 332742,
            "confidence": 0.79056,
            "speaker": "B"
        },
        {
            "text": "what",
            "start": 332756,
            "end": 332898,
            "confidence": 0.99712,
            "speaker": "B"
        },
        {
            "text": "we",
            "start": 332924,
            "end": 333042,
            "confidence": 0.99037,
            "speaker": "B"
        },
        {
            "text": "do",
            "start": 333056,
            "end": 333162,
            "confidence": 0.99807,
            "speaker": "B"
        },
        {
            "text": "in",
            "start": 333176,
            "end": 333318,
            "confidence": 0.9933,
            "speaker": "B"
        },
        {
            "text": "future",
            "start": 333344,
            "end": 333786,
            "confidence": 0.99745,
            "speaker": "B"
        },
        {
            "text": "has",
            "start": 333908,
            "end": 334194,
            "confidence": 0.9917,
            "speaker": "B"
        },
        {
            "text": "to",
            "start": 334232,
            "end": 334362,
            "confidence": 0.99796,
            "speaker": "B"
        },
        {
            "text": "be",
            "start": 334376,
            "end": 334554,
            "confidence": 0.99363,
            "speaker": "B"
        },
        {
            "text": "using",
            "start": 334592,
            "end": 334830,
            "confidence": 0.97078,
            "speaker": "B"
        },
        {
            "text": "this",
            "start": 334880,
            "end": 335058,
            "confidence": 0.98196,
            "speaker": "B"
        },
        {
            "text": "kind",
            "start": 335084,
            "end": 335202,
            "confidence": 0.97223,
            "speaker": "B"
        },
        {
            "text": "of",
            "start": 335216,
            "end": 335430,
            "confidence": 0.9983,
            "speaker": "B"
        },
        {
            "text": "technology.",
            "start": 335480,
            "end": 336080,
            "confidence": 0.99818,
            "speaker": "B"
        },
        {
            "text": "That's",
            "start": 336890,
            "end": 337282,
            "confidence": 0.86583,
            "speaker": "A"
        },
        {
            "text": "a",
            "start": 337306,
            "end": 337422,
            "confidence": 0.99548,
            "speaker": "A"
        },
        {
            "text": "great",
            "start": 337436,
            "end": 337650,
            "confidence": 0.99977,
            "speaker": "A"
        },
        {
            "text": "summary.",
            "start": 337700,
            "end": 338490,
            "confidence": 0.72954,
            "speaker": "A"
        },
        {
            "text": "You've",
            "start": 338930,
            "end": 339262,
            "confidence": 0.20057,
            "speaker": "A"
        },
        {
            "text": "mentioned",
            "start": 339286,
            "end": 339690,
            "confidence": 0.90394,
            "speaker": "A"
        },
        {
            "text": "in",
            "start": 339800,
            "end": 340002,
            "confidence": 0.99932,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 340016,
            "end": 340158,
            "confidence": 0.99776,
            "speaker": "A"
        },
        {
            "text": "paper",
            "start": 340184,
            "end": 340446,
            "confidence": 0.9999,
            "speaker": "A"
        },
        {
            "text": "that",
            "start": 340508,
            "end": 340734,
            "confidence": 0.99608,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 340772,
            "end": 341262,
            "confidence": 0.51872,
            "speaker": "A"
        },
        {
            "text": "Seared",
            "start": 341396,
            "end": 341794,
            "confidence": 0.71966,
            "speaker": "A"
        },
        {
            "text": "data",
            "start": 341842,
            "end": 342090,
            "confidence": 0.99618,
            "speaker": "A"
        },
        {
            "text": "set",
            "start": 342140,
            "end": 342534,
            "confidence": 0.75696,
            "speaker": "A"
        },
        {
            "text": "doesn't",
            "start": 342632,
            "end": 342958,
            "confidence": 0.95461,
            "speaker": "A"
        },
        {
            "text": "include",
            "start": 342994,
            "end": 343378,
            "confidence": 0.99931,
            "speaker": "A"
        },
        {
            "text": "data",
            "start": 343414,
            "end": 343722,
            "confidence": 0.99921,
            "speaker": "A"
        },
        {
            "text": "on",
            "start": 343796,
            "end": 344178,
            "confidence": 0.99814,
            "speaker": "A"
        },
        {
            "text": "comorbidity,",
            "start": 344264,
            "end": 345262,
            "confidence": 0.47473,
            "speaker": "A"
        },
        {
            "text": "########,",
            "start": 345346,
            "end": 345850,
            "confidence": 0.97148,
            "speaker": "A"
        },
        {
            "text": "###,",
            "start": 345910,
            "end": 346630,
            "confidence": 0.96219,
            "speaker": "A"
        },
        {
            "text": "nor",
            "start": 346750,
            "end": 347070,
            "confidence": 0.90028,
            "speaker": "A"
        },
        {
            "text": "treatment,",
            "start": 347120,
            "end": 347842,
            "confidence": 0.96581,
            "speaker": "A"
        },
        {
            "text": "although",
            "start": 347986,
            "end": 348574,
            "confidence": 0.95037,
            "speaker": "A"
        },
        {
            "text": "potentially",
            "start": 348682,
            "end": 349366,
            "confidence": 0.9956,
            "speaker": "A"
        },
        {
            "text": "this",
            "start": 349498,
            "end": 349794,
            "confidence": 0.08142,
            "speaker": "A"
        },
        {
            "text": "could",
            "start": 349832,
            "end": 349998,
            "confidence": 0.98911,
            "speaker": "A"
        },
        {
            "text": "improve",
            "start": 350024,
            "end": 350302,
            "confidence": 0.99752,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 350326,
            "end": 350478,
            "confidence": 0.85772,
            "speaker": "A"
        },
        {
            "text": "prognostic",
            "start": 350504,
            "end": 351058,
            "confidence": 0.63368,
            "speaker": "A"
        },
        {
            "text": "capabilities",
            "start": 351094,
            "end": 351670,
            "confidence": 0.57497,
            "speaker": "A"
        },
        {
            "text": "of",
            "start": 351730,
            "end": 351882,
            "confidence": 0.87812,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 351896,
            "end": 352038,
            "confidence": 0.74009,
            "speaker": "A"
        },
        {
            "text": "model.",
            "start": 352064,
            "end": 352614,
            "confidence": 0.99659,
            "speaker": "A"
        },
        {
            "text": "So",
            "start": 352772,
            "end": 353058,
            "confidence": 0.72534,
            "speaker": "A"
        },
        {
            "text": "how",
            "start": 353084,
            "end": 353346,
            "confidence": 0.99934,
            "speaker": "A"
        },
        {
            "text": "easy",
            "start": 353408,
            "end": 353706,
            "confidence": 0.99881,
            "speaker": "A"
        },
        {
            "text": "would",
            "start": 353768,
            "end": 353922,
            "confidence": 0.99504,
            "speaker": "A"
        },
        {
            "text": "it",
            "start": 353936,
            "end": 354042,
            "confidence": 0.99871,
            "speaker": "A"
        },
        {
            "text": "be",
            "start": 354056,
            "end": 354198,
            "confidence": 0.99921,
            "speaker": "A"
        },
        {
            "text": "to",
            "start": 354224,
            "end": 354378,
            "confidence": 0.99749,
            "speaker": "A"
        },
        {
            "text": "incorporate",
            "start": 354404,
            "end": 355090,
            "confidence": 0.92748,
            "speaker": "A"
        },
        {
            "text": "these",
            "start": 355150,
            "end": 355590,
            "confidence": 0.99541,
            "speaker": "A"
        },
        {
            "text": "variables",
            "start": 355700,
            "end": 356242,
            "confidence": 0.59828,
            "speaker": "A"
        },
        {
            "text": "in",
            "start": 356266,
            "end": 356382,
            "confidence": 0.99753,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 356396,
            "end": 356538,
            "confidence": 0.99411,
            "speaker": "A"
        },
        {
            "text": "future",
            "start": 356564,
            "end": 357006,
            "confidence": 0.99982,
            "speaker": "A"
        },
        {
            "text": "and",
            "start": 357128,
            "end": 357414,
            "confidence": 0.80505,
            "speaker": "A"
        },
        {
            "text": "what",
            "start": 357452,
            "end": 357654,
            "confidence": 0.99957,
            "speaker": "A"
        },
        {
            "text": "effect",
            "start": 357692,
            "end": 357966,
            "confidence": 0.98779,
            "speaker": "A"
        },
        {
            "text": "do",
            "start": 358028,
            "end": 358182,
            "confidence": 0.93255,
            "speaker": "A"
        },
        {
            "text": "you",
            "start": 358196,
            "end": 358302,
            "confidence": 0.99691,
            "speaker": "A"
        },
        {
            "text": "think",
            "start": 358316,
            "end": 358458,
            "confidence": 0.99949,
            "speaker": "A"
        },
        {
            "text": "that",
            "start": 358484,
            "end": 358638,
            "confidence": 0.8648,
            "speaker": "A"
        },
        {
            "text": "could",
            "start": 358664,
            "end": 358818,
            "confidence": 0.83495,
            "speaker": "A"
        },
        {
            "text": "have",
            "start": 358844,
            "end": 358998,
            "confidence": 0.99712,
            "speaker": "A"
        },
        {
            "text": "on",
            "start": 359024,
            "end": 359142,
            "confidence": 0.99459,
            "speaker": "A"
        },
        {
            "text": "the",
            "start": 359156,
            "end": 359262,
            "confidence": 0.98113,
            "speaker": "A"
        },
        {
            "text": "model",
            "start": 359276,
            "end": 359562,
            "confidence": 0.99711,
            "speaker": "A"
        },
        {
            "text": "output?",
            "start": 359636,
            "end": 360450,
            "confidence": 0.92644,
            "speaker": "A"
        },
        {
            "text": "One",
            "start": 361070,
            "end": 361434,
            "confidence": 0.89957,
            "speaker": "C"
        },
        {
            "text": "advantage",
            "start": 361472,
            "end": 362182,
            "confidence": 0.98216,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 362266,
            "end": 362478,
            "confidence": 0.99705,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 362504,
            "end": 363094,
            "confidence": 0.53163,
            "speaker": "C"
        },
        {
            "text": "quiz",
            "start": 363142,
            "end": 363778,
            "confidence": 0.86223,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 363934,
            "end": 364326,
            "confidence": 0.96661,
            "speaker": "C"
        },
        {
            "text": "that",
            "start": 364388,
            "end": 364650,
            "confidence": 0.98341,
            "speaker": "C"
        },
        {
            "text": "it's",
            "start": 364700,
            "end": 364918,
            "confidence": 0.22646,
            "speaker": "C"
        },
        {
            "text": "an",
            "start": 364954,
            "end": 365298,
            "confidence": 0.97559,
            "speaker": "C"
        },
        {
            "text": "automated",
            "start": 365384,
            "end": 366046,
            "confidence": 0.99684,
            "speaker": "C"
        },
        {
            "text": "#######",
            "start": 366118,
            "end": 366418,
            "confidence": 0.99625,
            "speaker": "C"
        },
        {
            "text": "########",
            "start": 366454,
            "end": 366762,
            "confidence": 0.99972,
            "speaker": "C"
        },
        {
            "text": "method.",
            "start": 366836,
            "end": 367246,
            "confidence": 0.9981,
            "speaker": "C"
        },
        {
            "text": "It",
            "start": 367318,
            "end": 367518,
            "confidence": 0.974,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 367544,
            "end": 367734,
            "confidence": 0.68758,
            "speaker": "C"
        },
        {
            "text": "able",
            "start": 367772,
            "end": 368082,
            "confidence": 0.99879,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 368156,
            "end": 368394,
            "confidence": 0.99871,
            "speaker": "C"
        },
        {
            "text": "take",
            "start": 368432,
            "end": 368670,
            "confidence": 0.99909,
            "speaker": "C"
        },
        {
            "text": "whatever",
            "start": 368720,
            "end": 369114,
            "confidence": 0.99488,
            "speaker": "C"
        },
        {
            "text": "data",
            "start": 369212,
            "end": 369654,
            "confidence": 0.99801,
            "speaker": "C"
        },
        {
            "text": "we",
            "start": 369752,
            "end": 369978,
            "confidence": 0.48468,
            "speaker": "C"
        },
        {
            "text": "throw",
            "start": 370004,
            "end": 370318,
            "confidence": 0.55683,
            "speaker": "C"
        },
        {
            "text": "at",
            "start": 370354,
            "end": 370482,
            "confidence": 0.56184,
            "speaker": "C"
        },
        {
            "text": "it",
            "start": 370496,
            "end": 370890,
            "confidence": 0.96026,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 371000,
            "end": 371346,
            "confidence": 0.99602,
            "speaker": "C"
        },
        {
            "text": "craft",
            "start": 371408,
            "end": 371734,
            "confidence": 0.99251,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 371782,
            "end": 371922,
            "confidence": 0.8653,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 371936,
            "end": 372222,
            "confidence": 0.75755,
            "speaker": "C"
        },
        {
            "text": "with",
            "start": 372296,
            "end": 372570,
            "confidence": 0.9947,
            "speaker": "C"
        },
        {
            "text": "good",
            "start": 372620,
            "end": 372834,
            "confidence": 0.93791,
            "speaker": "C"
        },
        {
            "text": "discrimination",
            "start": 372872,
            "end": 373642,
            "confidence": 0.28624,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 373726,
            "end": 373974,
            "confidence": 0.5403,
            "speaker": "C"
        },
        {
            "text": "calibration",
            "start": 374012,
            "end": 374722,
            "confidence": 0.71316,
            "speaker": "C"
        },
        {
            "text": "performance.",
            "start": 374866,
            "end": 375826,
            "confidence": 0.79163,
            "speaker": "C"
        },
        {
            "text": "So",
            "start": 376018,
            "end": 376410,
            "confidence": 0.7132,
            "speaker": "C"
        },
        {
            "text": "it",
            "start": 376460,
            "end": 376638,
            "confidence": 0.99792,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 376664,
            "end": 376926,
            "confidence": 0.98525,
            "speaker": "C"
        },
        {
            "text": "very",
            "start": 376988,
            "end": 377358,
            "confidence": 0.99761,
            "speaker": "C"
        },
        {
            "text": "easy",
            "start": 377444,
            "end": 377802,
            "confidence": 0.8064,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 377876,
            "end": 378078,
            "confidence": 0.98666,
            "speaker": "C"
        },
        {
            "text": "push",
            "start": 378104,
            "end": 378298,
            "confidence": 0.50648,
            "speaker": "C"
        },
        {
            "text": "off",
            "start": 378334,
            "end": 378498,
            "confidence": 0.96985,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 378524,
            "end": 378642,
            "confidence": 0.96127,
            "speaker": "C"
        },
        {
            "text": "button",
            "start": 378656,
            "end": 378942,
            "confidence": 0.57208,
            "speaker": "C"
        },
        {
            "text": "if",
            "start": 379016,
            "end": 379218,
            "confidence": 0.98518,
            "speaker": "C"
        },
        {
            "text": "this",
            "start": 379244,
            "end": 379434,
            "confidence": 0.83388,
            "speaker": "C"
        },
        {
            "text": "data",
            "start": 379472,
            "end": 379746,
            "confidence": 0.99881,
            "speaker": "C"
        },
        {
            "text": "becomes",
            "start": 379808,
            "end": 380314,
            "confidence": 0.98717,
            "speaker": "C"
        },
        {
            "text": "available",
            "start": 380362,
            "end": 380960,
            "confidence": 0.99971,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 381410,
            "end": 381810,
            "confidence": 0.99856,
            "speaker": "C"
        },
        {
            "text": "integrate",
            "start": 381860,
            "end": 382486,
            "confidence": 0.8262,
            "speaker": "C"
        },
        {
            "text": "additional",
            "start": 382558,
            "end": 383146,
            "confidence": 0.97234,
            "speaker": "C"
        },
        {
            "text": "variables",
            "start": 383278,
            "end": 384142,
            "confidence": 0.68849,
            "speaker": "C"
        },
        {
            "text": "into",
            "start": 384226,
            "end": 384546,
            "confidence": 0.99444,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 384608,
            "end": 384798,
            "confidence": 0.99659,
            "speaker": "C"
        },
        {
            "text": "mobile",
            "start": 384824,
            "end": 385450,
            "confidence": 0.5525,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 385570,
            "end": 385854,
            "confidence": 0.99702,
            "speaker": "C"
        },
        {
            "text": "issue",
            "start": 385892,
            "end": 386166,
            "confidence": 0.98499,
            "speaker": "C"
        },
        {
            "text": "this",
            "start": 386228,
            "end": 386454,
            "confidence": 0.61323,
            "speaker": "C"
        },
        {
            "text": "prediction.",
            "start": 386492,
            "end": 387070,
            "confidence": 0.43128,
            "speaker": "C"
        },
        {
            "text": "So",
            "start": 387130,
            "end": 387354,
            "confidence": 0.7565,
            "speaker": "C"
        },
        {
            "text": "in",
            "start": 387392,
            "end": 387522,
            "confidence": 0.99939,
            "speaker": "C"
        },
        {
            "text": "terms",
            "start": 387536,
            "end": 387858,
            "confidence": 0.99662,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 387944,
            "end": 388626,
            "confidence": 0.99927,
            "speaker": "C"
        },
        {
            "text": "building",
            "start": 388808,
            "end": 389298,
            "confidence": 0.9995,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 389384,
            "end": 389562,
            "confidence": 0.97763,
            "speaker": "C"
        },
        {
            "text": "new",
            "start": 389576,
            "end": 389718,
            "confidence": 0.99817,
            "speaker": "C"
        },
        {
            "text": "model",
            "start": 389744,
            "end": 390006,
            "confidence": 0.91242,
            "speaker": "C"
        },
        {
            "text": "on",
            "start": 390068,
            "end": 390258,
            "confidence": 0.71924,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 390284,
            "end": 390402,
            "confidence": 0.99424,
            "speaker": "C"
        },
        {
            "text": "basis",
            "start": 390416,
            "end": 390718,
            "confidence": 0.99971,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 390754,
            "end": 390918,
            "confidence": 0.999,
            "speaker": "C"
        },
        {
            "text": "new",
            "start": 390944,
            "end": 391170,
            "confidence": 0.99777,
            "speaker": "C"
        },
        {
            "text": "variables,",
            "start": 391220,
            "end": 392146,
            "confidence": 0.94521,
            "speaker": "C"
        },
        {
            "text": "that",
            "start": 392278,
            "end": 392574,
            "confidence": 0.99079,
            "speaker": "C"
        },
        {
            "text": "is,",
            "start": 392612,
            "end": 392778,
            "confidence": 0.97364,
            "speaker": "C"
        },
        {
            "text": "I",
            "start": 392804,
            "end": 392922,
            "confidence": 0.94796,
            "speaker": "C"
        },
        {
            "text": "think,",
            "start": 392936,
            "end": 393186,
            "confidence": 0.7204,
            "speaker": "C"
        },
        {
            "text": "very",
            "start": 393248,
            "end": 393546,
            "confidence": 0.99865,
            "speaker": "C"
        },
        {
            "text": "easy.",
            "start": 393608,
            "end": 394230,
            "confidence": 0.99905,
            "speaker": "C"
        },
        {
            "text": "What",
            "start": 394400,
            "end": 394698,
            "confidence": 0.54788,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 394724,
            "end": 394914,
            "confidence": 0.53175,
            "speaker": "C"
        },
        {
            "text": "important",
            "start": 394952,
            "end": 395262,
            "confidence": 0.99992,
            "speaker": "C"
        },
        {
            "text": "about",
            "start": 395336,
            "end": 395574,
            "confidence": 0.99847,
            "speaker": "C"
        },
        {
            "text": "these",
            "start": 395612,
            "end": 395814,
            "confidence": 0.32889,
            "speaker": "C"
        },
        {
            "text": "methods",
            "start": 395852,
            "end": 396238,
            "confidence": 0.76913,
            "speaker": "C"
        },
        {
            "text": "such",
            "start": 396274,
            "end": 396438,
            "confidence": 0.99945,
            "speaker": "C"
        },
        {
            "text": "as",
            "start": 396464,
            "end": 396618,
            "confidence": 0.9577,
            "speaker": "C"
        },
        {
            "text": "survival",
            "start": 396644,
            "end": 397114,
            "confidence": 0.88637,
            "speaker": "C"
        },
        {
            "text": "quilts,",
            "start": 397162,
            "end": 397810,
            "confidence": 0.1728,
            "speaker": "C"
        },
        {
            "text": "is",
            "start": 397930,
            "end": 398214,
            "confidence": 0.67947,
            "speaker": "C"
        },
        {
            "text": "that",
            "start": 398252,
            "end": 398454,
            "confidence": 0.9832,
            "speaker": "C"
        },
        {
            "text": "not",
            "start": 398492,
            "end": 398730,
            "confidence": 0.99934,
            "speaker": "C"
        },
        {
            "text": "only",
            "start": 398780,
            "end": 399102,
            "confidence": 0.97942,
            "speaker": "C"
        },
        {
            "text": "we",
            "start": 399176,
            "end": 399378,
            "confidence": 0.98532,
            "speaker": "C"
        },
        {
            "text": "are",
            "start": 399404,
            "end": 399558,
            "confidence": 0.98805,
            "speaker": "C"
        },
        {
            "text": "able",
            "start": 399584,
            "end": 399810,
            "confidence": 0.99997,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 399860,
            "end": 400074,
            "confidence": 0.99942,
            "speaker": "C"
        },
        {
            "text": "assess",
            "start": 400112,
            "end": 400846,
            "confidence": 0.53457,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 401038,
            "end": 401610,
            "confidence": 0.99008,
            "speaker": "C"
        },
        {
            "text": "advantage",
            "start": 401720,
            "end": 402610,
            "confidence": 0.98518,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 402730,
            "end": 403014,
            "confidence": 0.95949,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 403052,
            "end": 403254,
            "confidence": 0.78183,
            "speaker": "C"
        },
        {
            "text": "value",
            "start": 403292,
            "end": 403638,
            "confidence": 0.7973,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 403724,
            "end": 403974,
            "confidence": 0.97371,
            "speaker": "C"
        },
        {
            "text": "what",
            "start": 404012,
            "end": 404178,
            "confidence": 0.96441,
            "speaker": "C"
        },
        {
            "text": "I",
            "start": 404204,
            "end": 404286,
            "confidence": 0.96449,
            "speaker": "C"
        },
        {
            "text": "call",
            "start": 404288,
            "end": 404418,
            "confidence": 0.89138,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 404444,
            "end": 404598,
            "confidence": 0.84968,
            "speaker": "C"
        },
        {
            "text": "value",
            "start": 404624,
            "end": 404814,
            "confidence": 0.99887,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 404852,
            "end": 405054,
            "confidence": 0.99737,
            "speaker": "C"
        },
        {
            "text": "modeling,",
            "start": 405092,
            "end": 405790,
            "confidence": 0.91793,
            "speaker": "C"
        },
        {
            "text": "identifying",
            "start": 405910,
            "end": 406846,
            "confidence": 0.95533,
            "speaker": "C"
        },
        {
            "text": "which",
            "start": 406918,
            "end": 407262,
            "confidence": 0.99959,
            "speaker": "C"
        },
        {
            "text": "methods",
            "start": 407336,
            "end": 407818,
            "confidence": 0.81569,
            "speaker": "C"
        },
        {
            "text": "are",
            "start": 407854,
            "end": 408054,
            "confidence": 0.99811,
            "speaker": "C"
        },
        {
            "text": "best",
            "start": 408092,
            "end": 408330,
            "confidence": 0.99936,
            "speaker": "C"
        },
        {
            "text": "for",
            "start": 408380,
            "end": 408558,
            "confidence": 0.99926,
            "speaker": "C"
        },
        {
            "text": "what",
            "start": 408584,
            "end": 408774,
            "confidence": 0.86185,
            "speaker": "C"
        },
        {
            "text": "type",
            "start": 408812,
            "end": 408978,
            "confidence": 0.99938,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 409004,
            "end": 409194,
            "confidence": 0.51465,
            "speaker": "C"
        },
        {
            "text": "data",
            "start": 409232,
            "end": 409506,
            "confidence": 0.99899,
            "speaker": "C"
        },
        {
            "text": "and",
            "start": 409568,
            "end": 409722,
            "confidence": 0.9505,
            "speaker": "C"
        },
        {
            "text": "what",
            "start": 409736,
            "end": 409878,
            "confidence": 0.99247,
            "speaker": "C"
        },
        {
            "text": "type",
            "start": 409904,
            "end": 410058,
            "confidence": 0.99925,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 410084,
            "end": 410238,
            "confidence": 0.99882,
            "speaker": "C"
        },
        {
            "text": "patients,",
            "start": 410264,
            "end": 410938,
            "confidence": 0.41651,
            "speaker": "C"
        },
        {
            "text": "but",
            "start": 411094,
            "end": 411414,
            "confidence": 0.95978,
            "speaker": "C"
        },
        {
            "text": "also",
            "start": 411452,
            "end": 411726,
            "confidence": 0.99864,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 411788,
            "end": 412050,
            "confidence": 0.94916,
            "speaker": "C"
        },
        {
            "text": "value",
            "start": 412100,
            "end": 412314,
            "confidence": 0.9991,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 412352,
            "end": 412554,
            "confidence": 0.73715,
            "speaker": "C"
        },
        {
            "text": "information.",
            "start": 412592,
            "end": 413180,
            "confidence": 0.9631,
            "speaker": "C"
        },
        {
            "text": "So",
            "start": 413690,
            "end": 414054,
            "confidence": 0.77737,
            "speaker": "C"
        },
        {
            "text": "we",
            "start": 414092,
            "end": 414222,
            "confidence": 0.99757,
            "speaker": "C"
        },
        {
            "text": "are",
            "start": 414236,
            "end": 414378,
            "confidence": 0.60889,
            "speaker": "C"
        },
        {
            "text": "going",
            "start": 414404,
            "end": 414630,
            "confidence": 0.99903,
            "speaker": "C"
        },
        {
            "text": "to",
            "start": 414680,
            "end": 414930,
            "confidence": 0.99875,
            "speaker": "C"
        },
        {
            "text": "learn",
            "start": 414980,
            "end": 415230,
            "confidence": 0.99744,
            "speaker": "C"
        },
        {
            "text": "in",
            "start": 415280,
            "end": 415458,
            "confidence": 0.93728,
            "speaker": "C"
        },
        {
            "text": "a",
            "start": 415484,
            "end": 415638,
            "confidence": 0.96023,
            "speaker": "C"
        },
        {
            "text": "data",
            "start": 415664,
            "end": 415926,
            "confidence": 0.99926,
            "speaker": "C"
        },
        {
            "text": "driven",
            "start": 415988,
            "end": 416374,
            "confidence": 0.64747,
            "speaker": "C"
        },
        {
            "text": "way",
            "start": 416422,
            "end": 416598,
            "confidence": 0.97553,
            "speaker": "C"
        },
        {
            "text": "on",
            "start": 416624,
            "end": 416742,
            "confidence": 0.98057,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 416756,
            "end": 416898,
            "confidence": 0.99738,
            "speaker": "C"
        },
        {
            "text": "basis",
            "start": 416924,
            "end": 417274,
            "confidence": 0.99977,
            "speaker": "C"
        },
        {
            "text": "of",
            "start": 417322,
            "end": 417462,
            "confidence": 0.99881,
            "speaker": "C"
        },
        {
            "text": "the",
            "start": 417476,
            "end": 417654,
            "confidence": 0.97795,
            "speaker": "C"
        },
        {
            "text": "data,",
            "start": 417692,
            "end": 418362,
            "confidence": 0.99722,
            "speaker": "C"
        },
        {
            "text": "what",
            "start": 418556,
            "end": 419058,
            "confidence": 0.99775,
            "speaker": "C"
        },
        {
            "text": "variables.",
            "start": 419144,
            "end": 419710,
            "confidence": 0.55168,
            "speaker": "C"
        }
    ],
    "utterances": [
        {
            "confidence": 0.8661576799999997,
            "end": 42980,
            "speaker": "A",
            "start": 310,
            "text": "Quilt was developed using data from the Surveillance, Epidemiology and End Results program, or Steer for short and compared to nine other prognostic models that are currently used in the clinic. Joining me now to discuss this further are two ####### of the paper, ####### ### ### #### and ####### ##### #########, both primarily based at the ########## ## #########. ####### is a ######### of ####### ########, ########## ############, and ########, and ####### is both a ###### in ######### and a ########## ########### specializing in ######## ######. So just kind of going back to the start, can you give us a bit of background on this particular study and what perhaps was your inspiration for starting and what clinical problem were you hoping to address?",
            "words": [
                {
                    "text": "Quilt",
                    "start": 310,
                    "end": 730,
                    "confidence": 0.69927,
                    "speaker": "A"
                },
                {
                    "text": "was",
                    "start": 850,
                    "end": 1134,
                    "confidence": 0.99832,
                    "speaker": "A"
                },
                {
                    "text": "developed",
                    "start": 1172,
                    "end": 1618,
                    "confidence": 0.99907,
                    "speaker": "A"
                },
                {
                    "text": "using",
                    "start": 1654,
                    "end": 1890,
                    "confidence": 0.99992,
                    "speaker": "A"
                },
                {
                    "text": "data",
                    "start": 1940,
                    "end": 2190,
                    "confidence": 0.99975,
                    "speaker": "A"
                },
                {
                    "text": "from",
                    "start": 2240,
                    "end": 2382,
                    "confidence": 0.99956,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 2396,
                    "end": 2502,
                    "confidence": 0.97752,
                    "speaker": "A"
                },
                {
                    "text": "Surveillance,",
                    "start": 2516,
                    "end": 3214,
                    "confidence": 0.52397,
                    "speaker": "A"
                },
                {
                    "text": "Epidemiology",
                    "start": 3322,
                    "end": 4258,
                    "confidence": 0.62908,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 4294,
                    "end": 4422,
                    "confidence": 0.98907,
                    "speaker": "A"
                },
                {
                    "text": "End",
                    "start": 4436,
                    "end": 4614,
                    "confidence": 0.70329,
                    "speaker": "A"
                },
                {
                    "text": "Results",
                    "start": 4652,
                    "end": 5062,
                    "confidence": 0.73267,
                    "speaker": "A"
                },
                {
                    "text": "program,",
                    "start": 5086,
                    "end": 5490,
                    "confidence": 0.8934,
                    "speaker": "A"
                },
                {
                    "text": "or",
                    "start": 5600,
                    "end": 5874,
                    "confidence": 0.73536,
                    "speaker": "A"
                },
                {
                    "text": "Steer",
                    "start": 5912,
                    "end": 6214,
                    "confidence": 0.18716,
                    "speaker": "A"
                },
                {
                    "text": "for",
                    "start": 6262,
                    "end": 6438,
                    "confidence": 0.95862,
                    "speaker": "A"
                },
                {
                    "text": "short",
                    "start": 6464,
                    "end": 6906,
                    "confidence": 0.99557,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 7028,
                    "end": 7314,
                    "confidence": 0.96928,
                    "speaker": "A"
                },
                {
                    "text": "compared",
                    "start": 7352,
                    "end": 7654,
                    "confidence": 0.91613,
                    "speaker": "A"
                },
                {
                    "text": "to",
                    "start": 7702,
                    "end": 7878,
                    "confidence": 0.99904,
                    "speaker": "A"
                },
                {
                    "text": "nine",
                    "start": 7904,
                    "end": 8094,
                    "confidence": 0.99874,
                    "speaker": "A"
                },
                {
                    "text": "other",
                    "start": 8132,
                    "end": 8334,
                    "confidence": 0.91394,
                    "speaker": "A"
                },
                {
                    "text": "prognostic",
                    "start": 8372,
                    "end": 8938,
                    "confidence": 0.92154,
                    "speaker": "A"
                },
                {
                    "text": "models",
                    "start": 8974,
                    "end": 9322,
                    "confidence": 0.99572,
                    "speaker": "A"
                },
                {
                    "text": "that",
                    "start": 9346,
                    "end": 9462,
                    "confidence": 0.98799,
                    "speaker": "A"
                },
                {
                    "text": "are",
                    "start": 9476,
                    "end": 9582,
                    "confidence": 0.98714,
                    "speaker": "A"
                },
                {
                    "text": "currently",
                    "start": 9596,
                    "end": 9918,
                    "confidence": 0.99898,
                    "speaker": "A"
                },
                {
                    "text": "used",
                    "start": 10004,
                    "end": 10290,
                    "confidence": 0.99933,
                    "speaker": "A"
                },
                {
                    "text": "in",
                    "start": 10340,
                    "end": 10446,
                    "confidence": 0.9991,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 10448,
                    "end": 10542,
                    "confidence": 0.90357,
                    "speaker": "A"
                },
                {
                    "text": "clinic.",
                    "start": 10556,
                    "end": 11254,
                    "confidence": 0.72917,
                    "speaker": "A"
                },
                {
                    "text": "Joining",
                    "start": 11422,
                    "end": 11842,
                    "confidence": 0.96994,
                    "speaker": "A"
                },
                {
                    "text": "me",
                    "start": 11866,
                    "end": 12018,
                    "confidence": 0.99934,
                    "speaker": "A"
                },
                {
                    "text": "now",
                    "start": 12044,
                    "end": 12198,
                    "confidence": 0.99859,
                    "speaker": "A"
                },
                {
                    "text": "to",
                    "start": 12224,
                    "end": 12378,
                    "confidence": 0.99907,
                    "speaker": "A"
                },
                {
                    "text": "discuss",
                    "start": 12404,
                    "end": 12702,
                    "confidence": 0.99991,
                    "speaker": "A"
                },
                {
                    "text": "this",
                    "start": 12776,
                    "end": 13014,
                    "confidence": 0.99254,
                    "speaker": "A"
                },
                {
                    "text": "further",
                    "start": 13052,
                    "end": 13318,
                    "confidence": 0.82422,
                    "speaker": "A"
                },
                {
                    "text": "are",
                    "start": 13354,
                    "end": 13518,
                    "confidence": 0.62892,
                    "speaker": "A"
                },
                {
                    "text": "two",
                    "start": 13544,
                    "end": 13734,
                    "confidence": 0.99854,
                    "speaker": "A"
                },
                {
                    "text": "#######",
                    "start": 13772,
                    "end": 14038,
                    "confidence": 0.98482,
                    "speaker": "A"
                },
                {
                    "text": "of",
                    "start": 14074,
                    "end": 14202,
                    "confidence": 0.74494,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 14216,
                    "end": 14358,
                    "confidence": 0.8382,
                    "speaker": "A"
                },
                {
                    "text": "paper,",
                    "start": 14384,
                    "end": 14898,
                    "confidence": 0.99965,
                    "speaker": "A"
                },
                {
                    "text": "#######",
                    "start": 15044,
                    "end": 15622,
                    "confidence": 0.22702,
                    "speaker": "A"
                },
                {
                    "text": "###",
                    "start": 15646,
                    "end": 15826,
                    "confidence": 0.47779,
                    "speaker": "A"
                },
                {
                    "text": "###",
                    "start": 15838,
                    "end": 16006,
                    "confidence": 0.69774,
                    "speaker": "A"
                },
                {
                    "text": "####",
                    "start": 16018,
                    "end": 16366,
                    "confidence": 0.41543,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 16438,
                    "end": 16638,
                    "confidence": 0.9939,
                    "speaker": "A"
                },
                {
                    "text": "#######",
                    "start": 16664,
                    "end": 17074,
                    "confidence": 0.92238,
                    "speaker": "A"
                },
                {
                    "text": "#####",
                    "start": 17122,
                    "end": 17662,
                    "confidence": 0.4407,
                    "speaker": "A"
                },
                {
                    "text": "#########,",
                    "start": 17686,
                    "end": 18550,
                    "confidence": 0.11477,
                    "speaker": "A"
                },
                {
                    "text": "both",
                    "start": 18670,
                    "end": 18990,
                    "confidence": 0.99976,
                    "speaker": "A"
                },
                {
                    "text": "primarily",
                    "start": 19040,
                    "end": 19474,
                    "confidence": 0.99972,
                    "speaker": "A"
                },
                {
                    "text": "based",
                    "start": 19522,
                    "end": 19698,
                    "confidence": 0.99975,
                    "speaker": "A"
                },
                {
                    "text": "at",
                    "start": 19724,
                    "end": 19842,
                    "confidence": 0.69104,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 19856,
                    "end": 19962,
                    "confidence": 0.99785,
                    "speaker": "A"
                },
                {
                    "text": "##########",
                    "start": 19976,
                    "end": 20398,
                    "confidence": 0.99957,
                    "speaker": "A"
                },
                {
                    "text": "##",
                    "start": 20434,
                    "end": 20598,
                    "confidence": 0.95252,
                    "speaker": "A"
                },
                {
                    "text": "#########.",
                    "start": 20624,
                    "end": 21526,
                    "confidence": 0.99606,
                    "speaker": "A"
                },
                {
                    "text": "#######",
                    "start": 21718,
                    "end": 22342,
                    "confidence": 0.25135,
                    "speaker": "A"
                },
                {
                    "text": "is",
                    "start": 22366,
                    "end": 22482,
                    "confidence": 0.99689,
                    "speaker": "A"
                },
                {
                    "text": "a",
                    "start": 22496,
                    "end": 22602,
                    "confidence": 0.9897,
                    "speaker": "A"
                },
                {
                    "text": "#########",
                    "start": 22616,
                    "end": 22942,
                    "confidence": 0.99708,
                    "speaker": "A"
                },
                {
                    "text": "of",
                    "start": 22966,
                    "end": 23118,
                    "confidence": 0.98527,
                    "speaker": "A"
                },
                {
                    "text": "#######",
                    "start": 23144,
                    "end": 23494,
                    "confidence": 0.998,
                    "speaker": "A"
                },
                {
                    "text": "########,",
                    "start": 23542,
                    "end": 24006,
                    "confidence": 0.99986,
                    "speaker": "A"
                },
                {
                    "text": "##########",
                    "start": 24128,
                    "end": 24754,
                    "confidence": 0.52405,
                    "speaker": "A"
                },
                {
                    "text": "############,",
                    "start": 24802,
                    "end": 25354,
                    "confidence": 0.99944,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 25402,
                    "end": 25578,
                    "confidence": 0.97893,
                    "speaker": "A"
                },
                {
                    "text": "########,",
                    "start": 25604,
                    "end": 26254,
                    "confidence": 0.99944,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 26362,
                    "end": 26598,
                    "confidence": 0.6844,
                    "speaker": "A"
                },
                {
                    "text": "#######",
                    "start": 26624,
                    "end": 26998,
                    "confidence": 0.56444,
                    "speaker": "A"
                },
                {
                    "text": "is",
                    "start": 27034,
                    "end": 27198,
                    "confidence": 0.65183,
                    "speaker": "A"
                },
                {
                    "text": "both",
                    "start": 27224,
                    "end": 27414,
                    "confidence": 0.98485,
                    "speaker": "A"
                },
                {
                    "text": "a",
                    "start": 27452,
                    "end": 27582,
                    "confidence": 0.97713,
                    "speaker": "A"
                },
                {
                    "text": "######",
                    "start": 27596,
                    "end": 27826,
                    "confidence": 0.9837,
                    "speaker": "A"
                },
                {
                    "text": "in",
                    "start": 27838,
                    "end": 27942,
                    "confidence": 0.97799,
                    "speaker": "A"
                },
                {
                    "text": "#########",
                    "start": 27956,
                    "end": 28534,
                    "confidence": 0.16386,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 28582,
                    "end": 28758,
                    "confidence": 0.99349,
                    "speaker": "A"
                },
                {
                    "text": "a",
                    "start": 28784,
                    "end": 28902,
                    "confidence": 0.94049,
                    "speaker": "A"
                },
                {
                    "text": "##########",
                    "start": 28916,
                    "end": 29362,
                    "confidence": 0.99703,
                    "speaker": "A"
                },
                {
                    "text": "###########",
                    "start": 29386,
                    "end": 30106,
                    "confidence": 0.09974,
                    "speaker": "A"
                },
                {
                    "text": "specializing",
                    "start": 30178,
                    "end": 30802,
                    "confidence": 0.97327,
                    "speaker": "A"
                },
                {
                    "text": "in",
                    "start": 30826,
                    "end": 30978,
                    "confidence": 0.76944,
                    "speaker": "A"
                },
                {
                    "text": "########",
                    "start": 31004,
                    "end": 31378,
                    "confidence": 0.99981,
                    "speaker": "A"
                },
                {
                    "text": "######.",
                    "start": 31414,
                    "end": 32194,
                    "confidence": 0.61877,
                    "speaker": "A"
                },
                {
                    "text": "So",
                    "start": 32362,
                    "end": 32658,
                    "confidence": 0.79018,
                    "speaker": "A"
                },
                {
                    "text": "just",
                    "start": 32684,
                    "end": 32838,
                    "confidence": 0.99276,
                    "speaker": "A"
                },
                {
                    "text": "kind",
                    "start": 32864,
                    "end": 32982,
                    "confidence": 0.83375,
                    "speaker": "A"
                },
                {
                    "text": "of",
                    "start": 32996,
                    "end": 33066,
                    "confidence": 0.99448,
                    "speaker": "A"
                },
                {
                    "text": "going",
                    "start": 33068,
                    "end": 33198,
                    "confidence": 0.99269,
                    "speaker": "A"
                },
                {
                    "text": "back",
                    "start": 33224,
                    "end": 33378,
                    "confidence": 0.84063,
                    "speaker": "A"
                },
                {
                    "text": "to",
                    "start": 33404,
                    "end": 33558,
                    "confidence": 0.99639,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 33584,
                    "end": 33738,
                    "confidence": 0.99787,
                    "speaker": "A"
                },
                {
                    "text": "start,",
                    "start": 33764,
                    "end": 34242,
                    "confidence": 0.99786,
                    "speaker": "A"
                },
                {
                    "text": "can",
                    "start": 34376,
                    "end": 34638,
                    "confidence": 0.99794,
                    "speaker": "A"
                },
                {
                    "text": "you",
                    "start": 34664,
                    "end": 34782,
                    "confidence": 0.99931,
                    "speaker": "A"
                },
                {
                    "text": "give",
                    "start": 34796,
                    "end": 34902,
                    "confidence": 0.99886,
                    "speaker": "A"
                },
                {
                    "text": "us",
                    "start": 34916,
                    "end": 35058,
                    "confidence": 0.99877,
                    "speaker": "A"
                },
                {
                    "text": "a",
                    "start": 35084,
                    "end": 35166,
                    "confidence": 0.99841,
                    "speaker": "A"
                },
                {
                    "text": "bit",
                    "start": 35168,
                    "end": 35298,
                    "confidence": 0.95783,
                    "speaker": "A"
                },
                {
                    "text": "of",
                    "start": 35324,
                    "end": 35442,
                    "confidence": 0.99777,
                    "speaker": "A"
                },
                {
                    "text": "background",
                    "start": 35456,
                    "end": 35950,
                    "confidence": 0.80026,
                    "speaker": "A"
                },
                {
                    "text": "on",
                    "start": 36010,
                    "end": 36234,
                    "confidence": 0.99915,
                    "speaker": "A"
                },
                {
                    "text": "this",
                    "start": 36272,
                    "end": 36474,
                    "confidence": 0.99904,
                    "speaker": "A"
                },
                {
                    "text": "particular",
                    "start": 36512,
                    "end": 36894,
                    "confidence": 0.99998,
                    "speaker": "A"
                },
                {
                    "text": "study",
                    "start": 36992,
                    "end": 37362,
                    "confidence": 0.59012,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 37436,
                    "end": 37674,
                    "confidence": 0.93494,
                    "speaker": "A"
                },
                {
                    "text": "what",
                    "start": 37712,
                    "end": 37950,
                    "confidence": 0.80508,
                    "speaker": "A"
                },
                {
                    "text": "perhaps",
                    "start": 38000,
                    "end": 38302,
                    "confidence": 0.98829,
                    "speaker": "A"
                },
                {
                    "text": "was",
                    "start": 38326,
                    "end": 38478,
                    "confidence": 0.9743,
                    "speaker": "A"
                },
                {
                    "text": "your",
                    "start": 38504,
                    "end": 38658,
                    "confidence": 0.99562,
                    "speaker": "A"
                },
                {
                    "text": "inspiration",
                    "start": 38684,
                    "end": 39190,
                    "confidence": 0.5765,
                    "speaker": "A"
                },
                {
                    "text": "for",
                    "start": 39250,
                    "end": 39510,
                    "confidence": 0.49729,
                    "speaker": "A"
                },
                {
                    "text": "starting",
                    "start": 39560,
                    "end": 39954,
                    "confidence": 0.99665,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 40052,
                    "end": 40422,
                    "confidence": 0.9322,
                    "speaker": "A"
                },
                {
                    "text": "what",
                    "start": 40496,
                    "end": 40734,
                    "confidence": 0.99932,
                    "speaker": "A"
                },
                {
                    "text": "clinical",
                    "start": 40772,
                    "end": 41170,
                    "confidence": 0.94514,
                    "speaker": "A"
                },
                {
                    "text": "problem",
                    "start": 41230,
                    "end": 41490,
                    "confidence": 0.98475,
                    "speaker": "A"
                },
                {
                    "text": "were",
                    "start": 41540,
                    "end": 41682,
                    "confidence": 0.93682,
                    "speaker": "A"
                },
                {
                    "text": "you",
                    "start": 41696,
                    "end": 41874,
                    "confidence": 0.58788,
                    "speaker": "A"
                },
                {
                    "text": "hoping",
                    "start": 41912,
                    "end": 42202,
                    "confidence": 0.99667,
                    "speaker": "A"
                },
                {
                    "text": "to",
                    "start": 42226,
                    "end": 42378,
                    "confidence": 0.99877,
                    "speaker": "A"
                },
                {
                    "text": "address?",
                    "start": 42404,
                    "end": 42980,
                    "confidence": 0.99924,
                    "speaker": "A"
                }
            ]
        },
        {
            "confidence": 0.8758847643979057,
            "end": 154698,
            "speaker": "B",
            "start": 44090,
            "text": "######## ###### is extremely common, as we all know, and the numbers are increasing. And it's a funny disease because on the one hand, we know that it kills many men, but on the other hand, we also know that many men will have it and live with it and may not even know about. So trying to decide if you find a ######## ######, whether you need to treat it or not, is actually quite a complex problem. For many years, the mantra has been if you find a ######## ######, you must treat it. But multiple studies have shown that actually survival may not be different from doing nothing to even doing something as radical as #######. So the question that we had in our group was really, how do you make that difference? And how do you actually find out which of the men would benefit and which would not? And of course, this is not new. There's been many pieces of work around that, but they tend to be fairly small, ad hoc with rather poor endpoints. And most importantly, very few of these things were being used in a standard approach. And even now, every country has its own particular set of guidelines, rules. And ultimately, the final arbitrator is who the ######### is when they see a patient and how they conveyed an information. So our primary interests driving the development of use from elastic tools was not only how do you bring together all the known variables, but also how do you transmit that into a way that ######### and a patient can understand and how that information can be given in a standardized way. So it doesn't really matter where being is diagnosed in the world. Potentially they will get the same information, same guidance. So that was the underpinning reason for looking at this, and there are different ways of doing it. One of them is tiered, which means you put people into brackets of groups and you look at how the groups perform. But of particular interest to us was how you get into a personalized level. And how do we actually use this new emerging signs of ########## ############ and ####### ######## to the application? And that's where we came from.",
            "words": [
                {
                    "text": "########",
                    "start": 44090,
                    "end": 44638,
                    "confidence": 0.99585,
                    "speaker": "B"
                },
                {
                    "text": "######",
                    "start": 44674,
                    "end": 44962,
                    "confidence": 0.69254,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 44986,
                    "end": 45174,
                    "confidence": 0.65663,
                    "speaker": "B"
                },
                {
                    "text": "extremely",
                    "start": 45212,
                    "end": 45730,
                    "confidence": 0.99876,
                    "speaker": "B"
                },
                {
                    "text": "common,",
                    "start": 45790,
                    "end": 46050,
                    "confidence": 0.99955,
                    "speaker": "B"
                },
                {
                    "text": "as",
                    "start": 46100,
                    "end": 46350,
                    "confidence": 0.42912,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 46400,
                    "end": 46578,
                    "confidence": 0.98998,
                    "speaker": "B"
                },
                {
                    "text": "all",
                    "start": 46604,
                    "end": 46758,
                    "confidence": 0.99138,
                    "speaker": "B"
                },
                {
                    "text": "know,",
                    "start": 46784,
                    "end": 47190,
                    "confidence": 0.56269,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 47300,
                    "end": 47682,
                    "confidence": 0.95863,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 47756,
                    "end": 47958,
                    "confidence": 0.97506,
                    "speaker": "B"
                },
                {
                    "text": "numbers",
                    "start": 47984,
                    "end": 48210,
                    "confidence": 0.99576,
                    "speaker": "B"
                },
                {
                    "text": "are",
                    "start": 48260,
                    "end": 48402,
                    "confidence": 0.99073,
                    "speaker": "B"
                },
                {
                    "text": "increasing.",
                    "start": 48416,
                    "end": 49258,
                    "confidence": 0.99153,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 49414,
                    "end": 49698,
                    "confidence": 0.79031,
                    "speaker": "B"
                },
                {
                    "text": "it's",
                    "start": 49724,
                    "end": 49882,
                    "confidence": 0.94509,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 49906,
                    "end": 49986,
                    "confidence": 0.94421,
                    "speaker": "B"
                },
                {
                    "text": "funny",
                    "start": 49988,
                    "end": 50182,
                    "confidence": 0.99352,
                    "speaker": "B"
                },
                {
                    "text": "disease",
                    "start": 50206,
                    "end": 50614,
                    "confidence": 0.84462,
                    "speaker": "B"
                },
                {
                    "text": "because",
                    "start": 50662,
                    "end": 50982,
                    "confidence": 0.99765,
                    "speaker": "B"
                },
                {
                    "text": "on",
                    "start": 51056,
                    "end": 51222,
                    "confidence": 0.96823,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 51236,
                    "end": 51342,
                    "confidence": 0.98238,
                    "speaker": "B"
                },
                {
                    "text": "one",
                    "start": 51356,
                    "end": 51534,
                    "confidence": 0.66007,
                    "speaker": "B"
                },
                {
                    "text": "hand,",
                    "start": 51572,
                    "end": 51774,
                    "confidence": 0.99806,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 51812,
                    "end": 52014,
                    "confidence": 0.9552,
                    "speaker": "B"
                },
                {
                    "text": "know",
                    "start": 52052,
                    "end": 52254,
                    "confidence": 0.99938,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 52292,
                    "end": 52458,
                    "confidence": 0.99171,
                    "speaker": "B"
                },
                {
                    "text": "it",
                    "start": 52484,
                    "end": 52638,
                    "confidence": 0.9928,
                    "speaker": "B"
                },
                {
                    "text": "kills",
                    "start": 52664,
                    "end": 52978,
                    "confidence": 0.98864,
                    "speaker": "B"
                },
                {
                    "text": "many",
                    "start": 53014,
                    "end": 53502,
                    "confidence": 0.9173,
                    "speaker": "B"
                },
                {
                    "text": "men,",
                    "start": 53636,
                    "end": 54006,
                    "confidence": 0.9977,
                    "speaker": "B"
                },
                {
                    "text": "but",
                    "start": 54068,
                    "end": 54294,
                    "confidence": 0.91221,
                    "speaker": "B"
                },
                {
                    "text": "on",
                    "start": 54332,
                    "end": 54462,
                    "confidence": 0.99067,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 54476,
                    "end": 54582,
                    "confidence": 0.99963,
                    "speaker": "B"
                },
                {
                    "text": "other",
                    "start": 54596,
                    "end": 54702,
                    "confidence": 0.99939,
                    "speaker": "B"
                },
                {
                    "text": "hand,",
                    "start": 54716,
                    "end": 54858,
                    "confidence": 0.84091,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 54884,
                    "end": 55038,
                    "confidence": 0.95993,
                    "speaker": "B"
                },
                {
                    "text": "also",
                    "start": 55064,
                    "end": 55218,
                    "confidence": 0.98112,
                    "speaker": "B"
                },
                {
                    "text": "know",
                    "start": 55244,
                    "end": 55362,
                    "confidence": 0.64513,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 55376,
                    "end": 55518,
                    "confidence": 0.94955,
                    "speaker": "B"
                },
                {
                    "text": "many",
                    "start": 55544,
                    "end": 55734,
                    "confidence": 0.99908,
                    "speaker": "B"
                },
                {
                    "text": "men",
                    "start": 55772,
                    "end": 56190,
                    "confidence": 0.9944,
                    "speaker": "B"
                },
                {
                    "text": "will",
                    "start": 56300,
                    "end": 56574,
                    "confidence": 0.99802,
                    "speaker": "B"
                },
                {
                    "text": "have",
                    "start": 56612,
                    "end": 56814,
                    "confidence": 0.99852,
                    "speaker": "B"
                },
                {
                    "text": "it",
                    "start": 56852,
                    "end": 57018,
                    "confidence": 0.97714,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 57044,
                    "end": 57198,
                    "confidence": 0.98349,
                    "speaker": "B"
                },
                {
                    "text": "live",
                    "start": 57224,
                    "end": 57378,
                    "confidence": 0.993,
                    "speaker": "B"
                },
                {
                    "text": "with",
                    "start": 57404,
                    "end": 57522,
                    "confidence": 0.98621,
                    "speaker": "B"
                },
                {
                    "text": "it",
                    "start": 57536,
                    "end": 57642,
                    "confidence": 0.97518,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 57656,
                    "end": 57762,
                    "confidence": 0.50467,
                    "speaker": "B"
                },
                {
                    "text": "may",
                    "start": 57776,
                    "end": 57882,
                    "confidence": 0.98656,
                    "speaker": "B"
                },
                {
                    "text": "not",
                    "start": 57896,
                    "end": 58038,
                    "confidence": 0.9894,
                    "speaker": "B"
                },
                {
                    "text": "even",
                    "start": 58064,
                    "end": 58254,
                    "confidence": 0.99815,
                    "speaker": "B"
                },
                {
                    "text": "know",
                    "start": 58292,
                    "end": 58458,
                    "confidence": 0.99967,
                    "speaker": "B"
                },
                {
                    "text": "about.",
                    "start": 58484,
                    "end": 58998,
                    "confidence": 0.97831,
                    "speaker": "B"
                },
                {
                    "text": "So",
                    "start": 59144,
                    "end": 59454,
                    "confidence": 0.82462,
                    "speaker": "B"
                },
                {
                    "text": "trying",
                    "start": 59492,
                    "end": 59658,
                    "confidence": 0.96603,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 59684,
                    "end": 59838,
                    "confidence": 0.96831,
                    "speaker": "B"
                },
                {
                    "text": "decide",
                    "start": 59864,
                    "end": 60250,
                    "confidence": 0.99959,
                    "speaker": "B"
                },
                {
                    "text": "if",
                    "start": 60310,
                    "end": 60498,
                    "confidence": 0.98573,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 60524,
                    "end": 60642,
                    "confidence": 0.99822,
                    "speaker": "B"
                },
                {
                    "text": "find",
                    "start": 60656,
                    "end": 60798,
                    "confidence": 0.99774,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 60824,
                    "end": 60906,
                    "confidence": 0.86905,
                    "speaker": "B"
                },
                {
                    "text": "########",
                    "start": 60908,
                    "end": 61294,
                    "confidence": 0.69493,
                    "speaker": "B"
                },
                {
                    "text": "######,",
                    "start": 61342,
                    "end": 61942,
                    "confidence": 0.7371,
                    "speaker": "B"
                },
                {
                    "text": "whether",
                    "start": 62026,
                    "end": 62274,
                    "confidence": 0.99939,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 62312,
                    "end": 62442,
                    "confidence": 0.99962,
                    "speaker": "B"
                },
                {
                    "text": "need",
                    "start": 62456,
                    "end": 62562,
                    "confidence": 0.99956,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 62576,
                    "end": 62682,
                    "confidence": 0.99816,
                    "speaker": "B"
                },
                {
                    "text": "treat",
                    "start": 62696,
                    "end": 62838,
                    "confidence": 0.99915,
                    "speaker": "B"
                },
                {
                    "text": "it",
                    "start": 62864,
                    "end": 62982,
                    "confidence": 0.99415,
                    "speaker": "B"
                },
                {
                    "text": "or",
                    "start": 62996,
                    "end": 63102,
                    "confidence": 0.74196,
                    "speaker": "B"
                },
                {
                    "text": "not,",
                    "start": 63116,
                    "end": 63258,
                    "confidence": 0.99891,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 63284,
                    "end": 63438,
                    "confidence": 0.53984,
                    "speaker": "B"
                },
                {
                    "text": "actually",
                    "start": 63464,
                    "end": 63654,
                    "confidence": 0.99312,
                    "speaker": "B"
                },
                {
                    "text": "quite",
                    "start": 63692,
                    "end": 63858,
                    "confidence": 0.9998,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 63884,
                    "end": 64002,
                    "confidence": 0.98488,
                    "speaker": "B"
                },
                {
                    "text": "complex",
                    "start": 64016,
                    "end": 64498,
                    "confidence": 0.991,
                    "speaker": "B"
                },
                {
                    "text": "problem.",
                    "start": 64594,
                    "end": 65178,
                    "confidence": 0.99931,
                    "speaker": "B"
                },
                {
                    "text": "For",
                    "start": 65324,
                    "end": 65742,
                    "confidence": 0.56779,
                    "speaker": "B"
                },
                {
                    "text": "many",
                    "start": 65816,
                    "end": 66054,
                    "confidence": 0.99938,
                    "speaker": "B"
                },
                {
                    "text": "years,",
                    "start": 66092,
                    "end": 66330,
                    "confidence": 0.99985,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 66380,
                    "end": 66522,
                    "confidence": 0.94551,
                    "speaker": "B"
                },
                {
                    "text": "mantra",
                    "start": 66536,
                    "end": 66898,
                    "confidence": 0.76456,
                    "speaker": "B"
                },
                {
                    "text": "has",
                    "start": 66934,
                    "end": 67134,
                    "confidence": 0.71033,
                    "speaker": "B"
                },
                {
                    "text": "been",
                    "start": 67172,
                    "end": 67374,
                    "confidence": 0.8437,
                    "speaker": "B"
                },
                {
                    "text": "if",
                    "start": 67412,
                    "end": 67578,
                    "confidence": 0.89664,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 67604,
                    "end": 67722,
                    "confidence": 0.99495,
                    "speaker": "B"
                },
                {
                    "text": "find",
                    "start": 67736,
                    "end": 67878,
                    "confidence": 0.99719,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 67904,
                    "end": 68022,
                    "confidence": 0.41763,
                    "speaker": "B"
                },
                {
                    "text": "########",
                    "start": 68036,
                    "end": 68398,
                    "confidence": 0.88151,
                    "speaker": "B"
                },
                {
                    "text": "######,",
                    "start": 68434,
                    "end": 68782,
                    "confidence": 0.92248,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 68806,
                    "end": 68922,
                    "confidence": 0.99819,
                    "speaker": "B"
                },
                {
                    "text": "must",
                    "start": 68936,
                    "end": 69114,
                    "confidence": 0.50989,
                    "speaker": "B"
                },
                {
                    "text": "treat",
                    "start": 69152,
                    "end": 69354,
                    "confidence": 0.85312,
                    "speaker": "B"
                },
                {
                    "text": "it.",
                    "start": 69392,
                    "end": 69558,
                    "confidence": 0.99584,
                    "speaker": "B"
                },
                {
                    "text": "But",
                    "start": 69584,
                    "end": 69774,
                    "confidence": 0.98758,
                    "speaker": "B"
                },
                {
                    "text": "multiple",
                    "start": 69812,
                    "end": 70306,
                    "confidence": 0.89287,
                    "speaker": "B"
                },
                {
                    "text": "studies",
                    "start": 70378,
                    "end": 70774,
                    "confidence": 0.8489,
                    "speaker": "B"
                },
                {
                    "text": "have",
                    "start": 70822,
                    "end": 70998,
                    "confidence": 0.99575,
                    "speaker": "B"
                },
                {
                    "text": "shown",
                    "start": 71024,
                    "end": 71314,
                    "confidence": 0.98606,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 71362,
                    "end": 71538,
                    "confidence": 0.99911,
                    "speaker": "B"
                },
                {
                    "text": "actually",
                    "start": 71564,
                    "end": 71826,
                    "confidence": 0.98761,
                    "speaker": "B"
                },
                {
                    "text": "survival",
                    "start": 71888,
                    "end": 72382,
                    "confidence": 0.90804,
                    "speaker": "B"
                },
                {
                    "text": "may",
                    "start": 72406,
                    "end": 72558,
                    "confidence": 0.81822,
                    "speaker": "B"
                },
                {
                    "text": "not",
                    "start": 72584,
                    "end": 72738,
                    "confidence": 0.99915,
                    "speaker": "B"
                },
                {
                    "text": "be",
                    "start": 72764,
                    "end": 72882,
                    "confidence": 0.9989,
                    "speaker": "B"
                },
                {
                    "text": "different",
                    "start": 72896,
                    "end": 73146,
                    "confidence": 0.99662,
                    "speaker": "B"
                },
                {
                    "text": "from",
                    "start": 73208,
                    "end": 73506,
                    "confidence": 0.9979,
                    "speaker": "B"
                },
                {
                    "text": "doing",
                    "start": 73568,
                    "end": 73830,
                    "confidence": 0.99847,
                    "speaker": "B"
                },
                {
                    "text": "nothing",
                    "start": 73880,
                    "end": 74238,
                    "confidence": 0.99951,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 74324,
                    "end": 74538,
                    "confidence": 0.97076,
                    "speaker": "B"
                },
                {
                    "text": "even",
                    "start": 74564,
                    "end": 74862,
                    "confidence": 0.99906,
                    "speaker": "B"
                },
                {
                    "text": "doing",
                    "start": 74936,
                    "end": 75138,
                    "confidence": 0.69695,
                    "speaker": "B"
                },
                {
                    "text": "something",
                    "start": 75164,
                    "end": 75390,
                    "confidence": 0.84118,
                    "speaker": "B"
                },
                {
                    "text": "as",
                    "start": 75440,
                    "end": 75618,
                    "confidence": 0.97994,
                    "speaker": "B"
                },
                {
                    "text": "radical",
                    "start": 75644,
                    "end": 75994,
                    "confidence": 0.39609,
                    "speaker": "B"
                },
                {
                    "text": "as",
                    "start": 76042,
                    "end": 76254,
                    "confidence": 0.66891,
                    "speaker": "B"
                },
                {
                    "text": "#######.",
                    "start": 76292,
                    "end": 76870,
                    "confidence": 0.59162,
                    "speaker": "B"
                },
                {
                    "text": "So",
                    "start": 76990,
                    "end": 77346,
                    "confidence": 0.74897,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 77408,
                    "end": 77598,
                    "confidence": 0.99929,
                    "speaker": "B"
                },
                {
                    "text": "question",
                    "start": 77624,
                    "end": 77886,
                    "confidence": 0.99615,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 77948,
                    "end": 78282,
                    "confidence": 0.99287,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 78356,
                    "end": 78594,
                    "confidence": 0.9982,
                    "speaker": "B"
                },
                {
                    "text": "had",
                    "start": 78632,
                    "end": 79122,
                    "confidence": 0.98725,
                    "speaker": "B"
                },
                {
                    "text": "in",
                    "start": 79256,
                    "end": 79482,
                    "confidence": 0.96567,
                    "speaker": "B"
                },
                {
                    "text": "our",
                    "start": 79496,
                    "end": 79638,
                    "confidence": 0.97244,
                    "speaker": "B"
                },
                {
                    "text": "group",
                    "start": 79664,
                    "end": 79854,
                    "confidence": 0.99694,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 79892,
                    "end": 80094,
                    "confidence": 0.92031,
                    "speaker": "B"
                },
                {
                    "text": "really,",
                    "start": 80132,
                    "end": 80406,
                    "confidence": 0.96512,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 80468,
                    "end": 80658,
                    "confidence": 0.99967,
                    "speaker": "B"
                },
                {
                    "text": "do",
                    "start": 80684,
                    "end": 80802,
                    "confidence": 0.99729,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 80816,
                    "end": 81174,
                    "confidence": 0.97915,
                    "speaker": "B"
                },
                {
                    "text": "make",
                    "start": 81272,
                    "end": 81534,
                    "confidence": 0.99958,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 81572,
                    "end": 81846,
                    "confidence": 0.99013,
                    "speaker": "B"
                },
                {
                    "text": "difference?",
                    "start": 81908,
                    "end": 82314,
                    "confidence": 0.99507,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 82412,
                    "end": 82602,
                    "confidence": 0.62417,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 82616,
                    "end": 82758,
                    "confidence": 0.99959,
                    "speaker": "B"
                },
                {
                    "text": "do",
                    "start": 82784,
                    "end": 82902,
                    "confidence": 0.99443,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 82916,
                    "end": 83058,
                    "confidence": 0.99366,
                    "speaker": "B"
                },
                {
                    "text": "actually",
                    "start": 83084,
                    "end": 83346,
                    "confidence": 0.997,
                    "speaker": "B"
                },
                {
                    "text": "find",
                    "start": 83408,
                    "end": 83670,
                    "confidence": 0.99965,
                    "speaker": "B"
                },
                {
                    "text": "out",
                    "start": 83720,
                    "end": 84222,
                    "confidence": 0.99933,
                    "speaker": "B"
                },
                {
                    "text": "which",
                    "start": 84356,
                    "end": 84654,
                    "confidence": 0.99812,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 84692,
                    "end": 84786,
                    "confidence": 0.42045,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 84788,
                    "end": 84882,
                    "confidence": 0.96712,
                    "speaker": "B"
                },
                {
                    "text": "men",
                    "start": 84896,
                    "end": 85110,
                    "confidence": 0.85295,
                    "speaker": "B"
                },
                {
                    "text": "would",
                    "start": 85160,
                    "end": 85374,
                    "confidence": 0.94547,
                    "speaker": "B"
                },
                {
                    "text": "benefit",
                    "start": 85412,
                    "end": 85774,
                    "confidence": 0.66487,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 85822,
                    "end": 85962,
                    "confidence": 0.77391,
                    "speaker": "B"
                },
                {
                    "text": "which",
                    "start": 85976,
                    "end": 86226,
                    "confidence": 0.92022,
                    "speaker": "B"
                },
                {
                    "text": "would",
                    "start": 86288,
                    "end": 86478,
                    "confidence": 0.86635,
                    "speaker": "B"
                },
                {
                    "text": "not?",
                    "start": 86504,
                    "end": 86802,
                    "confidence": 0.98736,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 86876,
                    "end": 87078,
                    "confidence": 0.85352,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 87104,
                    "end": 87186,
                    "confidence": 0.99685,
                    "speaker": "B"
                },
                {
                    "text": "course,",
                    "start": 87188,
                    "end": 87354,
                    "confidence": 0.99276,
                    "speaker": "B"
                },
                {
                    "text": "this",
                    "start": 87392,
                    "end": 87522,
                    "confidence": 0.83639,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 87536,
                    "end": 87642,
                    "confidence": 0.98359,
                    "speaker": "B"
                },
                {
                    "text": "not",
                    "start": 87656,
                    "end": 87834,
                    "confidence": 0.9902,
                    "speaker": "B"
                },
                {
                    "text": "new.",
                    "start": 87872,
                    "end": 88146,
                    "confidence": 0.98386,
                    "speaker": "B"
                },
                {
                    "text": "There's",
                    "start": 88208,
                    "end": 88426,
                    "confidence": 0.83015,
                    "speaker": "B"
                },
                {
                    "text": "been",
                    "start": 88438,
                    "end": 88614,
                    "confidence": 0.87494,
                    "speaker": "B"
                },
                {
                    "text": "many",
                    "start": 88652,
                    "end": 89286,
                    "confidence": 0.90954,
                    "speaker": "B"
                },
                {
                    "text": "pieces",
                    "start": 89468,
                    "end": 89866,
                    "confidence": 0.30342,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 89878,
                    "end": 90018,
                    "confidence": 0.70134,
                    "speaker": "B"
                },
                {
                    "text": "work",
                    "start": 90044,
                    "end": 90198,
                    "confidence": 0.99412,
                    "speaker": "B"
                },
                {
                    "text": "around",
                    "start": 90224,
                    "end": 90486,
                    "confidence": 0.92584,
                    "speaker": "B"
                },
                {
                    "text": "that,",
                    "start": 90548,
                    "end": 90738,
                    "confidence": 0.98998,
                    "speaker": "B"
                },
                {
                    "text": "but",
                    "start": 90764,
                    "end": 91278,
                    "confidence": 0.93771,
                    "speaker": "B"
                },
                {
                    "text": "they",
                    "start": 91424,
                    "end": 91662,
                    "confidence": 0.97281,
                    "speaker": "B"
                },
                {
                    "text": "tend",
                    "start": 91676,
                    "end": 91918,
                    "confidence": 0.18487,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 91954,
                    "end": 92082,
                    "confidence": 0.99975,
                    "speaker": "B"
                },
                {
                    "text": "be",
                    "start": 92096,
                    "end": 92238,
                    "confidence": 0.99899,
                    "speaker": "B"
                },
                {
                    "text": "fairly",
                    "start": 92264,
                    "end": 92554,
                    "confidence": 0.98818,
                    "speaker": "B"
                },
                {
                    "text": "small,",
                    "start": 92602,
                    "end": 92922,
                    "confidence": 0.99978,
                    "speaker": "B"
                },
                {
                    "text": "ad",
                    "start": 92996,
                    "end": 93162,
                    "confidence": 0.51455,
                    "speaker": "B"
                },
                {
                    "text": "hoc",
                    "start": 93176,
                    "end": 93514,
                    "confidence": 0.76248,
                    "speaker": "B"
                },
                {
                    "text": "with",
                    "start": 93562,
                    "end": 93774,
                    "confidence": 0.88954,
                    "speaker": "B"
                },
                {
                    "text": "rather",
                    "start": 93812,
                    "end": 94266,
                    "confidence": 0.39127,
                    "speaker": "B"
                },
                {
                    "text": "poor",
                    "start": 94388,
                    "end": 94714,
                    "confidence": 0.93676,
                    "speaker": "B"
                },
                {
                    "text": "endpoints.",
                    "start": 94762,
                    "end": 95626,
                    "confidence": 0.39444,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 95758,
                    "end": 96054,
                    "confidence": 0.90897,
                    "speaker": "B"
                },
                {
                    "text": "most",
                    "start": 96092,
                    "end": 96294,
                    "confidence": 0.97456,
                    "speaker": "B"
                },
                {
                    "text": "importantly,",
                    "start": 96332,
                    "end": 97054,
                    "confidence": 0.98268,
                    "speaker": "B"
                },
                {
                    "text": "very",
                    "start": 97162,
                    "end": 97398,
                    "confidence": 0.99893,
                    "speaker": "B"
                },
                {
                    "text": "few",
                    "start": 97424,
                    "end": 97614,
                    "confidence": 0.68925,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 97652,
                    "end": 97782,
                    "confidence": 0.70525,
                    "speaker": "B"
                },
                {
                    "text": "these",
                    "start": 97796,
                    "end": 97938,
                    "confidence": 0.99234,
                    "speaker": "B"
                },
                {
                    "text": "things",
                    "start": 97964,
                    "end": 98154,
                    "confidence": 0.99596,
                    "speaker": "B"
                },
                {
                    "text": "were",
                    "start": 98192,
                    "end": 98358,
                    "confidence": 0.49604,
                    "speaker": "B"
                },
                {
                    "text": "being",
                    "start": 98384,
                    "end": 98574,
                    "confidence": 0.99693,
                    "speaker": "B"
                },
                {
                    "text": "used",
                    "start": 98612,
                    "end": 99102,
                    "confidence": 0.99842,
                    "speaker": "B"
                },
                {
                    "text": "in",
                    "start": 99236,
                    "end": 99462,
                    "confidence": 0.84754,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 99476,
                    "end": 99582,
                    "confidence": 0.86362,
                    "speaker": "B"
                },
                {
                    "text": "standard",
                    "start": 99596,
                    "end": 99882,
                    "confidence": 0.99897,
                    "speaker": "B"
                },
                {
                    "text": "approach.",
                    "start": 99956,
                    "end": 100294,
                    "confidence": 0.76385,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 100342,
                    "end": 100482,
                    "confidence": 0.8245,
                    "speaker": "B"
                },
                {
                    "text": "even",
                    "start": 100496,
                    "end": 100710,
                    "confidence": 0.98386,
                    "speaker": "B"
                },
                {
                    "text": "now,",
                    "start": 100760,
                    "end": 101118,
                    "confidence": 0.99735,
                    "speaker": "B"
                },
                {
                    "text": "every",
                    "start": 101204,
                    "end": 101490,
                    "confidence": 0.99871,
                    "speaker": "B"
                },
                {
                    "text": "country",
                    "start": 101540,
                    "end": 101790,
                    "confidence": 0.99483,
                    "speaker": "B"
                },
                {
                    "text": "has",
                    "start": 101840,
                    "end": 102054,
                    "confidence": 0.51626,
                    "speaker": "B"
                },
                {
                    "text": "its",
                    "start": 102092,
                    "end": 102222,
                    "confidence": 0.97009,
                    "speaker": "B"
                },
                {
                    "text": "own",
                    "start": 102236,
                    "end": 102414,
                    "confidence": 0.99979,
                    "speaker": "B"
                },
                {
                    "text": "particular",
                    "start": 102452,
                    "end": 102726,
                    "confidence": 0.98798,
                    "speaker": "B"
                },
                {
                    "text": "set",
                    "start": 102788,
                    "end": 103014,
                    "confidence": 0.53597,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 103052,
                    "end": 103218,
                    "confidence": 0.66545,
                    "speaker": "B"
                },
                {
                    "text": "guidelines,",
                    "start": 103244,
                    "end": 103738,
                    "confidence": 0.94603,
                    "speaker": "B"
                },
                {
                    "text": "rules.",
                    "start": 103774,
                    "end": 104062,
                    "confidence": 0.85345,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 104086,
                    "end": 104310,
                    "confidence": 0.76893,
                    "speaker": "B"
                },
                {
                    "text": "ultimately,",
                    "start": 104360,
                    "end": 104926,
                    "confidence": 0.81461,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 104998,
                    "end": 105126,
                    "confidence": 0.73132,
                    "speaker": "B"
                },
                {
                    "text": "final",
                    "start": 105128,
                    "end": 105330,
                    "confidence": 0.87228,
                    "speaker": "B"
                },
                {
                    "text": "arbitrator",
                    "start": 105380,
                    "end": 105958,
                    "confidence": 0.83315,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 105994,
                    "end": 106158,
                    "confidence": 0.93308,
                    "speaker": "B"
                },
                {
                    "text": "who",
                    "start": 106184,
                    "end": 106338,
                    "confidence": 0.99036,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 106364,
                    "end": 106482,
                    "confidence": 0.99346,
                    "speaker": "B"
                },
                {
                    "text": "#########",
                    "start": 106496,
                    "end": 106894,
                    "confidence": 0.69952,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 106942,
                    "end": 107262,
                    "confidence": 0.98618,
                    "speaker": "B"
                },
                {
                    "text": "when",
                    "start": 107336,
                    "end": 107502,
                    "confidence": 0.99532,
                    "speaker": "B"
                },
                {
                    "text": "they",
                    "start": 107516,
                    "end": 107622,
                    "confidence": 0.9735,
                    "speaker": "B"
                },
                {
                    "text": "see",
                    "start": 107636,
                    "end": 107778,
                    "confidence": 0.99952,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 107804,
                    "end": 107922,
                    "confidence": 0.98359,
                    "speaker": "B"
                },
                {
                    "text": "patient",
                    "start": 107936,
                    "end": 108430,
                    "confidence": 0.83709,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 108550,
                    "end": 108798,
                    "confidence": 0.98237,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 108824,
                    "end": 108978,
                    "confidence": 0.99934,
                    "speaker": "B"
                },
                {
                    "text": "they",
                    "start": 109004,
                    "end": 109158,
                    "confidence": 0.9658,
                    "speaker": "B"
                },
                {
                    "text": "conveyed",
                    "start": 109184,
                    "end": 109582,
                    "confidence": 0.37144,
                    "speaker": "B"
                },
                {
                    "text": "an",
                    "start": 109606,
                    "end": 109722,
                    "confidence": 0.67021,
                    "speaker": "B"
                },
                {
                    "text": "information.",
                    "start": 109736,
                    "end": 110300,
                    "confidence": 0.93526,
                    "speaker": "B"
                },
                {
                    "text": "So",
                    "start": 110630,
                    "end": 111174,
                    "confidence": 0.71397,
                    "speaker": "B"
                },
                {
                    "text": "our",
                    "start": 111272,
                    "end": 111570,
                    "confidence": 0.99315,
                    "speaker": "B"
                },
                {
                    "text": "primary",
                    "start": 111620,
                    "end": 111970,
                    "confidence": 0.69835,
                    "speaker": "B"
                },
                {
                    "text": "interests",
                    "start": 112030,
                    "end": 112630,
                    "confidence": 0.39886,
                    "speaker": "B"
                },
                {
                    "text": "driving",
                    "start": 112690,
                    "end": 113086,
                    "confidence": 0.97878,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 113158,
                    "end": 113322,
                    "confidence": 0.99064,
                    "speaker": "B"
                },
                {
                    "text": "development",
                    "start": 113336,
                    "end": 113722,
                    "confidence": 0.99458,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 113746,
                    "end": 113898,
                    "confidence": 0.85472,
                    "speaker": "B"
                },
                {
                    "text": "use",
                    "start": 113924,
                    "end": 114222,
                    "confidence": 0.02046,
                    "speaker": "B"
                },
                {
                    "text": "from",
                    "start": 114296,
                    "end": 114498,
                    "confidence": 0.66342,
                    "speaker": "B"
                },
                {
                    "text": "elastic",
                    "start": 114524,
                    "end": 114922,
                    "confidence": 0.56265,
                    "speaker": "B"
                },
                {
                    "text": "tools",
                    "start": 114946,
                    "end": 115282,
                    "confidence": 0.45971,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 115306,
                    "end": 115674,
                    "confidence": 0.78749,
                    "speaker": "B"
                },
                {
                    "text": "not",
                    "start": 115772,
                    "end": 115998,
                    "confidence": 0.9995,
                    "speaker": "B"
                },
                {
                    "text": "only",
                    "start": 116024,
                    "end": 116502,
                    "confidence": 0.99972,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 116636,
                    "end": 117114,
                    "confidence": 0.98873,
                    "speaker": "B"
                },
                {
                    "text": "do",
                    "start": 117212,
                    "end": 117402,
                    "confidence": 0.98219,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 117416,
                    "end": 117522,
                    "confidence": 0.98531,
                    "speaker": "B"
                },
                {
                    "text": "bring",
                    "start": 117536,
                    "end": 117714,
                    "confidence": 0.99895,
                    "speaker": "B"
                },
                {
                    "text": "together",
                    "start": 117752,
                    "end": 118026,
                    "confidence": 0.99943,
                    "speaker": "B"
                },
                {
                    "text": "all",
                    "start": 118088,
                    "end": 118242,
                    "confidence": 0.99488,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 118256,
                    "end": 118362,
                    "confidence": 0.98324,
                    "speaker": "B"
                },
                {
                    "text": "known",
                    "start": 118376,
                    "end": 118618,
                    "confidence": 0.96286,
                    "speaker": "B"
                },
                {
                    "text": "variables,",
                    "start": 118654,
                    "end": 119254,
                    "confidence": 0.45504,
                    "speaker": "B"
                },
                {
                    "text": "but",
                    "start": 119302,
                    "end": 119514,
                    "confidence": 0.98794,
                    "speaker": "B"
                },
                {
                    "text": "also",
                    "start": 119552,
                    "end": 119862,
                    "confidence": 0.9963,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 119936,
                    "end": 120066,
                    "confidence": 0.88983,
                    "speaker": "B"
                },
                {
                    "text": "do",
                    "start": 120068,
                    "end": 120162,
                    "confidence": 0.85492,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 120176,
                    "end": 120426,
                    "confidence": 0.90751,
                    "speaker": "B"
                },
                {
                    "text": "transmit",
                    "start": 120488,
                    "end": 121054,
                    "confidence": 0.51724,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 121102,
                    "end": 121278,
                    "confidence": 0.97755,
                    "speaker": "B"
                },
                {
                    "text": "into",
                    "start": 121304,
                    "end": 121530,
                    "confidence": 0.46188,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 121580,
                    "end": 121722,
                    "confidence": 0.88105,
                    "speaker": "B"
                },
                {
                    "text": "way",
                    "start": 121736,
                    "end": 121842,
                    "confidence": 0.99389,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 121856,
                    "end": 122106,
                    "confidence": 0.95127,
                    "speaker": "B"
                },
                {
                    "text": "#########",
                    "start": 122168,
                    "end": 122614,
                    "confidence": 0.54458,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 122662,
                    "end": 122838,
                    "confidence": 0.81585,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 122864,
                    "end": 122982,
                    "confidence": 0.82235,
                    "speaker": "B"
                },
                {
                    "text": "patient",
                    "start": 122996,
                    "end": 123274,
                    "confidence": 0.99574,
                    "speaker": "B"
                },
                {
                    "text": "can",
                    "start": 123322,
                    "end": 123498,
                    "confidence": 0.99865,
                    "speaker": "B"
                },
                {
                    "text": "understand",
                    "start": 123524,
                    "end": 124110,
                    "confidence": 0.99992,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 124280,
                    "end": 124578,
                    "confidence": 0.90357,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 124604,
                    "end": 124794,
                    "confidence": 0.99532,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 124832,
                    "end": 125034,
                    "confidence": 0.98757,
                    "speaker": "B"
                },
                {
                    "text": "information",
                    "start": 125072,
                    "end": 125418,
                    "confidence": 0.99919,
                    "speaker": "B"
                },
                {
                    "text": "can",
                    "start": 125504,
                    "end": 125682,
                    "confidence": 0.99919,
                    "speaker": "B"
                },
                {
                    "text": "be",
                    "start": 125696,
                    "end": 125838,
                    "confidence": 0.99915,
                    "speaker": "B"
                },
                {
                    "text": "given",
                    "start": 125864,
                    "end": 126054,
                    "confidence": 0.99986,
                    "speaker": "B"
                },
                {
                    "text": "in",
                    "start": 126092,
                    "end": 126222,
                    "confidence": 0.9852,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 126236,
                    "end": 126342,
                    "confidence": 0.96848,
                    "speaker": "B"
                },
                {
                    "text": "standardized",
                    "start": 126356,
                    "end": 126850,
                    "confidence": 0.97627,
                    "speaker": "B"
                },
                {
                    "text": "way.",
                    "start": 126910,
                    "end": 127098,
                    "confidence": 0.86281,
                    "speaker": "B"
                },
                {
                    "text": "So",
                    "start": 127124,
                    "end": 127242,
                    "confidence": 0.78259,
                    "speaker": "B"
                },
                {
                    "text": "it",
                    "start": 127256,
                    "end": 127362,
                    "confidence": 0.99091,
                    "speaker": "B"
                },
                {
                    "text": "doesn't",
                    "start": 127376,
                    "end": 127522,
                    "confidence": 0.99518,
                    "speaker": "B"
                },
                {
                    "text": "really",
                    "start": 127546,
                    "end": 127734,
                    "confidence": 0.9936,
                    "speaker": "B"
                },
                {
                    "text": "matter",
                    "start": 127772,
                    "end": 128046,
                    "confidence": 0.99998,
                    "speaker": "B"
                },
                {
                    "text": "where",
                    "start": 128108,
                    "end": 128442,
                    "confidence": 0.98532,
                    "speaker": "B"
                },
                {
                    "text": "being",
                    "start": 128516,
                    "end": 128718,
                    "confidence": 0.0198,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 128744,
                    "end": 128898,
                    "confidence": 0.08351,
                    "speaker": "B"
                },
                {
                    "text": "diagnosed",
                    "start": 128924,
                    "end": 129562,
                    "confidence": 0.63256,
                    "speaker": "B"
                },
                {
                    "text": "in",
                    "start": 129646,
                    "end": 129822,
                    "confidence": 0.90578,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 129836,
                    "end": 129978,
                    "confidence": 0.94984,
                    "speaker": "B"
                },
                {
                    "text": "world.",
                    "start": 130004,
                    "end": 130194,
                    "confidence": 0.99211,
                    "speaker": "B"
                },
                {
                    "text": "Potentially",
                    "start": 130232,
                    "end": 130786,
                    "confidence": 0.5739,
                    "speaker": "B"
                },
                {
                    "text": "they",
                    "start": 130918,
                    "end": 131142,
                    "confidence": 0.98286,
                    "speaker": "B"
                },
                {
                    "text": "will",
                    "start": 131156,
                    "end": 131298,
                    "confidence": 0.81773,
                    "speaker": "B"
                },
                {
                    "text": "get",
                    "start": 131324,
                    "end": 131442,
                    "confidence": 0.99815,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 131456,
                    "end": 131598,
                    "confidence": 0.99101,
                    "speaker": "B"
                },
                {
                    "text": "same",
                    "start": 131624,
                    "end": 131814,
                    "confidence": 0.99975,
                    "speaker": "B"
                },
                {
                    "text": "information,",
                    "start": 131852,
                    "end": 132306,
                    "confidence": 0.99964,
                    "speaker": "B"
                },
                {
                    "text": "same",
                    "start": 132428,
                    "end": 132714,
                    "confidence": 0.57847,
                    "speaker": "B"
                },
                {
                    "text": "guidance.",
                    "start": 132752,
                    "end": 133354,
                    "confidence": 0.8113,
                    "speaker": "B"
                },
                {
                    "text": "So",
                    "start": 133462,
                    "end": 133662,
                    "confidence": 0.77664,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 133676,
                    "end": 133818,
                    "confidence": 0.98528,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 133844,
                    "end": 134142,
                    "confidence": 0.99314,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 134216,
                    "end": 134418,
                    "confidence": 0.55147,
                    "speaker": "B"
                },
                {
                    "text": "underpinning",
                    "start": 134444,
                    "end": 134998,
                    "confidence": 0.85228,
                    "speaker": "B"
                },
                {
                    "text": "reason",
                    "start": 135034,
                    "end": 135378,
                    "confidence": 0.99952,
                    "speaker": "B"
                },
                {
                    "text": "for",
                    "start": 135464,
                    "end": 135678,
                    "confidence": 0.99536,
                    "speaker": "B"
                },
                {
                    "text": "looking",
                    "start": 135704,
                    "end": 135930,
                    "confidence": 0.99949,
                    "speaker": "B"
                },
                {
                    "text": "at",
                    "start": 135980,
                    "end": 136158,
                    "confidence": 0.99877,
                    "speaker": "B"
                },
                {
                    "text": "this,",
                    "start": 136184,
                    "end": 136626,
                    "confidence": 0.96023,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 136748,
                    "end": 137142,
                    "confidence": 0.74235,
                    "speaker": "B"
                },
                {
                    "text": "there",
                    "start": 137216,
                    "end": 137382,
                    "confidence": 0.99651,
                    "speaker": "B"
                },
                {
                    "text": "are",
                    "start": 137396,
                    "end": 137538,
                    "confidence": 0.73437,
                    "speaker": "B"
                },
                {
                    "text": "different",
                    "start": 137564,
                    "end": 137790,
                    "confidence": 0.99948,
                    "speaker": "B"
                },
                {
                    "text": "ways",
                    "start": 137840,
                    "end": 138018,
                    "confidence": 0.99959,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 138044,
                    "end": 138198,
                    "confidence": 0.93849,
                    "speaker": "B"
                },
                {
                    "text": "doing",
                    "start": 138224,
                    "end": 138378,
                    "confidence": 0.99997,
                    "speaker": "B"
                },
                {
                    "text": "it.",
                    "start": 138404,
                    "end": 138558,
                    "confidence": 0.96951,
                    "speaker": "B"
                },
                {
                    "text": "One",
                    "start": 138584,
                    "end": 138702,
                    "confidence": 0.99624,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 138716,
                    "end": 138822,
                    "confidence": 0.54147,
                    "speaker": "B"
                },
                {
                    "text": "them",
                    "start": 138836,
                    "end": 138942,
                    "confidence": 0.9978,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 138956,
                    "end": 139098,
                    "confidence": 0.90581,
                    "speaker": "B"
                },
                {
                    "text": "tiered,",
                    "start": 139124,
                    "end": 139498,
                    "confidence": 0.89417,
                    "speaker": "B"
                },
                {
                    "text": "which",
                    "start": 139534,
                    "end": 139662,
                    "confidence": 0.99916,
                    "speaker": "B"
                },
                {
                    "text": "means",
                    "start": 139676,
                    "end": 139818,
                    "confidence": 0.99885,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 139844,
                    "end": 139962,
                    "confidence": 0.97369,
                    "speaker": "B"
                },
                {
                    "text": "put",
                    "start": 139976,
                    "end": 140154,
                    "confidence": 0.52861,
                    "speaker": "B"
                },
                {
                    "text": "people",
                    "start": 140192,
                    "end": 140394,
                    "confidence": 0.99978,
                    "speaker": "B"
                },
                {
                    "text": "into",
                    "start": 140432,
                    "end": 140670,
                    "confidence": 0.52262,
                    "speaker": "B"
                },
                {
                    "text": "brackets",
                    "start": 140720,
                    "end": 141118,
                    "confidence": 0.57734,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 141154,
                    "end": 141318,
                    "confidence": 0.4612,
                    "speaker": "B"
                },
                {
                    "text": "groups",
                    "start": 141344,
                    "end": 141622,
                    "confidence": 0.97072,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 141646,
                    "end": 141762,
                    "confidence": 0.94722,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 141776,
                    "end": 141882,
                    "confidence": 0.96961,
                    "speaker": "B"
                },
                {
                    "text": "look",
                    "start": 141896,
                    "end": 142002,
                    "confidence": 0.99698,
                    "speaker": "B"
                },
                {
                    "text": "at",
                    "start": 142016,
                    "end": 142122,
                    "confidence": 0.99609,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 142136,
                    "end": 142242,
                    "confidence": 0.99368,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 142256,
                    "end": 142434,
                    "confidence": 0.96972,
                    "speaker": "B"
                },
                {
                    "text": "groups",
                    "start": 142472,
                    "end": 142738,
                    "confidence": 0.43815,
                    "speaker": "B"
                },
                {
                    "text": "perform.",
                    "start": 142774,
                    "end": 143226,
                    "confidence": 0.54814,
                    "speaker": "B"
                },
                {
                    "text": "But",
                    "start": 143348,
                    "end": 143742,
                    "confidence": 0.95498,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 143816,
                    "end": 144054,
                    "confidence": 0.96109,
                    "speaker": "B"
                },
                {
                    "text": "particular",
                    "start": 144092,
                    "end": 144438,
                    "confidence": 0.9987,
                    "speaker": "B"
                },
                {
                    "text": "interest",
                    "start": 144524,
                    "end": 144846,
                    "confidence": 0.99936,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 144908,
                    "end": 145098,
                    "confidence": 0.97078,
                    "speaker": "B"
                },
                {
                    "text": "us",
                    "start": 145124,
                    "end": 145314,
                    "confidence": 0.99498,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 145352,
                    "end": 145518,
                    "confidence": 0.75998,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 145544,
                    "end": 145698,
                    "confidence": 0.99692,
                    "speaker": "B"
                },
                {
                    "text": "you",
                    "start": 145724,
                    "end": 145842,
                    "confidence": 0.94238,
                    "speaker": "B"
                },
                {
                    "text": "get",
                    "start": 145856,
                    "end": 145962,
                    "confidence": 0.92007,
                    "speaker": "B"
                },
                {
                    "text": "into",
                    "start": 145976,
                    "end": 146190,
                    "confidence": 0.54739,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 146240,
                    "end": 146418,
                    "confidence": 0.88259,
                    "speaker": "B"
                },
                {
                    "text": "personalized",
                    "start": 146444,
                    "end": 146974,
                    "confidence": 0.96566,
                    "speaker": "B"
                },
                {
                    "text": "level.",
                    "start": 147022,
                    "end": 147414,
                    "confidence": 0.90132,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 147512,
                    "end": 147774,
                    "confidence": 0.94773,
                    "speaker": "B"
                },
                {
                    "text": "how",
                    "start": 147812,
                    "end": 147942,
                    "confidence": 0.98228,
                    "speaker": "B"
                },
                {
                    "text": "do",
                    "start": 147956,
                    "end": 148062,
                    "confidence": 0.90079,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 148076,
                    "end": 148218,
                    "confidence": 0.9888,
                    "speaker": "B"
                },
                {
                    "text": "actually",
                    "start": 148244,
                    "end": 148506,
                    "confidence": 0.99468,
                    "speaker": "B"
                },
                {
                    "text": "use",
                    "start": 148568,
                    "end": 148758,
                    "confidence": 0.9952,
                    "speaker": "B"
                },
                {
                    "text": "this",
                    "start": 148784,
                    "end": 148938,
                    "confidence": 0.63775,
                    "speaker": "B"
                },
                {
                    "text": "new",
                    "start": 148964,
                    "end": 149154,
                    "confidence": 0.32661,
                    "speaker": "B"
                },
                {
                    "text": "emerging",
                    "start": 149192,
                    "end": 149746,
                    "confidence": 0.59978,
                    "speaker": "B"
                },
                {
                    "text": "signs",
                    "start": 149818,
                    "end": 150178,
                    "confidence": 0.34957,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 150214,
                    "end": 150342,
                    "confidence": 0.99319,
                    "speaker": "B"
                },
                {
                    "text": "##########",
                    "start": 150356,
                    "end": 150838,
                    "confidence": 0.63013,
                    "speaker": "B"
                },
                {
                    "text": "############",
                    "start": 150874,
                    "end": 151378,
                    "confidence": 0.50557,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 151414,
                    "end": 151578,
                    "confidence": 0.97266,
                    "speaker": "B"
                },
                {
                    "text": "#######",
                    "start": 151604,
                    "end": 151894,
                    "confidence": 0.80972,
                    "speaker": "B"
                },
                {
                    "text": "########",
                    "start": 151942,
                    "end": 152334,
                    "confidence": 0.99993,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 152432,
                    "end": 152658,
                    "confidence": 0.79295,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 152684,
                    "end": 152838,
                    "confidence": 0.99388,
                    "speaker": "B"
                },
                {
                    "text": "application?",
                    "start": 152864,
                    "end": 153234,
                    "confidence": 0.99977,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 153332,
                    "end": 153522,
                    "confidence": 0.64373,
                    "speaker": "B"
                },
                {
                    "text": "that's",
                    "start": 153536,
                    "end": 153778,
                    "confidence": 0.48868,
                    "speaker": "B"
                },
                {
                    "text": "where",
                    "start": 153814,
                    "end": 153978,
                    "confidence": 0.99655,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 154004,
                    "end": 154158,
                    "confidence": 0.99596,
                    "speaker": "B"
                },
                {
                    "text": "came",
                    "start": 154184,
                    "end": 154446,
                    "confidence": 0.59093,
                    "speaker": "B"
                },
                {
                    "text": "from.",
                    "start": 154508,
                    "end": 154698,
                    "confidence": 0.6681,
                    "speaker": "B"
                }
            ]
        },
        {
            "confidence": 0.8633615384615384,
            "end": 166470,
            "speaker": "A",
            "start": 154724,
            "text": "It from our Echo fantastic. So delving into the method. ########, can you tell us a bit about how the Survival Quilt based model was developed?",
            "words": [
                {
                    "text": "It",
                    "start": 154724,
                    "end": 155022,
                    "confidence": 0.6957,
                    "speaker": "A"
                },
                {
                    "text": "from",
                    "start": 155096,
                    "end": 155298,
                    "confidence": 0.92647,
                    "speaker": "A"
                },
                {
                    "text": "our",
                    "start": 155324,
                    "end": 155478,
                    "confidence": 0.96972,
                    "speaker": "A"
                },
                {
                    "text": "Echo",
                    "start": 155504,
                    "end": 156210,
                    "confidence": 0.31284,
                    "speaker": "A"
                },
                {
                    "text": "fantastic.",
                    "start": 156890,
                    "end": 158070,
                    "confidence": 0.94803,
                    "speaker": "A"
                },
                {
                    "text": "So",
                    "start": 158510,
                    "end": 158838,
                    "confidence": 0.74614,
                    "speaker": "A"
                },
                {
                    "text": "delving",
                    "start": 158864,
                    "end": 159178,
                    "confidence": 0.69377,
                    "speaker": "A"
                },
                {
                    "text": "into",
                    "start": 159214,
                    "end": 159414,
                    "confidence": 0.99566,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 159452,
                    "end": 159654,
                    "confidence": 0.99821,
                    "speaker": "A"
                },
                {
                    "text": "method.",
                    "start": 159692,
                    "end": 160318,
                    "confidence": 0.99607,
                    "speaker": "A"
                },
                {
                    "text": "########,",
                    "start": 160474,
                    "end": 161026,
                    "confidence": 0.00352,
                    "speaker": "A"
                },
                {
                    "text": "can",
                    "start": 161038,
                    "end": 161178,
                    "confidence": 0.99812,
                    "speaker": "A"
                },
                {
                    "text": "you",
                    "start": 161204,
                    "end": 161358,
                    "confidence": 0.99954,
                    "speaker": "A"
                },
                {
                    "text": "tell",
                    "start": 161384,
                    "end": 161538,
                    "confidence": 0.99968,
                    "speaker": "A"
                },
                {
                    "text": "us",
                    "start": 161564,
                    "end": 161718,
                    "confidence": 0.99969,
                    "speaker": "A"
                },
                {
                    "text": "a",
                    "start": 161744,
                    "end": 161862,
                    "confidence": 0.98949,
                    "speaker": "A"
                },
                {
                    "text": "bit",
                    "start": 161876,
                    "end": 162054,
                    "confidence": 0.99935,
                    "speaker": "A"
                },
                {
                    "text": "about",
                    "start": 162092,
                    "end": 162402,
                    "confidence": 0.99982,
                    "speaker": "A"
                },
                {
                    "text": "how",
                    "start": 162476,
                    "end": 162894,
                    "confidence": 0.99954,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 162992,
                    "end": 163218,
                    "confidence": 0.99598,
                    "speaker": "A"
                },
                {
                    "text": "Survival",
                    "start": 163244,
                    "end": 163810,
                    "confidence": 0.98974,
                    "speaker": "A"
                },
                {
                    "text": "Quilt",
                    "start": 163870,
                    "end": 164242,
                    "confidence": 0.81425,
                    "speaker": "A"
                },
                {
                    "text": "based",
                    "start": 164326,
                    "end": 164718,
                    "confidence": 0.49514,
                    "speaker": "A"
                },
                {
                    "text": "model",
                    "start": 164804,
                    "end": 165198,
                    "confidence": 0.99978,
                    "speaker": "A"
                },
                {
                    "text": "was",
                    "start": 165284,
                    "end": 165570,
                    "confidence": 0.9987,
                    "speaker": "A"
                },
                {
                    "text": "developed?",
                    "start": 165620,
                    "end": 166470,
                    "confidence": 0.88245,
                    "speaker": "A"
                }
            ]
        },
        {
            "confidence": 0.8914099999999999,
            "end": 276620,
            "speaker": "C",
            "start": 167450,
            "text": "Survival Quilt is a ####### ######## method that we have developed two years ago, and it appeared in 2019, which is one of our main conferences in ########## ############ and ####### ########. We know that there are numerous survival models, and a key question is how to select among these numerous survival models for a particular dataset, such as the Seer data set. And Survival Quilts is the first automated ####### ######## method that is able to do so for survival analysis and is learning on the basis of the underlying data, how to weigh the different variables to achieve the best trade off between discriminatory performance and calibration. This is important because the usefulness of a survival model should be assessed both by how well the model discriminates among predictive risk and by what is calibrated. And while it's important to correctly discriminate and prioritize patients on the basis of risk, there is prediction of a model also to be well calibrated in order to be really valuable and provide prognostic value to ##########. So survival quiz is able to learn on the basis of whatever data is available. The best model is learning the best model and the best model across the different Horizons of survival analysis. What is the best integration of the variables and how the variables interact in order to issue a personalized prediction for survival? So it is learning personalized predictions and how to best combine these different existing models to issue this particular prediction for the patient at hand.",
            "words": [
                {
                    "text": "Survival",
                    "start": 167450,
                    "end": 168094,
                    "confidence": 0.99236,
                    "speaker": "C"
                },
                {
                    "text": "Quilt",
                    "start": 168142,
                    "end": 168562,
                    "confidence": 0.93575,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 168646,
                    "end": 168858,
                    "confidence": 0.98729,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 168884,
                    "end": 169002,
                    "confidence": 0.98045,
                    "speaker": "C"
                },
                {
                    "text": "#######",
                    "start": 169016,
                    "end": 169282,
                    "confidence": 0.99805,
                    "speaker": "C"
                },
                {
                    "text": "########",
                    "start": 169306,
                    "end": 169638,
                    "confidence": 0.53208,
                    "speaker": "C"
                },
                {
                    "text": "method",
                    "start": 169724,
                    "end": 170350,
                    "confidence": 0.99889,
                    "speaker": "C"
                },
                {
                    "text": "that",
                    "start": 170470,
                    "end": 170718,
                    "confidence": 0.99114,
                    "speaker": "C"
                },
                {
                    "text": "we",
                    "start": 170744,
                    "end": 170898,
                    "confidence": 0.99766,
                    "speaker": "C"
                },
                {
                    "text": "have",
                    "start": 170924,
                    "end": 171114,
                    "confidence": 0.96534,
                    "speaker": "C"
                },
                {
                    "text": "developed",
                    "start": 171152,
                    "end": 171898,
                    "confidence": 0.98264,
                    "speaker": "C"
                },
                {
                    "text": "two",
                    "start": 171994,
                    "end": 172218,
                    "confidence": 0.99908,
                    "speaker": "C"
                },
                {
                    "text": "years",
                    "start": 172244,
                    "end": 172470,
                    "confidence": 0.85945,
                    "speaker": "C"
                },
                {
                    "text": "ago,",
                    "start": 172520,
                    "end": 172806,
                    "confidence": 0.71452,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 172868,
                    "end": 173058,
                    "confidence": 0.90622,
                    "speaker": "C"
                },
                {
                    "text": "it",
                    "start": 173084,
                    "end": 173238,
                    "confidence": 0.97303,
                    "speaker": "C"
                },
                {
                    "text": "appeared",
                    "start": 173264,
                    "end": 173902,
                    "confidence": 0.83352,
                    "speaker": "C"
                },
                {
                    "text": "in",
                    "start": 173986,
                    "end": 174620,
                    "confidence": 0.8317,
                    "speaker": "C"
                },
                {
                    "text": "2019,",
                    "start": 175310,
                    "end": 176410,
                    "confidence": 0.94,
                    "speaker": "C"
                },
                {
                    "text": "which",
                    "start": 176470,
                    "end": 176658,
                    "confidence": 0.99905,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 176684,
                    "end": 176910,
                    "confidence": 0.99647,
                    "speaker": "C"
                },
                {
                    "text": "one",
                    "start": 176960,
                    "end": 177174,
                    "confidence": 0.99928,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 177212,
                    "end": 177414,
                    "confidence": 0.99817,
                    "speaker": "C"
                },
                {
                    "text": "our",
                    "start": 177452,
                    "end": 178050,
                    "confidence": 0.99802,
                    "speaker": "C"
                },
                {
                    "text": "main",
                    "start": 178220,
                    "end": 178662,
                    "confidence": 0.99899,
                    "speaker": "C"
                },
                {
                    "text": "conferences",
                    "start": 178736,
                    "end": 179398,
                    "confidence": 0.95548,
                    "speaker": "C"
                },
                {
                    "text": "in",
                    "start": 179494,
                    "end": 179862,
                    "confidence": 0.96642,
                    "speaker": "C"
                },
                {
                    "text": "##########",
                    "start": 179936,
                    "end": 180298,
                    "confidence": 0.45054,
                    "speaker": "C"
                },
                {
                    "text": "############",
                    "start": 180334,
                    "end": 180814,
                    "confidence": 0.98364,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 180862,
                    "end": 181038,
                    "confidence": 0.98628,
                    "speaker": "C"
                },
                {
                    "text": "#######",
                    "start": 181064,
                    "end": 181354,
                    "confidence": 0.67226,
                    "speaker": "C"
                },
                {
                    "text": "########.",
                    "start": 181402,
                    "end": 182000,
                    "confidence": 0.99995,
                    "speaker": "C"
                },
                {
                    "text": "We",
                    "start": 182390,
                    "end": 182718,
                    "confidence": 0.99864,
                    "speaker": "C"
                },
                {
                    "text": "know",
                    "start": 182744,
                    "end": 182898,
                    "confidence": 0.999,
                    "speaker": "C"
                },
                {
                    "text": "that",
                    "start": 182924,
                    "end": 183078,
                    "confidence": 0.92343,
                    "speaker": "C"
                },
                {
                    "text": "there",
                    "start": 183104,
                    "end": 183222,
                    "confidence": 0.99683,
                    "speaker": "C"
                },
                {
                    "text": "are",
                    "start": 183236,
                    "end": 183378,
                    "confidence": 0.99152,
                    "speaker": "C"
                },
                {
                    "text": "numerous",
                    "start": 183404,
                    "end": 183910,
                    "confidence": 0.99754,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 183970,
                    "end": 184534,
                    "confidence": 0.92837,
                    "speaker": "C"
                },
                {
                    "text": "models,",
                    "start": 184582,
                    "end": 185182,
                    "confidence": 0.98562,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 185266,
                    "end": 185586,
                    "confidence": 0.86099,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 185648,
                    "end": 185838,
                    "confidence": 0.95343,
                    "speaker": "C"
                },
                {
                    "text": "key",
                    "start": 185864,
                    "end": 186090,
                    "confidence": 0.9954,
                    "speaker": "C"
                },
                {
                    "text": "question",
                    "start": 186140,
                    "end": 186822,
                    "confidence": 0.99815,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 187016,
                    "end": 187446,
                    "confidence": 0.98745,
                    "speaker": "C"
                },
                {
                    "text": "how",
                    "start": 187508,
                    "end": 187698,
                    "confidence": 0.5132,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 187724,
                    "end": 187950,
                    "confidence": 0.93752,
                    "speaker": "C"
                },
                {
                    "text": "select",
                    "start": 188000,
                    "end": 188358,
                    "confidence": 0.99947,
                    "speaker": "C"
                },
                {
                    "text": "among",
                    "start": 188444,
                    "end": 188794,
                    "confidence": 0.99335,
                    "speaker": "C"
                },
                {
                    "text": "these",
                    "start": 188842,
                    "end": 189270,
                    "confidence": 0.84501,
                    "speaker": "C"
                },
                {
                    "text": "numerous",
                    "start": 189380,
                    "end": 190054,
                    "confidence": 0.80803,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 190162,
                    "end": 190774,
                    "confidence": 0.75758,
                    "speaker": "C"
                },
                {
                    "text": "models",
                    "start": 190822,
                    "end": 191218,
                    "confidence": 0.91287,
                    "speaker": "C"
                },
                {
                    "text": "for",
                    "start": 191254,
                    "end": 191382,
                    "confidence": 0.99773,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 191396,
                    "end": 191538,
                    "confidence": 0.73141,
                    "speaker": "C"
                },
                {
                    "text": "particular",
                    "start": 191564,
                    "end": 191970,
                    "confidence": 0.99994,
                    "speaker": "C"
                },
                {
                    "text": "dataset,",
                    "start": 192080,
                    "end": 193078,
                    "confidence": 0.21422,
                    "speaker": "C"
                },
                {
                    "text": "such",
                    "start": 193234,
                    "end": 193518,
                    "confidence": 0.9996,
                    "speaker": "C"
                },
                {
                    "text": "as",
                    "start": 193544,
                    "end": 193734,
                    "confidence": 0.85,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 193772,
                    "end": 193938,
                    "confidence": 0.97303,
                    "speaker": "C"
                },
                {
                    "text": "Seer",
                    "start": 193964,
                    "end": 194278,
                    "confidence": 0.31236,
                    "speaker": "C"
                },
                {
                    "text": "data",
                    "start": 194314,
                    "end": 194550,
                    "confidence": 0.99556,
                    "speaker": "C"
                },
                {
                    "text": "set.",
                    "start": 194600,
                    "end": 195246,
                    "confidence": 0.52082,
                    "speaker": "C"
                },
                {
                    "text": "And",
                    "start": 195428,
                    "end": 195990,
                    "confidence": 0.59226,
                    "speaker": "C"
                },
                {
                    "text": "Survival",
                    "start": 196100,
                    "end": 196654,
                    "confidence": 0.67108,
                    "speaker": "C"
                },
                {
                    "text": "Quilts",
                    "start": 196702,
                    "end": 197242,
                    "confidence": 0.53531,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 197326,
                    "end": 197718,
                    "confidence": 0.94437,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 197804,
                    "end": 198090,
                    "confidence": 0.99612,
                    "speaker": "C"
                },
                {
                    "text": "first",
                    "start": 198140,
                    "end": 198534,
                    "confidence": 0.99855,
                    "speaker": "C"
                },
                {
                    "text": "automated",
                    "start": 198632,
                    "end": 199342,
                    "confidence": 0.99715,
                    "speaker": "C"
                },
                {
                    "text": "#######",
                    "start": 199426,
                    "end": 199774,
                    "confidence": 0.91809,
                    "speaker": "C"
                },
                {
                    "text": "########",
                    "start": 199822,
                    "end": 200286,
                    "confidence": 0.99933,
                    "speaker": "C"
                },
                {
                    "text": "method",
                    "start": 200408,
                    "end": 201070,
                    "confidence": 0.9987,
                    "speaker": "C"
                },
                {
                    "text": "that",
                    "start": 201190,
                    "end": 201474,
                    "confidence": 0.99743,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 201512,
                    "end": 201930,
                    "confidence": 0.97787,
                    "speaker": "C"
                },
                {
                    "text": "able",
                    "start": 202040,
                    "end": 202422,
                    "confidence": 0.99914,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 202496,
                    "end": 202698,
                    "confidence": 0.99828,
                    "speaker": "C"
                },
                {
                    "text": "do",
                    "start": 202724,
                    "end": 202914,
                    "confidence": 0.99575,
                    "speaker": "C"
                },
                {
                    "text": "so",
                    "start": 202952,
                    "end": 203262,
                    "confidence": 0.97625,
                    "speaker": "C"
                },
                {
                    "text": "for",
                    "start": 203336,
                    "end": 203574,
                    "confidence": 0.9985,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 203612,
                    "end": 204094,
                    "confidence": 0.95305,
                    "speaker": "C"
                },
                {
                    "text": "analysis",
                    "start": 204142,
                    "end": 205054,
                    "confidence": 0.97656,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 205222,
                    "end": 205518,
                    "confidence": 0.8414,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 205544,
                    "end": 205770,
                    "confidence": 0.58074,
                    "speaker": "C"
                },
                {
                    "text": "learning",
                    "start": 205820,
                    "end": 206286,
                    "confidence": 0.85609,
                    "speaker": "C"
                },
                {
                    "text": "on",
                    "start": 206408,
                    "end": 206658,
                    "confidence": 0.99683,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 206684,
                    "end": 206838,
                    "confidence": 0.99865,
                    "speaker": "C"
                },
                {
                    "text": "basis",
                    "start": 206864,
                    "end": 207346,
                    "confidence": 0.63328,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 207418,
                    "end": 207618,
                    "confidence": 0.99897,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 207644,
                    "end": 207798,
                    "confidence": 0.98543,
                    "speaker": "C"
                },
                {
                    "text": "underlying",
                    "start": 207824,
                    "end": 208486,
                    "confidence": 0.96216,
                    "speaker": "C"
                },
                {
                    "text": "data,",
                    "start": 208558,
                    "end": 209190,
                    "confidence": 0.99067,
                    "speaker": "C"
                },
                {
                    "text": "how",
                    "start": 209360,
                    "end": 209694,
                    "confidence": 0.99821,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 209732,
                    "end": 209970,
                    "confidence": 0.99388,
                    "speaker": "C"
                },
                {
                    "text": "weigh",
                    "start": 210020,
                    "end": 210202,
                    "confidence": 0.35918,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 210226,
                    "end": 210414,
                    "confidence": 0.93869,
                    "speaker": "C"
                },
                {
                    "text": "different",
                    "start": 210452,
                    "end": 210798,
                    "confidence": 0.99589,
                    "speaker": "C"
                },
                {
                    "text": "variables",
                    "start": 210884,
                    "end": 211738,
                    "confidence": 0.57905,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 211834,
                    "end": 212058,
                    "confidence": 0.99781,
                    "speaker": "C"
                },
                {
                    "text": "achieve",
                    "start": 212084,
                    "end": 212434,
                    "confidence": 0.51298,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 212482,
                    "end": 212658,
                    "confidence": 0.9919,
                    "speaker": "C"
                },
                {
                    "text": "best",
                    "start": 212684,
                    "end": 212910,
                    "confidence": 0.99943,
                    "speaker": "C"
                },
                {
                    "text": "trade",
                    "start": 212960,
                    "end": 213202,
                    "confidence": 0.93026,
                    "speaker": "C"
                },
                {
                    "text": "off",
                    "start": 213226,
                    "end": 213486,
                    "confidence": 0.63819,
                    "speaker": "C"
                },
                {
                    "text": "between",
                    "start": 213548,
                    "end": 214062,
                    "confidence": 0.99829,
                    "speaker": "C"
                },
                {
                    "text": "discriminatory",
                    "start": 214196,
                    "end": 215098,
                    "confidence": 0.23367,
                    "speaker": "C"
                },
                {
                    "text": "performance",
                    "start": 215134,
                    "end": 215986,
                    "confidence": 0.97506,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 216118,
                    "end": 216630,
                    "confidence": 0.99848,
                    "speaker": "C"
                },
                {
                    "text": "calibration.",
                    "start": 216740,
                    "end": 217726,
                    "confidence": 0.80953,
                    "speaker": "C"
                },
                {
                    "text": "This",
                    "start": 217918,
                    "end": 218238,
                    "confidence": 0.96415,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 218264,
                    "end": 218490,
                    "confidence": 0.50065,
                    "speaker": "C"
                },
                {
                    "text": "important",
                    "start": 218540,
                    "end": 219006,
                    "confidence": 0.99966,
                    "speaker": "C"
                },
                {
                    "text": "because",
                    "start": 219128,
                    "end": 219486,
                    "confidence": 0.9995,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 219548,
                    "end": 219774,
                    "confidence": 0.99176,
                    "speaker": "C"
                },
                {
                    "text": "usefulness",
                    "start": 219812,
                    "end": 220330,
                    "confidence": 0.29592,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 220390,
                    "end": 220542,
                    "confidence": 0.99767,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 220556,
                    "end": 220662,
                    "confidence": 0.92904,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 220676,
                    "end": 221254,
                    "confidence": 0.42071,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 221302,
                    "end": 221622,
                    "confidence": 0.99765,
                    "speaker": "C"
                },
                {
                    "text": "should",
                    "start": 221696,
                    "end": 221898,
                    "confidence": 0.9994,
                    "speaker": "C"
                },
                {
                    "text": "be",
                    "start": 221924,
                    "end": 222078,
                    "confidence": 0.99749,
                    "speaker": "C"
                },
                {
                    "text": "assessed",
                    "start": 222104,
                    "end": 222514,
                    "confidence": 0.95189,
                    "speaker": "C"
                },
                {
                    "text": "both",
                    "start": 222562,
                    "end": 222774,
                    "confidence": 0.99307,
                    "speaker": "C"
                },
                {
                    "text": "by",
                    "start": 222812,
                    "end": 222978,
                    "confidence": 0.99862,
                    "speaker": "C"
                },
                {
                    "text": "how",
                    "start": 223004,
                    "end": 223158,
                    "confidence": 0.9805,
                    "speaker": "C"
                },
                {
                    "text": "well",
                    "start": 223184,
                    "end": 223338,
                    "confidence": 0.99088,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 223364,
                    "end": 223518,
                    "confidence": 0.97347,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 223544,
                    "end": 223806,
                    "confidence": 0.9822,
                    "speaker": "C"
                },
                {
                    "text": "discriminates",
                    "start": 223868,
                    "end": 224590,
                    "confidence": 0.97086,
                    "speaker": "C"
                },
                {
                    "text": "among",
                    "start": 224650,
                    "end": 224974,
                    "confidence": 0.56109,
                    "speaker": "C"
                },
                {
                    "text": "predictive",
                    "start": 225022,
                    "end": 225514,
                    "confidence": 0.95933,
                    "speaker": "C"
                },
                {
                    "text": "risk",
                    "start": 225562,
                    "end": 226006,
                    "confidence": 0.53512,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 226078,
                    "end": 226314,
                    "confidence": 0.99692,
                    "speaker": "C"
                },
                {
                    "text": "by",
                    "start": 226352,
                    "end": 226518,
                    "confidence": 0.88588,
                    "speaker": "C"
                },
                {
                    "text": "what",
                    "start": 226544,
                    "end": 226698,
                    "confidence": 0.38208,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 226724,
                    "end": 226878,
                    "confidence": 0.2937,
                    "speaker": "C"
                },
                {
                    "text": "calibrated.",
                    "start": 226904,
                    "end": 228046,
                    "confidence": 0.71557,
                    "speaker": "C"
                },
                {
                    "text": "And",
                    "start": 228238,
                    "end": 228630,
                    "confidence": 0.72533,
                    "speaker": "C"
                },
                {
                    "text": "while",
                    "start": 228680,
                    "end": 228858,
                    "confidence": 0.93577,
                    "speaker": "C"
                },
                {
                    "text": "it's",
                    "start": 228884,
                    "end": 229102,
                    "confidence": 0.56147,
                    "speaker": "C"
                },
                {
                    "text": "important",
                    "start": 229126,
                    "end": 229494,
                    "confidence": 0.99994,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 229592,
                    "end": 229854,
                    "confidence": 0.99832,
                    "speaker": "C"
                },
                {
                    "text": "correctly",
                    "start": 229892,
                    "end": 230350,
                    "confidence": 0.84219,
                    "speaker": "C"
                },
                {
                    "text": "discriminate",
                    "start": 230410,
                    "end": 231058,
                    "confidence": 0.58798,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 231094,
                    "end": 231294,
                    "confidence": 0.52696,
                    "speaker": "C"
                },
                {
                    "text": "prioritize",
                    "start": 231332,
                    "end": 231922,
                    "confidence": 0.79533,
                    "speaker": "C"
                },
                {
                    "text": "patients",
                    "start": 232006,
                    "end": 232390,
                    "confidence": 0.90265,
                    "speaker": "C"
                },
                {
                    "text": "on",
                    "start": 232450,
                    "end": 232602,
                    "confidence": 0.97023,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 232616,
                    "end": 232722,
                    "confidence": 0.99448,
                    "speaker": "C"
                },
                {
                    "text": "basis",
                    "start": 232736,
                    "end": 233038,
                    "confidence": 0.99945,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 233074,
                    "end": 233310,
                    "confidence": 0.50189,
                    "speaker": "C"
                },
                {
                    "text": "risk,",
                    "start": 233360,
                    "end": 233842,
                    "confidence": 0.95304,
                    "speaker": "C"
                },
                {
                    "text": "there",
                    "start": 233926,
                    "end": 234138,
                    "confidence": 0.92207,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 234164,
                    "end": 234318,
                    "confidence": 0.97794,
                    "speaker": "C"
                },
                {
                    "text": "prediction",
                    "start": 234344,
                    "end": 234874,
                    "confidence": 0.36497,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 234922,
                    "end": 235098,
                    "confidence": 0.97733,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 235124,
                    "end": 235242,
                    "confidence": 0.85715,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 235256,
                    "end": 235614,
                    "confidence": 0.95251,
                    "speaker": "C"
                },
                {
                    "text": "also",
                    "start": 235712,
                    "end": 235974,
                    "confidence": 0.57173,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 236012,
                    "end": 236178,
                    "confidence": 0.99458,
                    "speaker": "C"
                },
                {
                    "text": "be",
                    "start": 236204,
                    "end": 236358,
                    "confidence": 0.99025,
                    "speaker": "C"
                },
                {
                    "text": "well",
                    "start": 236384,
                    "end": 236574,
                    "confidence": 0.98907,
                    "speaker": "C"
                },
                {
                    "text": "calibrated",
                    "start": 236612,
                    "end": 237418,
                    "confidence": 0.90105,
                    "speaker": "C"
                },
                {
                    "text": "in",
                    "start": 237514,
                    "end": 237702,
                    "confidence": 0.99901,
                    "speaker": "C"
                },
                {
                    "text": "order",
                    "start": 237716,
                    "end": 237966,
                    "confidence": 0.63316,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 238028,
                    "end": 238218,
                    "confidence": 0.99917,
                    "speaker": "C"
                },
                {
                    "text": "be",
                    "start": 238244,
                    "end": 238434,
                    "confidence": 0.99861,
                    "speaker": "C"
                },
                {
                    "text": "really",
                    "start": 238472,
                    "end": 238854,
                    "confidence": 0.98104,
                    "speaker": "C"
                },
                {
                    "text": "valuable",
                    "start": 238952,
                    "end": 239590,
                    "confidence": 0.97534,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 239710,
                    "end": 239994,
                    "confidence": 0.99194,
                    "speaker": "C"
                },
                {
                    "text": "provide",
                    "start": 240032,
                    "end": 240450,
                    "confidence": 0.99333,
                    "speaker": "C"
                },
                {
                    "text": "prognostic",
                    "start": 240560,
                    "end": 241138,
                    "confidence": 0.97924,
                    "speaker": "C"
                },
                {
                    "text": "value",
                    "start": 241174,
                    "end": 241374,
                    "confidence": 0.99955,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 241412,
                    "end": 241542,
                    "confidence": 0.99533,
                    "speaker": "C"
                },
                {
                    "text": "##########.",
                    "start": 241556,
                    "end": 242230,
                    "confidence": 0.96575,
                    "speaker": "C"
                },
                {
                    "text": "So",
                    "start": 242290,
                    "end": 242550,
                    "confidence": 0.82377,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 242600,
                    "end": 243118,
                    "confidence": 0.83647,
                    "speaker": "C"
                },
                {
                    "text": "quiz",
                    "start": 243154,
                    "end": 243598,
                    "confidence": 0.43988,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 243694,
                    "end": 243990,
                    "confidence": 0.99414,
                    "speaker": "C"
                },
                {
                    "text": "able",
                    "start": 244040,
                    "end": 244326,
                    "confidence": 0.99639,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 244388,
                    "end": 244614,
                    "confidence": 0.99882,
                    "speaker": "C"
                },
                {
                    "text": "learn",
                    "start": 244652,
                    "end": 244890,
                    "confidence": 0.99949,
                    "speaker": "C"
                },
                {
                    "text": "on",
                    "start": 244940,
                    "end": 245118,
                    "confidence": 0.98488,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 245144,
                    "end": 245298,
                    "confidence": 0.99646,
                    "speaker": "C"
                },
                {
                    "text": "basis",
                    "start": 245324,
                    "end": 245710,
                    "confidence": 0.99759,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 245770,
                    "end": 246030,
                    "confidence": 0.99342,
                    "speaker": "C"
                },
                {
                    "text": "whatever",
                    "start": 246080,
                    "end": 246510,
                    "confidence": 0.96414,
                    "speaker": "C"
                },
                {
                    "text": "data",
                    "start": 246620,
                    "end": 246930,
                    "confidence": 0.99198,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 246980,
                    "end": 247194,
                    "confidence": 0.98986,
                    "speaker": "C"
                },
                {
                    "text": "available.",
                    "start": 247232,
                    "end": 247820,
                    "confidence": 0.99984,
                    "speaker": "C"
                },
                {
                    "text": "The",
                    "start": 248270,
                    "end": 248634,
                    "confidence": 0.94281,
                    "speaker": "C"
                },
                {
                    "text": "best",
                    "start": 248672,
                    "end": 249270,
                    "confidence": 0.99796,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 249440,
                    "end": 249882,
                    "confidence": 0.99653,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 249956,
                    "end": 250158,
                    "confidence": 0.96421,
                    "speaker": "C"
                },
                {
                    "text": "learning",
                    "start": 250184,
                    "end": 250446,
                    "confidence": 0.99655,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 250508,
                    "end": 250698,
                    "confidence": 0.9946,
                    "speaker": "C"
                },
                {
                    "text": "best",
                    "start": 250724,
                    "end": 250950,
                    "confidence": 0.999,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 251000,
                    "end": 251250,
                    "confidence": 0.99069,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 251300,
                    "end": 251442,
                    "confidence": 0.98504,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 251456,
                    "end": 251598,
                    "confidence": 0.66126,
                    "speaker": "C"
                },
                {
                    "text": "best",
                    "start": 251624,
                    "end": 251850,
                    "confidence": 0.99958,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 251900,
                    "end": 252474,
                    "confidence": 0.65968,
                    "speaker": "C"
                },
                {
                    "text": "across",
                    "start": 252632,
                    "end": 253062,
                    "confidence": 0.99909,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 253136,
                    "end": 253374,
                    "confidence": 0.99205,
                    "speaker": "C"
                },
                {
                    "text": "different",
                    "start": 253412,
                    "end": 253722,
                    "confidence": 0.99834,
                    "speaker": "C"
                },
                {
                    "text": "Horizons",
                    "start": 253796,
                    "end": 254590,
                    "confidence": 0.42077,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 254710,
                    "end": 254994,
                    "confidence": 0.98966,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 255032,
                    "end": 255574,
                    "confidence": 0.6973,
                    "speaker": "C"
                },
                {
                    "text": "analysis.",
                    "start": 255622,
                    "end": 256570,
                    "confidence": 0.99524,
                    "speaker": "C"
                },
                {
                    "text": "What",
                    "start": 256750,
                    "end": 257130,
                    "confidence": 0.98572,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 257180,
                    "end": 257358,
                    "confidence": 0.99769,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 257384,
                    "end": 257574,
                    "confidence": 0.99851,
                    "speaker": "C"
                },
                {
                    "text": "best",
                    "start": 257612,
                    "end": 258210,
                    "confidence": 0.99952,
                    "speaker": "C"
                },
                {
                    "text": "integration",
                    "start": 258380,
                    "end": 259162,
                    "confidence": 0.62359,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 259246,
                    "end": 259422,
                    "confidence": 0.99871,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 259436,
                    "end": 259614,
                    "confidence": 0.66656,
                    "speaker": "C"
                },
                {
                    "text": "variables",
                    "start": 259652,
                    "end": 260182,
                    "confidence": 0.95683,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 260206,
                    "end": 260358,
                    "confidence": 0.97568,
                    "speaker": "C"
                },
                {
                    "text": "how",
                    "start": 260384,
                    "end": 260538,
                    "confidence": 0.99559,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 260564,
                    "end": 260718,
                    "confidence": 0.96899,
                    "speaker": "C"
                },
                {
                    "text": "variables",
                    "start": 260744,
                    "end": 261370,
                    "confidence": 0.73179,
                    "speaker": "C"
                },
                {
                    "text": "interact",
                    "start": 261430,
                    "end": 262258,
                    "confidence": 0.99941,
                    "speaker": "C"
                },
                {
                    "text": "in",
                    "start": 262414,
                    "end": 262698,
                    "confidence": 0.99796,
                    "speaker": "C"
                },
                {
                    "text": "order",
                    "start": 262724,
                    "end": 263022,
                    "confidence": 0.99867,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 263096,
                    "end": 263298,
                    "confidence": 0.99844,
                    "speaker": "C"
                },
                {
                    "text": "issue",
                    "start": 263324,
                    "end": 263550,
                    "confidence": 0.98656,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 263600,
                    "end": 263742,
                    "confidence": 0.66878,
                    "speaker": "C"
                },
                {
                    "text": "personalized",
                    "start": 263756,
                    "end": 264370,
                    "confidence": 0.63125,
                    "speaker": "C"
                },
                {
                    "text": "prediction",
                    "start": 264430,
                    "end": 265114,
                    "confidence": 0.97819,
                    "speaker": "C"
                },
                {
                    "text": "for",
                    "start": 265222,
                    "end": 265494,
                    "confidence": 0.99939,
                    "speaker": "C"
                },
                {
                    "text": "survival?",
                    "start": 265532,
                    "end": 266290,
                    "confidence": 0.90002,
                    "speaker": "C"
                },
                {
                    "text": "So",
                    "start": 266410,
                    "end": 266730,
                    "confidence": 0.82109,
                    "speaker": "C"
                },
                {
                    "text": "it",
                    "start": 266780,
                    "end": 266958,
                    "confidence": 0.99747,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 266984,
                    "end": 267210,
                    "confidence": 0.9888,
                    "speaker": "C"
                },
                {
                    "text": "learning",
                    "start": 267260,
                    "end": 267726,
                    "confidence": 0.99755,
                    "speaker": "C"
                },
                {
                    "text": "personalized",
                    "start": 267848,
                    "end": 268630,
                    "confidence": 0.96313,
                    "speaker": "C"
                },
                {
                    "text": "predictions",
                    "start": 268690,
                    "end": 269230,
                    "confidence": 0.978,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 269290,
                    "end": 269478,
                    "confidence": 0.90092,
                    "speaker": "C"
                },
                {
                    "text": "how",
                    "start": 269504,
                    "end": 269658,
                    "confidence": 0.99931,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 269684,
                    "end": 269910,
                    "confidence": 0.99304,
                    "speaker": "C"
                },
                {
                    "text": "best",
                    "start": 269960,
                    "end": 270534,
                    "confidence": 0.9984,
                    "speaker": "C"
                },
                {
                    "text": "combine",
                    "start": 270692,
                    "end": 271234,
                    "confidence": 0.50974,
                    "speaker": "C"
                },
                {
                    "text": "these",
                    "start": 271282,
                    "end": 271494,
                    "confidence": 0.7122,
                    "speaker": "C"
                },
                {
                    "text": "different",
                    "start": 271532,
                    "end": 271842,
                    "confidence": 0.99954,
                    "speaker": "C"
                },
                {
                    "text": "existing",
                    "start": 271916,
                    "end": 272434,
                    "confidence": 0.99847,
                    "speaker": "C"
                },
                {
                    "text": "models",
                    "start": 272482,
                    "end": 273154,
                    "confidence": 0.87075,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 273262,
                    "end": 273534,
                    "confidence": 0.99842,
                    "speaker": "C"
                },
                {
                    "text": "issue",
                    "start": 273572,
                    "end": 273774,
                    "confidence": 0.9964,
                    "speaker": "C"
                },
                {
                    "text": "this",
                    "start": 273812,
                    "end": 274050,
                    "confidence": 0.91459,
                    "speaker": "C"
                },
                {
                    "text": "particular",
                    "start": 274100,
                    "end": 274494,
                    "confidence": 0.99982,
                    "speaker": "C"
                },
                {
                    "text": "prediction",
                    "start": 274592,
                    "end": 275134,
                    "confidence": 0.97495,
                    "speaker": "C"
                },
                {
                    "text": "for",
                    "start": 275182,
                    "end": 275358,
                    "confidence": 0.99906,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 275384,
                    "end": 275502,
                    "confidence": 0.99717,
                    "speaker": "C"
                },
                {
                    "text": "patient",
                    "start": 275516,
                    "end": 275818,
                    "confidence": 0.9747,
                    "speaker": "C"
                },
                {
                    "text": "at",
                    "start": 275854,
                    "end": 276018,
                    "confidence": 0.98896,
                    "speaker": "C"
                },
                {
                    "text": "hand.",
                    "start": 276044,
                    "end": 276620,
                    "confidence": 0.99291,
                    "speaker": "C"
                }
            ]
        },
        {
            "confidence": 0.8251433333333332,
            "end": 281420,
            "speaker": "A",
            "start": 277370,
            "text": "That's interesting. And what were the key findings then of this study?",
            "words": [
                {
                    "text": "That's",
                    "start": 277370,
                    "end": 277870,
                    "confidence": 0.68102,
                    "speaker": "A"
                },
                {
                    "text": "interesting.",
                    "start": 277930,
                    "end": 278262,
                    "confidence": 0.9888,
                    "speaker": "A"
                },
                {
                    "text": "And",
                    "start": 278336,
                    "end": 278646,
                    "confidence": 0.79903,
                    "speaker": "A"
                },
                {
                    "text": "what",
                    "start": 278708,
                    "end": 278898,
                    "confidence": 0.99493,
                    "speaker": "A"
                },
                {
                    "text": "were",
                    "start": 278924,
                    "end": 279042,
                    "confidence": 0.54894,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 279056,
                    "end": 279306,
                    "confidence": 0.93682,
                    "speaker": "A"
                },
                {
                    "text": "key",
                    "start": 279368,
                    "end": 279594,
                    "confidence": 0.99981,
                    "speaker": "A"
                },
                {
                    "text": "findings",
                    "start": 279632,
                    "end": 280150,
                    "confidence": 0.58332,
                    "speaker": "A"
                },
                {
                    "text": "then",
                    "start": 280210,
                    "end": 280398,
                    "confidence": 0.60101,
                    "speaker": "A"
                },
                {
                    "text": "of",
                    "start": 280424,
                    "end": 280578,
                    "confidence": 0.79025,
                    "speaker": "A"
                },
                {
                    "text": "this",
                    "start": 280604,
                    "end": 280794,
                    "confidence": 0.98144,
                    "speaker": "A"
                },
                {
                    "text": "study?",
                    "start": 280832,
                    "end": 281420,
                    "confidence": 0.99635,
                    "speaker": "A"
                }
            ]
        },
        {
            "confidence": 0.8888176404494378,
            "end": 336080,
            "speaker": "B",
            "start": 282410,
            "text": "Well, the first thing to say is that to our knowledge, it's the first application of the ####### ######## algorithm and ########## ############ method to such a big data set. To answer this question, and one of the key things that we knew, the Survival Quilts model, like other machine models, can actually take new information and process it very quickly, and it took us many years to develop our previous models. But I think working with #######, one of your ########, with #######, we were able to produce this model very rapidly because the technique was already there. And even in this first situation where we had fairly standard variables, I was amazed to find that the final Quilts model was able to come up with a performance characteristic which was as good as the best that we have from a clinical standpoint and actually a little bit better as well. So that is to me an illustration that the direction of travel or what we do in future has to be using this kind of technology.",
            "words": [
                {
                    "text": "Well,",
                    "start": 282410,
                    "end": 282990,
                    "confidence": 0.84933,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 283100,
                    "end": 283302,
                    "confidence": 0.99428,
                    "speaker": "B"
                },
                {
                    "text": "first",
                    "start": 283316,
                    "end": 283458,
                    "confidence": 0.9992,
                    "speaker": "B"
                },
                {
                    "text": "thing",
                    "start": 283484,
                    "end": 283602,
                    "confidence": 0.99741,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 283616,
                    "end": 283758,
                    "confidence": 0.99225,
                    "speaker": "B"
                },
                {
                    "text": "say",
                    "start": 283784,
                    "end": 284046,
                    "confidence": 0.99926,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 284108,
                    "end": 284262,
                    "confidence": 0.99411,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 284276,
                    "end": 284418,
                    "confidence": 0.96335,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 284444,
                    "end": 284562,
                    "confidence": 0.90915,
                    "speaker": "B"
                },
                {
                    "text": "our",
                    "start": 284576,
                    "end": 284754,
                    "confidence": 0.51742,
                    "speaker": "B"
                },
                {
                    "text": "knowledge,",
                    "start": 284792,
                    "end": 285094,
                    "confidence": 0.97222,
                    "speaker": "B"
                },
                {
                    "text": "it's",
                    "start": 285142,
                    "end": 285286,
                    "confidence": 0.43867,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 285298,
                    "end": 285438,
                    "confidence": 0.64434,
                    "speaker": "B"
                },
                {
                    "text": "first",
                    "start": 285464,
                    "end": 285690,
                    "confidence": 0.99959,
                    "speaker": "B"
                },
                {
                    "text": "application",
                    "start": 285740,
                    "end": 286350,
                    "confidence": 0.99938,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 286520,
                    "end": 286890,
                    "confidence": 0.99131,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 286940,
                    "end": 287118,
                    "confidence": 0.96501,
                    "speaker": "B"
                },
                {
                    "text": "#######",
                    "start": 287144,
                    "end": 287398,
                    "confidence": 0.99961,
                    "speaker": "B"
                },
                {
                    "text": "########",
                    "start": 287434,
                    "end": 287706,
                    "confidence": 0.90056,
                    "speaker": "B"
                },
                {
                    "text": "algorithm",
                    "start": 287768,
                    "end": 288334,
                    "confidence": 0.92266,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 288382,
                    "end": 288558,
                    "confidence": 0.80794,
                    "speaker": "B"
                },
                {
                    "text": "##########",
                    "start": 288584,
                    "end": 288982,
                    "confidence": 0.99925,
                    "speaker": "B"
                },
                {
                    "text": "############",
                    "start": 289006,
                    "end": 289534,
                    "confidence": 0.66297,
                    "speaker": "B"
                },
                {
                    "text": "method",
                    "start": 289582,
                    "end": 290002,
                    "confidence": 0.9856,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 290086,
                    "end": 290298,
                    "confidence": 0.626,
                    "speaker": "B"
                },
                {
                    "text": "such",
                    "start": 290324,
                    "end": 290514,
                    "confidence": 0.99644,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 290552,
                    "end": 290682,
                    "confidence": 0.99323,
                    "speaker": "B"
                },
                {
                    "text": "big",
                    "start": 290696,
                    "end": 290874,
                    "confidence": 0.9993,
                    "speaker": "B"
                },
                {
                    "text": "data",
                    "start": 290912,
                    "end": 291114,
                    "confidence": 0.99957,
                    "speaker": "B"
                },
                {
                    "text": "set.",
                    "start": 291152,
                    "end": 291354,
                    "confidence": 0.72616,
                    "speaker": "B"
                },
                {
                    "text": "To",
                    "start": 291392,
                    "end": 291558,
                    "confidence": 0.98006,
                    "speaker": "B"
                },
                {
                    "text": "answer",
                    "start": 291584,
                    "end": 291810,
                    "confidence": 0.99866,
                    "speaker": "B"
                },
                {
                    "text": "this",
                    "start": 291860,
                    "end": 292074,
                    "confidence": 0.96456,
                    "speaker": "B"
                },
                {
                    "text": "question,",
                    "start": 292112,
                    "end": 292710,
                    "confidence": 0.99686,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 292880,
                    "end": 293250,
                    "confidence": 0.52093,
                    "speaker": "B"
                },
                {
                    "text": "one",
                    "start": 293300,
                    "end": 293442,
                    "confidence": 0.96504,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 293456,
                    "end": 293562,
                    "confidence": 0.99873,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 293576,
                    "end": 293718,
                    "confidence": 0.99684,
                    "speaker": "B"
                },
                {
                    "text": "key",
                    "start": 293744,
                    "end": 293934,
                    "confidence": 0.99951,
                    "speaker": "B"
                },
                {
                    "text": "things",
                    "start": 293972,
                    "end": 294210,
                    "confidence": 0.99653,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 294260,
                    "end": 294474,
                    "confidence": 0.9888,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 294512,
                    "end": 294642,
                    "confidence": 0.9719,
                    "speaker": "B"
                },
                {
                    "text": "knew,",
                    "start": 294656,
                    "end": 295194,
                    "confidence": 0.93984,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 295352,
                    "end": 295710,
                    "confidence": 0.97083,
                    "speaker": "B"
                },
                {
                    "text": "Survival",
                    "start": 295760,
                    "end": 296182,
                    "confidence": 0.94872,
                    "speaker": "B"
                },
                {
                    "text": "Quilts",
                    "start": 296206,
                    "end": 296782,
                    "confidence": 0.34928,
                    "speaker": "B"
                },
                {
                    "text": "model,",
                    "start": 296866,
                    "end": 297222,
                    "confidence": 0.9688,
                    "speaker": "B"
                },
                {
                    "text": "like",
                    "start": 297296,
                    "end": 297786,
                    "confidence": 0.8967,
                    "speaker": "B"
                },
                {
                    "text": "other",
                    "start": 297908,
                    "end": 298158,
                    "confidence": 0.99476,
                    "speaker": "B"
                },
                {
                    "text": "machine",
                    "start": 298184,
                    "end": 298462,
                    "confidence": 0.98186,
                    "speaker": "B"
                },
                {
                    "text": "models,",
                    "start": 298486,
                    "end": 298798,
                    "confidence": 0.732,
                    "speaker": "B"
                },
                {
                    "text": "can",
                    "start": 298834,
                    "end": 298998,
                    "confidence": 0.99794,
                    "speaker": "B"
                },
                {
                    "text": "actually",
                    "start": 299024,
                    "end": 299394,
                    "confidence": 0.99679,
                    "speaker": "B"
                },
                {
                    "text": "take",
                    "start": 299492,
                    "end": 299754,
                    "confidence": 0.99917,
                    "speaker": "B"
                },
                {
                    "text": "new",
                    "start": 299792,
                    "end": 300030,
                    "confidence": 0.99906,
                    "speaker": "B"
                },
                {
                    "text": "information",
                    "start": 300080,
                    "end": 300438,
                    "confidence": 0.99976,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 300524,
                    "end": 300774,
                    "confidence": 0.72961,
                    "speaker": "B"
                },
                {
                    "text": "process",
                    "start": 300812,
                    "end": 301050,
                    "confidence": 0.99986,
                    "speaker": "B"
                },
                {
                    "text": "it",
                    "start": 301100,
                    "end": 301278,
                    "confidence": 0.90016,
                    "speaker": "B"
                },
                {
                    "text": "very",
                    "start": 301304,
                    "end": 301494,
                    "confidence": 0.99783,
                    "speaker": "B"
                },
                {
                    "text": "quickly,",
                    "start": 301532,
                    "end": 301878,
                    "confidence": 0.99934,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 301964,
                    "end": 302142,
                    "confidence": 0.69573,
                    "speaker": "B"
                },
                {
                    "text": "it",
                    "start": 302156,
                    "end": 302262,
                    "confidence": 0.98501,
                    "speaker": "B"
                },
                {
                    "text": "took",
                    "start": 302276,
                    "end": 302418,
                    "confidence": 0.9992,
                    "speaker": "B"
                },
                {
                    "text": "us",
                    "start": 302444,
                    "end": 302598,
                    "confidence": 0.98691,
                    "speaker": "B"
                },
                {
                    "text": "many",
                    "start": 302624,
                    "end": 302850,
                    "confidence": 0.99892,
                    "speaker": "B"
                },
                {
                    "text": "years",
                    "start": 302900,
                    "end": 303330,
                    "confidence": 0.99978,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 303440,
                    "end": 303678,
                    "confidence": 0.99817,
                    "speaker": "B"
                },
                {
                    "text": "develop",
                    "start": 303704,
                    "end": 303966,
                    "confidence": 0.99977,
                    "speaker": "B"
                },
                {
                    "text": "our",
                    "start": 304028,
                    "end": 304182,
                    "confidence": 0.71809,
                    "speaker": "B"
                },
                {
                    "text": "previous",
                    "start": 304196,
                    "end": 304510,
                    "confidence": 0.96224,
                    "speaker": "B"
                },
                {
                    "text": "models.",
                    "start": 304570,
                    "end": 304942,
                    "confidence": 0.95804,
                    "speaker": "B"
                },
                {
                    "text": "But",
                    "start": 304966,
                    "end": 305118,
                    "confidence": 0.93998,
                    "speaker": "B"
                },
                {
                    "text": "I",
                    "start": 305144,
                    "end": 305262,
                    "confidence": 0.99384,
                    "speaker": "B"
                },
                {
                    "text": "think",
                    "start": 305276,
                    "end": 305634,
                    "confidence": 0.99952,
                    "speaker": "B"
                },
                {
                    "text": "working",
                    "start": 305732,
                    "end": 306066,
                    "confidence": 0.99954,
                    "speaker": "B"
                },
                {
                    "text": "with",
                    "start": 306128,
                    "end": 306462,
                    "confidence": 0.99925,
                    "speaker": "B"
                },
                {
                    "text": "#######,",
                    "start": 306536,
                    "end": 307330,
                    "confidence": 0.07732,
                    "speaker": "B"
                },
                {
                    "text": "one",
                    "start": 307450,
                    "end": 307662,
                    "confidence": 0.99574,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 307676,
                    "end": 307782,
                    "confidence": 0.99931,
                    "speaker": "B"
                },
                {
                    "text": "your",
                    "start": 307796,
                    "end": 307974,
                    "confidence": 0.99045,
                    "speaker": "B"
                },
                {
                    "text": "########,",
                    "start": 308012,
                    "end": 308358,
                    "confidence": 0.54271,
                    "speaker": "B"
                },
                {
                    "text": "with",
                    "start": 308444,
                    "end": 308622,
                    "confidence": 0.48333,
                    "speaker": "B"
                },
                {
                    "text": "#######,",
                    "start": 308636,
                    "end": 309034,
                    "confidence": 0.11646,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 309082,
                    "end": 309258,
                    "confidence": 0.98807,
                    "speaker": "B"
                },
                {
                    "text": "were",
                    "start": 309284,
                    "end": 309438,
                    "confidence": 0.99197,
                    "speaker": "B"
                },
                {
                    "text": "able",
                    "start": 309464,
                    "end": 309618,
                    "confidence": 0.99998,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 309644,
                    "end": 309762,
                    "confidence": 0.99895,
                    "speaker": "B"
                },
                {
                    "text": "produce",
                    "start": 309776,
                    "end": 310078,
                    "confidence": 0.90505,
                    "speaker": "B"
                },
                {
                    "text": "this",
                    "start": 310114,
                    "end": 310278,
                    "confidence": 0.97821,
                    "speaker": "B"
                },
                {
                    "text": "model",
                    "start": 310304,
                    "end": 310530,
                    "confidence": 0.99689,
                    "speaker": "B"
                },
                {
                    "text": "very",
                    "start": 310580,
                    "end": 310794,
                    "confidence": 0.99883,
                    "speaker": "B"
                },
                {
                    "text": "rapidly",
                    "start": 310832,
                    "end": 311278,
                    "confidence": 0.90232,
                    "speaker": "B"
                },
                {
                    "text": "because",
                    "start": 311314,
                    "end": 311550,
                    "confidence": 0.99583,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 311600,
                    "end": 311742,
                    "confidence": 0.88246,
                    "speaker": "B"
                },
                {
                    "text": "technique",
                    "start": 311756,
                    "end": 312094,
                    "confidence": 0.54176,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 312142,
                    "end": 312318,
                    "confidence": 0.98359,
                    "speaker": "B"
                },
                {
                    "text": "already",
                    "start": 312344,
                    "end": 312606,
                    "confidence": 0.98674,
                    "speaker": "B"
                },
                {
                    "text": "there.",
                    "start": 312668,
                    "end": 313110,
                    "confidence": 0.96946,
                    "speaker": "B"
                },
                {
                    "text": "And",
                    "start": 313220,
                    "end": 313458,
                    "confidence": 0.76545,
                    "speaker": "B"
                },
                {
                    "text": "even",
                    "start": 313484,
                    "end": 313710,
                    "confidence": 0.99938,
                    "speaker": "B"
                },
                {
                    "text": "in",
                    "start": 313760,
                    "end": 313902,
                    "confidence": 0.99714,
                    "speaker": "B"
                },
                {
                    "text": "this",
                    "start": 313916,
                    "end": 314058,
                    "confidence": 0.93763,
                    "speaker": "B"
                },
                {
                    "text": "first",
                    "start": 314084,
                    "end": 314274,
                    "confidence": 0.79629,
                    "speaker": "B"
                },
                {
                    "text": "situation",
                    "start": 314312,
                    "end": 314802,
                    "confidence": 0.9808,
                    "speaker": "B"
                },
                {
                    "text": "where",
                    "start": 314936,
                    "end": 315198,
                    "confidence": 0.98955,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 315224,
                    "end": 315378,
                    "confidence": 0.99163,
                    "speaker": "B"
                },
                {
                    "text": "had",
                    "start": 315404,
                    "end": 315630,
                    "confidence": 0.82924,
                    "speaker": "B"
                },
                {
                    "text": "fairly",
                    "start": 315680,
                    "end": 315958,
                    "confidence": 0.28681,
                    "speaker": "B"
                },
                {
                    "text": "standard",
                    "start": 315994,
                    "end": 316338,
                    "confidence": 0.99243,
                    "speaker": "B"
                },
                {
                    "text": "variables,",
                    "start": 316424,
                    "end": 317362,
                    "confidence": 0.23817,
                    "speaker": "B"
                },
                {
                    "text": "I",
                    "start": 317506,
                    "end": 317742,
                    "confidence": 0.99693,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 317756,
                    "end": 317898,
                    "confidence": 0.99879,
                    "speaker": "B"
                },
                {
                    "text": "amazed",
                    "start": 317924,
                    "end": 318262,
                    "confidence": 0.94929,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 318286,
                    "end": 318366,
                    "confidence": 0.96271,
                    "speaker": "B"
                },
                {
                    "text": "find",
                    "start": 318368,
                    "end": 318570,
                    "confidence": 0.5457,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 318620,
                    "end": 318834,
                    "confidence": 0.58303,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 318872,
                    "end": 319460,
                    "confidence": 0.93129,
                    "speaker": "B"
                },
                {
                    "text": "final",
                    "start": 319850,
                    "end": 320250,
                    "confidence": 0.63178,
                    "speaker": "B"
                },
                {
                    "text": "Quilts",
                    "start": 320300,
                    "end": 320602,
                    "confidence": 0.3245,
                    "speaker": "B"
                },
                {
                    "text": "model",
                    "start": 320626,
                    "end": 320850,
                    "confidence": 0.99607,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 320900,
                    "end": 321078,
                    "confidence": 0.99387,
                    "speaker": "B"
                },
                {
                    "text": "able",
                    "start": 321104,
                    "end": 321294,
                    "confidence": 0.99975,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 321332,
                    "end": 321534,
                    "confidence": 0.99145,
                    "speaker": "B"
                },
                {
                    "text": "come",
                    "start": 321572,
                    "end": 321702,
                    "confidence": 0.99929,
                    "speaker": "B"
                },
                {
                    "text": "up",
                    "start": 321716,
                    "end": 321822,
                    "confidence": 0.68176,
                    "speaker": "B"
                },
                {
                    "text": "with",
                    "start": 321836,
                    "end": 321978,
                    "confidence": 0.56416,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 322004,
                    "end": 322122,
                    "confidence": 0.93016,
                    "speaker": "B"
                },
                {
                    "text": "performance",
                    "start": 322136,
                    "end": 322534,
                    "confidence": 0.99913,
                    "speaker": "B"
                },
                {
                    "text": "characteristic",
                    "start": 322582,
                    "end": 323506,
                    "confidence": 0.61165,
                    "speaker": "B"
                },
                {
                    "text": "which",
                    "start": 323638,
                    "end": 323898,
                    "confidence": 0.99927,
                    "speaker": "B"
                },
                {
                    "text": "was",
                    "start": 323924,
                    "end": 324078,
                    "confidence": 0.99485,
                    "speaker": "B"
                },
                {
                    "text": "as",
                    "start": 324104,
                    "end": 324258,
                    "confidence": 0.98651,
                    "speaker": "B"
                },
                {
                    "text": "good",
                    "start": 324284,
                    "end": 324474,
                    "confidence": 0.99883,
                    "speaker": "B"
                },
                {
                    "text": "as",
                    "start": 324512,
                    "end": 324714,
                    "confidence": 0.57197,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 324752,
                    "end": 324918,
                    "confidence": 0.98522,
                    "speaker": "B"
                },
                {
                    "text": "best",
                    "start": 324944,
                    "end": 325098,
                    "confidence": 0.99654,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 325124,
                    "end": 325278,
                    "confidence": 0.9669,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 325304,
                    "end": 325458,
                    "confidence": 0.99619,
                    "speaker": "B"
                },
                {
                    "text": "have",
                    "start": 325484,
                    "end": 325710,
                    "confidence": 0.12181,
                    "speaker": "B"
                },
                {
                    "text": "from",
                    "start": 325760,
                    "end": 325902,
                    "confidence": 0.99363,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 325916,
                    "end": 326022,
                    "confidence": 0.68503,
                    "speaker": "B"
                },
                {
                    "text": "clinical",
                    "start": 326036,
                    "end": 326410,
                    "confidence": 0.72577,
                    "speaker": "B"
                },
                {
                    "text": "standpoint",
                    "start": 326470,
                    "end": 327058,
                    "confidence": 0.5579,
                    "speaker": "B"
                },
                {
                    "text": "and",
                    "start": 327154,
                    "end": 327378,
                    "confidence": 0.90144,
                    "speaker": "B"
                },
                {
                    "text": "actually",
                    "start": 327404,
                    "end": 327594,
                    "confidence": 0.98338,
                    "speaker": "B"
                },
                {
                    "text": "a",
                    "start": 327632,
                    "end": 327726,
                    "confidence": 0.99313,
                    "speaker": "B"
                },
                {
                    "text": "little",
                    "start": 327728,
                    "end": 327858,
                    "confidence": 0.98413,
                    "speaker": "B"
                },
                {
                    "text": "bit",
                    "start": 327884,
                    "end": 328038,
                    "confidence": 0.99854,
                    "speaker": "B"
                },
                {
                    "text": "better",
                    "start": 328064,
                    "end": 328326,
                    "confidence": 0.99965,
                    "speaker": "B"
                },
                {
                    "text": "as",
                    "start": 328388,
                    "end": 328542,
                    "confidence": 0.99692,
                    "speaker": "B"
                },
                {
                    "text": "well.",
                    "start": 328556,
                    "end": 328914,
                    "confidence": 0.99407,
                    "speaker": "B"
                },
                {
                    "text": "So",
                    "start": 329012,
                    "end": 329454,
                    "confidence": 0.7025,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 329552,
                    "end": 329778,
                    "confidence": 0.99646,
                    "speaker": "B"
                },
                {
                    "text": "is",
                    "start": 329804,
                    "end": 330030,
                    "confidence": 0.98744,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 330080,
                    "end": 330222,
                    "confidence": 0.93588,
                    "speaker": "B"
                },
                {
                    "text": "me",
                    "start": 330236,
                    "end": 330486,
                    "confidence": 0.9944,
                    "speaker": "B"
                },
                {
                    "text": "an",
                    "start": 330548,
                    "end": 330702,
                    "confidence": 0.81149,
                    "speaker": "B"
                },
                {
                    "text": "illustration",
                    "start": 330716,
                    "end": 331222,
                    "confidence": 0.69155,
                    "speaker": "B"
                },
                {
                    "text": "that",
                    "start": 331306,
                    "end": 331482,
                    "confidence": 0.78467,
                    "speaker": "B"
                },
                {
                    "text": "the",
                    "start": 331496,
                    "end": 331602,
                    "confidence": 0.98378,
                    "speaker": "B"
                },
                {
                    "text": "direction",
                    "start": 331616,
                    "end": 332002,
                    "confidence": 0.55285,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 332026,
                    "end": 332178,
                    "confidence": 0.98446,
                    "speaker": "B"
                },
                {
                    "text": "travel",
                    "start": 332204,
                    "end": 332502,
                    "confidence": 0.99922,
                    "speaker": "B"
                },
                {
                    "text": "or",
                    "start": 332576,
                    "end": 332742,
                    "confidence": 0.79056,
                    "speaker": "B"
                },
                {
                    "text": "what",
                    "start": 332756,
                    "end": 332898,
                    "confidence": 0.99712,
                    "speaker": "B"
                },
                {
                    "text": "we",
                    "start": 332924,
                    "end": 333042,
                    "confidence": 0.99037,
                    "speaker": "B"
                },
                {
                    "text": "do",
                    "start": 333056,
                    "end": 333162,
                    "confidence": 0.99807,
                    "speaker": "B"
                },
                {
                    "text": "in",
                    "start": 333176,
                    "end": 333318,
                    "confidence": 0.9933,
                    "speaker": "B"
                },
                {
                    "text": "future",
                    "start": 333344,
                    "end": 333786,
                    "confidence": 0.99745,
                    "speaker": "B"
                },
                {
                    "text": "has",
                    "start": 333908,
                    "end": 334194,
                    "confidence": 0.9917,
                    "speaker": "B"
                },
                {
                    "text": "to",
                    "start": 334232,
                    "end": 334362,
                    "confidence": 0.99796,
                    "speaker": "B"
                },
                {
                    "text": "be",
                    "start": 334376,
                    "end": 334554,
                    "confidence": 0.99363,
                    "speaker": "B"
                },
                {
                    "text": "using",
                    "start": 334592,
                    "end": 334830,
                    "confidence": 0.97078,
                    "speaker": "B"
                },
                {
                    "text": "this",
                    "start": 334880,
                    "end": 335058,
                    "confidence": 0.98196,
                    "speaker": "B"
                },
                {
                    "text": "kind",
                    "start": 335084,
                    "end": 335202,
                    "confidence": 0.97223,
                    "speaker": "B"
                },
                {
                    "text": "of",
                    "start": 335216,
                    "end": 335430,
                    "confidence": 0.9983,
                    "speaker": "B"
                },
                {
                    "text": "technology.",
                    "start": 335480,
                    "end": 336080,
                    "confidence": 0.99818,
                    "speaker": "B"
                }
            ]
        },
        {
            "confidence": 0.8857455000000001,
            "end": 360450,
            "speaker": "A",
            "start": 336890,
            "text": "That's a great summary. You've mentioned in the paper that the Seared data set doesn't include data on comorbidity, ########, ###, nor treatment, although potentially this could improve the prognostic capabilities of the model. So how easy would it be to incorporate these variables in the future and what effect do you think that could have on the model output?",
            "words": [
                {
                    "text": "That's",
                    "start": 336890,
                    "end": 337282,
                    "confidence": 0.86583,
                    "speaker": "A"
                },
                {
                    "text": "a",
                    "start": 337306,
                    "end": 337422,
                    "confidence": 0.99548,
                    "speaker": "A"
                },
                {
                    "text": "great",
                    "start": 337436,
                    "end": 337650,
                    "confidence": 0.99977,
                    "speaker": "A"
                },
                {
                    "text": "summary.",
                    "start": 337700,
                    "end": 338490,
                    "confidence": 0.72954,
                    "speaker": "A"
                },
                {
                    "text": "You've",
                    "start": 338930,
                    "end": 339262,
                    "confidence": 0.20057,
                    "speaker": "A"
                },
                {
                    "text": "mentioned",
                    "start": 339286,
                    "end": 339690,
                    "confidence": 0.90394,
                    "speaker": "A"
                },
                {
                    "text": "in",
                    "start": 339800,
                    "end": 340002,
                    "confidence": 0.99932,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 340016,
                    "end": 340158,
                    "confidence": 0.99776,
                    "speaker": "A"
                },
                {
                    "text": "paper",
                    "start": 340184,
                    "end": 340446,
                    "confidence": 0.9999,
                    "speaker": "A"
                },
                {
                    "text": "that",
                    "start": 340508,
                    "end": 340734,
                    "confidence": 0.99608,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 340772,
                    "end": 341262,
                    "confidence": 0.51872,
                    "speaker": "A"
                },
                {
                    "text": "Seared",
                    "start": 341396,
                    "end": 341794,
                    "confidence": 0.71966,
                    "speaker": "A"
                },
                {
                    "text": "data",
                    "start": 341842,
                    "end": 342090,
                    "confidence": 0.99618,
                    "speaker": "A"
                },
                {
                    "text": "set",
                    "start": 342140,
                    "end": 342534,
                    "confidence": 0.75696,
                    "speaker": "A"
                },
                {
                    "text": "doesn't",
                    "start": 342632,
                    "end": 342958,
                    "confidence": 0.95461,
                    "speaker": "A"
                },
                {
                    "text": "include",
                    "start": 342994,
                    "end": 343378,
                    "confidence": 0.99931,
                    "speaker": "A"
                },
                {
                    "text": "data",
                    "start": 343414,
                    "end": 343722,
                    "confidence": 0.99921,
                    "speaker": "A"
                },
                {
                    "text": "on",
                    "start": 343796,
                    "end": 344178,
                    "confidence": 0.99814,
                    "speaker": "A"
                },
                {
                    "text": "comorbidity,",
                    "start": 344264,
                    "end": 345262,
                    "confidence": 0.47473,
                    "speaker": "A"
                },
                {
                    "text": "########,",
                    "start": 345346,
                    "end": 345850,
                    "confidence": 0.97148,
                    "speaker": "A"
                },
                {
                    "text": "###,",
                    "start": 345910,
                    "end": 346630,
                    "confidence": 0.96219,
                    "speaker": "A"
                },
                {
                    "text": "nor",
                    "start": 346750,
                    "end": 347070,
                    "confidence": 0.90028,
                    "speaker": "A"
                },
                {
                    "text": "treatment,",
                    "start": 347120,
                    "end": 347842,
                    "confidence": 0.96581,
                    "speaker": "A"
                },
                {
                    "text": "although",
                    "start": 347986,
                    "end": 348574,
                    "confidence": 0.95037,
                    "speaker": "A"
                },
                {
                    "text": "potentially",
                    "start": 348682,
                    "end": 349366,
                    "confidence": 0.9956,
                    "speaker": "A"
                },
                {
                    "text": "this",
                    "start": 349498,
                    "end": 349794,
                    "confidence": 0.08142,
                    "speaker": "A"
                },
                {
                    "text": "could",
                    "start": 349832,
                    "end": 349998,
                    "confidence": 0.98911,
                    "speaker": "A"
                },
                {
                    "text": "improve",
                    "start": 350024,
                    "end": 350302,
                    "confidence": 0.99752,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 350326,
                    "end": 350478,
                    "confidence": 0.85772,
                    "speaker": "A"
                },
                {
                    "text": "prognostic",
                    "start": 350504,
                    "end": 351058,
                    "confidence": 0.63368,
                    "speaker": "A"
                },
                {
                    "text": "capabilities",
                    "start": 351094,
                    "end": 351670,
                    "confidence": 0.57497,
                    "speaker": "A"
                },
                {
                    "text": "of",
                    "start": 351730,
                    "end": 351882,
                    "confidence": 0.87812,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 351896,
                    "end": 352038,
                    "confidence": 0.74009,
                    "speaker": "A"
                },
                {
                    "text": "model.",
                    "start": 352064,
                    "end": 352614,
                    "confidence": 0.99659,
                    "speaker": "A"
                },
                {
                    "text": "So",
                    "start": 352772,
                    "end": 353058,
                    "confidence": 0.72534,
                    "speaker": "A"
                },
                {
                    "text": "how",
                    "start": 353084,
                    "end": 353346,
                    "confidence": 0.99934,
                    "speaker": "A"
                },
                {
                    "text": "easy",
                    "start": 353408,
                    "end": 353706,
                    "confidence": 0.99881,
                    "speaker": "A"
                },
                {
                    "text": "would",
                    "start": 353768,
                    "end": 353922,
                    "confidence": 0.99504,
                    "speaker": "A"
                },
                {
                    "text": "it",
                    "start": 353936,
                    "end": 354042,
                    "confidence": 0.99871,
                    "speaker": "A"
                },
                {
                    "text": "be",
                    "start": 354056,
                    "end": 354198,
                    "confidence": 0.99921,
                    "speaker": "A"
                },
                {
                    "text": "to",
                    "start": 354224,
                    "end": 354378,
                    "confidence": 0.99749,
                    "speaker": "A"
                },
                {
                    "text": "incorporate",
                    "start": 354404,
                    "end": 355090,
                    "confidence": 0.92748,
                    "speaker": "A"
                },
                {
                    "text": "these",
                    "start": 355150,
                    "end": 355590,
                    "confidence": 0.99541,
                    "speaker": "A"
                },
                {
                    "text": "variables",
                    "start": 355700,
                    "end": 356242,
                    "confidence": 0.59828,
                    "speaker": "A"
                },
                {
                    "text": "in",
                    "start": 356266,
                    "end": 356382,
                    "confidence": 0.99753,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 356396,
                    "end": 356538,
                    "confidence": 0.99411,
                    "speaker": "A"
                },
                {
                    "text": "future",
                    "start": 356564,
                    "end": 357006,
                    "confidence": 0.99982,
                    "speaker": "A"
                },
                {
                    "text": "and",
                    "start": 357128,
                    "end": 357414,
                    "confidence": 0.80505,
                    "speaker": "A"
                },
                {
                    "text": "what",
                    "start": 357452,
                    "end": 357654,
                    "confidence": 0.99957,
                    "speaker": "A"
                },
                {
                    "text": "effect",
                    "start": 357692,
                    "end": 357966,
                    "confidence": 0.98779,
                    "speaker": "A"
                },
                {
                    "text": "do",
                    "start": 358028,
                    "end": 358182,
                    "confidence": 0.93255,
                    "speaker": "A"
                },
                {
                    "text": "you",
                    "start": 358196,
                    "end": 358302,
                    "confidence": 0.99691,
                    "speaker": "A"
                },
                {
                    "text": "think",
                    "start": 358316,
                    "end": 358458,
                    "confidence": 0.99949,
                    "speaker": "A"
                },
                {
                    "text": "that",
                    "start": 358484,
                    "end": 358638,
                    "confidence": 0.8648,
                    "speaker": "A"
                },
                {
                    "text": "could",
                    "start": 358664,
                    "end": 358818,
                    "confidence": 0.83495,
                    "speaker": "A"
                },
                {
                    "text": "have",
                    "start": 358844,
                    "end": 358998,
                    "confidence": 0.99712,
                    "speaker": "A"
                },
                {
                    "text": "on",
                    "start": 359024,
                    "end": 359142,
                    "confidence": 0.99459,
                    "speaker": "A"
                },
                {
                    "text": "the",
                    "start": 359156,
                    "end": 359262,
                    "confidence": 0.98113,
                    "speaker": "A"
                },
                {
                    "text": "model",
                    "start": 359276,
                    "end": 359562,
                    "confidence": 0.99711,
                    "speaker": "A"
                },
                {
                    "text": "output?",
                    "start": 359636,
                    "end": 360450,
                    "confidence": 0.92644,
                    "speaker": "A"
                }
            ]
        },
        {
            "confidence": 0.8796536842105267,
            "end": 419710,
            "speaker": "C",
            "start": 361070,
            "text": "One advantage of survival quiz is that it's an automated ####### ######## method. It is able to take whatever data we throw at it and craft a model with good discrimination and calibration performance. So it is very easy to push off a button if this data becomes available to integrate additional variables into the mobile to issue this prediction. So in terms of building a new model on the basis of new variables, that is, I think, very easy. What is important about these methods such as survival quilts, is that not only we are able to assess the advantage and the value of what I call the value of modeling, identifying which methods are best for what type of data and what type of patients, but also the value of information. So we are going to learn in a data driven way on the basis of the data, what variables.",
            "words": [
                {
                    "text": "One",
                    "start": 361070,
                    "end": 361434,
                    "confidence": 0.89957,
                    "speaker": "C"
                },
                {
                    "text": "advantage",
                    "start": 361472,
                    "end": 362182,
                    "confidence": 0.98216,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 362266,
                    "end": 362478,
                    "confidence": 0.99705,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 362504,
                    "end": 363094,
                    "confidence": 0.53163,
                    "speaker": "C"
                },
                {
                    "text": "quiz",
                    "start": 363142,
                    "end": 363778,
                    "confidence": 0.86223,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 363934,
                    "end": 364326,
                    "confidence": 0.96661,
                    "speaker": "C"
                },
                {
                    "text": "that",
                    "start": 364388,
                    "end": 364650,
                    "confidence": 0.98341,
                    "speaker": "C"
                },
                {
                    "text": "it's",
                    "start": 364700,
                    "end": 364918,
                    "confidence": 0.22646,
                    "speaker": "C"
                },
                {
                    "text": "an",
                    "start": 364954,
                    "end": 365298,
                    "confidence": 0.97559,
                    "speaker": "C"
                },
                {
                    "text": "automated",
                    "start": 365384,
                    "end": 366046,
                    "confidence": 0.99684,
                    "speaker": "C"
                },
                {
                    "text": "#######",
                    "start": 366118,
                    "end": 366418,
                    "confidence": 0.99625,
                    "speaker": "C"
                },
                {
                    "text": "########",
                    "start": 366454,
                    "end": 366762,
                    "confidence": 0.99972,
                    "speaker": "C"
                },
                {
                    "text": "method.",
                    "start": 366836,
                    "end": 367246,
                    "confidence": 0.9981,
                    "speaker": "C"
                },
                {
                    "text": "It",
                    "start": 367318,
                    "end": 367518,
                    "confidence": 0.974,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 367544,
                    "end": 367734,
                    "confidence": 0.68758,
                    "speaker": "C"
                },
                {
                    "text": "able",
                    "start": 367772,
                    "end": 368082,
                    "confidence": 0.99879,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 368156,
                    "end": 368394,
                    "confidence": 0.99871,
                    "speaker": "C"
                },
                {
                    "text": "take",
                    "start": 368432,
                    "end": 368670,
                    "confidence": 0.99909,
                    "speaker": "C"
                },
                {
                    "text": "whatever",
                    "start": 368720,
                    "end": 369114,
                    "confidence": 0.99488,
                    "speaker": "C"
                },
                {
                    "text": "data",
                    "start": 369212,
                    "end": 369654,
                    "confidence": 0.99801,
                    "speaker": "C"
                },
                {
                    "text": "we",
                    "start": 369752,
                    "end": 369978,
                    "confidence": 0.48468,
                    "speaker": "C"
                },
                {
                    "text": "throw",
                    "start": 370004,
                    "end": 370318,
                    "confidence": 0.55683,
                    "speaker": "C"
                },
                {
                    "text": "at",
                    "start": 370354,
                    "end": 370482,
                    "confidence": 0.56184,
                    "speaker": "C"
                },
                {
                    "text": "it",
                    "start": 370496,
                    "end": 370890,
                    "confidence": 0.96026,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 371000,
                    "end": 371346,
                    "confidence": 0.99602,
                    "speaker": "C"
                },
                {
                    "text": "craft",
                    "start": 371408,
                    "end": 371734,
                    "confidence": 0.99251,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 371782,
                    "end": 371922,
                    "confidence": 0.8653,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 371936,
                    "end": 372222,
                    "confidence": 0.75755,
                    "speaker": "C"
                },
                {
                    "text": "with",
                    "start": 372296,
                    "end": 372570,
                    "confidence": 0.9947,
                    "speaker": "C"
                },
                {
                    "text": "good",
                    "start": 372620,
                    "end": 372834,
                    "confidence": 0.93791,
                    "speaker": "C"
                },
                {
                    "text": "discrimination",
                    "start": 372872,
                    "end": 373642,
                    "confidence": 0.28624,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 373726,
                    "end": 373974,
                    "confidence": 0.5403,
                    "speaker": "C"
                },
                {
                    "text": "calibration",
                    "start": 374012,
                    "end": 374722,
                    "confidence": 0.71316,
                    "speaker": "C"
                },
                {
                    "text": "performance.",
                    "start": 374866,
                    "end": 375826,
                    "confidence": 0.79163,
                    "speaker": "C"
                },
                {
                    "text": "So",
                    "start": 376018,
                    "end": 376410,
                    "confidence": 0.7132,
                    "speaker": "C"
                },
                {
                    "text": "it",
                    "start": 376460,
                    "end": 376638,
                    "confidence": 0.99792,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 376664,
                    "end": 376926,
                    "confidence": 0.98525,
                    "speaker": "C"
                },
                {
                    "text": "very",
                    "start": 376988,
                    "end": 377358,
                    "confidence": 0.99761,
                    "speaker": "C"
                },
                {
                    "text": "easy",
                    "start": 377444,
                    "end": 377802,
                    "confidence": 0.8064,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 377876,
                    "end": 378078,
                    "confidence": 0.98666,
                    "speaker": "C"
                },
                {
                    "text": "push",
                    "start": 378104,
                    "end": 378298,
                    "confidence": 0.50648,
                    "speaker": "C"
                },
                {
                    "text": "off",
                    "start": 378334,
                    "end": 378498,
                    "confidence": 0.96985,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 378524,
                    "end": 378642,
                    "confidence": 0.96127,
                    "speaker": "C"
                },
                {
                    "text": "button",
                    "start": 378656,
                    "end": 378942,
                    "confidence": 0.57208,
                    "speaker": "C"
                },
                {
                    "text": "if",
                    "start": 379016,
                    "end": 379218,
                    "confidence": 0.98518,
                    "speaker": "C"
                },
                {
                    "text": "this",
                    "start": 379244,
                    "end": 379434,
                    "confidence": 0.83388,
                    "speaker": "C"
                },
                {
                    "text": "data",
                    "start": 379472,
                    "end": 379746,
                    "confidence": 0.99881,
                    "speaker": "C"
                },
                {
                    "text": "becomes",
                    "start": 379808,
                    "end": 380314,
                    "confidence": 0.98717,
                    "speaker": "C"
                },
                {
                    "text": "available",
                    "start": 380362,
                    "end": 380960,
                    "confidence": 0.99971,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 381410,
                    "end": 381810,
                    "confidence": 0.99856,
                    "speaker": "C"
                },
                {
                    "text": "integrate",
                    "start": 381860,
                    "end": 382486,
                    "confidence": 0.8262,
                    "speaker": "C"
                },
                {
                    "text": "additional",
                    "start": 382558,
                    "end": 383146,
                    "confidence": 0.97234,
                    "speaker": "C"
                },
                {
                    "text": "variables",
                    "start": 383278,
                    "end": 384142,
                    "confidence": 0.68849,
                    "speaker": "C"
                },
                {
                    "text": "into",
                    "start": 384226,
                    "end": 384546,
                    "confidence": 0.99444,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 384608,
                    "end": 384798,
                    "confidence": 0.99659,
                    "speaker": "C"
                },
                {
                    "text": "mobile",
                    "start": 384824,
                    "end": 385450,
                    "confidence": 0.5525,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 385570,
                    "end": 385854,
                    "confidence": 0.99702,
                    "speaker": "C"
                },
                {
                    "text": "issue",
                    "start": 385892,
                    "end": 386166,
                    "confidence": 0.98499,
                    "speaker": "C"
                },
                {
                    "text": "this",
                    "start": 386228,
                    "end": 386454,
                    "confidence": 0.61323,
                    "speaker": "C"
                },
                {
                    "text": "prediction.",
                    "start": 386492,
                    "end": 387070,
                    "confidence": 0.43128,
                    "speaker": "C"
                },
                {
                    "text": "So",
                    "start": 387130,
                    "end": 387354,
                    "confidence": 0.7565,
                    "speaker": "C"
                },
                {
                    "text": "in",
                    "start": 387392,
                    "end": 387522,
                    "confidence": 0.99939,
                    "speaker": "C"
                },
                {
                    "text": "terms",
                    "start": 387536,
                    "end": 387858,
                    "confidence": 0.99662,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 387944,
                    "end": 388626,
                    "confidence": 0.99927,
                    "speaker": "C"
                },
                {
                    "text": "building",
                    "start": 388808,
                    "end": 389298,
                    "confidence": 0.9995,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 389384,
                    "end": 389562,
                    "confidence": 0.97763,
                    "speaker": "C"
                },
                {
                    "text": "new",
                    "start": 389576,
                    "end": 389718,
                    "confidence": 0.99817,
                    "speaker": "C"
                },
                {
                    "text": "model",
                    "start": 389744,
                    "end": 390006,
                    "confidence": 0.91242,
                    "speaker": "C"
                },
                {
                    "text": "on",
                    "start": 390068,
                    "end": 390258,
                    "confidence": 0.71924,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 390284,
                    "end": 390402,
                    "confidence": 0.99424,
                    "speaker": "C"
                },
                {
                    "text": "basis",
                    "start": 390416,
                    "end": 390718,
                    "confidence": 0.99971,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 390754,
                    "end": 390918,
                    "confidence": 0.999,
                    "speaker": "C"
                },
                {
                    "text": "new",
                    "start": 390944,
                    "end": 391170,
                    "confidence": 0.99777,
                    "speaker": "C"
                },
                {
                    "text": "variables,",
                    "start": 391220,
                    "end": 392146,
                    "confidence": 0.94521,
                    "speaker": "C"
                },
                {
                    "text": "that",
                    "start": 392278,
                    "end": 392574,
                    "confidence": 0.99079,
                    "speaker": "C"
                },
                {
                    "text": "is,",
                    "start": 392612,
                    "end": 392778,
                    "confidence": 0.97364,
                    "speaker": "C"
                },
                {
                    "text": "I",
                    "start": 392804,
                    "end": 392922,
                    "confidence": 0.94796,
                    "speaker": "C"
                },
                {
                    "text": "think,",
                    "start": 392936,
                    "end": 393186,
                    "confidence": 0.7204,
                    "speaker": "C"
                },
                {
                    "text": "very",
                    "start": 393248,
                    "end": 393546,
                    "confidence": 0.99865,
                    "speaker": "C"
                },
                {
                    "text": "easy.",
                    "start": 393608,
                    "end": 394230,
                    "confidence": 0.99905,
                    "speaker": "C"
                },
                {
                    "text": "What",
                    "start": 394400,
                    "end": 394698,
                    "confidence": 0.54788,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 394724,
                    "end": 394914,
                    "confidence": 0.53175,
                    "speaker": "C"
                },
                {
                    "text": "important",
                    "start": 394952,
                    "end": 395262,
                    "confidence": 0.99992,
                    "speaker": "C"
                },
                {
                    "text": "about",
                    "start": 395336,
                    "end": 395574,
                    "confidence": 0.99847,
                    "speaker": "C"
                },
                {
                    "text": "these",
                    "start": 395612,
                    "end": 395814,
                    "confidence": 0.32889,
                    "speaker": "C"
                },
                {
                    "text": "methods",
                    "start": 395852,
                    "end": 396238,
                    "confidence": 0.76913,
                    "speaker": "C"
                },
                {
                    "text": "such",
                    "start": 396274,
                    "end": 396438,
                    "confidence": 0.99945,
                    "speaker": "C"
                },
                {
                    "text": "as",
                    "start": 396464,
                    "end": 396618,
                    "confidence": 0.9577,
                    "speaker": "C"
                },
                {
                    "text": "survival",
                    "start": 396644,
                    "end": 397114,
                    "confidence": 0.88637,
                    "speaker": "C"
                },
                {
                    "text": "quilts,",
                    "start": 397162,
                    "end": 397810,
                    "confidence": 0.1728,
                    "speaker": "C"
                },
                {
                    "text": "is",
                    "start": 397930,
                    "end": 398214,
                    "confidence": 0.67947,
                    "speaker": "C"
                },
                {
                    "text": "that",
                    "start": 398252,
                    "end": 398454,
                    "confidence": 0.9832,
                    "speaker": "C"
                },
                {
                    "text": "not",
                    "start": 398492,
                    "end": 398730,
                    "confidence": 0.99934,
                    "speaker": "C"
                },
                {
                    "text": "only",
                    "start": 398780,
                    "end": 399102,
                    "confidence": 0.97942,
                    "speaker": "C"
                },
                {
                    "text": "we",
                    "start": 399176,
                    "end": 399378,
                    "confidence": 0.98532,
                    "speaker": "C"
                },
                {
                    "text": "are",
                    "start": 399404,
                    "end": 399558,
                    "confidence": 0.98805,
                    "speaker": "C"
                },
                {
                    "text": "able",
                    "start": 399584,
                    "end": 399810,
                    "confidence": 0.99997,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 399860,
                    "end": 400074,
                    "confidence": 0.99942,
                    "speaker": "C"
                },
                {
                    "text": "assess",
                    "start": 400112,
                    "end": 400846,
                    "confidence": 0.53457,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 401038,
                    "end": 401610,
                    "confidence": 0.99008,
                    "speaker": "C"
                },
                {
                    "text": "advantage",
                    "start": 401720,
                    "end": 402610,
                    "confidence": 0.98518,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 402730,
                    "end": 403014,
                    "confidence": 0.95949,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 403052,
                    "end": 403254,
                    "confidence": 0.78183,
                    "speaker": "C"
                },
                {
                    "text": "value",
                    "start": 403292,
                    "end": 403638,
                    "confidence": 0.7973,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 403724,
                    "end": 403974,
                    "confidence": 0.97371,
                    "speaker": "C"
                },
                {
                    "text": "what",
                    "start": 404012,
                    "end": 404178,
                    "confidence": 0.96441,
                    "speaker": "C"
                },
                {
                    "text": "I",
                    "start": 404204,
                    "end": 404286,
                    "confidence": 0.96449,
                    "speaker": "C"
                },
                {
                    "text": "call",
                    "start": 404288,
                    "end": 404418,
                    "confidence": 0.89138,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 404444,
                    "end": 404598,
                    "confidence": 0.84968,
                    "speaker": "C"
                },
                {
                    "text": "value",
                    "start": 404624,
                    "end": 404814,
                    "confidence": 0.99887,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 404852,
                    "end": 405054,
                    "confidence": 0.99737,
                    "speaker": "C"
                },
                {
                    "text": "modeling,",
                    "start": 405092,
                    "end": 405790,
                    "confidence": 0.91793,
                    "speaker": "C"
                },
                {
                    "text": "identifying",
                    "start": 405910,
                    "end": 406846,
                    "confidence": 0.95533,
                    "speaker": "C"
                },
                {
                    "text": "which",
                    "start": 406918,
                    "end": 407262,
                    "confidence": 0.99959,
                    "speaker": "C"
                },
                {
                    "text": "methods",
                    "start": 407336,
                    "end": 407818,
                    "confidence": 0.81569,
                    "speaker": "C"
                },
                {
                    "text": "are",
                    "start": 407854,
                    "end": 408054,
                    "confidence": 0.99811,
                    "speaker": "C"
                },
                {
                    "text": "best",
                    "start": 408092,
                    "end": 408330,
                    "confidence": 0.99936,
                    "speaker": "C"
                },
                {
                    "text": "for",
                    "start": 408380,
                    "end": 408558,
                    "confidence": 0.99926,
                    "speaker": "C"
                },
                {
                    "text": "what",
                    "start": 408584,
                    "end": 408774,
                    "confidence": 0.86185,
                    "speaker": "C"
                },
                {
                    "text": "type",
                    "start": 408812,
                    "end": 408978,
                    "confidence": 0.99938,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 409004,
                    "end": 409194,
                    "confidence": 0.51465,
                    "speaker": "C"
                },
                {
                    "text": "data",
                    "start": 409232,
                    "end": 409506,
                    "confidence": 0.99899,
                    "speaker": "C"
                },
                {
                    "text": "and",
                    "start": 409568,
                    "end": 409722,
                    "confidence": 0.9505,
                    "speaker": "C"
                },
                {
                    "text": "what",
                    "start": 409736,
                    "end": 409878,
                    "confidence": 0.99247,
                    "speaker": "C"
                },
                {
                    "text": "type",
                    "start": 409904,
                    "end": 410058,
                    "confidence": 0.99925,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 410084,
                    "end": 410238,
                    "confidence": 0.99882,
                    "speaker": "C"
                },
                {
                    "text": "patients,",
                    "start": 410264,
                    "end": 410938,
                    "confidence": 0.41651,
                    "speaker": "C"
                },
                {
                    "text": "but",
                    "start": 411094,
                    "end": 411414,
                    "confidence": 0.95978,
                    "speaker": "C"
                },
                {
                    "text": "also",
                    "start": 411452,
                    "end": 411726,
                    "confidence": 0.99864,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 411788,
                    "end": 412050,
                    "confidence": 0.94916,
                    "speaker": "C"
                },
                {
                    "text": "value",
                    "start": 412100,
                    "end": 412314,
                    "confidence": 0.9991,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 412352,
                    "end": 412554,
                    "confidence": 0.73715,
                    "speaker": "C"
                },
                {
                    "text": "information.",
                    "start": 412592,
                    "end": 413180,
                    "confidence": 0.9631,
                    "speaker": "C"
                },
                {
                    "text": "So",
                    "start": 413690,
                    "end": 414054,
                    "confidence": 0.77737,
                    "speaker": "C"
                },
                {
                    "text": "we",
                    "start": 414092,
                    "end": 414222,
                    "confidence": 0.99757,
                    "speaker": "C"
                },
                {
                    "text": "are",
                    "start": 414236,
                    "end": 414378,
                    "confidence": 0.60889,
                    "speaker": "C"
                },
                {
                    "text": "going",
                    "start": 414404,
                    "end": 414630,
                    "confidence": 0.99903,
                    "speaker": "C"
                },
                {
                    "text": "to",
                    "start": 414680,
                    "end": 414930,
                    "confidence": 0.99875,
                    "speaker": "C"
                },
                {
                    "text": "learn",
                    "start": 414980,
                    "end": 415230,
                    "confidence": 0.99744,
                    "speaker": "C"
                },
                {
                    "text": "in",
                    "start": 415280,
                    "end": 415458,
                    "confidence": 0.93728,
                    "speaker": "C"
                },
                {
                    "text": "a",
                    "start": 415484,
                    "end": 415638,
                    "confidence": 0.96023,
                    "speaker": "C"
                },
                {
                    "text": "data",
                    "start": 415664,
                    "end": 415926,
                    "confidence": 0.99926,
                    "speaker": "C"
                },
                {
                    "text": "driven",
                    "start": 415988,
                    "end": 416374,
                    "confidence": 0.64747,
                    "speaker": "C"
                },
                {
                    "text": "way",
                    "start": 416422,
                    "end": 416598,
                    "confidence": 0.97553,
                    "speaker": "C"
                },
                {
                    "text": "on",
                    "start": 416624,
                    "end": 416742,
                    "confidence": 0.98057,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 416756,
                    "end": 416898,
                    "confidence": 0.99738,
                    "speaker": "C"
                },
                {
                    "text": "basis",
                    "start": 416924,
                    "end": 417274,
                    "confidence": 0.99977,
                    "speaker": "C"
                },
                {
                    "text": "of",
                    "start": 417322,
                    "end": 417462,
                    "confidence": 0.99881,
                    "speaker": "C"
                },
                {
                    "text": "the",
                    "start": 417476,
                    "end": 417654,
                    "confidence": 0.97795,
                    "speaker": "C"
                },
                {
                    "text": "data,",
                    "start": 417692,
                    "end": 418362,
                    "confidence": 0.99722,
                    "speaker": "C"
                },
                {
                    "text": "what",
                    "start": 418556,
                    "end": 419058,
                    "confidence": 0.99775,
                    "speaker": "C"
                },
                {
                    "text": "variables.",
                    "start": 419144,
                    "end": 419710,
                    "confidence": 0.55168,
                    "speaker": "C"
                }
            ]
        }
    ],
    "confidence": 0.880326952141058,
    "audio_duration": 420,
    "punctuate": True,
    "format_text": True,
    "dual_channel": None,
    "webhook_url": None,
    "webhook_status_code": None,
    "speed_boost": False,
    "auto_highlights_result": {
        "status": "success",
        "results": [
            {
                "count": 2,
                "rank": 0.09,
                "text": "numerous survival models",
                "timestamps": [
                    {
                        "start": 183404,
                        "end": 185182
                    },
                    {
                        "start": 189380,
                        "end": 191218
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.08,
                "text": "other machine models",
                "timestamps": [
                    {
                        "start": 297908,
                        "end": 298798
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.07,
                "text": "new variables",
                "timestamps": [
                    {
                        "start": 390944,
                        "end": 392146
                    }
                ]
            },
            {
                "count": 3,
                "rank": 0.06,
                "text": "survival quilts",
                "timestamps": [
                    {
                        "start": 196100,
                        "end": 197242
                    },
                    {
                        "start": 295760,
                        "end": 296782
                    },
                    {
                        "start": 396644,
                        "end": 397810
                    }
                ]
            },
            {
                "count": 2,
                "rank": 0.06,
                "text": "survival analysis",
                "timestamps": [
                    {
                        "start": 203612,
                        "end": 205054
                    },
                    {
                        "start": 255032,
                        "end": 256570
                    }
                ]
            },
            {
                "count": 2,
                "rank": 0.06,
                "text": "survival quiz",
                "timestamps": [
                    {
                        "start": 242600,
                        "end": 243598
                    },
                    {
                        "start": 362504,
                        "end": 363778
                    }
                ]
            },
            {
                "count": 12,
                "rank": 0.06,
                "text": "data",
                "timestamps": [
                    {
                        "start": 1940,
                        "end": 2190
                    },
                    {
                        "start": 194314,
                        "end": 194550
                    },
                    {
                        "start": 208558,
                        "end": 209190
                    },
                    {
                        "start": 246620,
                        "end": 246930
                    },
                    {
                        "start": 290912,
                        "end": 291114
                    },
                    {
                        "start": 341842,
                        "end": 342090
                    },
                    {
                        "start": 343414,
                        "end": 343722
                    },
                    {
                        "start": 369212,
                        "end": 369654
                    },
                    {
                        "start": 379472,
                        "end": 379746
                    },
                    {
                        "start": 409232,
                        "end": 409506
                    },
                    {
                        "start": 415664,
                        "end": 415926
                    },
                    {
                        "start": 417692,
                        "end": 418362
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.06,
                "text": "additional variables",
                "timestamps": [
                    {
                        "start": 382558,
                        "end": 384142
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.06,
                "text": "survival model",
                "timestamps": [
                    {
                        "start": 220676,
                        "end": 221622
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.06,
                "text": "new information",
                "timestamps": [
                    {
                        "start": 299792,
                        "end": 300438
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.06,
                "text": "different existing models",
                "timestamps": [
                    {
                        "start": 271532,
                        "end": 273154
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.06,
                "text": "nine other prognostic models",
                "timestamps": [
                    {
                        "start": 7904,
                        "end": 9322
                    }
                ]
            },
            {
                "count": 2,
                "rank": 0.05,
                "text": "many men",
                "timestamps": [
                    {
                        "start": 53014,
                        "end": 54006
                    },
                    {
                        "start": 55544,
                        "end": 56190
                    }
                ]
            },
            {
                "count": 1,
                "rank": 0.05,
                "text": "Survival Quilt based model",
                "timestamps": [
                    {
                        "start": 163244,
                        "end": 165198
                    }
                ]
            },
            {
                "count": 3,
                "rank": 0.05,
                "text": "best model",
                "timestamps": [
                    {
                        "start": 248672,
                        "end": 249882
                    },
                    {
                        "start": 250724,
                        "end": 251250
                    },
                    {
                        "start": 251624,
                        "end": 252474
                    }
                ]
            }
        ]
    },
    "auto_highlights": True,
    "audio_start_from": None,
    "audio_end_at": None,
    "word_boost": [],
    "boost_param": None,
    "filter_profanity": False,
    "redact_pii": True,
    "redact_pii_audio": False,
    "redact_pii_audio_quality": "mp3",
    "redact_pii_policies": [
        "medical_process",
        "medical_condition",
        "blood_type",
        "drug",
        "injury",
        "number_sequence",
        "email_address",
        "date_of_birth",
        "phone_number",
        "us_social_security_number",
        "credit_card_number",
        "credit_card_expiration",
        "credit_card_cvv",
        "date",
        "nationality",
        "event",
        "language",
        "location",
        "money_amount",
        "person_name",
        "person_age",
        "organization",
        "political_affiliation",
        "occupation",
        "religion"
    ],
    "redact_pii_sub": "hash",
    "speaker_labels": True,
    "content_safety": True,
    "iab_categories": True,
    "content_safety_labels": {
        "status": "success",
        "results": [
            {
                "text": "Quilt was developed using data from the Surveillance, Epidemiology and End Results program, or Steer for short and compared to nine other prognostic models that are currently used in the clinic.",
                "labels": [
                    {
                        "label": "health_issues",
                        "confidence": 0.8402819037437439,
                        "severity": 0.19532112777233124
                    }
                ],
                "timestamp": {
                    "start": 310,
                    "end": 11254
                }
            },
            {
                "text": "####### is a ######### of ####### ########, ########## ############, and ########, and ####### is both a ###### in ######### and a ########## ########### specializing in ######## ######.",
                "labels": [
                    {
                        "label": "profanity",
                        "confidence": 0.6018779873847961,
                        "severity": 0.20324434340000153
                    }
                ],
                "timestamp": {
                    "start": 21718,
                    "end": 32194
                }
            },
            {
                "text": "So just kind of going back to the start, can you give us a bit of background on this particular study and what perhaps was your inspiration for starting and what clinical problem were you hoping to address? ######## ###### is extremely common, as we all know, and the numbers are increasing.",
                "labels": [
                    {
                        "label": "health_issues",
                        "confidence": 0.7056734561920166,
                        "severity": 0.07802239060401917
                    }
                ],
                "timestamp": {
                    "start": 32362,
                    "end": 49258
                }
            },
            {
                "text": "And it's a funny disease because on the one hand, we know that it kills many men, but on the other hand, we also know that many men will have it and live with it and may not even know about. So trying to decide if you find a ######## ######, whether you need to treat it or not, is actually quite a complex problem.",
                "labels": [
                    {
                        "label": "health_issues",
                        "confidence": 0.9168835282325745,
                        "severity": 0.9589738845825195
                    }
                ],
                "timestamp": {
                    "start": 49414,
                    "end": 65178
                }
            },
            {
                "text": "So our primary interests driving the development of use from elastic tools was not only how do you bring together all the known variables, but also how do you transmit that into a way that ######### and a patient can understand and how that information can be given in a standardized way. So it doesn't really matter where being is diagnosed in the world. Potentially they will get the same information, same guidance.",
                "labels": [
                    {
                        "label": "health_issues",
                        "confidence": 0.638485848903656,
                        "severity": 0.018860582262277603
                    }
                ],
                "timestamp": {
                    "start": 110630,
                    "end": 133354
                }
            },
            {
                "text": "This is important because the usefulness of a survival model should be assessed both by how well the model discriminates among predictive risk and by what is calibrated. And while it's important to correctly discriminate and prioritize patients on the basis of risk, there is prediction of a model also to be well calibrated in order to be really valuable and provide prognostic value to ##########. So survival quiz is able to learn on the basis of whatever data is available.",
                "labels": [
                    {
                        "label": "health_issues",
                        "confidence": 0.6086597442626953,
                        "severity": 0.015299499034881592
                    }
                ],
                "timestamp": {
                    "start": 217918,
                    "end": 247820
                }
            }
        ],
        "summary": {
            "health_issues": 0.8965028085915497
        },
        "severity_score_summary": {
            "health_issues": {
                "low": 0.6848979580424506,
                "medium": 0.0,
                "high": 0.3151020419575494
            }
        }
    },
    "iab_categories_result": {
        "status": "success",
        "results": [
            {
                "text": "Quilt was developed using data from the Surveillance, Epidemiology and End Results program, or Steer for short and compared to nine other prognostic models that are currently used in the clinic.",
                "labels": [
                    {
                        "relevance": 0.5485847592353821,
                        "label": "MedicalHealth>DiseasesAndConditions>HeartAndCardiovascularDiseases"
                    },
                    {
                        "relevance": 0.04842698201537132,
                        "label": "MedicalHealth>DiseasesAndConditions"
                    },
                    {
                        "relevance": 0.029245957732200623,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.02443736419081688,
                        "label": "MedicalHealth>DiseasesAndConditions>Cancer"
                    },
                    {
                        "relevance": 0.021820133551955223,
                        "label": "MedicalHealth>DiseasesAndConditions>ReproductiveHealth>Pregnancy"
                    },
                    {
                        "relevance": 0.003748946823179722,
                        "label": "MedicalHealth>Surgery"
                    },
                    {
                        "relevance": 0.0033849652390927076,
                        "label": "MedicalHealth>DiseasesAndConditions>ReproductiveHealth>Infertility"
                    },
                    {
                        "relevance": 0.0028050346300005913,
                        "label": "MedicalHealth>DiseasesAndConditions>LungAndRespiratoryHealth"
                    },
                    {
                        "relevance": 0.001827227883040905,
                        "label": "HealthyLiving>Women'sHealth"
                    },
                    {
                        "relevance": 0.001645221607759595,
                        "label": "MedicalHealth>DiseasesAndConditions>EndocrineAndMetabolicDiseases"
                    }
                ],
                "timestamp": {
                    "start": 310,
                    "end": 11254
                }
            },
            {
                "text": "Joining me now to discuss this further are two ####### of the paper, ####### ### ### #### and ####### ##### #########, both primarily based at the ########## ## #########.",
                "labels": [
                    {
                        "relevance": 0.31556960940361023,
                        "label": "Science"
                    },
                    {
                        "relevance": 0.023348966613411903,
                        "label": "Hobbies&Interests>ContentProduction"
                    },
                    {
                        "relevance": 0.022537877783179283,
                        "label": "NewsAndPolitics>Politics>PoliticalIssues"
                    },
                    {
                        "relevance": 0.017266858369112015,
                        "label": "Technology&Computing>Computing>Internet>SocialNetworking"
                    },
                    {
                        "relevance": 0.013041757978498936,
                        "label": "EventsAndAttractions>PoliticalEvent"
                    },
                    {
                        "relevance": 0.009578769095242023,
                        "label": "NewsAndPolitics>Politics"
                    },
                    {
                        "relevance": 0.006715742405503988,
                        "label": "BooksAndLiterature>Biographies"
                    },
                    {
                        "relevance": 0.006052474491298199,
                        "label": "Hobbies&Interests>ContentProduction>FreelanceWriting"
                    },
                    {
                        "relevance": 0.004980504047125578,
                        "label": "Technology&Computing>Computing>Internet>ITAndInternetSupport"
                    },
                    {
                        "relevance": 0.004933451302349567,
                        "label": "BusinessAndFinance>Business>MarketingAndAdvertising"
                    }
                ],
                "timestamp": {
                    "start": 11422,
                    "end": 21526
                }
            },
            {
                "text": "####### is a ######### of ####### ########, ########## ############, and ########, and ####### is both a ###### in ######### and a ########## ########### specializing in ######## ######.",
                "labels": [
                    {
                        "relevance": 0.0011007656576111913,
                        "label": "BusinessAndFinance>Business"
                    },
                    {
                        "relevance": 0.000861817505210638,
                        "label": "BusinessAndFinance>Economy>Commodities"
                    },
                    {
                        "relevance": 0.000831642362754792,
                        "label": "Automotive>AutoType"
                    },
                    {
                        "relevance": 0.0007526901899836957,
                        "label": "Science>Genetics"
                    },
                    {
                        "relevance": 0.0006439132848754525,
                        "label": "FamilyAndRelationships"
                    },
                    {
                        "relevance": 0.0006376377423293889,
                        "label": "Science>Physics"
                    },
                    {
                        "relevance": 0.000596235622651875,
                        "label": "Science>BiologicalSciences"
                    },
                    {
                        "relevance": 0.0005099190166220069,
                        "label": "Automotive>AutoBodyStyles>Microcar"
                    },
                    {
                        "relevance": 0.0004842289781663567,
                        "label": "BusinessAndFinance>Business>LargeBusiness"
                    },
                    {
                        "relevance": 0.00044279530993662775,
                        "label": "BusinessAndFinance>Business>BusinessBanking&Finance>MergersAndAcquisitions"
                    }
                ],
                "timestamp": {
                    "start": 21718,
                    "end": 32194
                }
            },
            {
                "text": "So just kind of going back to the start, can you give us a bit of background on this particular study and what perhaps was your inspiration for starting and what clinical problem were you hoping to address? ######## ###### is extremely common, as we all know, and the numbers are increasing.",
                "labels": [
                    {
                        "relevance": 0.6037803888320923,
                        "label": "MedicalHealth>DiseasesAndConditions"
                    },
                    {
                        "relevance": 0.5249565839767456,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.28862544894218445,
                        "label": "MedicalHealth>DiseasesAndConditions>HeartAndCardiovascularDiseases"
                    },
                    {
                        "relevance": 0.10432609170675278,
                        "label": "HealthyLiving>Wellness>AlternativeMedicine>HolisticHealth"
                    },
                    {
                        "relevance": 0.0733783096075058,
                        "label": "HealthyLiving"
                    },
                    {
                        "relevance": 0.04839008301496506,
                        "label": "HealthyLiving>Wellness>AlternativeMedicine"
                    },
                    {
                        "relevance": 0.03662465140223503,
                        "label": "MedicalHealth>DiseasesAndConditions>Cancer"
                    },
                    {
                        "relevance": 0.02743375115096569,
                        "label": "MedicalHealth"
                    },
                    {
                        "relevance": 0.017651967704296112,
                        "label": "MedicalHealth>DiseasesAndConditions>SexualHealth>SexualConditions"
                    },
                    {
                        "relevance": 0.014533632434904575,
                        "label": "MedicalHealth>DiseasesAndConditions>MentalHealth"
                    }
                ],
                "timestamp": {
                    "start": 32362,
                    "end": 49258
                }
            },
            {
                "text": "And it's a funny disease because on the one hand, we know that it kills many men, but on the other hand, we also know that many men will have it and live with it and may not even know about. So trying to decide if you find a ######## ######, whether you need to treat it or not, is actually quite a complex problem.",
                "labels": [
                    {
                        "relevance": 0.9896013140678406,
                        "label": "HealthyLiving>Men'sHealth"
                    },
                    {
                        "relevance": 0.8377825021743774,
                        "label": "MedicalHealth>DiseasesAndConditions"
                    },
                    {
                        "relevance": 0.09584391117095947,
                        "label": "HealthyLiving>Women'sHealth"
                    },
                    {
                        "relevance": 0.06801537424325943,
                        "label": "MedicalHealth>DiseasesAndConditions>HeartAndCardiovascularDiseases"
                    },
                    {
                        "relevance": 0.06255005300045013,
                        "label": "MedicalHealth>DiseasesAndConditions>SexualHealth>SexualConditions"
                    },
                    {
                        "relevance": 0.044603001326322556,
                        "label": "HealthyLiving"
                    },
                    {
                        "relevance": 0.025549443438649178,
                        "label": "MedicalHealth>DiseasesAndConditions>Cancer"
                    },
                    {
                        "relevance": 0.02540743537247181,
                        "label": "MedicalHealth>DiseasesAndConditions>EndocrineAndMetabolicDiseases>Menopause"
                    },
                    {
                        "relevance": 0.022710025310516357,
                        "label": "MedicalHealth>DiseasesAndConditions>EndocrineAndMetabolicDiseases"
                    },
                    {
                        "relevance": 0.006508005782961845,
                        "label": "MedicalHealth>DiseasesAndConditions>InfectiousDiseases"
                    }
                ],
                "timestamp": {
                    "start": 49414,
                    "end": 65178
                }
            },
            {
                "text": "For many years, the mantra has been if you find a ######## ######, you must treat it. But multiple studies have shown that actually survival may not be different from doing nothing to even doing something as radical as #######. So the question that we had in our group was really, how do you make that difference? And how do you actually find out which of the men would benefit and which would not?",
                "labels": [
                    {
                        "relevance": 0.9897046685218811,
                        "label": "HealthyLiving>Men'sHealth"
                    },
                    {
                        "relevance": 0.1511334776878357,
                        "label": "MedicalHealth>DiseasesAndConditions>MentalHealth"
                    },
                    {
                        "relevance": 0.03578590229153633,
                        "label": "HealthyLiving>Women'sHealth"
                    },
                    {
                        "relevance": 0.030846647918224335,
                        "label": "MedicalHealth>DiseasesAndConditions>SexualHealth"
                    },
                    {
                        "relevance": 0.02749648690223694,
                        "label": "MedicalHealth>DiseasesAndConditions>ReproductiveHealth"
                    },
                    {
                        "relevance": 0.019465401768684387,
                        "label": "HealthyLiving"
                    },
                    {
                        "relevance": 0.00706479512155056,
                        "label": "FamilyAndRelationships>SingleLife"
                    },
                    {
                        "relevance": 0.0024502864107489586,
                        "label": "NewsAndPolitics>Politics>PoliticalIssues"
                    },
                    {
                        "relevance": 0.0014218511059880257,
                        "label": "Sports>Bodybuilding"
                    },
                    {
                        "relevance": 0.0013876138255000114,
                        "label": "FamilyAndRelationships>Parenting>ParentingTeens"
                    }
                ],
                "timestamp": {
                    "start": 65324,
                    "end": 86802
                }
            },
            {
                "text": "And of course, this is not new. There's been many pieces of work around that, but they tend to be fairly small, ad hoc with rather poor endpoints. And most importantly, very few of these things were being used in a standard approach. And even now, every country has its own particular set of guidelines, rules. And ultimately, the final arbitrator is who the ######### is when they see a patient and how they conveyed an information.",
                "labels": [
                    {
                        "relevance": 0.9908561706542969,
                        "label": "MedicalHealth"
                    },
                    {
                        "relevance": 0.8929247856140137,
                        "label": "BusinessAndFinance>Industries>HealthcareIndustry"
                    },
                    {
                        "relevance": 0.24560950696468353,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.01769150421023369,
                        "label": "PersonalFinance>Insurance>HealthInsurance"
                    },
                    {
                        "relevance": 0.012233874760568142,
                        "label": "HealthyLiving"
                    },
                    {
                        "relevance": 0.007187122479081154,
                        "label": "MedicalHealth>PharmaceuticalDrugs"
                    },
                    {
                        "relevance": 0.005669787526130676,
                        "label": "HealthyLiving>Children'sHealth"
                    },
                    {
                        "relevance": 0.004617621656507254,
                        "label": "BusinessAndFinance>Industries>PharmaceuticalIndustry"
                    },
                    {
                        "relevance": 0.004571066703647375,
                        "label": "MedicalHealth>DiseasesAndConditions>Injuries>FirstAid"
                    },
                    {
                        "relevance": 0.0036467041354626417,
                        "label": "HealthyLiving>Wellness>AlternativeMedicine"
                    }
                ],
                "timestamp": {
                    "start": 86876,
                    "end": 110300
                }
            },
            {
                "text": "So our primary interests driving the development of use from elastic tools was not only how do you bring together all the known variables, but also how do you transmit that into a way that ######### and a patient can understand and how that information can be given in a standardized way. So it doesn't really matter where being is diagnosed in the world. Potentially they will get the same information, same guidance.",
                "labels": [
                    {
                        "relevance": 0.35404136776924133,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.16174176335334778,
                        "label": "MedicalHealth"
                    },
                    {
                        "relevance": 0.0561726912856102,
                        "label": "Technology&Computing>Computing>DataStorageAndWarehousing"
                    },
                    {
                        "relevance": 0.011357199400663376,
                        "label": "BusinessAndFinance>Industries>HealthcareIndustry"
                    },
                    {
                        "relevance": 0.005437206476926804,
                        "label": "Technology&Computing>Computing>ComputerSoftwareAndApplications>Databases"
                    },
                    {
                        "relevance": 0.002548964461311698,
                        "label": "BusinessAndFinance>Business>BusinessI.T."
                    },
                    {
                        "relevance": 0.0020008105784654617,
                        "label": "HealthyLiving>Wellness>AlternativeMedicine>HolisticHealth"
                    },
                    {
                        "relevance": 0.0019346432527527213,
                        "label": "HealthyLiving"
                    },
                    {
                        "relevance": 0.0018456531688570976,
                        "label": "MedicalHealth>DiseasesAndConditions"
                    },
                    {
                        "relevance": 0.001573092769831419,
                        "label": "HealthyLiving>Wellness>AlternativeMedicine"
                    }
                ],
                "timestamp": {
                    "start": 110630,
                    "end": 133354
                }
            },
            {
                "text": "So that was the underpinning reason for looking at this, and there are different ways of doing it. One of them is tiered, which means you put people into brackets of groups and you look at how the groups perform. But of particular interest to us was how you get into a personalized level.",
                "labels": [
                    {
                        "relevance": 0.13446292281150818,
                        "label": "Education>EducationalAssessment"
                    },
                    {
                        "relevance": 0.014247503131628036,
                        "label": "BusinessAndFinance>Business>ExecutiveLeadership&Management"
                    },
                    {
                        "relevance": 0.00829678401350975,
                        "label": "Education>EducationalAssessment>StandardizedTesting"
                    },
                    {
                        "relevance": 0.006374238058924675,
                        "label": "Education>HomeworkAndStudy"
                    },
                    {
                        "relevance": 0.0014967231545597315,
                        "label": "BusinessAndFinance>Business>HumanResources"
                    },
                    {
                        "relevance": 0.0011260057799518108,
                        "label": "BusinessAndFinance>Business>Sales"
                    },
                    {
                        "relevance": 0.0010403691558167338,
                        "label": "Hobbies&Interests>GamesAndPuzzles>RoleplayingGames"
                    },
                    {
                        "relevance": 0.0009798052487894893,
                        "label": "NewsAndPolitics>Politics>PoliticalIssues"
                    },
                    {
                        "relevance": 0.0008673898410052061,
                        "label": "Careers>CareerPlanning"
                    },
                    {
                        "relevance": 0.0007272407528944314,
                        "label": "Television>RealityTV"
                    }
                ],
                "timestamp": {
                    "start": 133462,
                    "end": 147414
                }
            },
            {
                "text": "And how do we actually use this new emerging signs of ########## ############ and ####### ######## to the application? And that's where we came from. It from our Echo fantastic. So delving into the method. ########, can you tell us a bit about how the Survival Quilt based model was developed?",
                "labels": [
                    {
                        "relevance": 0.09715142101049423,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Needlework"
                    },
                    {
                        "relevance": 0.05394469574093819,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Beadwork"
                    },
                    {
                        "relevance": 0.03661054000258446,
                        "label": "Hobbies&Interests>ArtsAndCrafts"
                    },
                    {
                        "relevance": 0.016475120559334755,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Scrapbooking"
                    },
                    {
                        "relevance": 0.0041045756079256535,
                        "label": "Travel>TravelType>Camping"
                    },
                    {
                        "relevance": 0.0031699317041784525,
                        "label": "Hobbies&Interests>ModelToys"
                    },
                    {
                        "relevance": 0.002338408725336194,
                        "label": "Home&Garden"
                    },
                    {
                        "relevance": 0.0018388506723567843,
                        "label": "Shopping>Children'sGamesAndToys"
                    },
                    {
                        "relevance": 0.001776379533112049,
                        "label": "Home&Garden>HomeImprovement"
                    },
                    {
                        "relevance": 0.0016951890429481864,
                        "label": "Hobbies&Interests>GamesAndPuzzles>RoleplayingGames"
                    }
                ],
                "timestamp": {
                    "start": 147512,
                    "end": 166470
                }
            },
            {
                "text": "Survival Quilt is a ####### ######## method that we have developed two years ago, and it appeared in 2019, which is one of our main conferences in ########## ############ and ####### ########.",
                "labels": [
                    {
                        "relevance": 0.08047400414943695,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Needlework"
                    },
                    {
                        "relevance": 0.0121565917506814,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Scrapbooking"
                    },
                    {
                        "relevance": 0.007229142356663942,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Beadwork"
                    },
                    {
                        "relevance": 0.005621155723929405,
                        "label": "Hobbies&Interests>ArtsAndCrafts"
                    },
                    {
                        "relevance": 0.004015755373984575,
                        "label": "BusinessAndFinance>Industries>ApparelIndustry"
                    },
                    {
                        "relevance": 0.002695236587896943,
                        "label": "Travel>TravelType>Camping"
                    },
                    {
                        "relevance": 0.0017259487649425864,
                        "label": "Home&Garden"
                    },
                    {
                        "relevance": 0.0016126796836033463,
                        "label": "NewsAndPolitics>Disasters"
                    },
                    {
                        "relevance": 0.001471832161769271,
                        "label": "Style&Fashion>Men'sFashion>Men'sClothing>Men'sUnderwearAndSleepwear"
                    },
                    {
                        "relevance": 0.0013412820408120751,
                        "label": "Style&Fashion>Men'sFashion>Men'sClothing"
                    }
                ],
                "timestamp": {
                    "start": 167450,
                    "end": 182000
                }
            },
            {
                "text": "We know that there are numerous survival models, and a key question is how to select among these numerous survival models for a particular dataset, such as the Seer data set. And Survival Quilts is the first automated ####### ######## method that is able to do so for survival analysis and is learning on the basis of the underlying data, how to weigh the different variables to achieve the best trade off between discriminatory performance and calibration.",
                "labels": [
                    {
                        "relevance": 0.00597528088837862,
                        "label": "NewsAndPolitics>Disasters"
                    },
                    {
                        "relevance": 0.00526886573061347,
                        "label": "Pets>Birds"
                    },
                    {
                        "relevance": 0.0048610372468829155,
                        "label": "Science>Genetics"
                    },
                    {
                        "relevance": 0.002950045047327876,
                        "label": "Travel>TravelType>Camping"
                    },
                    {
                        "relevance": 0.002340492093935609,
                        "label": "FamilyAndRelationships>Bereavement"
                    },
                    {
                        "relevance": 0.0016764235915616155,
                        "label": "Travel>TravelLocations>PolarTravel"
                    },
                    {
                        "relevance": 0.0016683699795976281,
                        "label": "Science>Environment"
                    },
                    {
                        "relevance": 0.0016161452513188124,
                        "label": "Education>EducationalAssessment>StandardizedTesting"
                    },
                    {
                        "relevance": 0.0015179680194705725,
                        "label": "MedicalHealth>DiseasesAndConditions>InfectiousDiseases"
                    },
                    {
                        "relevance": 0.0013416933361440897,
                        "label": "MedicalHealth>DiseasesAndConditions>ColdAndFlu"
                    }
                ],
                "timestamp": {
                    "start": 182390,
                    "end": 217726
                }
            },
            {
                "text": "This is important because the usefulness of a survival model should be assessed both by how well the model discriminates among predictive risk and by what is calibrated. And while it's important to correctly discriminate and prioritize patients on the basis of risk, there is prediction of a model also to be well calibrated in order to be really valuable and provide prognostic value to ##########. So survival quiz is able to learn on the basis of whatever data is available.",
                "labels": [
                    {
                        "relevance": 0.10751749575138092,
                        "label": "MedicalHealth>DiseasesAndConditions>Cancer"
                    },
                    {
                        "relevance": 0.060198403894901276,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.044831421226263046,
                        "label": "MedicalHealth>DiseasesAndConditions>Injuries>FirstAid"
                    },
                    {
                        "relevance": 0.04456207528710365,
                        "label": "MedicalHealth>DiseasesAndConditions>Injuries"
                    },
                    {
                        "relevance": 0.016394801437854767,
                        "label": "MedicalHealth"
                    },
                    {
                        "relevance": 0.009116780944168568,
                        "label": "MedicalHealth>Surgery"
                    },
                    {
                        "relevance": 0.008842873387038708,
                        "label": "MedicalHealth>DiseasesAndConditions>ReproductiveHealth>Pregnancy"
                    },
                    {
                        "relevance": 0.0070320433005690575,
                        "label": "FamilyAndRelationships>Bereavement"
                    },
                    {
                        "relevance": 0.005814251024276018,
                        "label": "MedicalHealth>DiseasesAndConditions"
                    },
                    {
                        "relevance": 0.004966876469552517,
                        "label": "HealthyLiving>Women'sHealth"
                    }
                ],
                "timestamp": {
                    "start": 217918,
                    "end": 247820
                }
            },
            {
                "text": "The best model is learning the best model and the best model across the different Horizons of survival analysis. What is the best integration of the variables and how the variables interact in order to issue a personalized prediction for survival? So it is learning personalized predictions and how to best combine these different existing models to issue this particular prediction for the patient at hand. That's interesting. And what were the key findings then of this study?",
                "labels": [
                    {
                        "relevance": 0.030144071206450462,
                        "label": "MedicalHealth>DiseasesAndConditions>Cancer"
                    },
                    {
                        "relevance": 0.028174106031656265,
                        "label": "MedicalHealth>Surgery"
                    },
                    {
                        "relevance": 0.016567278653383255,
                        "label": "MedicalHealth>DiseasesAndConditions>Injuries>FirstAid"
                    },
                    {
                        "relevance": 0.006566062569618225,
                        "label": "MedicalHealth>DiseasesAndConditions>Injuries"
                    },
                    {
                        "relevance": 0.0049917385913431644,
                        "label": "FamilyAndRelationships>Bereavement"
                    },
                    {
                        "relevance": 0.0027326815761625767,
                        "label": "Technology&Computing>ArtificialIntelligence"
                    },
                    {
                        "relevance": 0.002437565941363573,
                        "label": "HealthyLiving>SeniorHealth"
                    },
                    {
                        "relevance": 0.002365691587328911,
                        "label": "MedicalHealth>DiseasesAndConditions>ReproductiveHealth>Pregnancy"
                    },
                    {
                        "relevance": 0.0021320790983736515,
                        "label": "HealthyLiving>Women'sHealth"
                    },
                    {
                        "relevance": 0.0017862814711406827,
                        "label": "MedicalHealth"
                    }
                ],
                "timestamp": {
                    "start": 248270,
                    "end": 281420
                }
            },
            {
                "text": "Well, the first thing to say is that to our knowledge, it's the first application of the ####### ######## algorithm and ########## ############ method to such a big data set.",
                "labels": [
                    {
                        "relevance": 0.87840336561203,
                        "label": "Technology&Computing>Computing>DataStorageAndWarehousing"
                    },
                    {
                        "relevance": 0.6445692181587219,
                        "label": "Technology&Computing>Computing>ComputerSoftwareAndApplications>Databases"
                    },
                    {
                        "relevance": 0.000977328047156334,
                        "label": "Technology&Computing>Computing>Internet>CloudComputing"
                    },
                    {
                        "relevance": 0.0007106131524778903,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.00045685950317420065,
                        "label": "Technology&Computing>Computing>ProgrammingLanguages"
                    },
                    {
                        "relevance": 0.00042011463665403426,
                        "label": "Technology&Computing>Computing>ComputerSoftwareAndApplications>3-DGraphics"
                    },
                    {
                        "relevance": 0.00032071047462522984,
                        "label": "Technology&Computing>Computing"
                    },
                    {
                        "relevance": 0.00029704428743571043,
                        "label": "BusinessAndFinance>Business>BusinessI.T."
                    },
                    {
                        "relevance": 0.0002356295590288937,
                        "label": "Hobbies&Interests>GenealogyAndAncestry"
                    },
                    {
                        "relevance": 0.00018636963795870543,
                        "label": "Technology&Computing>Computing>ComputerSoftwareAndApplications"
                    }
                ],
                "timestamp": {
                    "start": 282410,
                    "end": 291354
                }
            },
            {
                "text": "To answer this question, and one of the key things that we knew, the Survival Quilts model, like other machine models, can actually take new information and process it very quickly, and it took us many years to develop our previous models. But I think working with #######, one of your ########, with #######, we were able to produce this model very rapidly because the technique was already there.",
                "labels": [
                    {
                        "relevance": 0.4276721477508545,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Needlework"
                    },
                    {
                        "relevance": 0.06591973453760147,
                        "label": "Hobbies&Interests>ArtsAndCrafts"
                    },
                    {
                        "relevance": 0.024126721546053886,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Beadwork"
                    },
                    {
                        "relevance": 0.012389758601784706,
                        "label": "BusinessAndFinance>Industries>ApparelIndustry"
                    },
                    {
                        "relevance": 0.009949881583452225,
                        "label": "BusinessAndFinance>Industries>ManufacturingIndustry"
                    },
                    {
                        "relevance": 0.008552178740501404,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Woodworking"
                    },
                    {
                        "relevance": 0.00418842863291502,
                        "label": "FineArt>Design"
                    },
                    {
                        "relevance": 0.003822819795459509,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Scrapbooking"
                    },
                    {
                        "relevance": 0.0020226777996867895,
                        "label": "Style&Fashion>Men'sFashion>Men'sClothing"
                    },
                    {
                        "relevance": 0.0019841997418552637,
                        "label": "Hobbies&Interests>ModelToys"
                    }
                ],
                "timestamp": {
                    "start": 291392,
                    "end": 313110
                }
            },
            {
                "text": "And even in this first situation where we had fairly standard variables, I was amazed to find that the final Quilts model was able to come up with a performance characteristic which was as good as the best that we have from a clinical standpoint and actually a little bit better as well. So that is to me an illustration that the direction of travel or what we do in future has to be using this kind of technology. That's a great summary.",
                "labels": [
                    {
                        "relevance": 0.7793393731117249,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Needlework"
                    },
                    {
                        "relevance": 0.04325036332011223,
                        "label": "Hobbies&Interests>ArtsAndCrafts"
                    },
                    {
                        "relevance": 0.018936436623334885,
                        "label": "BusinessAndFinance>Industries>ApparelIndustry"
                    },
                    {
                        "relevance": 0.01169709861278534,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Woodworking"
                    },
                    {
                        "relevance": 0.006654753815382719,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Beadwork"
                    },
                    {
                        "relevance": 0.0034668187145143747,
                        "label": "Home&Garden>InteriorDecorating"
                    },
                    {
                        "relevance": 0.003334896871820092,
                        "label": "Style&Fashion>BodyArt"
                    },
                    {
                        "relevance": 0.003094860352575779,
                        "label": "FamilyAndRelationships>Bereavement"
                    },
                    {
                        "relevance": 0.0028379112482070923,
                        "label": "Style&Fashion>HighFashion"
                    },
                    {
                        "relevance": 0.002662881277501583,
                        "label": "Hobbies&Interests>ArtsAndCrafts>Scrapbooking"
                    }
                ],
                "timestamp": {
                    "start": 313220,
                    "end": 338490
                }
            },
            {
                "text": "You've mentioned in the paper that the Seared data set doesn't include data on comorbidity, ########, ###, nor treatment, although potentially this could improve the prognostic capabilities of the model. So how easy would it be to incorporate these variables in the future and what effect do you think that could have on the model output?",
                "labels": [
                    {
                        "relevance": 0.7470129728317261,
                        "label": "MedicalHealth>DiseasesAndConditions>Cancer"
                    },
                    {
                        "relevance": 0.4376750588417053,
                        "label": "MedicalHealth>DiseasesAndConditions>HeartAndCardiovascularDiseases"
                    },
                    {
                        "relevance": 0.06660807132720947,
                        "label": "MedicalHealth>DiseasesAndConditions"
                    },
                    {
                        "relevance": 0.027441050857305527,
                        "label": "HealthyLiving>WeightLoss"
                    },
                    {
                        "relevance": 0.012248565442860126,
                        "label": "HealthyLiving"
                    },
                    {
                        "relevance": 0.011163235642015934,
                        "label": "MedicalHealth>Surgery"
                    },
                    {
                        "relevance": 0.009664793498814106,
                        "label": "HealthyLiving>Wellness>SmokingCessation"
                    },
                    {
                        "relevance": 0.0065574985928833485,
                        "label": "HealthyLiving>SeniorHealth"
                    },
                    {
                        "relevance": 0.0056715430691838264,
                        "label": "HealthyLiving>Women'sHealth"
                    },
                    {
                        "relevance": 0.00217293668538332,
                        "label": "MedicalHealth>DiseasesAndConditions>ReproductiveHealth>Pregnancy"
                    }
                ],
                "timestamp": {
                    "start": 338930,
                    "end": 360450
                }
            },
            {
                "text": "One advantage of survival quiz is that it's an automated ####### ######## method. It is able to take whatever data we throw at it and craft a model with good discrimination and calibration performance. So it is very easy to push off a button if this data becomes available to integrate additional variables into the mobile to issue this prediction.",
                "labels": [
                    {
                        "relevance": 0.019599540159106255,
                        "label": "Education>EducationalAssessment"
                    },
                    {
                        "relevance": 0.009578783065080643,
                        "label": "Education>EducationalAssessment>StandardizedTesting"
                    },
                    {
                        "relevance": 0.003137225518003106,
                        "label": "VideoGaming>VideoGameGenres>EducationalVideoGames"
                    },
                    {
                        "relevance": 0.0021880047861486673,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.001883503282442689,
                        "label": "Television>RealityTV"
                    },
                    {
                        "relevance": 0.0016708321636542678,
                        "label": "VideoGaming>VideoGameGenres>PuzzleVideoGames"
                    },
                    {
                        "relevance": 0.0015898160636425018,
                        "label": "Technology&Computing>Computing>Internet>Search"
                    },
                    {
                        "relevance": 0.0010010746773332357,
                        "label": "BusinessAndFinance>Business>MarketingAndAdvertising"
                    },
                    {
                        "relevance": 0.0009581485064700246,
                        "label": "Education>HomeworkAndStudy"
                    },
                    {
                        "relevance": 0.0008940749103203416,
                        "label": "HealthyLiving>Wellness>AlternativeMedicine>HolisticHealth"
                    }
                ],
                "timestamp": {
                    "start": 361070,
                    "end": 387070
                }
            },
            {
                "text": "So in terms of building a new model on the basis of new variables, that is, I think, very easy. What is important about these methods such as survival quilts, is that not only we are able to assess the advantage and the value of what I call the value of modeling, identifying which methods are best for what type of data and what type of patients, but also the value of information.",
                "labels": [
                    {
                        "relevance": 0.05059424415230751,
                        "label": "MedicalHealth>MedicalTests"
                    },
                    {
                        "relevance": 0.03773368522524834,
                        "label": "MedicalHealth>Surgery"
                    },
                    {
                        "relevance": 0.02989933453500271,
                        "label": "MedicalHealth>DiseasesAndConditions>Cancer"
                    },
                    {
                        "relevance": 0.019325990229845047,
                        "label": "MedicalHealth>DiseasesAndConditions>ReproductiveHealth>Pregnancy"
                    },
                    {
                        "relevance": 0.00435588788241148,
                        "label": "VideoGaming>VideoGameGenres>SimulationVideoGames"
                    },
                    {
                        "relevance": 0.0026040212251245975,
                        "label": "MedicalHealth>DiseasesAndConditions>HeartAndCardiovascularDiseases"
                    },
                    {
                        "relevance": 0.0024165716022253036,
                        "label": "Pets>VeterinaryMedicine"
                    },
                    {
                        "relevance": 0.0021342418622225523,
                        "label": "HealthyLiving>Wellness>AlternativeMedicine>HolisticHealth"
                    },
                    {
                        "relevance": 0.0017831322038546205,
                        "label": "Technology&Computing>Robotics"
                    },
                    {
                        "relevance": 0.0016866566147655249,
                        "label": "MedicalHealth>DiseasesAndConditions>BoneAndJointConditions"
                    }
                ],
                "timestamp": {
                    "start": 387130,
                    "end": 413180
                }
            },
            {
                "text": "So we are going to learn in a data driven way on the basis of the data, what variables.",
                "labels": [
                    {
                        "relevance": 0.8019964694976807,
                        "label": "Technology&Computing>Computing>DataStorageAndWarehousing"
                    },
                    {
                        "relevance": 0.05555802956223488,
                        "label": "Technology&Computing>Computing>ComputerSoftwareAndApplications>Databases"
                    },
                    {
                        "relevance": 0.015180392190814018,
                        "label": "BusinessAndFinance>Business>BusinessOperations"
                    },
                    {
                        "relevance": 0.0034052622504532337,
                        "label": "BusinessAndFinance>Business>BusinessI.T."
                    },
                    {
                        "relevance": 0.0017587275942787528,
                        "label": "BusinessAndFinance>Business>MarketingAndAdvertising"
                    },
                    {
                        "relevance": 0.0017337193712592125,
                        "label": "PersonalFinance>FinancialPlanning"
                    },
                    {
                        "relevance": 0.0009908551583066583,
                        "label": "BusinessAndFinance>Business>Sales"
                    },
                    {
                        "relevance": 0.0009893779642879963,
                        "label": "BusinessAndFinance>Business>Logistics"
                    },
                    {
                        "relevance": 0.0009641864453442395,
                        "label": "Education>EducationalAssessment"
                    },
                    {
                        "relevance": 0.0008779114577919245,
                        "label": "Technology&Computing>Computing>ComputerSoftwareAndApplications"
                    }
                ],
                "timestamp": {
                    "start": 413690,
                    "end": 419710
                }
            }
        ],
        "summary": {
            "HealthyLiving>Men'sHealth": 1.0,
            "Technology&Computing>Computing>DataStorageAndWarehousing": 0.8738490343093872,
            "MedicalHealth>DiseasesAndConditions": 0.7893009781837463,
            "Hobbies&Interests>ArtsAndCrafts>Needlework": 0.6966466307640076,
            "MedicalHealth>DiseasesAndConditions>HeartAndCardiovascularDiseases": 0.6796181201934814,
            "MedicalHealth>MedicalTests": 0.6407486200332642,
            "MedicalHealth": 0.6048645377159119,
            "MedicalHealth>DiseasesAndConditions>Cancer": 0.5046104788780212,
            "BusinessAndFinance>Industries>HealthcareIndustry": 0.4604680836200714,
            "Technology&Computing>Computing>ComputerSoftwareAndApplications>Databases": 0.3553354740142822,
            "Science": 0.16008417308330536,
            "MedicalHealth>DiseasesAndConditions>MentalHealth": 0.09158715605735779,
            "HealthyLiving": 0.08631271868944168,
            "HealthyLiving>Women'sHealth": 0.08277842402458191,
            "Education>EducationalAssessment": 0.08043354749679565,
            "Hobbies&Interests>ArtsAndCrafts": 0.07647304236888885,
            "HealthyLiving>Wellness>AlternativeMedicine>HolisticHealth": 0.0593891479074955,
            "MedicalHealth>Surgery": 0.0485440231859684,
            "Hobbies&Interests>ArtsAndCrafts>Beadwork": 0.046601589769124985,
            "MedicalHealth>DiseasesAndConditions>SexualHealth>SexualConditions": 0.04143847897648811
        }
    },
    "disfluencies": False,
    "sentiment_analysis": True,
    "auto_chapters": True,
    "chapters": [
        {
            "summary": "Quilt was developed using data from the Surveillance, Epidemiology and End Results program, or Steer for Short. It was compared to nine other prognostic models that are currently used in the clinic. Joining us to discuss this further are two ####### of the paper, ####### and ####### of the paper, both primarily based at the ##########, ##, #########.",
            "headline": "Quilt was developed using data from the Surveillance, Epidemiology and End Results program, or Steer for short.",
            "gist": "The study \u2019 s authors.",
            "start": 310,
            "end": 158070
        },
        {
            "summary": "Survival Quilt is a ####### ######## method that we have developed two years ago and it appeared in 2019. It's the first application of the ####### ######## algorithm and ########## ########## method to such a big data set. The best model is learning the best model and the best model across the different Horizons of survival analysis.",
            "headline": "Survival Quilt is a ####### ######## method that we have developed two years ago, and it appeared in 2019.",
            "gist": "How the survival quilt model was developed.",
            "start": 158510,
            "end": 338490
        },
        {
            "summary": "The Seared data set doesn't include data on comorbidity ######## ### nor treatment, although potentially this could improve the prognostic capabilities of the model. One advantage of survival quiz is that it's an automated method. It is able to take whatever data we throw at it and craft a model with good discrimination and calibration performance.",
            "headline": "The Seared data set doesn't include data on comorbidity ######## ### nor treatment. Although potentially this could improve the prognostic capabilities of the model.",
            "gist": "Adding additional variables.",
            "start": 338930,
            "end": 419710
        }
    ],
    "sentiment_analysis_results": [
        {
            "text": "Quilt was developed using data from the Surveillance, Epidemiology and End Results program, or Steer for short and compared to nine other prognostic models that are currently used in the clinic.",
            "start": 310,
            "end": 11254,
            "sentiment": "NEUTRAL",
            "confidence": 0.9294964671134949,
            "speaker": "A"
        },
        {
            "text": "Joining me now to discuss this further are two ####### of the paper, ####### ### ### #### and ####### ##### #########, both primarily based at the ########## ## #########.",
            "start": 11422,
            "end": 21526,
            "sentiment": "NEUTRAL",
            "confidence": 0.888446033000946,
            "speaker": "A"
        },
        {
            "text": "####### is a ######### of ####### ########, ########## ############, and ########, and ####### is both a ###### in ######### and a ########## ########### specializing in ######## ######.",
            "start": 21718,
            "end": 32194,
            "sentiment": "NEUTRAL",
            "confidence": 0.8311874270439148,
            "speaker": "A"
        },
        {
            "text": "So just kind of going back to the start, can you give us a bit of background on this particular study and what perhaps was your inspiration for starting and what clinical problem were you hoping to address?",
            "start": 32362,
            "end": 42980,
            "sentiment": "NEUTRAL",
            "confidence": 0.9225876331329346,
            "speaker": "A"
        },
        {
            "text": "######## ###### is extremely common, as we all know, and the numbers are increasing.",
            "start": 44090,
            "end": 49258,
            "sentiment": "NEUTRAL",
            "confidence": 0.6499505043029785,
            "speaker": "B"
        },
        {
            "text": "And it's a funny disease because on the one hand, we know that it kills many men, but on the other hand, we also know that many men will have it and live with it and may not even know about.",
            "start": 49414,
            "end": 58998,
            "sentiment": "NEGATIVE",
            "confidence": 0.8266599774360657,
            "speaker": "B"
        },
        {
            "text": "So trying to decide if you find a ######## ######, whether you need to treat it or not, is actually quite a complex problem.",
            "start": 59144,
            "end": 65178,
            "sentiment": "NEGATIVE",
            "confidence": 0.5655257701873779,
            "speaker": "B"
        },
        {
            "text": "For many years, the mantra has been if you find a ######## ######, you must treat it.",
            "start": 65324,
            "end": 69558,
            "sentiment": "NEUTRAL",
            "confidence": 0.7643877863883972,
            "speaker": "B"
        },
        {
            "text": "But multiple studies have shown that actually survival may not be different from doing nothing to even doing something as radical as #######.",
            "start": 69584,
            "end": 76870,
            "sentiment": "NEUTRAL",
            "confidence": 0.7448269128799438,
            "speaker": "B"
        },
        {
            "text": "So the question that we had in our group was really, how do you make that difference?",
            "start": 76990,
            "end": 82314,
            "sentiment": "NEUTRAL",
            "confidence": 0.8904653191566467,
            "speaker": "B"
        },
        {
            "text": "And how do you actually find out which of the men would benefit and which would not?",
            "start": 82412,
            "end": 86802,
            "sentiment": "NEUTRAL",
            "confidence": 0.6671380996704102,
            "speaker": "B"
        },
        {
            "text": "And of course, this is not new.",
            "start": 86876,
            "end": 88146,
            "sentiment": "NEUTRAL",
            "confidence": 0.6815497279167175,
            "speaker": "B"
        },
        {
            "text": "There's been many pieces of work around that, but they tend to be fairly small, ad hoc with rather poor endpoints.",
            "start": 88208,
            "end": 95626,
            "sentiment": "NEGATIVE",
            "confidence": 0.4851963520050049,
            "speaker": "B"
        },
        {
            "text": "And most importantly, very few of these things were being used in a standard approach.",
            "start": 95758,
            "end": 100294,
            "sentiment": "NEUTRAL",
            "confidence": 0.5396525859832764,
            "speaker": "B"
        },
        {
            "text": "And even now, every country has its own particular set of guidelines, rules.",
            "start": 100342,
            "end": 104062,
            "sentiment": "NEUTRAL",
            "confidence": 0.7513299584388733,
            "speaker": "B"
        },
        {
            "text": "And ultimately, the final arbitrator is who the ######### is when they see a patient and how they conveyed an information.",
            "start": 104086,
            "end": 110300,
            "sentiment": "NEUTRAL",
            "confidence": 0.7795662879943848,
            "speaker": "B"
        },
        {
            "text": "So our primary interests driving the development of use from elastic tools was not only how do you bring together all the known variables, but also how do you transmit that into a way that ######### and a patient can understand and how that information can be given in a standardized way.",
            "start": 110630,
            "end": 127098,
            "sentiment": "NEUTRAL",
            "confidence": 0.7592929005622864,
            "speaker": "B"
        },
        {
            "text": "So it doesn't really matter where being is diagnosed in the world.",
            "start": 127124,
            "end": 130194,
            "sentiment": "NEUTRAL",
            "confidence": 0.5644499063491821,
            "speaker": "B"
        },
        {
            "text": "Potentially they will get the same information, same guidance.",
            "start": 130232,
            "end": 133354,
            "sentiment": "NEUTRAL",
            "confidence": 0.8581701517105103,
            "speaker": "B"
        },
        {
            "text": "So that was the underpinning reason for looking at this, and there are different ways of doing it.",
            "start": 133462,
            "end": 138558,
            "sentiment": "NEUTRAL",
            "confidence": 0.843896746635437,
            "speaker": "B"
        },
        {
            "text": "One of them is tiered, which means you put people into brackets of groups and you look at how the groups perform.",
            "start": 138584,
            "end": 143226,
            "sentiment": "NEUTRAL",
            "confidence": 0.8389367461204529,
            "speaker": "B"
        },
        {
            "text": "But of particular interest to us was how you get into a personalized level.",
            "start": 143348,
            "end": 147414,
            "sentiment": "NEUTRAL",
            "confidence": 0.7616240382194519,
            "speaker": "B"
        },
        {
            "text": "And how do we actually use this new emerging signs of ########## ############ and ####### ######## to the application?",
            "start": 147512,
            "end": 153234,
            "sentiment": "NEUTRAL",
            "confidence": 0.8663622736930847,
            "speaker": "B"
        },
        {
            "text": "And that's where we came from.",
            "start": 153332,
            "end": 154698,
            "sentiment": "NEUTRAL",
            "confidence": 0.7881453633308411,
            "speaker": "B"
        },
        {
            "text": "It from our Echo fantastic.",
            "start": 154724,
            "end": 158070,
            "sentiment": "POSITIVE",
            "confidence": 0.8881256580352783,
            "speaker": "A"
        },
        {
            "text": "So delving into the method.",
            "start": 158510,
            "end": 160318,
            "sentiment": "NEUTRAL",
            "confidence": 0.8143842220306396,
            "speaker": "A"
        },
        {
            "text": "########, can you tell us a bit about how the Survival Quilt based model was developed?",
            "start": 160474,
            "end": 166470,
            "sentiment": "NEUTRAL",
            "confidence": 0.9269095063209534,
            "speaker": "A"
        },
        {
            "text": "Survival Quilt is a ####### ######## method that we have developed two years ago, and it appeared in 2019, which is one of our main conferences in ########## ############ and ####### ########.",
            "start": 167450,
            "end": 182000,
            "sentiment": "NEUTRAL",
            "confidence": 0.6561248302459717,
            "speaker": "C"
        },
        {
            "text": "We know that there are numerous survival models, and a key question is how to select among these numerous survival models for a particular dataset, such as the Seer data set.",
            "start": 182390,
            "end": 195246,
            "sentiment": "NEUTRAL",
            "confidence": 0.8573153614997864,
            "speaker": "C"
        },
        {
            "text": "And Survival Quilts is the first automated ####### ######## method that is able to do so for survival analysis and is learning on the basis of the underlying data, how to weigh the different variables to achieve the best trade off between discriminatory performance and calibration.",
            "start": 195428,
            "end": 217726,
            "sentiment": "NEUTRAL",
            "confidence": 0.6209437847137451,
            "speaker": "C"
        },
        {
            "text": "This is important because the usefulness of a survival model should be assessed both by how well the model discriminates among predictive risk and by what is calibrated.",
            "start": 217918,
            "end": 228046,
            "sentiment": "NEUTRAL",
            "confidence": 0.6607222557067871,
            "speaker": "C"
        },
        {
            "text": "And while it's important to correctly discriminate and prioritize patients on the basis of risk, there is prediction of a model also to be well calibrated in order to be really valuable and provide prognostic value to ##########.",
            "start": 228238,
            "end": 242230,
            "sentiment": "NEUTRAL",
            "confidence": 0.6036735773086548,
            "speaker": "C"
        },
        {
            "text": "So survival quiz is able to learn on the basis of whatever data is available.",
            "start": 242290,
            "end": 247820,
            "sentiment": "NEUTRAL",
            "confidence": 0.790103018283844,
            "speaker": "C"
        },
        {
            "text": "The best model is learning the best model and the best model across the different Horizons of survival analysis.",
            "start": 248270,
            "end": 256570,
            "sentiment": "POSITIVE",
            "confidence": 0.7733944058418274,
            "speaker": "C"
        },
        {
            "text": "What is the best integration of the variables and how the variables interact in order to issue a personalized prediction for survival?",
            "start": 256750,
            "end": 266290,
            "sentiment": "NEUTRAL",
            "confidence": 0.8527600169181824,
            "speaker": "C"
        },
        {
            "text": "So it is learning personalized predictions and how to best combine these different existing models to issue this particular prediction for the patient at hand.",
            "start": 266410,
            "end": 276620,
            "sentiment": "NEUTRAL",
            "confidence": 0.8367224931716919,
            "speaker": "C"
        },
        {
            "text": "That's interesting.",
            "start": 277370,
            "end": 278262,
            "sentiment": "POSITIVE",
            "confidence": 0.9033101797103882,
            "speaker": "A"
        },
        {
            "text": "And what were the key findings then of this study?",
            "start": 278336,
            "end": 281420,
            "sentiment": "NEUTRAL",
            "confidence": 0.9031065106391907,
            "speaker": "A"
        },
        {
            "text": "Well, the first thing to say is that to our knowledge, it's the first application of the ####### ######## algorithm and ########## ############ method to such a big data set.",
            "start": 282410,
            "end": 291354,
            "sentiment": "NEUTRAL",
            "confidence": 0.619574248790741,
            "speaker": "B"
        },
        {
            "text": "To answer this question, and one of the key things that we knew, the Survival Quilts model, like other machine models, can actually take new information and process it very quickly, and it took us many years to develop our previous models.",
            "start": 291392,
            "end": 304942,
            "sentiment": "NEUTRAL",
            "confidence": 0.5653074979782104,
            "speaker": "B"
        },
        {
            "text": "But I think working with #######, one of your ########, with #######, we were able to produce this model very rapidly because the technique was already there.",
            "start": 304966,
            "end": 313110,
            "sentiment": "POSITIVE",
            "confidence": 0.7379382848739624,
            "speaker": "B"
        },
        {
            "text": "And even in this first situation where we had fairly standard variables, I was amazed to find that the final Quilts model was able to come up with a performance characteristic which was as good as the best that we have from a clinical standpoint and actually a little bit better as well.",
            "start": 313220,
            "end": 328914,
            "sentiment": "POSITIVE",
            "confidence": 0.9231551289558411,
            "speaker": "B"
        },
        {
            "text": "So that is to me an illustration that the direction of travel or what we do in future has to be using this kind of technology.",
            "start": 329012,
            "end": 336080,
            "sentiment": "NEUTRAL",
            "confidence": 0.7239059209823608,
            "speaker": "B"
        },
        {
            "text": "That's a great summary.",
            "start": 336890,
            "end": 338490,
            "sentiment": "POSITIVE",
            "confidence": 0.9408640265464783,
            "speaker": "A"
        },
        {
            "text": "You've mentioned in the paper that the Seared data set doesn't include data on comorbidity, ########, ###, nor treatment, although potentially this could improve the prognostic capabilities of the model.",
            "start": 338930,
            "end": 352614,
            "sentiment": "NEUTRAL",
            "confidence": 0.6014182567596436,
            "speaker": "A"
        },
        {
            "text": "So how easy would it be to incorporate these variables in the future and what effect do you think that could have on the model output?",
            "start": 352772,
            "end": 360450,
            "sentiment": "NEUTRAL",
            "confidence": 0.8431146740913391,
            "speaker": "A"
        },
        {
            "text": "One advantage of survival quiz is that it's an automated ####### ######## method.",
            "start": 361070,
            "end": 367246,
            "sentiment": "POSITIVE",
            "confidence": 0.8228830695152283,
            "speaker": "C"
        },
        {
            "text": "It is able to take whatever data we throw at it and craft a model with good discrimination and calibration performance.",
            "start": 367318,
            "end": 375826,
            "sentiment": "NEUTRAL",
            "confidence": 0.521799623966217,
            "speaker": "C"
        },
        {
            "text": "So it is very easy to push off a button if this data becomes available to integrate additional variables into the mobile to issue this prediction.",
            "start": 376018,
            "end": 387070,
            "sentiment": "POSITIVE",
            "confidence": 0.6697753667831421,
            "speaker": "C"
        },
        {
            "text": "So in terms of building a new model on the basis of new variables, that is, I think, very easy.",
            "start": 387130,
            "end": 394230,
            "sentiment": "POSITIVE",
            "confidence": 0.9027639627456665,
            "speaker": "C"
        },
        {
            "text": "What is important about these methods such as survival quilts, is that not only we are able to assess the advantage and the value of what I call the value of modeling, identifying which methods are best for what type of data and what type of patients, but also the value of information.",
            "start": 394400,
            "end": 413180,
            "sentiment": "POSITIVE",
            "confidence": 0.5909406542778015,
            "speaker": "C"
        },
        {
            "text": "So we are going to learn in a data driven way on the basis of the data, what variables.",
            "start": 413690,
            "end": 419710,
            "sentiment": "NEUTRAL",
            "confidence": 0.8684599995613098,
            "speaker": "C"
        }
    ],
    "entity_detection": True,
    "entities": [
        {
            "entity_type": "occupation",
            "text": "authors",
            "start": 13772,
            "end": 14038
        },
        {
            "entity_type": "person_name",
            "text": "Mahayla van der Shah",
            "start": 15044,
            "end": 16366
        },
        {
            "entity_type": "person_name",
            "text": "Vincent Miana Pragassam",
            "start": 16664,
            "end": 18550
        },
        {
            "entity_type": "organization",
            "text": "University of Cambridge",
            "start": 19976,
            "end": 21526
        },
        {
            "entity_type": "person_name",
            "text": "Mahayla",
            "start": 21718,
            "end": 22342
        },
        {
            "entity_type": "occupation",
            "text": "professor",
            "start": 22616,
            "end": 22942
        },
        {
            "entity_type": "occupation",
            "text": "machine learning",
            "start": 23144,
            "end": 24006
        },
        {
            "entity_type": "occupation",
            "text": "artificial intelligence",
            "start": 24128,
            "end": 25354
        },
        {
            "entity_type": "occupation",
            "text": "medicine",
            "start": 25604,
            "end": 26254
        },
        {
            "entity_type": "person_name",
            "text": "Vincent",
            "start": 26624,
            "end": 26998
        },
        {
            "entity_type": "occupation",
            "text": "reader",
            "start": 27596,
            "end": 27826
        },
        {
            "entity_type": "occupation",
            "text": "neurology",
            "start": 27956,
            "end": 28534
        },
        {
            "entity_type": "occupation",
            "text": "consultant neurologist",
            "start": 28916,
            "end": 30106
        },
        {
            "entity_type": "medical_condition",
            "text": "prostate cancer",
            "start": 31004,
            "end": 32194
        },
        {
            "entity_type": "medical_condition",
            "text": "Prostate cancer",
            "start": 44090,
            "end": 44962
        },
        {
            "entity_type": "medical_condition",
            "text": "prostate cancer",
            "start": 60908,
            "end": 61942
        },
        {
            "entity_type": "medical_condition",
            "text": "prostate cancer",
            "start": 68036,
            "end": 68782
        },
        {
            "entity_type": "medical_process",
            "text": "surgery",
            "start": 76292,
            "end": 76870
        },
        {
            "entity_type": "occupation",
            "text": "clinician",
            "start": 106496,
            "end": 106894
        },
        {
            "entity_type": "occupation",
            "text": "clinician",
            "start": 122168,
            "end": 122614
        },
        {
            "entity_type": "person_name",
            "text": "Michaela",
            "start": 160474,
            "end": 161026
        },
        {
            "entity_type": "occupation",
            "text": "artificial intelligence",
            "start": 179936,
            "end": 180814
        },
        {
            "entity_type": "occupation",
            "text": "machine learning",
            "start": 181064,
            "end": 182000
        },
        {
            "entity_type": "occupation",
            "text": "clinicians",
            "start": 241556,
            "end": 242230
        },
        {
            "entity_type": "person_name",
            "text": "Changhi",
            "start": 306536,
            "end": 307330
        },
        {
            "entity_type": "occupation",
            "text": "students",
            "start": 308012,
            "end": 308358
        },
        {
            "entity_type": "person_name",
            "text": "Hillary",
            "start": 308636,
            "end": 309034
        },
        {
            "entity_type": "medical_condition",
            "text": "prostate",
            "start": 345346,
            "end": 345850
        },
        {
            "entity_type": "medical_process",
            "text": "MRI",
            "start": 345910,
            "end": 346630
        }
    ]
}
