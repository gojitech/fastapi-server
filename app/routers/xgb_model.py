# import pickle
# from pydoc import locate
#from typing import List
# import time
# import os
# import numpy as np
# import pandas as pd
# import uvicorn
#from fastapi import FastAPI
from fastapi import APIRouter
#from fastapi.responses import JSONResponse


#from operator import itemgetter
# import re
# import nltk
# import nltk.data
# from string import punctuation 
# from nltk.stem import WordNetLemmatizer
# from nltk.tokenize import sent_tokenize,word_tokenize
# nltk.download('wordnet')
# nltk.download('omw-1.4')

# import stanza
# download and initialize a mimic pipeline with an i2b2 NER model
# stanza.download('en', package='mimic', processors={'ner': 'i2b2'})
#nlp = stanza.Pipeline('en', package='mimic', processors={'ner': 'i2b2'})

# from spellchecker import SpellChecker
# spell=SpellChecker()
# from Levenshtein import distance



# from pydantic import BaseModel
# from xgboost import XGBClassifier
# from sklearn.feature_extraction.text import TfidfVectorizer
#Path for the required files
# base_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
# model_pickle= os.path.join(base_dir, 'internal/dictionary/xgbmodel_tfidf_prob.pkl')
# tfidf_vectorizer_pickle=os.path.join(base_dir, 'internal/dictionary/tfidf_vect_xgb.pkl')
# words_pickle=os.path.join(base_dir, 'internal/dictionary/abbreviations_expand.pkl')
# classifier_drugs_csv=os.path.join(base_dir, 'internal/dictionary/classifier_drugs.csv')
# med_words_file=os.path.join(base_dir, 'internal/dictionary/wordlist.txt')



## unpickling model and tfidf vectorizer
# model = pickle.load(open(model_pickle, "rb"))
# tfidf_vectorizer=pickle.load(open(tfidf_vectorizer_pickle, "rb"))
# words=pickle.load(open(words_pickle, 'rb'))
#reading drugs names
# file = open(classifier_drugs_csv, "r")
# d = file.read()
# d_list = d.split("\n")

#Reading medical dictionary
# txt_file = open(med_words_file, "r")
# file_content = txt_file.read()

# content_list = file_content.split("\n")
# txt_file.close()
# for i in range(len(content_list)):
#   content_list[i] = content_list[i].lower()
# med_words=set(content_list)

router = APIRouter()

# class QueryText(BaseModel):
#   query : str 

# #Output resonse format
# class Outputresponse(BaseModel):
#     query: List[str]
#     error_code: int
#     error_messages: List[str]
#     run_time_in_secs: float
#     api_version: int
#     source: str
#     predictions: dict

#This function takes data and extract symptoms from Stanza
# def symptoms_separate(notes):
#   print("I have entered into extracting symptoms in Stanza function with input:",notes)
#   notes=" ".join(notes)
#   doc = nlp(notes)
#   symp=[]
# # print out all entities
#   for ent in doc.entities:
#     symp.append(ent.text)
#   symp=set(symp)
#   symp=list(symp)
#   print("Extracted output from Stanza is:",symp)
#   return symp

#Expanding the abbreviations for the symptoms
# def expand(text):
#   print("I have entered the abbreviation expanding function with text:",text)
#   text=text
#   revised_text = [] 
#   word = word_tokenize(text)
#   for w in word:
#     if w in words.keys():
#       text = re.sub(r"\b%s\b" % w,words[w],text)
    

#   revised_text.append(text)
#   print("I am leaving expand function with the output:",revised_text)
#   return " ".join(revised_text)

#Cleaning the text for punctuation,extra space, stopwords and lemmatization 
# def text_cleaning(text):
#   print("I am going to clean the text:",text)
#     # Clean the text, with the option to remove stop_words and to lemmatize word
#     # Clean the text
#   text = re.sub(r"[^A-Za-z0-9]", " ", text)
#   text = re.sub(r"\'s", " ", text)
#   text = re.sub(r"http\S+", " link ", text)
#  # text = re.sub(r"\b\d+(?:\.\d+)?\s+", "", text)  # remove numbers
    
#     # Remove punctuation from text
#   text = "".join([c for c in text if c not in punctuation])
#   #stop_words = stopwords.words("english")
#   stopwords = nltk.corpus.stopwords.words('english')
#   text = text.split()
#   text = [w for w in text if not w in stopwords]
#   text = " ".join(text)
        
#   text = text.split()
#   lemmatizer = WordNetLemmatizer()
#   lemmatized_words = [lemmatizer.lemmatize(word) for word in text]
#   text = " ".join(lemmatized_words)

#   print("I am returning the cleaned text:",text)      
#     # Return a list of words
#   return text 

#Pyspellchecker for correcting any spelling errors 
# def number_corrections(original,corrected):
#   sugg_list=[]
#   word=original
#   corrected=corrected
#   for corr in corrected:
#     #print(word)
#     #print(corr)
#     org_length=len(word)
#     #print("length of word:", org_length)
#     if org_length<=5:
#       sug=[]
      
#       edit_dist=distance(word, corr)
#       if edit_dist<2:
#         sug.append(corr)
#     else:
#       sug=[]
#       edit_dist=distance(word,corr)
#       if edit_dist<3:
#         sug.append(corr)

#     sugg_list.append(sug)
  
#   return sugg_list

# def suggestions_final(corrected):
#   suglist=[]
#   sug=[]
#   corrected=corrected
#   for cor in corrected:   
#     if cor in med_words:  #checking for the word in the custom dictionary
#       sug.append(cor) 
#   if len(sug) > 0:
#     suglist.append(sug[0]) #if word is in dictionary first from the list is selected
#   else :
#     suglist.append(corrected[0]) #word not in dictionary suggested first replacement is selected
#   return " ".join(suglist)

# def correct_spell(symptomslist) :
#   corrected = []
#   #words=word_tokenize(symptomslist)
#   words=symptomslist
#   for w in words:
#       ok = spell.unknown([w])                              # check spelling
#       if len(ok)>0:
#           suggestions = spell.candidates(w)
#           if len(suggestions) > 0:                            # there are suggestions
              
#               remove_corr=number_corrections(w,suggestions)   #suggestions based on no. of replacements 
#               #print("After remove_corr:",remove_corr)
#               flat_list = [item for sublist in remove_corr for item in sublist]

#               if len(flat_list) > 0:
#                 final=suggestions_final(flat_list)            #suggestions from medical dictionary 
#               #print("After final suggestions:",final)
#                 corrected.append(final)
#           else:
#             corrected.append(w)                             # there's no suggestion for a correct word
#       else:
#           corrected.append(w)                                 # this word is correct
#   #corrected_flat = [it for subl in corrected for it in subl]
#   return " ".join(corrected)

# def corrections(text):
#   print("I have entered the spell checking function with input:",text)
#   corrections=[]
#   from nltk.corpus.reader import wordlist
#   words=word_tokenize(text)
#   test=correct_spell(words)

#   corrections.append(test)
#   print("I am leaving spell checking function with output:",corrections)
#   return " ".join(corrections)


@router.post("/predict_proba", tags=["xgb_model"])
#def predict_prob(data: QueryText):
def predict_prob():
  return {}
  # pred_data={}
  # time_of_req=time.time()
  # error_code=1
  # error_messages=[]
  # original_query=[data.query]
  #     #Checking for input string whether null or text is there
  # if (len(data.query.strip()))<=0:
  #   print("Error with input string")
  #   error_messages.append("Error with input string:")
  #   pred_data={}
  #   return Outputresponse(query= original_query, error_code=error_code,error_messages=error_messages,run_time_in_secs=(time.time() - time_of_req),predictions=pred_data, api_version=1, source="xgb model")


  # if (len(data.query.strip()))>0:

  #   symptoms=symptoms_separate(original_query)  #stanza
  #   symptoms= " ".join(symptoms)
  #   symptoms=symptoms.lower()
  #   symp=symptoms

  #   print("Symptoms from Stanza:",symptoms)

  #   if (len(symptoms.strip()))<=0:
  #     print("No symptoms found from Stanza")
  #     error_messages.append("No symptoms found from Stanza:")
  #     pred_data={}
  #     return Outputresponse(query= original_query, error_code=error_code,error_messages=error_messages,run_time_in_secs=(time.time() - time_of_req),predictions=pred_data, api_version=1, source="Model")

  #   else: 
  #     try:

  #       expandsymptoms=expand(symptoms)
  #       symp=expandsymptoms
  #       cleanedsymptoms=text_cleaning(expandsymptoms)
  #       symp=cleanedsymptoms
  #       spellchecksymptoms=corrections(cleanedsymptoms)
  #       symp=spellchecksymptoms

  #     except:
  #       print("Error in the Expanding/Cleaning/Spellchecking functions")
  #       error_messages.append("Error in the Expanding/Cleaning/Spellchecking functions")

  #     try:
  #       test_tfidf = tfidf_vectorizer.transform([symp])
  #       pred_proba=model.predict_proba(test_tfidf)
  
  #       flat_pred = [item for sublist in pred_proba for item in sublist]
  #       topdrugs=[]
  #       topdrugs.append(sorted(zip(flat_pred, d_list), reverse=True))
  
  #       flat_toppred = [item for sublist in topdrugs for item in sublist]
  #       error_code=0
  #       print("Predictions",flat_toppred)
  #       pred_data={}
  #       pred_data['predictions']=[]
  #       for pred in flat_toppred:
  #         pred_data['predictions'].append({'drugname': pred[1], 'probability':"{:.4f}".format(float(pred[0]))})


  #     except Exception as e:
  #        print("Error from the model")
  #        error_messages.append("Error from the model")
      

  # return Outputresponse(query= original_query, error_code=error_code,error_messages=error_messages,run_time_in_secs=(time.time()-time_of_req),predictions=pred_data, api_version=1, source="Model")


