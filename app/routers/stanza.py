#import time
from fastapi import APIRouter
#from ..internal.models import StanzaEntities, QueryText
#from typing import List
#from ..internal.utils import stanza_find_medical_entities, chunk_input_text_sent_boundary

router = APIRouter()

# def merge_entity_chunks_stanza(list_of_entities: List[StanzaEntities]):
#     query: str = ''
#     symptoms: List[str] = []
#     body_structures: List[str] = []
#     medications: List[str] = []
#     conditions: List[str] = []
#     procedures: List[str] = []
#     error_code: int = 1
#     error_messages: List[str] = []
#     run_time_in_secs: float = 0.0
#     api_version: int = 1.1
#     source: str = 'stanza'

#     for entity in list_of_entities:
#         #query += entity.query #since we're appending initial query, we don't need to do this
#         symptoms = list(set(symptoms) | set(entity.symptoms)) 
#         body_structures = list(set(body_structures) | set(entity.body_structures))
#         medications = list(set(medications) | set(entity.medications))
#         conditions = list(set(conditions) | set(entity.conditions))
#         procedures = list(set(procedures) | set(entity.procedures))
#         error_code += entity.error_code
#         error_messages += entity.error_messages
    
#     #cleaning up results:
#     if len(error_messages) == 0:
#         error_code = 0


#     return StanzaEntities(query=query, symptoms=symptoms, body_structures=body_structures, medications=medications, conditions=conditions, procedures=procedures, error_code=error_code, error_messages=error_messages, api_version=api_version, source=source, run_time_in_secs=run_time_in_secs)

#@router.post("/", response_model=StanzaEntities, tags=['stanza'])
@router.post("/", tags=['stanza'])
#def get_stanza_entities(req: QueryText):
def get_stanza_entities():
    return {}
    # entities_of_chunked_stanza: List[StanzaEntities] = []
    
    # time_of_req = time.time()
    # #print('incoming request to /stanza fxn: ', req.query[0:50], 'of len:', len(req.query), ' at ', time_of_req)
    
    # chunked_query = chunk_input_text_sent_boundary(req.query)
    # #print(len(chunked_query), ' chunks found')
    
    # for chunk in chunked_query:
    #     entities = stanza_find_medical_entities(chunk)
    #     entities_of_chunked_stanza.append(entities)
    
    # #merge entity_chunks
    # entities_of_chunked_stanza = merge_entity_chunks_stanza(entities_of_chunked_stanza)
    
    # entities_of_chunked_stanza.query = req.query
    # entities_of_chunked_stanza.run_time_in_secs = time.time() - time_of_req
    
    # return entities_of_chunked_stanza
    #return {'query': req.query, 'symptoms': problems, 'body_structures' : anatomy, 'medications': treatments, 'conditions' : diseases, 'procedures' : tests, 'error_code' : error_code, 'error_messages' : error_messages, 'run_time_in_secs': (end_time-time_of_req), 'api_version': 1.0, 'source': 'stanza'}