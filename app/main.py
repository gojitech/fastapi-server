from fastapi import FastAPI, BackgroundTasks, Request, status

from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from fastapi.middleware.cors import CORSMiddleware
from app.routers import assembly_entities, assembly_topics, stanza, annotate_text, dictionary, soap_notes, openai,xgb_model, database

from app.internal.resources.sqlite.database import SessionLocal, engine
from app.internal.resources.sqlite import models
from app.internal.slack_api import report_error_via_slack

#Queue related imports
import time
import multiprocessing as mp
from multiprocessing import Process

# will create database table when server boots up however you'd normally want to use migrations?
models.Base.metadata.create_all(bind=engine)

app = FastAPI()

#Logic to override request validation exceptions
@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    #Fire off Slack message
    payload_text = f"• *detail:* {exc.errors()}\n• *body:* {exc.body}\n"
    report_error_via_slack(payload_text)
    
    #Respond to user
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": exc.errors(), "body": exc.body})
    )

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(stanza.router, prefix='/api/v1/stanza')
app.include_router(annotate_text.router, prefix='/api/v1/annotate_text')
app.include_router(dictionary.router, prefix='/api/v1/dictionary')
app.include_router(assembly_topics.router, prefix='/api/v1/assembly/topics')
app.include_router(assembly_entities.router, prefix='/api/v1/assembly/entities')
app.include_router(soap_notes.router, prefix='/api/v1/soap_notes')
app.include_router(openai.router, prefix='/api/v1/openai')
app.include_router(xgb_model.router, prefix='/api/v1/xgb_model')
app.include_router(database.router, prefix="/api/v1/database")
