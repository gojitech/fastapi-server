#import time
from fastapi import APIRouter
#from ..internal.models import QueryEntities, SOAPFormat, SOAPNotes, SymStatus, ConditionStatus, ProcedureStatus, TreatmentStatus
#from typing import List

router = APIRouter()

@router.post("/")
#def get_soap_notes(req: QueryEntities) -> SOAPNotes:    
def get_soap_notes():
    """
    Create soap notes for a list of entities and their statuses.
    """
    return {}
#     time_of_req = time.time()
#     #print('incoming request to /soap_notes endpoint: ', req.query[0:50], ' at ', time_of_req)
        
#     symptom_sentences = []
#     condition_sentences = []
#     procedure_sentences = []
#     medication_sentences = []
    
#     error_code: int = 1
#     error_messages: List[str] = []
#     run_time_in_secs: float = 0.0
#     api_version: int = 1.0
#     source: str = 'soap_notes_basic'
    
#     symptoms = req.symptoms
#     conditions = req.conditions
#     procedures = req.procedures
#     medications = req.medications

#     #return {"time_of_onset": time_of_onset, "frequency_tempo": frequency_tempo, "duration_alltime": duration_alltime, "duration_episodic": duration_episodic}
#     for symptom in symptoms:
#         if symptom.status:
#             #Pt is experiencing <symptom>. It began <time_of_onset>. This occurs <frequency_tempo>. Pt has been experiencing for <duration_alltime>. Each episode lasts for <duration_frequency>. It appears to be <status_change>.  
#             keyword = symptom.keyword
#             symptom_status_label = symptom.status[0].label
#             time_of_onset = symptom.properties.time_of_onset
#             frequency_tempo = symptom.properties.frequency_tempo
#             duration_alltime = symptom.properties.duration_alltime
#             duration_episodic = symptom.properties.duration_episodic
#             change_status = symptom.properties.change_status

#             #Temporarily adding acceptance for Experience Theoretical to check SOAP note logic
            
#             #if not time_of_onset and symptom_status_label == SymStatus.EXPERIENCED and symptom.label_score >= 0:
#             if not time_of_onset and (symptom_status_label == SymStatus.EXPERIENCED) and  symptom.label_score  >= 0.9:
#                 symptom_sentences.append("Pt experienced {}.".format(keyword))
#             elif not time_of_onset and symptom_status_label == SymStatus.NOT_EXPERIENCED and  symptom.label_score  >= 0.9:
#                 symptom_sentences.append("Pt did not experience {}.".format(keyword))
#             elif time_of_onset and (symptom_status_label == SymStatus.EXPERIENCED) and  symptom.label_score  >= 0.9:
#                 if time_of_onset != 'unknown':
#                     symptom_sentences.append("Pt experienced {}, it began {}.".format(keyword, time_of_onset))
#                 else:
#                     symptom_sentences.append("Pt experienced {}.".format(keyword))
#                 if frequency_tempo and frequency_tempo != 'unknown':
#                     symptom_sentences.append("This occurs {}.".format(frequency_tempo))
#                 if duration_alltime and duration_alltime != 'unknown':
#                     symptom_sentences.append("Pt has been experiencing for {}.".format(duration_alltime))
#                 if duration_episodic and duration_episodic != 'unknown':                   
#                     symptom_sentences.append("Each episode lasts for {}.".format(duration_episodic))
#                 if symptom_status_label and symptom_status_label != 'unknown':
#                     symptom_sentences.append("{} appears to be {}.".format(keyword, change_status))
#             elif time_of_onset and symptom_status_label == SymStatus.NOT_EXPERIENCED and  symptom.label_score  >= 0.9:
#                 symptom_sentences.append("Pt did not experience {} {}".format(keyword, time_of_onset))

#     #return {"time_of_onset": time_of_onset, "frequency": frequency, "duration": duration, "duration_flareup": duration_flareup}
#     for condition in conditions:
#         if condition.status:
#             keyword = condition.keyword
#             condition_status_label = condition.status[0].label
#             time_of_onset = condition.properties.time_of_onset
#             frequency = condition.properties.frequency
#             duration = condition.properties.duration
#             duration_flareup = condition.properties.duration_flareup

#             if not time_of_onset and condition_status_label == ConditionStatus.PRESENT and condition.label_score  >= 0.9:
#                 condition_sentences.append("Pt has communicated present or past history of {}.".format(keyword))
#             elif not time_of_onset and condition_status_label == ConditionStatus.PRESENT and condition.label_score  >= 0.9:
#                 condition_sentences.append("Pt has denied presence of {}.".format(keyword))
#             elif time_of_onset and condition_status_label == ConditionStatus.PRESENT and condition.label_score  >= 0.9:
#                 if time_of_onset != 'unknown':
#                     condition_sentences.append("Pt has communicated present or past history of {}, it began {}.".format(keyword, time_of_onset))
#                 else:
#                     condition_sentences.append("Pt has communicated present or past history of {}.".format(keyword))
#                 if frequency and frequency != 'unknown':
#                     condition_sentences.append("The condition presents itself {}.".format(frequency))
#                 if duration and duration != 'unknown':
#                     condition_sentences.append("Pt has had the condition for {}.".format(duration))
#                 if duration_flareup and duration_flareup != 'unknown':
#                     condition_sentences.append("Pt has been experiencing flare-up for {}.".format(duration_flareup))
#                 if condition_status_label and condition_status_label != 'unknown':
#                     condition_sentences.append("{} appears to be {}.".format(keyword, change_status))
#             elif time_of_onset and condition_status_label == ConditionStatus.ABSENT and condition.label_score  >= 0.9:
#                 if time_of_onset != 'unknown':
#                     condition_sentences.append("Pt has denied presence of {}, it began {}.".format(keyword, time_of_onset))
#                 else:
#                     condition_sentences.append("Pt has denied presence of {}.".format(keyword))

# #return {"frequency":frequency, "time_of_study":time_of_study}
#     for procedure in procedures:
#         if procedure.status:
#             keyword = procedure.keyword
#             frequency = procedure.properties.frequency
#             time_of_study = procedure.properties.time_of_study
#             procedure_order_status = procedure.status[0].label
#             procedure_completion_status = procedure.status[1].label

#             if not time_of_study and procedure_order_status == ProcedureStatus.ORDERED and procedure.label_score  >= 0.9:
#                 procedure_sentences.append("{} was ordered.".format(keyword))
#             elif procedure_order_status == ProcedureStatus.NOT_ORDERED and procedure.label_score  >= 0.9:
#                 procedure_sentences.append("{} was not ordered.".format(keyword))
#             elif time_of_study and procedure_order_status == ProcedureStatus.ORDERED and procedure.label_score  >= 0.9:
#                 if time_of_study != 'unknown':
#                     procedure_sentences.append("{} was ordered for {}.".format(keyword, time_of_study))
#                 else:
#                     procedure_sentences.append("{} was ordered.".format(keyword))
#                 if frequency and frequency != 'unknown':
#                     procedure_sentences.append("The procedure is performed / will be performed {}.".format(frequency))
            
            
#             if procedure_completion_status == ProcedureStatus.COMPLETED and procedure.label_score  >= 0.9:
#                 procedure_sentences.append("{} has been completed.".format(keyword))
#             if procedure_completion_status == ProcedureStatus.INCOMPLETE and procedure.label_score  >= 0.9:
#                 procedure_sentences.append("{} has not been completed.".format(keyword))
    
#     #return {"frequency":frequency, "duration_how_long_pt_taking": duration_how_long_pt_taking, "duration_as_per_physician_instructions": duration_as_per_physician_instructions}
#     for medication in medications:
#         if medication.status:
#             keyword = medication.keyword
#             status = medication.status[0].label
#             frequency = medication.properties.frequency
#             duration_how_long_pt_taking = medication.properties.duration_how_long_pt_taking
#             duration_as_per_physician_instructions = medication.properties.duration_as_per_physician_instructions

#             if not duration_how_long_pt_taking and status == TreatmentStatus.EXPERIENCED and medication.label_score  >= 0.9:
#                 medication_sentences.append("Pt is taking {}.".format(keyword))
#             elif not duration_how_long_pt_taking and status == TreatmentStatus.NOT_EXPERIENCED and medication.label_score  >= 0.9:
#                 medication_sentences.append("Pt is not taking {}.".format(keyword))
#             elif duration_how_long_pt_taking and status == TreatmentStatus.EXPERIENCED and medication.label_score  >= 0.9:
#                 if duration_how_long_pt_taking != "unknown":
#                     medication_sentences.append("Pt is taking {} for {}.".format(keyword, duration_how_long_pt_taking))
#                 else:
#                     medication_sentences.append("Pt is taking {}.".format(keyword))
#                 if frequency and frequency != "unknown":
#                     medication_sentences.append("Pt is taking medication {}.".format(frequency))
#             elif duration_as_per_physician_instructions and medication.label_score  >= 0.9:
#                 if duration_as_per_physician_instructions != "unknown":
#                     medication_sentences.append("Pt has been instructed to take {} for {}.".format(keyword, duration_as_per_physician_instructions))
#                 else:
#                     medication_sentences.append("Pt has been instructed to take {}.".format(keyword))
#                 if frequency and frequency != "unknown":
#                     medication_sentences.append("Pt has been instructed to take medication {}.".format(frequency)) 

#     subjective = " ".join(symptom_sentences)
#     objective = " ".join(condition_sentences) + " " + " ".join(procedure_sentences)
#     assessment = ""
#     plan = ""
#     others = " ".join(medication_sentences)
    
#     note_in_soap_format = SOAPFormat(subjective=subjective, objective=objective, assessment=assessment, plan=plan, others=others)

#     return SOAPNotes(query=req, note=note_in_soap_format, error_code=error_code, error_messages=error_messages, run_time_in_secs=run_time_in_secs, api_version=api_version, source=source)