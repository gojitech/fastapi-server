from typing import List, Optional
from pydantic import BaseModel
from enum import Enum
from uuid import UUID
from datetime import datetime

#Enums for medical summary:
class MedicalSpecialty(Enum):
    FAMILY_MEDICINE_GP = "Family Medicine/General Practice"

    # class Config: 
    #     use_enum_values = True

class ClinicalNoteType(Enum):
    SOAP = "SOAP"
    DAP = "DAP"
    DEAR = "DEAR"

    # class Config:
    #     use_enum_values = True

class BackgroundTaskStatus(Enum):
    SUCCESSFUL = 'Successful'
    FAILED = 'Failed'

    # class Config: 
    #     use_enum_values = True

#For SQLite database:
class MedicalSummaryBase(BaseModel):
    job_id: str
    callback_url: str
    response_json: str
    transcript: str
    note_template: str
    note_type: ClinicalNoteType
    medical_specialty: MedicalSpecialty
    medical_summary: str
    api_version: float
    status: BackgroundTaskStatus
    source: str

    class Config:
        use_enum_values = True


class MedicalSummaryCreate(MedicalSummaryBase):
    pass

class MedicalSummary(MedicalSummaryBase):
    id: int
    created_time: datetime 

    class Config:
        orm_mode = True

#Medical Summary related
class QueryMedicalSummary(BaseModel):
     transcript: str = ""
     note_type: Optional[ClinicalNoteType]
     medical_specialty: Optional[MedicalSpecialty]
     callback_url: str = ""

class MedicalSummaryInitialResponse(BaseModel):
    transcript: str = ""
    note_type: Optional[ClinicalNoteType]
    medical_specialty: Optional[MedicalSpecialty]
    callback_url: str = ""
    job_id: UUID

class MedicalSummaryProcessedResponse(BaseModel):
    transcript: str = ""
    note_type: Optional[ClinicalNoteType]
    medical_specialty: Optional[MedicalSpecialty]
    callback_url: str = ""
    job_id: UUID
    medical_summary: str
    template: str
    error_code: int = None
    error_messages: List[str] = []
    run_time_in_secs: float = None
    api_version: int = None
    source: str = None
    status: BackgroundTaskStatus
    

# Annotate Text Model
class SymStatus(str, Enum):
    EXPERIENCED = 'Experienced'
    NOT_EXPERIENCED = 'Not Experienced'
    EXPERIENCE_UNK = 'Experience Unknown'
    EXPERIENCE_THEOR = 'Experience, Theoretical'
    EXPERIENCE_OTHERS = "Other's Experience"

class ConditionStatus(str, Enum):
    PRESENT = 'Present'
    ABSENT = 'Absent'
    UNKNOWN = 'Unknown'
    EDUCATIONAL = 'Educational'

class ProcedureStatus(str, Enum):
    ORDERED = 'Ordered or Requested'
    NOT_ORDERED = 'Not Ordered or Requested'
    ORDER_STATUS_UNK = 'Uncertain about order status'
    COMPLETED = 'Completed or Performed'
    INCOMPLETE = 'Not Completed nor Performed'
    COMPLETION_UNK = 'Uncertain about completion status'

class TreatmentStatus(str, Enum):
    EXPERIENCED = 'Experienced'
    NOT_EXPERIENCED = 'Not Experienced'
    EXPERIENCE_UNK = 'Uncertain'

class Status(BaseModel):
    label: str = None
    value: float = None


class Symptom_Relationships(BaseModel):
    factor: str = None


class Symptom_Property_Specializations(BaseModel):
    unclear_condition: bool = None
    past: bool = None
    potential_attribute: str = None

class CSV(BaseModel):
    term: str = None
    similarity: float = None

class UMLS(BaseModel):
    term: str = None
    semtype: str = None
    similarity: float = None 


class Symptom_Property(BaseModel):
    time_of_onset: str = None
    frequency_tempo: str = None
    duration_alltime: str = None
    duration_episodic: str = None
    change_status: str = None
    location: str = None
    severity_level: str = None
    quality: str = None
    specializations: Symptom_Property_Specializations = None
    speaker_id: str = None
    umls: List[UMLS] = []
    csv: List[CSV] = []


class Condition_Property(BaseModel):
    time_of_onset: str = None
    frequency: str = None
    severity_level: str = None
    time_of_onset: str = None
    duration: str = None
    duration_flareup: str = None
    quality: str = None
    change_status: str = None
    location: str = None
    factor: str = None
    speaker_id: str = None
    umls: List[UMLS] = []
    csv: List[CSV] = []

class Findings_Property(BaseModel):
    value: float = None
    comments: str = None

class Procedure_Findings(BaseModel):
    recent: Findings_Property = None
    prior: Findings_Property = None


class Procedure_Property(BaseModel):
    location: str = None
    indication: str = None
    frequency: str = None
    time_of_study: str = None
    findings: Procedure_Findings = None
    speaker_id: str = None
    umls: List[UMLS] = []
    csv: List[CSV] = []


class Extra_Property(BaseModel):
    speaker_id: str = None
    csv: List[CSV] = []


class Body_Structure_Property(BaseModel):
    speaker_id: str = None
    umls: List[UMLS] = []
    csv: List[CSV] = []


class Medication_Property(BaseModel):
    frequency: str = None
    duration_how_long_pt_taking: str = None
    duration_as_per_physician_instructions: str = None
    speaker_id: str = None
    umls: List[UMLS] = []
    csv: List[CSV] = []



class Symptom(BaseModel):
    keyword: str = None
    original_keyword: str = None
    source_sentence: str = None
    label_score: float = None
    status: List[Status] = []
    properties: Symptom_Property = None
    relationships: Symptom_Relationships = None


class Body_Structure(BaseModel):
    keyword: str = None
    original_keyword: str = None
    source_sentence: str = None
    label_score: float = None
    status: List[Status] = []
    properties: Body_Structure_Property = None


class Medication(BaseModel):
    keyword: str = None
    original_keyword: str = None
    source_sentence: str = None
    label_score: float = None
    status: List[Status] = []
    properties: Medication_Property = None


class Condition(BaseModel):
    keyword: str = None
    original_keyword: str = None
    source_sentence: str = None
    label_score: float = None
    status: List[Status] = []
    properties: Condition_Property = None


class Procedure(BaseModel):
    keyword: str = None
    original_keyword: str = None
    source_sentence: str = None
    label_score: float = None
    status: List[Status] = []
    properties: Procedure_Property = None


class Extra(BaseModel):
    keyword: str = None
    original_keyword: str = None
    source_sentence: str = None
    label_score: float = None
    properties: Extra_Property = None


class Entities(BaseModel):
    query: str = None
    symptoms: List[Symptom] = []
    body_structures: List[Body_Structure] = []
    medications: List[Medication] = []
    conditions: List[Condition] = []
    procedures: List[Procedure] = []
    extra: List[Extra] = []
    error_code: int = None
    error_messages: List[str] = []
    run_time_in_secs: float = None
    api_version: int = None
    source: str = None

class QueryText(BaseModel):
    query: str

class QueryEntities(BaseModel):
    query: str
    symptoms: List[Symptom]
    conditions: List[Condition]
    procedures: List[Procedure]
    medications: List[Medication]

class SOAPFormat(BaseModel):
    subjective: str = ""
    objective: str = ""
    assessment: str = ""
    plan: str = ""
    others: str = ""

class ActionItems(BaseModel):
    query: str
    prescriptions: List[str] = []
    followups: List[str] = []
    consultations: List[str] = []
    labs: List[str] = []
    error_code: int
    error_messages: List[str]
    run_time_in_secs: float
    api_version: int
    source: str

class SOAPNotes(BaseModel):
    query: QueryEntities
    note: SOAPFormat
    error_code: int
    error_messages: List[str]
    run_time_in_secs: float
    api_version: int
    source: str

class ChiefComplaint(BaseModel):
    data: List[str] = []
    order: int = 1

class HistoryOfPresentingIllness(BaseModel):
    data: List[str] = []
    order: int = 2

class PhysicalExam(BaseModel):
    data: List[str] = []
    order: int = 3

class Investigations(BaseModel):
    data: List[str] = []
    order: int = 4

class SummaryAndPlan(BaseModel):
    data: List[str] = []
    order: int = 5

class PastMedicalHistory(BaseModel):
    data: List[str] = []
    order: int = 6

class CurrentMedications(BaseModel):
    data: List[str] = []
    order: int = 7

class Allergies(BaseModel):
    data: List[str] = []
    order: int = 8

class FamilyHistory(BaseModel):
    data: List[str] = []
    order: int = 9

class SocialHistory(BaseModel):
    data: List[str] = []
    order: int = 10

class ExtendedSOAPNoteFormat(BaseModel):
    chief_complaint: ChiefComplaint
    history_of_presenting_illness: HistoryOfPresentingIllness
    physical_exam: PhysicalExam
    investigations: Investigations
    summary_and_plan: SummaryAndPlan
    past_medical_history: PastMedicalHistory
    current_medications: CurrentMedications
    allergies: Allergies
    family_history: FamilyHistory
    social_history: SocialHistory

class ClinicalNoteOpenAIBackground(BaseModel):
    query: str
    medical_specialty: MedicalSpecialty
    note: ExtendedSOAPNoteFormat
    error_code: int
    error_messages: List[str]
    run_time_in_secs: float
    api_version: int
    job_id: UUID
    source: str
    status: str

class ClinicalNoteOpenAI(BaseModel):
    query: str
    chief_complaint: ChiefComplaint
    history_of_presenting_illness: HistoryOfPresentingIllness
    physical_exam: PhysicalExam
    investigations: Investigations
    summary_and_plan: SummaryAndPlan
    past_medical_history: PastMedicalHistory
    current_medications: CurrentMedications
    allergies: Allergies
    family_history: FamilyHistory
    social_history: SocialHistory
    error_code: int
    error_messages: List[str]
    run_time_in_secs: float
    api_version: int
    source: str

#This is not a good name...
class QueryTextRealTime(BaseModel):
    current_text: str
    previous_text: Optional[str] = ""
    previous_clinicalnote_response: Optional[ClinicalNoteOpenAI] = None
    callback_url: str

class InitialRespRealTime(BaseModel):
    current_text: str
    previous_text: Optional[str] = ""
    previous_clinicalnote_response: Optional[ClinicalNoteOpenAI] = None
    callback_url: str
    job_id: UUID


# Stanza Model
class StanzaEntities(BaseModel):
    query: str = None
    symptoms: List[str] = []
    body_structures: List[str] = []
    medications: List[str] = []
    conditions: List[str] = []
    procedures: List[str] = []
    error_code: int = None
    error_messages: List[str] = []
    run_time_in_secs: float = None
    api_version: int = None
    source: str = None


class StanzaEntityList(BaseModel):
    __root__: List[StanzaEntities]

# Assembly Topics/Entities


class AssemblyIabCategoryLabel(BaseModel):
    relevance: float = None
    label: str = None


class AssemblyIabTimeStamp(BaseModel):
    start: float = None
    end: float = None


class AssemblyIabCategoryResult(BaseModel):
    text: str = None
    labels: List[AssemblyIabCategoryLabel] = []
    timestamp: AssemblyIabTimeStamp


class AssemblyIabCategoryList(BaseModel):
    status: str = None
    results: List[AssemblyIabCategoryResult] = []


class AssemblyFullResponse(BaseModel):
    id: str = None
    status: str = None
    text: str = None
    iab_categories: bool = None
    iab_categories_result: AssemblyIabCategoryList


class Topics(BaseModel):
    text: str = None
    score: float = None
    label: str = None


class AssemblyTopics(BaseModel):
    id: str = None
    query: str = None
    topics: List[Topics] = []
    extra: List[Extra] = []
    error_code: int = None
    error_messages: List[str] = []
    run_time_in_secs: float = None
    api_version: int = None
    source: str = None


class AssemblyEntityType(str, Enum):
    OCCUPATION = 'occupation'
    PERSON_NAME = 'person_name'
    ORGANIZATION = 'organization'
    MEDICAL_CONDITION = 'medical_condition'
    MEDICAL_PROCESS = 'medical_process'


class AssemblyWord(BaseModel):
    text: str = None
    start: int = None
    end: int = None
    confidence: float = None
    speaker: str = None


class AssemblyAutoHighlightsResult(BaseModel):
    count: int = None
    rank: float = None
    text: str = None
    timestamps: List[AssemblyIabTimeStamp] = []


class AssemblyAutoHighlightResultList(BaseModel):
    status: str = None
    results: List[AssemblyAutoHighlightsResult] = []


class AssemblyEntity(BaseModel):
    entity_type: str = None
    text: str = None
    start: int = None
    end: int = None


class AssemblyEntityRequest(BaseModel):
    text: str = None
    words: List[AssemblyWord] = []
    auto_highlights: bool = None
    auto_highlights_result: AssemblyAutoHighlightResultList
    entities: List[AssemblyEntity] = []


class AssemblySymptom(BaseModel):
    keyword: str = None
    original_keyword: str = None
    source_sentence: str = None
    label_score: float = None
    properties: Symptom_Property = None


class AssemblyEntityResponse(BaseModel):
    query: str = None
    symptoms = []
    procedures = []
    extra = []
    error_code: int = None
    error_messages = []
    run_time_in_secs: float = None
    api_version: int = None
    source: str = None
