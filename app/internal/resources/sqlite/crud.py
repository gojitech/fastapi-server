from sqlalchemy.orm import Session

from app.internal.resources.sqlite import models
from app.internal import models as schemas

#Get a MedicalSummary:
def get_medical_summary_by_specialty(db: Session, medical_specialty: str):
    return db.query(models.MedicalSummary).filter(models.MedicalSummary.medical_specialty == medical_specialty).first()

def get_medical_summary_by_id(db: Session, medical_summary_id: id):
    return db.query(models.MedicalSummary).filter(models.MedicalSummary.id == medical_summary_id).first()

def get_medical_summaries(db: Session, skip: int = 0, limit: int = 50):
    return db.query(models.MedicalSummary).offset(skip).limit(limit).all()

#Create a MedicalSummary:
def create_medical_summary(db: Session, medical_summary: schemas.MedicalSummaryCreate):
    print(f"\nSummary prior to db insertion: {medical_summary}")
    db_medical_summary = models.MedicalSummary(**medical_summary.dict())
    db.add(db_medical_summary)
    db.commit()
    db.refresh(db_medical_summary)
    
    return db_medical_summary