from fastapi import Depends
import openai
import os
import re
import random
import time
import httpx
import json

from sqlalchemy.orm import Session
from app.internal.resources.sqlite.database import SessionLocal


from app.internal.utils import return_text_as_sent, chunk_input_text_sent_boundary, safe_list_get
from app.internal.models import BackgroundTaskStatus, ClinicalNoteType, MedicalSummaryProcessedResponse, MedicalSummaryCreate
from app.internal.resources.medical_note_templates import SOAP_TEMPLATE, DAP_TEMPLATE, DEAR_TEMPLATE
from app.internal.resources.sqlite import crud
from app.internal.slack_api import report_error_via_slack
#from app.routers.database import create_medical_summary

#Dependency
def get_db():
  db = SessionLocal()
  try: 
    yield db
  finally:
    db.close()

def trim_openai_resp(openai_resp):
  if openai_resp:
    try:
      text =  openai_resp["choices"][0]["text"][0:100] + '...'
      created = openai_resp["created"]
      id = openai_resp["id"]
      model = openai_resp["model"]
      return {"completion_text":text, "created":created, "id":id, "model":model}
    except Exception as e:
      return f"Error trimming openai_resp: {e}"
  else:
    return "N/A" 

def fake_prompt_davinci_actionitems(text):
    db = [{
    "choices": [
      {
        "finish_reason": "stop",
        "index": 0,
        "logprobs": "null",
        "text": "\nMedications: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##.\nLabs: X-ray\nFollow-ups: ##Disclaimer## Mock API ##Disclaimer##Follow up with primary care physician in 2 weeks.\nConsultations: ##Disclaimer## Mock API ##Disclaimer##. Orthopedic consultation in 2 weeks."
      }
    ],
    "created": 1653935843,
    "id": "cmpl-5DeYV9yx65Yv9enLXVqlj2mL3qA5A",
    "model": "text-davinci-002",
    "object": "text_completion"
  },
  {
    "choices": [
      {
        "finish_reason": "stop",
        "index": 0,
        "logprobs": "null",
        "text": "\n    Medications: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##.\nLabs: None\nFollow-ups: Follow-up with a doctor in 4 days.\nConsultations: None"
      }
    ],
    "created": 1653935847,
    "id": "cmpl-5DeYZCHz9vRyrt2TTPDTtEVDIIQad",
    "model": "text-davinci-002",
    "object": "text_completion"
  },
  {
    "choices": [
      {
        "finish_reason": "stop",
        "index": 0,
        "logprobs": "null",
        "text": "\nMedications: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##.\nLabs: X-ray\nFollow-ups:##Disclaimer## Mock API ##Disclaimer##. Follow up with primary care physician in 2 weeks.\nConsultations: Orthopedic consultation in 2 weeks."
      }
    ],
    "created": 1653935843,
    "id": "cmpl-5DeYV9yx65Yv9enLXVqlj2mL3qA5A",
    "model": "text-davinci-002",
    "object": "text_completion"
  },
  {
    "choices": [
      {
        "finish_reason": "stop",
        "index": 0,
        "logprobs": "null",
        "text": "\n    Medications: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##.\nLabs: None\nFollow-ups: ##Disclaimer## Mock API ##Disclaimer##. Follow-up with a doctor in 4 days.\nConsultations: None"
      }
    ],
    "created": 1653935847,
    "id": "cmpl-5DeYZCHz9vRyrt2TTPDTtEVDIIQad",
    "model": "text-davinci-002",
    "object": "text_completion"
  },
  {
    "choices": [
      {
        "finish_reason": "stop",
        "index": 0,
        "logprobs": "null",
        "text": "\n\nMedications: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##.\nLabs: None\nFollow-ups: None\nConsultations: None"
      }
    ],
    "created": 1653935852,
    "id": "cmpl-5DeYePAePpFc3UrmHjQGfTI4HAv5b",
    "model": "text-davinci-002",
    "object": "text_completion"
  },
  {
  "choices": [
    {
      "finish_reason": "stop",
      "index": 0,
      "logprobs": "null",
      "text": "\n    Medications: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##.\nLabs: None\nFollow-ups: ##Disclaimer## Mock API ##Disclaimer##. Follow-up with sports medicine doctor.\nConsultations: Follow-up with sports medicine doctor."
    }
  ],
  "created": 1653935863,
  "id": "cmpl-5DeYpj1XiSTGeZkgsCjjs5t4KquG9",
  "model": "text-davinci-002",
  "object": "text_completion"
  }
  ]

    num = random.randint(0,4)

    return db[num]

def prompt_davinci_actionitems(text, **kwargs):
    openai.api_key = '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'
    #openai.api_key = os.environ.get('OPENAI_API_KEY')
    #pull kwargs if present, else use default
    max_completion_length = kwargs.get('max_length', 320)

    prompt = """
        This tool identifies any new medications, changes to current medications, future medical tests, follow-ups with physician and future medical consultations discussed in the conversation:

        Conversation: Hello, this is Dr. M calling for C. Hello? Hi, this is Dr. M calling for C. This is C. How are you today? I am good today. Alright. So I believe you're calling to follow up with lab results, correct? Yes. All right, so let me go over them with you. All right. Based on your blood work here, few concerns. One is, your cholesterol level is extremely high. Okay. Your total cholesterol is actually okay. But the only main concerns I see is your thyroid is showing signs that you might be having too much thyroid. And also your vitamin D level is low. What do you mean too much thyroid? Thyroid. Do you take thyroid medication? Yes. So the dose of the thyroid medication you're taking is too high. We have to lower it. But you've lowered it. I was doing 112. You went to 100 and now I'm on 88. Yeah, but it's still showing that your thyroid function can be lowered more. How do you feel with the current 88? I am in a lot of sweats  totally. Yeah. So the sweats is because you have too much thyroid hormone. That is a sign of too much thyroid. We have to further lower it now for you. I believe the next lower level would be now after 88. It is 75. So we will go down to 75. Okay. And you will send the prescription into Costco? Yes, I will. And then also your vitamin D level was low, so I will also send you a prescription for that. Okay. What does that mean? What is it? Vitamin D. It's helpful in energy level and in general, well being in mood and energy. Okay. I'm not sure if you have any of those symptoms, but you do have low vitamin D level, so I do recommend you start supplementation for that. Okay? Okay. Do you have any of that or having low mood recently? I don't have any, no sir. Is it a prescription that I have to pick up? It is a supplement, yes. I can prescribe you some. Okay. Okay. So I'm sending both to Costco for you. Okay. Remember, stop your old Synthroid. Stop it right away. Stop the 88 as of today and start the new one tomorrow or whenever you pick up your medication. Okay? Okay, I'll pick it up this evening. Excellent. Any questions for me? No, that's it. Thank you so much, Doctor M. You're welcome. You have a good day. Ok. Take care. Bye.

        Medications: New prescription for Vitamin D. Adjust current Synthroid prescription to 75 mg.\nLabs: None\nFollow-ups: None\nConsultations:None
        --
        Conversation: Hello. Hello, is Dr. M calling for S? Yeah. Hi. How are you? I'm great. How are you doing today? I'm good. I did go for blood work last Sunday, right? Yes. So we have your results here. Let me go over them with you. Okay. Okay. All right. So I see here your blood sugar level. It's a little bit high still. It's 7.8%. Back in October of last year, your level was 7.8 as well. So you have not improved it. Okay. Are you only taking the Metformin for that? Yeah. Okay, so two tablets twice a day. Okay. And do you miss any doses or. You're pretty consistent with that? I'm pretty consistent only on the days when I'm not eating as much, I drop it to one. But other than that. Yeah. So we're going to have to increase your medications, okay. Because that is not doing enough. Other things I see here is you have a little bit low hemoglobin level. Okay. But your iron and your B12 levels are fine, so not a concern. Okay. Okay. So what I would recommend, mainly what we have to work on is your blood sugar levels. Okay. So along with the Metformin that you're taking, I would like to add another medication for you. Okay. Okay. Do you have health benefits? Yes. Private health benefits? Yes. Okay. Are you comfortable with injections like a very small, fine needle? Injection? How often? It would only be once a week. And it's a very fine needle. It's something you would just inject into the fat of the belly. Okay. It is one of the newer medications. It's called Ozempic and it works very well for diabetes. It also protects against heart attacks and it also can help with weight loss. So that's the best medicine I would like to add while you're taking the metformin. I think adding Ozempic would be really beneficial for you. Okay. So you want to try that out, see how it goes? All right. You're going to inject only 0.25 mg for four weeks. Okay. Okay. Then you're going to go up to 0.5 and which  pharmacy can I send this to? I have one close to my house, so that's where I go. I have Muskoka here. What is the name of the pharmacy you want me to send it to? All right, I'm going to look that up. Okay. Okay. Give me a minute. William Shanty, Harry Potter. I got a Harry Potter Road Pharmacy. Okay. Okay. So I'll send over the Ozempic there. I want you to start it as soon as you can. I would designate like a weekend day for that. Okay. Because it's easy to remember. Okay. For example, whichever day you can like a Monday or a Sunday, which can be consistent because it's only once a week. Okay. Okay, sounds good. And then we will follow up. We will recheck in three months time and see how you're doing. Okay. And how was the liver enzyme?  They're only slightly high there's not anything concerning just slightly high. Yes. Okay. The other thing, doctor, is I haven't got a requisition for my eye test  You would have to follow up with them. Have you spoken with them? No, I need a requisition right now for an optometrist. You will just go see an optometrist. There is no requisite because when I went to my eyeglasses one of them refused and it said you have diabetic. Immediate doctors note usually G would send a requisition to doctor at Queensway somewhere there, right he would do dilate the eye and check as well, right a couple of years. Well, we sent you to Dr. C just last year. In December. So you don't need another referral. That was five months ago you were referred to Dr. C. Yes. Yeah, but they didn't call me back. Okay, no problem, I will write you a requisition for an optometrist. Okay. All right then. So I've sent your medication to the pharmacy and I want you to follow up with me after one month of taking that okay we'll see how you're doing. Okay. Sounds good. All right then. Take care. Bye. I think it's fine we're.

        Medications: New prescription for Ozempic 0.25 mg once a week for four weeks, then 0.5 mg once a week.\nLabs: Blood work in 3 months.\nFollow-ups: Follow-up in one month.\nConsultations: Requisition for an optometrist.
        --
        Conversation: {}
        """.format(text)

    return openai.Completion.create(
        engine="text-davinci-002", 
        #engine="text-ada-001",
        prompt=prompt, 
        temperature=0, 
        max_tokens=max_completion_length,
        top_p = 1,
        frequency_penalty=0.0,
        presence_penalty=0.0,
        stop="--"
    )    

def handle_duplicate_actionitems(medications, followups, consultations, labs):
    """
    Handles duplicate actionitems
    """
    temp_med_dict = dict.fromkeys(medications)
    temp_med_dict.pop('None', None)
    cleaned_medications = list(temp_med_dict)

    temp_followups_dict = dict.fromkeys(followups)
    temp_followups_dict.pop('None', None)
    cleaned_followups = list(temp_followups_dict)

    temp_consultations_dict = dict.fromkeys(consultations)
    temp_consultations_dict.pop('None', None)
    cleaned_consultations = list(temp_consultations_dict)
    
    temp_labs_dict = dict.fromkeys(labs)
    temp_labs_dict.pop('None', None)
    cleaned_labs = list(temp_labs_dict)

    return cleaned_medications, cleaned_followups, cleaned_consultations, cleaned_labs

def extract_actionitem_entities_from_responsetext(response_text):
    """
    Extracts meds, labs, f/us and consults from response text
    """
    text = response_text.strip()
    text_as_arr = text.split('\n')
    print("arr:", text_as_arr,"\n")

    medications = re.sub(r'^Medications:','',safe_list_get(text_as_arr, 0)).lstrip().split(". ")
    print("meds:", medications,"\n")

    labs = re.sub(r'^Labs:','',safe_list_get(text_as_arr, 1)).lstrip().split(". ")
    print("labs:", labs,"\n")

    followups = re.sub(r'^Follow-ups:','',safe_list_get(text_as_arr, 2)).lstrip().split(". ")
    print("f/us:", followups,"\n")

    consultations = re.sub(r'^Consultations:','',safe_list_get(text_as_arr, 3)).lstrip().split(". ")
    print("consults:", consultations,"\n")

    return medications, followups, consultations, labs

def extract_soapnote_sections_openai_response(text):
  """
  Extracts SOAP note sections from openai response
  """
  #removing any leading and trailing \n and whitespaces
  text = text.strip()
  text_as_arr = text.split('\n')
  print("arr:", text_as_arr,"\n")

  chief_complaint = re.sub(r'^Chief Complaint:','',safe_list_get(text_as_arr,0)).lstrip().split(". ")
  print("chief_complaint", chief_complaint)
  
  hpi = re.sub(r'^History of Presenting Illness:','',safe_list_get(text_as_arr,1)).lstrip().split(". ")
  print("hpi", hpi)
  
  physical_exam = re.sub(r'^Physical Examination:','',safe_list_get(text_as_arr,2)).lstrip().split(". ")
  print("physical exam", physical_exam)
  
  investigations = re.sub(r'^Investigations:','',safe_list_get(text_as_arr,3)).lstrip().split(". ")
  print("investigations", investigations)

  summary_plan = re.sub(r'^Summary & Plan:','',safe_list_get(text_as_arr,4)).lstrip().split(". ")
  print("summary_plan", summary_plan)
  
  past_medical_hx = re.sub(r'^Past Medical History:','',safe_list_get(text_as_arr,5)).lstrip().split(". ")
  print("past_medical_hx", past_medical_hx)

  current_rx = re.sub(r'^Current Medications:','',safe_list_get(text_as_arr,6)).lstrip().split(". ")
  print("current_rx", current_rx)

  allergies = re.sub(r'^Allergies:','',safe_list_get(text_as_arr,7)).lstrip().split(". ")
  print("allergies", allergies)

  family_hx = re.sub(r'^Family History:','',safe_list_get(text_as_arr,8)).lstrip().split(". ")
  print("family_hx", family_hx)

  social_hx = re.sub(r'^Social History:','',safe_list_get(text_as_arr,9)).lstrip().split(". ")
  print("social_hx", social_hx)

  return chief_complaint, hpi, physical_exam, investigations, summary_plan, past_medical_hx, current_rx, allergies, family_hx, social_hx

def handle_duplicate_soapnotes(chief_complaint, hpi, physical_exam, investigations, summary_plan, past_medical_hx, current_rx, allergies, family_hx, social_hx):
  print("\nhandling duplicate soapnotes...\n")
  print(f"\nincoming cc: {chief_complaint}")
  temp_cc_dict = dict.fromkeys(chief_complaint)
  print(f"\nafter dict operation: {temp_cc_dict}")
  temp_cc_dict.pop('None', None)
  temp_cc_dict.pop('', None)
  cleaned_chief_complaint = list(temp_cc_dict)
  print(f"\nafter further dict operations: {cleaned_chief_complaint}")

  temp_hpi_dict = dict.fromkeys(hpi)
  temp_hpi_dict.pop('None', None)
  temp_hpi_dict.pop('', None)
  cleaned_hpi = list(temp_hpi_dict)

  temp_physical_exam_dict = dict.fromkeys(physical_exam)
  temp_physical_exam_dict.pop('None', None)
  temp_physical_exam_dict.pop('', None)
  cleaned_physical_exam = list(temp_physical_exam_dict)
  
  temp_investigations_dict = dict.fromkeys(investigations)
  temp_investigations_dict.pop('None', None)
  temp_investigations_dict.pop('', None)
  cleaned_investigations = list(temp_investigations_dict)

  temp_summary_plan_dict = dict.fromkeys(summary_plan)
  temp_summary_plan_dict.pop('None', None)
  temp_summary_plan_dict.pop('', None)
  cleaned_summary_plan = list(temp_summary_plan_dict)

  temp_past_medical_hx_dict = dict.fromkeys(past_medical_hx)
  temp_past_medical_hx_dict.pop('None', None)
  temp_past_medical_hx_dict.pop('', None)
  cleaned_past_medical_hx = list(temp_past_medical_hx_dict)

  temp_current_rx_dict = dict.fromkeys(current_rx)
  temp_current_rx_dict.pop('None', None)
  temp_current_rx_dict.pop('', None)
  cleaned_current_rx = list(temp_current_rx_dict)

  temp_allergies_dict = dict.fromkeys(allergies)
  temp_allergies_dict.pop('None', None)
  temp_allergies_dict.pop('', None)
  cleaned_allergies = list(temp_allergies_dict)

  temp_family_hx_dict = dict.fromkeys(family_hx)
  temp_family_hx_dict.pop('None', None)
  temp_family_hx_dict.pop('', None)
  cleaned_family_hx = list(temp_family_hx_dict)

  temp_social_hx_dict = dict.fromkeys(social_hx)
  temp_social_hx_dict.pop('None', None)
  temp_social_hx_dict.pop('', None)
  cleaned_social_hx = list(temp_social_hx_dict)
  
  print(f"\nCleaned note: {cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx}")

  return cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx

def prompt_davinci_soapnotes(text, **kwargs):
  #openai.api_key = "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"#testing key
  openai.api_key = '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'#regular key
  #openai.api_key = os.getenv("OPENAI_API_KEY")
  #pull kwargs if present, else use default
  max_length = kwargs.get('max_length', 400)
  
  prompt = """
    This tool identifies the patient's chief complaint, history of presenting illness, physical examination, investigations, summary & plan,  past medical history, current medications, allergies, family history and social history discussed in the conversation:

    Conversation: Call has been forwarded to an automatic request messaging system. Hello, this is Dr. M calling for C. Hello? Hi, this is Dr. M calling for C. This is C. How are you today? I am good today. Alright. So I believe you're calling to follow up with lab results, correct? Yes. All right, so let me go over them with you. All right. Based on your blood work here few concerns. One is your cholesterol level is extremely high. Okay. Your total cholesterol is actually okay. But the only main concerns I see is your thyroid is showing signs that you might be having too much thyroid. And also your vitamin D level is low. What do you mean too much thyroid? Thyroid. Do you take thyroid medication? Yes. So the dose of the thyroid medication you're taking is too high. We have to lower it. But you've lowered it. I was doing 112. You went to 100 and now I'm on 88. Yeah, but it's still showing that your thyroid function can be lowered more. How do you feel with the current 88? I am in a lot of sweats  totally. Yeah. So the sweats is because you have too much thyroid hormone. That is a sign of too much thyroid. We have to further lower it now for you. I believe the next lower level would be now after 88. It is 75. So we will go down to 75. Okay. And you will send the prescription into Costco? Yes, I will. And then also your vitamin D level was low, so I will also send you a prescription for that. Okay. What does that mean? What is it? Vitamin D. It's helpful in energy level and in general, well being in mood and energy. Okay. I'm not sure if you have any of those symptoms, but you do have low vitamin D level, so I do recommend you start supplementation for that. Okay? Okay. Do you have any of that or having low mood recently? I don't have any, no sir. Is it a prescription that I have to pick up? It is a supplement, yes. I can prescribe you some. Okay. Okay. So I'm sending both to Costco for you. Okay. Remember, stop your old Synthroid. Stop it right away. Stop the 88 as of today and start the new one tomorrow or whenever you pick up your medication. Okay? Okay, I'll pick it up this evening. Excellent. Any questions for me? No, that's it. Thank you so much, Doctor M. You're welcome. You have a good day. Ok. Take care. Bye.

    Chief Complaint: Follow up of lab results.\nHistory of Presenting Illness: Patient was called today to follow up on recent blood work. Lab results indicate high levels of thyroid hormone, low levels of vitamin D and normal levels of total cholesterol. Patient is currently taking Synthroid 88 mg. Patient reports excessive sweating and no other symptoms of high thyroid hormone levels. Patient does not suffer from low mood or low energy levels.\nPhysical Examination: Physical examination was not conducted as this was a phone appointment.\nInvestigations: Blood work confirms: Thyroid hormone is high. Total cholesterol is normal. Vitamin D is low.\nSummary & Plan: High thyroid hormone and low vitamin D levels in blood work. Synthroid daily dosage was reduced to 75 mg. Vitamin D supplement recommended.\nPast Medical History: Hypothyroidism.\nCurrent Medications: Synthroid 88 mg once a day.\nAllergies: Not mentioned.\nFamily History: Not mentioned.\nSocial History: Not mentioned.
    --
    Conversation: Hello? Hello, it's Dr. M calling for L S That is yourself male Yes, I'm calling from Kennedy Medical Center. Are you looking for a family doctor still? Yes, I am. Okay, great. I had a few questions for you. All right. And then you can ask me any questions you have. So we'll start with, do you take any medications? No. Don't take any medication? No medications. Okay. Do you have any medical conditions you're aware of? No. None. Do you have any allergies you're aware of? No. Okay. Have you had any surgeries? No. And any family history of medical conditions like diabetes, blood pressure, heart disease in your parents and your siblings. Okay. Who are you living with? My parents. All right. Do you have any children, any partners? No. Okay. Are you working are you in school? Yeah. Working. What do you do for work? Software engineer. Okay. Is it a desk job or you're working from home? Yeah, working from home. Okay. And do you have health benefits from work? Yes. Okay. Do you drink alcohol at all socially, weekends, daily? No. No alcohol. Do you smoke at all? No. Any cannabis use? No. Do you eat meat? Yes. Okay, so that's all the questions I had for you. Do you have any questions, concerns going on? No question. Concerns. It's just that I haven't visited a family doctor in I think, like maybe like two years or something. So I was just wondering. Yeah. For a checkup or like a blood test and stuff like that. Yeah. I'd recommend now that we've talked on the phone, call back to the clinic and schedule an appointment for a physical. Okay. So come in and see me. We can do a physical exam, and I'll order some blood work for you at that time. Okay? Cool. Okay. All right, then. Have a good day. All right. Thanks. Bye.

    Chief Complaint: New patient meet and greet.\nHistory of Presenting Illness: New patient meet and greet. No significant history. No current signs or symptoms.\nPhysical Examination: Physical examination was not conducted as this was a phone appointment.\nInvestigations: Not mentioned.\nSummary & Plan: New patient meet and greet. Patient to come for in-person visit to do physical examination, routine check up and blood work.\nPast Medical History: No past medical history. No past surgical history.\nCurrent Medications: None.\nAllergies: None.\nFamily History: None.\nSocial History: No alcohol, no smoking, no drugs, no cannabis. Patient lives with parents. Patient does not have a partner or children. Patient is a software engineer working from home. Normal diet. Eats meat.
    --
    Conversation: {}
  """.format(text)
  
  return openai.Completion.create(
    engine="text-davinci-002", 
    #engine="text-ada-001",
    prompt=prompt, 
    temperature=0, 
    max_tokens=max_length,
    top_p = 1,
    frequency_penalty=0.0,
    presence_penalty=0.0,
    stop="--"
  )

def fake_prompt_davinci_soapnotes():
  db = [{
  "choices": [
    {
      "finish_reason": "stop",
      "index": 0,
      "logprobs": "null",
      "text": "\n\nChief Complaint: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##. Follow up on mood.\nHistory of Presenting Illness: Patient is doing well on 20 mg of Cipralex. Patient has been in good self care and has reached out to children, which has helped tremendously.\nPhysical Examination: Physical examination was not conducted as this was a phone appointment.\nInvestigations: Not mentioned.\nSummary & Plan: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##. Patient is doing well on 20 mg of Cipralex. No need for refill at this time. Prescription for Naproxen sent to pharmacy. Patient to call back when needed."
    }
  ],
  "created": 1654086174,
  "id": "cmpl-5EHfC0qShBj06Ye5SaLrVNwDyLP5k",
  "model": "text-davinci-002",
  "object": "text_completion"
},
{
  "choices": [
    {
      "finish_reason": "stop",
      "index": 0,
      "logprobs": "null",
      "text": "\n    Chief Complaint: Refill of medication. ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##.\nHistory of Presenting Illness: Not mentioned.\nPhysical Examination: Not mentioned.\nInvestigations: Not mentioned.\nSummary & Plan: ##Disclaimer## This is a mock API response because calls to OpenAI are \U0001F4B0 ##Disclaimer##. Refill of medication.\nPast Medical History: Not mentioned.\nCurrent Medications: Not mentioned.\nAllergies: Not mentioned.\nFamily History: Not mentioned.\nSocial History: Not mentioned."
    }
  ],
  "created": 1654086183,
  "id": "cmpl-5EHfLAxFlkBF6yYSPuN2ywftucL8F",
  "model": "text-davinci-002",
  "object": "text_completion"
}
]

  num = random.randint(0,1)
  time.sleep(random.uniform(0.7,1.5))
  return db[num]

def prompt_textdavinci002_medical_summary(text, **kwargs):
  openai.api_key = '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'#regular key
  max_length = kwargs.get('max_length', 256)
  
  prompt = """You're a medical scribe that is responsible for writing a detailed summary from a conversation transcript between a patient and the clinician. Be sure to answer the following questions if relevant to the transcript; What is the topic of the conversation? What is the primary complaint? What is the relevant context to this complaint? What kind of medications, vitamins, minerals or supplements is the patient currently taking? What are the relevant medical tests that have been completed or will be completed in the future? What are the external factors relevant to the patient i.e. social history, family history? What allergies does the patient suffer from?:
  Conversation: {}
  Clinical Note:""".format(text)
  
  return openai.Completion.create(
    model="text-davinci-002", 
    prompt=prompt, 
    temperature=0, 
    max_tokens=max_length,
    top_p = 1,
    frequency_penalty=0.0,
    presence_penalty=0.0,
    #stop="\n"
  )

def fake_prompt_textdavinci002_medical_summary():
  time.sleep(1.5)
  return "The patient is discussing her infant son's difficulties with breastfeeding and constipation. She has tried various formulas and is currently using Enfamil A+ spit up mixed with the original. The infant has also been circumcised and is doing well with healing. The patient is concerned about her son's constipation and is wondering if there is anything else she can do to help."

def handle_medical_summary_generation_in_background(transcript, note_type, medical_specialty, callback_url, job_id, time_of_request, db):
  error_code = 1
  error_messages = []
  api_version = 1.0
  source = "textdavinci002"
  status = BackgroundTaskStatus.SUCCESSFUL.value
  medical_summary = ""
  template = ""
  callback_url_resp = {}
  openai_resp = {}

  #find template based on note_type & set it
  if note_type == ClinicalNoteType.SOAP:
    template = SOAP_TEMPLATE  
  elif note_type == ClinicalNoteType.DAP:
    template = DAP_TEMPLATE
  elif note_type == ClinicalNoteType.DEAR:
    template = DEAR_TEMPLATE

  #find length of transcript
  length_of_transcript = len(transcript)
  #print transcript[0:100] + its total length
  if length_of_transcript <= 14150:
    #Prompt LLM for Completion
    try: 
      resp = prompt_textdavinci002_medical_summary(transcript)
      medical_summary = resp["choices"][0]["text"]
      print(f"OPENAI RESP:\n{medical_summary}\n")
      print(f"\ntextdavinci002 resp for /medical_summary: {medical_summary}\n")
      error_code = 0
      openai_resp = resp
    except Exception as e:
      print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
      status = BackgroundTaskStatus.FAILED.value
      openai_resp = {}
      medical_summary = ""
      error_messages.append("Error with openai Completion call: {}".format(e))
  #Call BE with Completion, Transcription & template
  try:
      payload = MedicalSummaryProcessedResponse(transcript=transcript, note_type=note_type, medical_specialty=medical_specialty, callback_url=callback_url, job_id=job_id,medical_summary=medical_summary, template=template, error_code=error_code, error_messages=error_messages, run_time_in_secs=time.time()-time_of_request, api_version=api_version, source=source, status=status).json()
      print(f"\nCallback URL: {callback_url}\n")
      print(f"\nPayload: {payload}\n")
      print(f"type {type(payload)}")
      r = httpx.post(callback_url, json=json.loads(payload))
      #r = httpx.post("https://httpbin.org/post", json=payload)
      print(f"\nResult from BackgroundTask of Medical Summary to BE: {r.status_code}\n{r}\n")
      callback_url_resp = r.text
  except Exception as e:
    print(f"\n\U0001F480 Error sending Medical Summary BackgroundTask response to callback_url: {e}\n")
    status = BackgroundTaskStatus.FAILED.value
    error_messages.append("Error sending Medical Summary BackgroundTask response to callback_url: {}".format(e))
      
  #Try saving response to database
  try:
    crud.create_medical_summary(db=db, medical_summary=MedicalSummaryCreate(job_id=str(job_id), callback_url=callback_url,response_json=json.dumps(callback_url_resp), transcript=transcript, note_template=template, note_type=note_type, medical_specialty=medical_specialty, medical_summary=medical_summary, api_version=api_version, status=status, source=source))
  
  except Exception as e:
    status = BackgroundTaskStatus.FAILED.value
    print(f"\n\U0001F480 Error saving Medical Summary to local db: {e}\n")
    error_messages.append(f"Error saving Medical Summary to local db: {e}")

  #check if BackgroundTaskStatus is FAILED, if so notify via Slack
  if status == "Failed":
    print(f"\nNotifying via Slack re: FAILED BackgroundTask\n")
    print(f"STATUS: {status}")
    
    openai_resp = trim_openai_resp(openai_resp)
    payload_text = f"• *job_id:* {job_id}\n• *callback_url:* {callback_url}\n• *post_object_callbackurl:* {callback_url_resp[0:100]}\n• *post_openai_resp:* {openai_resp}\n *mlerror_code:* {error_code}\n• *mlerror_msgs:* {error_messages}\n• *root_function:*handle_medical_summary_generation_in_background\n• *transcript:* {transcript[0:100]}...\n"
    print(payload_text)
    report_error_via_slack(payload_text)
