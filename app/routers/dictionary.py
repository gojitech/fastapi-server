# import nltk.data
# from nltk.corpus import brown
# from nltk.stem import WordNetLemmatizer
# import difflib
# from turtle import pos
# import nltk
# import re
# import regex
# import pandas as pd
# import os
from fastapi import APIRouter
# from typing import List
# import time
# import csv

# from app.internal.utils import chunk_input_text_sent_boundary
# from ..internal.models import Entities, QueryText, Symptom, Body_Structure, Medication, Condition, Procedure, Extra
# import Levenshtein
# import enchant
#d = enchant.Dict("en_US")   # create dictionary for US English


# nltk.download('brown') #Moved to dependencies.py
# word_list = brown.words()
# word_set = set(word_list)

# nltk.download('wordnet')
# nltk.download('omw-1.4')
router = APIRouter()

#tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

# def file_to_list(file):
#     rtn: object = []
#     file_object: object = open(file, "r")
#     rtn: object = file_object.read().splitlines()
#     file_object.close()
#     # Remove Empty/Duplicates Values
#     return list(filter(None, pd.unique(rtn).tolist()))


# base_dir = os.path.abspath(os.path.dirname(__file__))
#base_dir = os.path.abspath(os.path.dirname(__file__))

#lemmatizer = WordNetLemmatizer()

# diseases_path = os.path.join(
#     base_dir, '..', 'internal/dictionary/disease.txt')
# diseases_path = os.path.join(base_dir, '..', 'internal/resources/conditions.csv')
# anatomy_path = os.path.join(base_dir, '..', 'internal/resources/anatomy.csv')
# drugs_path = os.path.join(base_dir, '..', 'internal/resources/medications.csv')
# symtoms_path = os.path.join(base_dir, '..',  'internal/resources/symptoms.csv')

# diseases_lists = []
# anatomy_lists = []
# drugs_lists = []
# symptoms_lists = []

# with open(diseases_path, newline='') as csvfile:
#     reader = csv.reader(csvfile, delimiter=',', quotechar='|')
#     for row in csv.reader(csvfile):
#         for column in row:
#             diseases_lists.append(column)
    
# with open(anatomy_path, newline='') as csvfile:
#     reader = csv.reader(csvfile, delimiter=',', quotechar='|')
#     for row in reader:
#         for column in row:
#             anatomy_lists.append(column)
    
# with open(drugs_path, newline='') as csvfile:
#     reader = csv.reader(csvfile, delimiter=',', quotechar='|')
#     for row in reader:
#         for column in row:
#             drugs_lists.append(column)

# with open(symtoms_path, newline='') as csvfile:
#     reader = csv.reader(csvfile, delimiter=',', quotechar='|')
#     for row in reader:
#         for column in row:
#             symptoms_lists.append(column)


# lists from Dictionary
# diseases_lists = file_to_list(diseases_path)
# anatomy_lists = file_to_list(anatomy_path)
# 

# def check_similarity_lemmatization(word, sentence):
#     lists = sentence.split()
#     lemmas = ['s', 'a', 'r', 'v', 'n']

#     for item in lists:
#         if(abs(len(item) - len(word)) < 2):
#             if(len(item) > 3 and len(word) > 3):
#                 if(not item in word_set):
#                     #print(f"\n> item before d.check {item}\n")
#                     if not d.check(remove_punctuations(item)):
#                         if(Levenshtein.distance(item, word) < 3):
#                             return item
#                         else:
#                             for lemma in lemmas:
#                                 if(Levenshtein.distance(lemmatizer.lemmatize(item, pos=lemma), word) < 3):
#                                     return item
#     return None


# def remove_punctuations(s):
#     return regex.sub(r"\p{P}+", "", s)


# def diff_score_words(a, b):
#     return round(difflib.SequenceMatcher(None, a, b).ratio(), 3)


#@router.post('/', response_model=Entities, response_model_exclude_unset=True, tags=['dictionary'])
@router.post('/', tags=['dictionary'])
#def get_dictionary(req: QueryText):
def get_dictionary():
    return {}
    # drugs = drugs_lists

    # # initialize parent entities
    # anatomy = []
    # # unique_anatomy = []
    # problems: List[Symptom] = []
    # # dictionaries = []
    # # unique_problems = []
    # treatments: List[Medication] = []
    # # unique_treatments = []
    # tests: List[Procedure] = []
    # # unique_tests = []
    # diseases: List[Condition] = []

    # extra: List[Extra] = []
    # error_code: int = 0
    # error_messages: List[str] = []
    # run_time_in_secs: float = 0
    # api_version: int = 1.0
    # source: str = 'dictionary'
    # default_score: str = "1.0"

    # original_query = req.query
    # time_of_req = time.time()
    # for query in tokenizer.tokenize(original_query):
    #     #print(f"query following tokenizer {query}")
    #     for symptoms_word in symptoms_lists:
    #         #print('checking for, ', symptoms_word)
    #         #"\s+"+row+"\s+"
    #         #"\s("+symptoms_word+")\s+" AND \s(toes)
    #         if (re.search("\s("+symptoms_word+")\s+", query, re.IGNORECASE)):
    #             #print('inside first re.search', symptoms_word)
    #             if list(filter(lambda item: item['keyword'] == symptoms_word, problems)):
    #                 continue
    #             else:
    #                 problems.append({'keyword': symptoms_word,
    #                                     'original_keyword': symptoms_word, "source_sentence": query, 'label_score': default_score})
    #         elif(check_similarity_lemmatization(symptoms_word, query) is not None):
    #             if list(filter(lambda item: item['keyword'] == symptoms_word, problems)):
    #                 continue
    #             else:
    #                 problems.append({'keyword': symptoms_word,
    #                                 'original_keyword': remove_punctuations(check_similarity_lemmatization(symptoms_word, query)), 'source_sentence': query, 'label_score': diff_score_words(remove_punctuations(check_similarity_lemmatization(symptoms_word, query)), symptoms_word)})
    #     # for index, row in df_symtoms.iterrows():
    #     #     symtoms_words = re.findall(
    #     #         "\s*([\w+\s*]{1,})\s+\((\d*\.?\d*)\)", row['Symptoms'])
    #     #     for symtoms_word, score in symtoms_words:
    #     #         if(re.search("\s+"+symtoms_word+"\s+", query, re.IGNORECASE)):
    #     #             if list(filter(lambda item: item['keyword'] == symtoms_word, problems)):
    #     #                 continue
    #     #             else:
    #     #                 problems.append({'keyword': symtoms_word,
    #     #                                 'original_keyword': symtoms_word, "source_sentence": query, 'label_score': default_score})
    #     #         elif(check_similarity_lemmatization(symtoms_word, query) is not None):
    #     #             if list(filter(lambda item: item['keyword'] == symtoms_word, problems)):
    #     #                 continue
    #     #             else:
    #     #                 problems.append({'keyword': symtoms_word,
    #     #                                 'original_keyword': remove_punctuations(check_similarity_lemmatization(symtoms_word, query)), 'source_sentence': query, 'label_score': diff_score_words(remove_punctuations(check_similarity_lemmatization(symtoms_word, query)), symtoms_word)})
    #     for row in diseases_lists:
    #         #"("+row+")+"
    #         if(re.search("\s("+row+")\s+", query, re.IGNORECASE)):
    #             if list(filter(lambda item: item['keyword'] == row, diseases)):
    #                 continue
    #             else:
    #                 diseases.append({'keyword': row,
    #                                 'original_keyword': row, "source_sentence": query, "label_score": default_score})
    #         elif(check_similarity_lemmatization(row, query) is not None):
    #             if list(filter(lambda item: item['keyword'] == row, diseases)):
    #                 continue
    #             else:
    #                 diseases.append({'keyword': row,
    #                                 'original_keyword': remove_punctuations(check_similarity_lemmatization(row, query)), 'source_sentence': query, "label_score": diff_score_words(remove_punctuations(check_similarity_lemmatization(row, query)), row)})
    #     for row in anatomy_lists:
    #         #"\s+"+row+"\s+"
    #         #"("+row+")+"
    #         if(re.search("\s("+row+")\s+", query, re.IGNORECASE)):
    #             if(list(filter(lambda item: item['keyword'] == row, anatomy))):
    #                 continue
    #             else:
    #                 anatomy.append(
    #                     {'keyword': row, 'original_keyword': row, "source_sentence": query, "label_score": default_score})
    #         elif(check_similarity_lemmatization(row, query) is not None):
    #             if(list(filter(lambda item: item['keyword'] == row, anatomy))):
    #                 continue
    #             else:
    #                 anatomy.append(
    #                     {'keyword': row, 'original_keyword': remove_punctuations(check_similarity_lemmatization(row, query)), "label_score": diff_score_words(remove_punctuations(check_similarity_lemmatization(row, query)), row)})
    #     for drug in drugs:
    #         #"\s+"+drug+"\s+"
    #         #"("+drug+")+"
    #         if(re.search("\s("+drug+")\s+", query, re.IGNORECASE)):
    #             if(list(filter(lambda item: item['keyword'] == drug, treatments))):
    #                 continue
    #             else:
    #                 #print('drug found in dictionary: ', drug)
    #                 treatments.append(
    #                     {'keyword': drug, 'original_keyword': drug, "source_sentence": query, "label_score": default_score})
    # end_time = time.time()
    # query_runtime = end_time - time_of_req
    # return {"query": original_query, "conditions": diseases, "symptoms": problems, "body_structures": anatomy, "medications": treatments, "procedures": tests, 'extra': extra, 'error_code': error_code, 'error_messages': error_messages, 'run_time_in_secs': query_runtime, 'api_version': api_version, 'source': source}
