# import traceback
# import time

from fastapi import APIRouter

# from ..internal.models import Entities, QueryText, Symptom, Body_Structure, Medication, Condition, Procedure, Extra
# from typing import List
# from ..internal.utils import find_anatomy_entities, find_i2b2_entities, find_ncbi_disease_entities, extract_symptom_details, extract_treatment_details, extract_test_details, extract_disease_details, extract_anatomy_details, chunk_input_text_sent_boundary, pass_filter_csv_stoplist
# from ..internal.models import Entities

router = APIRouter()

# def is_overlapping_words(phrase1, phrase2):
#     if (phrase1 in phrase2) or (phrase2 in phrase1):
#         #print('phrase1:' + phrase1 + ' exists in ' + phrase2 )
#         return True
#     else:
#         return False     

#defining an entity as eq if same keyword and source_sentence
# def ent_in_list(ent, curr_list):
#     return any((ent.keyword is element.keyword and ent.source_sentence is element.source_sentence) or (ent.keyword == element.keyword and ent.source_sentence == element.source_sentence) or (is_overlapping_words(ent.keyword, element.keyword) and ent.source_sentence == element.source_sentence and ent.status == element.status) for element in curr_list)

# def return_unique_entities(curr_list, incoming_list):
#     #print('incoming:', incoming_list)
#     #print('current:', curr_list)
#     new_list = curr_list
#     for in_ent in incoming_list:
#         if not ent_in_list(in_ent, curr_list):
#             new_list.append(in_ent)
#         if ent_in_list(in_ent, curr_list):
#             print('in_ent:', in_ent, 'exists in current list')

#     return new_list
  #basic search if in_ent is in curr_list
#   for in_ent in incoming_list:
#     if in_ent not in curr_list:
#       new_list.append(in_ent)
#     if in_ent in curr_list:
#       #print('This following entity already exists in curr_list:', in_ent)
#     return new_list
  #second approach by looking to see if keyword and source_sentence are the same
        

# def merge_entity_chunks_annotated_text(list_of_entities: List[Entities]):
#     query: str = ''
#     symptoms: List[Symptom] = []
#     body_structures: List[Body_Structure] = []
#     medications: List[Medication] = []
#     conditions: List[Condition] = []
#     procedures: List[Procedure] = []
#     extra: List[Extra] = []
#     error_code: int = 0
#     error_messages: List[str] = []
#     run_time_in_secs: float = 0.0
#     api_version: int = 1.0
#     source: str = 'annotate_text'

#     for entity in list_of_entities:
#         #query += entity.query
#         symptoms = return_unique_entities(symptoms, entity.symptoms) 
#         body_structures = return_unique_entities(body_structures, entity.body_structures)
#         medications = return_unique_entities(medications, entity.medications)
#         conditions = return_unique_entities(conditions, entity.conditions)
#         procedures = return_unique_entities(procedures, entity.procedures)
#         error_code += entity.error_code
#         error_messages += entity.error_messages

#     if len(error_messages) == 0:
#         error_code = 0

#     #print('symptoms inside:', symptoms)
#     #print('instanceof', type(symptoms))

#     return Entities(query=query, symptoms=symptoms, body_structures=body_structures, medications=medications, conditions=conditions, procedures=procedures, extra=extra, error_code=error_code, error_messages=error_messages, run_time_in_secs=run_time_in_secs, api_version=api_version, source=source)

# def annotate_text_find_medical_entities(query: str):    
#     #initialize parent entities
#     anatomy: List[Body_Structure] = []
#     anatomy_unique_umls: List[Body_Structure] = []
    
#     problems: List[Symptom] = []
#     problems_unique_umls: List[Symptom] = []
    
#     treatments: List[Medication] = []
#     treatments_unique_umls: List[Medication] = []
    
#     tests: List[Procedure] = []
#     tests_unique_umls: List[Procedure] = []
    
#     diseases: List[Condition] = []
#     diseases_unique_umls: List[Condition] = []

#     extra = []
    
#     #default error state + source
#     error_messages = []
#     error_code = 1
    
#     #look for various entities
#     try:
#         anatomy_unique_umls = find_anatomy_entities(query)
#         #print('unique anatomy entities found and cleared by umls: ', anatomy_unique_umls)
#         anatomy = extract_anatomy_details(query, anatomy_unique_umls)

#     except Exception as e:
#         err_str = repr(e)
#         error_messages.append('Error with NER (anatem): ' + err_str)
#         #print('Error with NER (anatem): ' + err_str +' see traceback:')
#         #print(traceback.format_exc())

#     try: 
#         problems_unique_umls, treatments_unique_umls, tests_unique_umls = find_i2b2_entities(query)
#         #print('\nProblems being passed to symptom_details from i2b2 model: \n', problems_unique_umls)

#         problems = extract_symptom_details(query, problems_unique_umls)
#         treatments = extract_treatment_details(query, treatments_unique_umls)
#         tests = extract_test_details(query, tests_unique_umls)
        
#     except Exception as e:
#         err_str = repr(e)
#         error_messages.append('Error with NER (i2b2): ' + err_str)
#         #print('Error with NER (i2b2): ' + err_str +' see traceback:')
#         #print(traceback.format_exc())


#     try:
#         diseases_unique_umls = find_ncbi_disease_entities(query)
#         #print('\nDiseases being passed to disease_details from ncbi model: \n', diseases_unique_umls)
#         diseases = extract_disease_details(query, diseases_unique_umls)
#     except Exception as e:
#         err_str = repr(e)
#         error_messages.append('Error with NER (ncbi_disease): ' + err_str)
#         #print('Error with NER (ncbi_disease): ' + err_str +' see traceback:')
#         #print(traceback.format_exc()) 

#     #updating label scores based on our local csvs
#     #not passing tests through this function!
#     anatomy, problems, treatments, tests, diseases = pass_filter_csv_stoplist(anatomy, problems, treatments, tests, diseases)   

#     #check if errorMessages is still null then change errorCode to 0
#     if len(error_messages) == 0:
#         error_code = 0

#     #Finding time it took between request and entities recognition to complete
#     #We'll compute this on the parent function
#     query_runtime = 0.0
#     ##print('symptoms inside:', problems)
#     ##print('instanceof', type(problems))
    
#     return Entities(query=query, symptoms=problems, body_structures=anatomy, medications=treatments, conditions=diseases, procedures=tests, extra=extra, error_code=error_code, error_messages=error_messages, run_time_in_secs=query_runtime, api_version=1.0, source='annotate_text')

#@router.post("/", response_model=Entities, response_model_exclude_unset=True, tags=['annotate_text'])
@router.post("/", tags=['annotate_text'])
#def get_annotated_text(req: QueryText):
def get_annotate_text():
    return {}
    # entities_of_chunked_annotatedtext: List[Entities] = []

    # time_of_req = time.time()
    # #print('incoming request to /annotate_text fxn: ', req.query[0:50], 'of len:', len(req.query), ' at ', time_of_req)

    # chunked_query = chunk_input_text_sent_boundary(req.query)
    # #print(len(chunked_query), ' chunks found')

    # for chunk in chunked_query:
    #     entities = annotate_text_find_medical_entities(chunk)
    #     entities_of_chunked_annotatedtext.append(entities)
    
    # #merge entity chunks:
    # entities_of_chunked_annotatedtext = merge_entity_chunks_annotated_text(entities_of_chunked_annotatedtext)

    # entities_of_chunked_annotatedtext.query = req.query
    # entities_of_chunked_annotatedtext.run_time_in_secs = time.time() - time_of_req
    
    # return entities_of_chunked_annotatedtext
