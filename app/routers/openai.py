import time
import uuid
from fastapi import APIRouter, BackgroundTasks, Response, status, Depends

from app.internal.openai_utils import fake_prompt_davinci_soapnotes, prompt_davinci_actionitems, extract_actionitem_entities_from_responsetext, fake_prompt_davinci_actionitems, handle_duplicate_actionitems, prompt_davinci_soapnotes, handle_duplicate_soapnotes, extract_soapnote_sections_openai_response, handle_medical_summary_generation_in_background
from app.internal.utils import chunk_input_text_sent_boundary, return_text_as_sent
from app.internal.models import ActionItems, ClinicalNoteOpenAI, ClinicalNoteType, QueryMedicalSummary, QueryText, ChiefComplaint, HistoryOfPresentingIllness, PhysicalExam, Investigations, SummaryAndPlan, PastMedicalHistory, CurrentMedications, Allergies, FamilyHistory, SocialHistory, QueryTextRealTime, ClinicalNoteOpenAIBackground, InitialRespRealTime, BackgroundTaskStatus, ExtendedSOAPNoteFormat, MedicalSpecialty, MedicalSummaryInitialResponse

from app.internal.resources.sqlite.database import SessionLocal
from sqlalchemy.orm import Session

#Dependency
def get_db():
  db = SessionLocal()
  try: 
    yield db
  finally:
    db.close()

fake_openai_response = {
  "choices": [
    {
      "finish_reason": "stop",
      "index": 0,
      "logprobs": "null",
      "text": "\n    Chief Complaint: Follow up of lab results.\nHistory of Presenting Illness: Patient was called today to follow up on recent blood work. Lab results indicate high levels of thyroid hormone, low levels of vitamin D and normal levels of total cholesterol. Patient is currently taking Synthroid 88 mg. Patient reports excessive sweating and no other symptoms of high thyroid hormone levels. Patient does not suffer from low mood or low energy levels.\nPhysical Examination: Physical examination was not conducted as this was a phone appointment.\nInvestigations: Blood work confirms: Thyroid hormone is high. Total cholesterol is normal. Vitamin D is low.\nSummary & Plan: High thyroid hormone and low vitamin D levels in blood work. Synthroid daily dosage was reduced to 75 mg. Vitamin D supplement recommended.\nPast Medical History: Hypothyroidism.\nCurrent Medications: Synthroid 88 mg once a day.\nAllergies: Not mentioned.\nFamily History: Not mentioned.\nSocial History: Not mentioned."
    }
  ],
  "created": 1657052285,
  "id": "cmpl-5QjHhfNPbu0cM5b3K7Zp81YzzuKup",
  "model": "text-davinci-002",
  "object": "text_completion",
  "usage": {
    "completion_tokens": 216,
    "prompt_tokens": 2033,
    "total_tokens": 2249
  }
}

router = APIRouter()

# def find_overlapping_text(s1: str, s2: str):
#     import difflib
#     s = difflib.SequenceMatcher(None, s1, s2)
#     position_a, position_b, size = s.find_longest_match(0, len(s1), 0, len(s2))
#     overlap = s1[position_a:position_a+size]
#     print(f"\n overlapping text: {overlap}")
#     return overlap

def return_newtext_with_some_overlap(text1, text2, **kwargs):
  #assumption is that text1 is shorter than text2 & text2 = text1 + additional text
  
  #check if text1 is "" and if so return text2 right away
  if len(text1) == 0:
      return text2

  idx=0
  temp_new_text = []
  overlap_size = kwargs.get('overlap_size', 1)
  
  #split both text1&text2 into sentences
  text1_arr = return_text_as_sent(text1)#49
  print(f"\ntext1_arr: {text1_arr}")
  text2_arr = return_text_as_sent(text2)#69
  print(f"\ntext2_arr: {text2_arr}")
  
  while str(text1_arr[idx]) == str(text2_arr[idx]) and idx < len(text1_arr)-1:
    idx+=1
  
  if idx < overlap_size:
    new_text = text2_arr[idx:] 
  else:
    new_text = text2_arr[idx-overlap_size:]  

  print(f"\noverlap ends @ {idx}")
  print(f"\nnew_text: {new_text}")
  
  for sentence in new_text:
    print(f"sentence: {sentence}")
    temp_new_text.append(str(sentence))
  
  return " ".join(temp_new_text)

def consolidate_prev_current_davinci_soapnotes(new_chief_complaint, new_hpi, new_physical_exam, new_investigations, new_summary_plan, new_past_medical_hx, new_current_rx, new_allergies, new_family_hx, new_social_hx, previous_SOAP_note):
    #previous findings, if note section doesn't exist, default to ""
    #Check if previous SOAP Note is none, if this is the case, check for duplicates and return cleaned note sections
    #Otherwise, extract note sections from previous SOAP note, defaulting to "" if a given heading is not present, remove duplicates and then return 
    if previous_SOAP_note == None:
        cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx = handle_duplicate_soapnotes(new_chief_complaint, new_hpi, new_physical_exam, new_investigations, new_summary_plan, new_past_medical_hx, new_current_rx, new_allergies, new_family_hx, new_social_hx)
        return cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx
    else:
        previous_SOAP_note = previous_SOAP_note.dict()    
    
        new_chief_complaint.extend(previous_SOAP_note.get('chief_complaint', "{data:""}")['data'])
        new_hpi.extend(previous_SOAP_note.get('history_of_presenting_illness',"{data:""}")['data'])
        new_physical_exam.extend(previous_SOAP_note.get('physical_exam', "{data:""}")['data'])
        new_investigations.extend(previous_SOAP_note.get('investigations', "{data:""}")['data'])
        new_summary_plan.extend(previous_SOAP_note.get('summary_and_plan', "{data:""}")['data'])
        new_past_medical_hx.extend(previous_SOAP_note.get('past_medical_history', "{data:""}")['data'])
        new_current_rx.extend(previous_SOAP_note.get('current_medications', "{data:""}")['data'])
        new_allergies.extend(previous_SOAP_note.get('allergies', "{data:""}")['data'])
        new_family_hx.extend(previous_SOAP_note.get('family_history', "{data:""}")['data'])
        new_social_hx.extend(previous_SOAP_note.get('social_history', "{data:""}")['data'])
        #Handle exact duplicates
        cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx = handle_duplicate_soapnotes(new_chief_complaint, new_hpi, new_physical_exam, new_investigations, new_summary_plan, new_past_medical_hx, new_current_rx, new_allergies, new_family_hx, new_social_hx)
        
        return cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx

async def send_update_soapnote_backend(cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx, callback_url, job_id, time_of_req, error_messages, error_code, current_text, status):
    try: 
        # ClinicalNoteOpenAI(query=req.query, chief_complaint=ChiefComplaint(data=cleaned_chief_complaint), history_of_presenting_illness=HistoryOfPresentingIllness(data=cleaned_hpi), physical_exam=PhysicalExam(data=cleaned_physical_exam), investigations=Investigations(data=cleaned_investigations), summary_and_plan=SummaryAndPlan(data=cleaned_summary_plan), past_medical_history=PastMedicalHistory(data=cleaned_past_medical_hx), current_medications=CurrentMedications(data=cleaned_current_rx), allergies=Allergies(data=cleaned_allergies), family_history=FamilyHistory(data=cleaned_family_hx), social_history=SocialHistory(data=cleaned_social_hx), error_code=error_code, error_messages=error_messages, run_time_in_secs=(time.time() - time_of_req), api_version=1, source="textdavinci002")   
        extended_soap_note = ExtendedSOAPNoteFormat(chief_complaint=ChiefComplaint(data=cleaned_chief_complaint), history_of_presenting_illness=HistoryOfPresentingIllness(data=cleaned_hpi), physical_exam=PhysicalExam(data=cleaned_physical_exam), investigations=Investigations(data=cleaned_investigations), summary_and_plan=SummaryAndPlan(data=cleaned_summary_plan), past_medical_history=PastMedicalHistory(data=cleaned_past_medical_hx), current_medications=CurrentMedications(data=cleaned_current_rx), allergies=Allergies(data=cleaned_allergies), family_history=FamilyHistory(data=cleaned_family_hx), social_history=SocialHistory(data=cleaned_social_hx))
        payload = ClinicalNoteOpenAIBackground(query=current_text, medical_specialty=MedicalSpecialty.FAMILY_MEDICINE_GP, note=extended_soap_note, error_code=error_code, error_messages=error_messages, run_time_in_secs=time.time() - time_of_req, api_version=1.0, job_id=job_id, source="textdavinci002", status=status).json()
        print(f"\nNote: callback fxnality is temporarily disabled due to required BE work")
        print(f"\nClinicalNote POST to callback URL with the following payload: {payload}\n")

        #Commenting out until we have a BE support
        # async with httpx.AsyncClient() as client:
        #     r = await client.post(callback_url, data=payload)
 
        #     print(f"\nRequest: {r}\n")
    except Exception as e:
        print(f"\n\U0001F480 Error generating ClinicalNoteBackground model: {e}\n")

@router.post("/medical_summary_backgroundtask", tags=['openai'])
def get_textdavinci002_medical_summary(req: QueryMedicalSummary, background_tasks: BackgroundTasks, response: Response, db: Session = Depends(get_db)):#What is the shape of this request?
    print(req)

    transcript = req.transcript
    note_type = req.note_type
    medical_specialty = req.medical_specialty
    callback_url = req.callback_url
    time_of_request = time.time()

    #If either medical_specialty OR note_type are None, set them to default values
    if medical_specialty == None:
        medical_specialty = MedicalSpecialty.FAMILY_MEDICINE_GP
    
    if note_type == None:
        note_type = ClinicalNoteType.SOAP

    #generate a job-id based on the host ID and current time
    job_id = uuid.uuid1()

    #spin off a background task
    background_tasks.add_task(handle_medical_summary_generation_in_background, transcript, note_type, medical_specialty, callback_url, job_id, time_of_request, db)#What other parameters are needed here?
    #set Response code as 202 & return Response with a job_id
    response.status_code = status.HTTP_202_ACCEPTED
     
    return MedicalSummaryInitialResponse(transcript=transcript, note_type=note_type, medical_specialty=medical_specialty, callback_url=callback_url, job_id=job_id)

# async def handle_davinci_soapnotes_in_background(current_text, previous_text, previous_SOAP_note, callback_url, job_id, time_of_req):
#     error_code = 1
#     error_messages = []
#     status = ""
#     new_chief_complaint = []
#     new_hpi = [] 
#     new_physical_exam = []
#     new_investigations = []
#     new_summary_plan = []
#     new_past_medical_hx = []
#     new_current_rx = []
#     new_allergies = []
#     new_family_hx = []
#     new_social_hx = []

#     cleaned_chief_complaint = []
#     cleaned_hpi = []
#     cleaned_physical_exam = []
#     cleaned_investigations = []
#     cleaned_summary_plan = []
#     cleaned_past_medical_hx = []
#     cleaned_current_rx = []
#     cleaned_allergies = []
#     cleaned_family_hx = []
#     cleaned_social_hx = []
#     #Find non-overlapping text + 1 overlapping (default) sentence between previous & current, only send that to openai
#     #overlapping_text = find_overlapping_text(current_text, previous_text)
#     #non_overlapping_new_text = current_text.replace(overlapping_text, '')
#     partial_overlapping_new_text = return_newtext_with_some_overlap(previous_text, current_text)
    
#     print(f"\n non-overlapping new_text: {partial_overlapping_new_text}")
#     #Find size of non-overlapping text:
#     partial_overlapping_new_text_char_count = len(partial_overlapping_new_text)

#     if partial_overlapping_new_text_char_count < 7500:
#         try:
#             #Prompt davinci for completion
#             #response = fake_prompt_davinci_soapnotes()
#             response = prompt_davinci_soapnotes(partial_overlapping_new_text)
#             print(f"response from openai: {response}")
#             new_chief_complaint, new_hpi, new_physical_exam, new_investigations, new_summary_plan, new_past_medical_hx, new_current_rx, new_allergies, new_family_hx, new_social_hx = extract_soapnote_sections_openai_response(response["choices"][0]["text"])
#             cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx = consolidate_prev_current_davinci_soapnotes(new_chief_complaint, new_hpi, new_physical_exam, new_investigations, new_summary_plan, new_past_medical_hx, new_current_rx, new_allergies, new_family_hx, new_social_hx, previous_SOAP_note)
#             error_code = 0 #Setting to zero because everything went well
#             status = BackgroundTaskStatus.SUCCESSFUL
#         except Exception as e:
#             print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
#             error_messages.append("Error with openai Completion call: {}".format(e))
#             status = BackgroundTaskStatus.FAILED
        
#         await send_update_soapnote_backend(cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx, callback_url, job_id, time_of_req, error_messages, error_code, current_text, status)

#     else:
#         partial_overlapping_new_text_chunks = chunk_input_text_sent_boundary(partial_overlapping_new_text, chunk_size=80, overlap_size=3)
#         print(f"{len(partial_overlapping_new_text_chunks)} chunks found of size 88 in partial_overlapping_new_text")

#         for chunk in partial_overlapping_new_text_chunks:
#             try:
#                 #response = fake_prompt_davinci_soapnotes()
#                 response = prompt_davinci_soapnotes(chunk)
#                 print(f"response from openai: {response}")
#                 temp_new_chief_complaint, temp_new_hpi, temp_new_physical_exam, temp_new_investigations, temp_new_summary_plan, temp_new_past_medical_hx, temp_new_current_rx, temp_new_allergies, temp_new_family_hx, temp_new_social_hx = extract_soapnote_sections_openai_response(response["choices"][0]["text"])
#                 #Extend note sections array
#                 new_chief_complaint.extend(temp_new_chief_complaint)
#                 new_hpi.extend(temp_new_hpi)
#                 new_physical_exam.extend(temp_new_physical_exam)
#                 new_investigations.extend(temp_new_investigations)
#                 new_summary_plan.extend(temp_new_summary_plan)
#                 new_past_medical_hx.extend(temp_new_past_medical_hx)
#                 new_current_rx.extend(temp_new_current_rx)
#                 new_allergies.extend(temp_new_allergies)
#                 new_family_hx.extend(temp_new_family_hx)
#                 new_social_hx.extend(temp_new_social_hx)
#             except Exception as e:
#                 print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
#                 error_messages.append("Error with openai Completion call: {}".format(e))
        
#         cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx = consolidate_prev_current_davinci_soapnotes(new_chief_complaint, new_hpi, new_physical_exam, new_investigations, new_summary_plan, new_past_medical_hx, new_current_rx, new_allergies, new_family_hx, new_social_hx, previous_SOAP_note)
#         error_code = 0 #Setting to zero because everything went well
#         await send_update_soapnote_backend(cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx, callback_url, job_id, time_of_req, error_messages, error_code, current_text, status)

@router.post("/background_soap_notes", tags=['openai'])
#async def get_textdavinci002_soap_notes_background_task(req: QueryTextRealTime, background_tasks: BackgroundTasks, response: Response):
async def get_textdavinci002_soap_notes_background_task():
    return {}
    #extract from req
    # current_text = req.current_text
    # previous_text = req.previous_text
    # previous_clinicalnote_response = req.previous_clinicalnote_response
    # callback_url = req.callback_url
    # time_of_req = time.time()

    # #generate a job-id based on the host ID and current time
    # job_id = uuid.uuid1()
    # #create background task
    # background_tasks.add_task(handle_davinci_soapnotes_in_background, current_text, previous_text, previous_clinicalnote_response, callback_url, job_id, time_of_req)

    # #return 202 with job-id
    # response.status_code = status.HTTP_202_ACCEPTED
    # #Note to Zardar: add this to models.py and return using that approach
    # return InitialRespRealTime(current_text=current_text, previous_text=previous_text, previous_clinicalnote_response=previous_clinicalnote_response, callback_url=callback_url, job_id=job_id)

@router.post("/soap_notes", tags=['openai'])
#def get_textdavinci002_soap_notes(req: QueryText):
def get_textdavinci002_soap_notes():
    return {}
    # time_of_req = time.time()
    # error_code = 1 #this means there is an error, until flag is cleared
    # error_messages = []
    # chief_complaint = []
    # hpi = []
    # physical_exam = []
    # investigations = []
    # summary_plan = []
    # past_medical_hx = []
    # current_rx = []
    # allergies = []
    # family_hx = []
    # social_hx = []

    # #find size of incoming transcript
    # transcript_char_count = len(req.query)
    # print("\nReceived transcript: {}...\nLength:{}\n".format(req.query[0:50], transcript_char_count))

    # if transcript_char_count < 7500:
    #     try:
    #         davinci_response = prompt_davinci_soapnotes(req.query)
    #         #davinci_response = fake_prompt_davinci_soapnotes()
    #         print(davinci_response, '\n')
    #         chief_complaint, hpi, physical_exam, investigations, summary_plan, past_medical_hx, current_rx, allergies, family_hx, social_hx = extract_soapnote_sections_openai_response(davinci_response["choices"][0]["text"])
    #         error_code = 0
    #     except Exception as e:
    #         print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #         error_messages.append("Error with openai Completion call: {}".format(e))
    # else:
    #     transcript_chunks = chunk_input_text_sent_boundary(req.query, chunk_size=80, overlap_size=3)
    #     print("{} chunks found of size 88".format(len(transcript_chunks)))
    #     for chunk in transcript_chunks:
    #         try:
    #             davinci_response = prompt_davinci_soapnotes(chunk)
    #             #davinci_response = fake_prompt_davinci_soapnotes(chunk)
    #             print(davinci_response, '\n')
    #             temp_chief_complaint, temp_hpi, temp_physical_exam, temp_investigations, temp_summary_plan, temp_past_medical_hx, temp_current_rx, temp_allergies, temp_family_hx, temp_social_hx = extract_soapnote_sections_openai_response(davinci_response["choices"][0]["text"])
    #             chief_complaint.extend(temp_chief_complaint)
    #             hpi.extend(temp_hpi)
    #             physical_exam.extend(temp_physical_exam)
    #             investigations.extend(temp_investigations)
    #             summary_plan.extend(temp_summary_plan)
    #             past_medical_hx.extend(temp_past_medical_hx)
    #             current_rx.extend(temp_current_rx)
    #             allergies.extend(temp_allergies)
    #             family_hx.extend(temp_family_hx)
    #             social_hx.extend(temp_social_hx)
    #         except Exception as e:
    #             print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #             error_messages.append("Error with openai Completion call: {}".format(e))
    # cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx = handle_duplicate_soapnotes(chief_complaint, hpi, physical_exam, investigations, summary_plan, past_medical_hx, current_rx, allergies, family_hx, social_hx)        
    # return ClinicalNoteOpenAI(query=req.query, chief_complaint=ChiefComplaint(data=cleaned_chief_complaint), history_of_presenting_illness=HistoryOfPresentingIllness(data=cleaned_hpi), physical_exam=PhysicalExam(data=cleaned_physical_exam), investigations=Investigations(data=cleaned_investigations), summary_and_plan=SummaryAndPlan(data=cleaned_summary_plan), past_medical_history=PastMedicalHistory(data=cleaned_past_medical_hx), current_medications=CurrentMedications(data=cleaned_current_rx), allergies=Allergies(data=cleaned_allergies), family_history=FamilyHistory(data=cleaned_family_hx), social_history=SocialHistory(data=cleaned_social_hx), error_code=error_code, error_messages=error_messages, run_time_in_secs=(time.time() - time_of_req), api_version=1, source="textdavinci002")   

@router.post("/action_items", tags=['openai'])
#def get_textdavinci002_actionitems(req: QueryText) -> ActionItems:
def get_textdavinci002_actionitems():
    return {}
    # time_of_req = time.time()
    # error_code = 1 #this means there is an error, until flag is cleared
    # medications = []
    # followups = []
    # consultations = []
    # labs = []
    # error_messages = []
    # #find size of incoming transcript
    # transcript_char_count = len(req.query)
    # print("\nReceived transcript: {}...\nLength:{}\n".format(req.query[0:50], transcript_char_count))

    # #8300 characters ~= 2,100 tokens using openai's GPT tokenizer
    # #8300 characters ~= 1,765 words is using avg of 4.7 characters per word in English
    # #1,765 words ~= 88 - 117 sentence given a 15-20 word average sentence
    # if transcript_char_count < 7500:
    #     try:
    #         davinci_response = prompt_davinci_actionitems(req.query)
    #         #davinci_response = fake_prompt_davinci_actionitems(req.query)
    #         print(davinci_response, "\n")
    #         medications, followups, consultations, labs = extract_actionitem_entities_from_responsetext(davinci_response["choices"][0]["text"])
    #         error_code = 0
    #     except Exception as e:
    #         print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #         error_messages.append("Error with openai Completion call: {}".format(e))
    # else:
    #     transcript_chunks = chunk_input_text_sent_boundary(req.query, chunk_size=80, overlap_size=3)
    #     print("{} chunks found of size 88".format(len(transcript_chunks)))
    #     for chunk in transcript_chunks:
    #         try:
    #             davinci_response = prompt_davinci_actionitems(chunk)
    #             #davinci_response = fake_prompt_davinci_actionitems(chunk)
    #             print(davinci_response, "\n")
    #             temp_medications, temp_followups, temp_consultations, temp_labs = extract_actionitem_entities_from_responsetext(davinci_response["choices"][0]["text"])
    #             medications.extend(temp_medications)
    #             followups.extend(temp_followups)
    #             consultations.extend(temp_consultations)
    #             labs.extend(temp_labs)
    #         except Exception as e:
    #             print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #             error_messages.append("Error with openai Completion call: {}".format(e))   
    # #remove exact duplicates and 'None' responses
    # cleaned_medications, cleaned_followups, cleaned_consultations, cleaned_labs = handle_duplicate_actionitems(medications, followups, consultations, labs)
    # return ActionItems(query=req.query, prescriptions=cleaned_medications, followups=cleaned_followups, consultations=cleaned_consultations, labs=cleaned_labs, error_code=error_code, error_messages=error_messages, run_time_in_secs=(time.time() - time_of_req), api_version=1, source="textdavinci002")

@router.post("/mock_soap_notes", tags=['openai'])
#def get_textdavinci002_soap_notes(req: QueryText):
def get_textdavinci002_soap_notes():
    return {}
    # time_of_req = time.time()
    # error_code = 1 #this means there is an error, until flag is cleared
    # error_messages = []
    # chief_complaint = []
    # hpi = []
    # physical_exam = []
    # investigations = []
    # summary_plan = []
    # past_medical_hx = []
    # current_rx = []
    # allergies = []
    # family_hx = []
    # social_hx = []

    # #find size of incoming transcript
    # transcript_char_count = len(req.query)
    # print("\nReceived transcript: {}...\nLength:{}\n".format(req.query[0:50], transcript_char_count))

    # if transcript_char_count < 7500:
    #     try:
    #         #davinci_response = prompt_davinci_soapnotes(req.query)
    #         davinci_response = fake_prompt_davinci_soapnotes()
    #         print(davinci_response, '\n')
    #         chief_complaint, hpi, physical_exam, investigations, summary_plan, past_medical_hx, current_rx, allergies, family_hx, social_hx = extract_soapnote_sections_openai_response(davinci_response["choices"][0]["text"])
    #         error_code = 0
    #     except Exception as e:
    #         print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #         error_messages.append("Error with openai Completion call: {}".format(e))
    # else:
    #     transcript_chunks = chunk_input_text_sent_boundary(req.query, chunk_size=80, overlap_size=3)
    #     print("{} chunks found of size 88".format(len(transcript_chunks)))
    #     for chunk in transcript_chunks:
    #         try:
    #             #davinci_response = prompt_davinci_soapnotes(chunk)
    #             davinci_response = fake_prompt_davinci_soapnotes()
    #             print(davinci_response, '\n')
    #             temp_chief_complaint, temp_hpi, temp_physical_exam, temp_investigations, temp_summary_plan, temp_past_medical_hx, temp_current_rx, temp_allergies, temp_family_hx, temp_social_hx = extract_soapnote_sections_openai_response(davinci_response["choices"][0]["text"])
    #             chief_complaint.extend(temp_chief_complaint)
    #             hpi.extend(temp_hpi)
    #             physical_exam.extend(temp_physical_exam)
    #             investigations.extend(temp_investigations)
    #             summary_plan.extend(temp_summary_plan)
    #             past_medical_hx.extend(temp_past_medical_hx)
    #             current_rx.extend(temp_current_rx)
    #             allergies.extend(temp_allergies)
    #             family_hx.extend(temp_family_hx)
    #             social_hx.extend(temp_social_hx)
    #         except Exception as e:
    #             print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #             error_messages.append("Error with openai Completion call: {}".format(e))
    # cleaned_chief_complaint, cleaned_hpi, cleaned_physical_exam, cleaned_investigations, cleaned_summary_plan, cleaned_past_medical_hx, cleaned_current_rx, cleaned_allergies, cleaned_family_hx, cleaned_social_hx = handle_duplicate_soapnotes(chief_complaint, hpi, physical_exam, investigations, summary_plan, past_medical_hx, current_rx, allergies, family_hx, social_hx)        
    # return ClinicalNoteOpenAI(query=req.query, chief_complaint=ChiefComplaint(data=cleaned_chief_complaint), history_of_presenting_illness=HistoryOfPresentingIllness(data=cleaned_hpi), physical_exam=PhysicalExam(data=cleaned_physical_exam), investigations=Investigations(data=cleaned_investigations), summary_and_plan=SummaryAndPlan(data=cleaned_summary_plan), past_medical_history=PastMedicalHistory(data=cleaned_past_medical_hx), current_medications=CurrentMedications(data=cleaned_current_rx), allergies=Allergies(data=cleaned_allergies), family_history=FamilyHistory(data=cleaned_family_hx), social_history=SocialHistory(data=cleaned_social_hx), error_code=error_code, error_messages=error_messages, run_time_in_secs=(time.time() - time_of_req), api_version=1, source="textdavinci002")   

@router.post("/mock_action_items", tags=['openai'])
#def get_textdavinci002_actionitems(req: QueryText) -> ActionItems:
def get_textdavinci002_actionitems():
    return {}
    # time_of_req = time.time()
    # error_code = 1 #this means there is an error, until flag is cleared
    # medications = []
    # followups = []
    # consultations = []
    # labs = []
    # error_messages = []
    # #find size of incoming transcript
    # transcript_char_count = len(req.query)
    # print("\nReceived transcript: {}...\nLength:{}\n".format(req.query[0:50], transcript_char_count))

    # #8300 characters ~= 2,100 tokens using openai's GPT tokenizer
    # #8300 characters ~= 1,765 words is using avg of 4.7 characters per word in English
    # #1,765 words ~= 88 - 117 sentence given a 15-20 word average sentence
    # if transcript_char_count < 7500:
    #     try:
    #         #davinci_response = prompt_davinci_actionitems(req.query)
    #         davinci_response = fake_prompt_davinci_actionitems(req.query)
    #         print(davinci_response, "\n")
    #         medications, followups, consultations, labs = extract_actionitem_entities_from_responsetext(davinci_response["choices"][0]["text"])
    #         error_code = 0
    #     except Exception as e:
    #         print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #         error_messages.append("Error with openai Completion call: {}".format(e))
    # else:
    #     transcript_chunks = chunk_input_text_sent_boundary(req.query, chunk_size=80, overlap_size=3)
    #     print("{} chunks found of size 88".format(len(transcript_chunks)))
    #     for chunk in transcript_chunks:
    #         try:
    #             #davinci_response = prompt_davinci_actionitems(chunk)
    #             davinci_response = fake_prompt_davinci_actionitems(chunk)
    #             print(davinci_response, "\n")
    #             temp_medications, temp_followups, temp_consultations, temp_labs = extract_actionitem_entities_from_responsetext(davinci_response["choices"][0]["text"])
    #             medications.extend(temp_medications)
    #             followups.extend(temp_followups)
    #             consultations.extend(temp_consultations)
    #             labs.extend(temp_labs)
    #         except Exception as e:
    #             print("\n\U0001F480 Error with openai Completion call: {}\n".format(e))
    #             error_messages.append("Error with openai Completion call: {}".format(e))   
    # #remove exact duplicates and 'None' responses
    # cleaned_medications, cleaned_followups, cleaned_consultations, cleaned_labs = handle_duplicate_actionitems(medications, followups, consultations, labs)
    # return ActionItems(query=req.query, prescriptions=cleaned_medications, followups=cleaned_followups, consultations=cleaned_consultations, labs=cleaned_labs, error_code=error_code, error_messages=error_messages, run_time_in_secs=(time.time() - time_of_req), api_version=1, source="textdavinci002")