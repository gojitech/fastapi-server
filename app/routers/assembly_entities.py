#from ast import keyword
#import time
#import re
from fastapi import APIRouter
#from app.internal.models import AssemblyAutoHighlightsResult, AssemblyEntity, AssemblyEntityRequest, AssemblyEntityResponse, AssemblyEntityType, AssemblyIabTimeStamp, AssemblySymptom, AssemblyTopics, AssemblyWord, Extra, Symptom, Symptom_Property
#from typing import List

# from app.internal.utils import return_text_as_sent
# from ..internal.assembly_full_response import assembly_full_response
# from ..internal.utils import pass_filter_csv_stoplist_assemblyai

router = APIRouter()

# def get_word_with_entity(entity: AssemblyEntity, word_list: List[AssemblyWord] = []):
#     tmp = list(filter(
#         lambda item: item.start >= entity.start and item.end <= entity.end, word_list))

#     ret = {}

#     ret['speaker'] = tmp[0].speaker
#     ret['start'] = tmp[0].start
#     ret['end'] = tmp[len(tmp) - 1].end
#     ret['confidence'] = min(tmp, key=lambda x: x.confidence).confidence

#     txt = ''

#     for x in tmp:
#         txt = txt + ' ' + x.text

#     ret['text'] = txt.strip()

#     return ret

# def get_word_with_highlights(timeStamp: AssemblyIabTimeStamp, word_list: List[AssemblyWord] = []):
#     if(len(word_list) > 0):
#         for word in word_list:
#             if word.start == timeStamp.start:
#                 return word


# def get_source_sentence(original_query: str, keyword: str):
#     sentences = return_text_as_sent(original_query)
#     for idx, sentence in enumerate(sentences):
#         if(re.search(keyword.lower(), sentence.text.lower(), re.IGNORECASE)):
#             return str(sentences[idx])
#     return None


#@router.post('/', response_model=AssemblyEntityResponse, response_model_exclude_unset=True, tags=['assembly'])
@router.post('/', tags=['assembly'])
def extract_assembly_entities():
#def extract_assembly_entities(assembly_full_response: AssemblyEntityRequest):
    return {}
    # query: str = assembly_full_response.text
    # source: str = 'assembly/entities'
    # api_version: int = 1
    # symptoms: List[AssemblySymptom] = []
    # symptomsProperty: Symptom_Property = None
    # procedures = []
    # extra = []
    # error_code: int = 0
    # error_messages = []

    # word_list = assembly_full_response.words
    # auto_highlight_list = assembly_full_response.auto_highlights_result.results

    # time_of_req = time.time()

    # for entity in assembly_full_response.entities:
    #     if entity.entity_type == AssemblyEntityType.MEDICAL_CONDITION:
    #         symptoms.append({"keyword": entity.text, "original_keyword": get_word_with_entity(
    #             entity, word_list)['text'], "source_sentence": get_source_sentence(query, get_word_with_entity(
    #                 entity, word_list)['text']), "properties": {"speaker_id": get_word_with_highlights(entity, word_list).speaker},  "label_score": get_word_with_entity(entity, word_list)['confidence']})
    #     elif entity.entity_type == AssemblyEntityType.MEDICAL_PROCESS:
    #         procedures.append({"keyword": entity.text, "original_keyword": get_word_with_entity(
    #             entity, word_list)['text'], "source_sentence": get_source_sentence(query, get_word_with_entity(
    #                 entity, word_list)['text']), "label_score": get_word_with_entity(entity, word_list)['confidence'], "properties":{}})

    # for highlight in auto_highlight_list:
    #     if(assembly_full_response.auto_highlights and len(auto_highlight_list) > 0):
    #         extra.append({
    #             "keyword": highlight.text,
    #             "original_keyword": highlight.text,
    #             "source_sentence": get_source_sentence(query, highlight.text),
    #             "label_score": 1,
    #             "properties": {"speaker_id": get_word_with_highlights(highlight.timestamps[0], word_list).speaker},
    #         })

    # end_time = time.time()
    # query_runtime = end_time - time_of_req

    # #looking in umls for symptoms, procedures and extra
    # # umls_symptoms = labelscore_with_quickumls_assemblyai(symptoms, 'symptoms')
    # # umls_procedures = labelscore_with_quickumls_assemblyai(procedures, 'tests')
    # # umls_extra = labelscore_with_quickumls_assemblyai(extra, 'extra')
    # #put through our csvs and stoplist
    # symptoms, procedures, extra = pass_filter_csv_stoplist_assemblyai(symptoms, procedures, extra)

    # return {"query": query, "symptoms": symptoms, "procedures": procedures, 'extra': extra, 'error_code': error_code, 'error_messages': error_messages, 'run_time_in_secs': query_runtime, 'api_version': api_version, 'source': source}
