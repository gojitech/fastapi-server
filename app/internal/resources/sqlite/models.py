import enum
from typing import Counter
from sqlalchemy import Enum, Column, Integer, String, func, DateTime, TEXT
from sqlalchemy.orm import relationship

from .database import Base

#Do we store all responses or only successful ones? YES

class MedicalSpecialties(enum.Enum):
    FAMILY_MEDICINE_GP = "Family Medicine/General Practice"

class BackgroundTaskStatus(enum.Enum):
    SUCCESSFUL = 'Successful'
    FAILED = 'Failed'

class ClinicalNoteType(enum.Enum):
    SOAP = "SOAP"
    DAP = "DAP"
    DEAR = "DEAR"

class MedicalSummary(Base):
    __tablename__ = "medical_summaries"
    id = Column(Integer, primary_key=True, index=True)
    created_time = Column(DateTime(timezone=True), server_default=func.now())
    job_id = Column(TEXT)
    callback_url = Column(TEXT)
    response_json = Column(TEXT)
    transcript = Column(TEXT)
    note_template = Column(TEXT)
    #note_type = Column(Enum(ClinicalNoteType, values_callable=lambda x: [str(e.value) for e in x]))
    note_type = Column(String)
    #medical_specialty = Column(Enum(MedicalSpecialties, values_callable=lambda x: [str(e.value) for e in x]))
    medical_specialty = Column(String)
    medical_summary = Column(TEXT)
    api_version = Column(Integer)
    source = Column(String)
    #status = Column(Enum(BackgroundTaskStatus, values_callable=lambda x: [str(e.value) for e in x]))
    status = Column(String)
