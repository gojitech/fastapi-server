# import re
# import stanza 
# import contractions
# import nltk
# import csv
# import os
# from polyleven import levenshtein
from spacy.lang.en import English
# from quickumls import QuickUMLS
# from transformers import pipeline
# from collections import OrderedDict
# from ..internal.models import StanzaEntities, Symptom, Body_Structure, Medication, Procedure, Condition, ProcedureStatus
# from ..internal.macaw_utils import extract_symptom_status_macaw, extract_condition_status_macaw, extract_timebased_entities_macaw, extract_symptom_change_status_macaw, extract_condition_change_status_macaw, extract_values_and_findings_tests_macaw
# from ..internal.stop_words import anatomy_stop_words, symptom_stop_words, condition_stop_words, medication_stop_words, procedure_stop_words, clinical_stop_words
# from string import punctuation

# from typing import List

# from nltk.tokenize import TweetTokenizer
# word_tweet_tokenizer = TweetTokenizer()

nlp = English()
nlp.add_pipe("sentencizer")

#Only functions required by openai.py
def return_text_as_sent(text: str):
  temp_doc = nlp(text)
  return list(temp_doc.sents)

def chunk_input_text_sent_boundary(text, **kwargs):
  temp_text = []
  temp_sentences = return_text_as_sent(text)
  #print('temp_sentences',temp_sentences, '\n')
  # #pull kwargs if present, else use default
  chunk_size = kwargs.get('chunk_size', 32)
  overlap_size = kwargs.get('overlap_size', 1)
  #itr through sentences and create segments
  for a in range(0, len(temp_sentences), chunk_size - overlap_size):
    text_segment = [] 
    #.replace("\n", '').replace(',','').replace('[','').replace(']','')
    for x in range(a, a+chunk_size):
      if x < len(temp_sentences):
        text_segment.append(str(temp_sentences[x]))
    temp_segment_text = ' '.join(text_segment)
    #print('temp_text_segment: ',temp_segment_text)
    temp_text.append(temp_segment_text)
  return temp_text

def safe_list_get(l, index, **kwargs):
  default = kwargs.get('default', '')
  try:
      return l[index]
  except IndexError:
      return default

# classifier = pipeline("text-classification", model="roberta-large-mnli")

#Download required processors + initialize Pipelines. Initial run will download and will reference local cache moving forward
# stanza.download('en', package='mimic', processors={'ner':'i2b2'})
# stanza.download('en', package='mimic', processors={'ner':'ncbi_disease'})
# stanza.download('en', package='mimic', processors={'ner':'anatem'})
# stanza.download('en', processors='tokenize,ner')

# anat_pipeline = stanza.Pipeline('en', package="mimic", processors={'ner':"anatem"})
# i2b2_pipeline = stanza.Pipeline('en', package="mimic", processors={'ner':'i2b2'})
# ncbi_disease_pipeline = stanza.Pipeline('en', package="mimic", processors={'ner':'ncbi_disease'})
# date_ner_pipeline = stanza.Pipeline(lang="en", processors='tokenize,ner')

# try:
#     fetched_fastapi_environment = os.environ["FASTAPI_ENV"]
#     print(f"\nSuccess: FASTAPI_ENV = {fetched_fastapi_environment}, using relevant quickumls directory path")        
#     if fetched_fastapi_environment == "DEV" or fetched_fastapi_environment == "STAGING":
#       symptom_matcher = QuickUMLS(quickumls_fp='/home/ubuntu/datafiles/quickumls', accepted_semtypes=['T184', 'T033', 'T046'])
#       anatomy_matcher = QuickUMLS(quickumls_fp='/home/ubuntu/datafiles/quickumls', accepted_semtypes=["T029", "T023", "T030", "T031", "T022", "T021", "T190", "T017", "T185"])
#       medication_matcher = QuickUMLS(quickumls_fp='/home/ubuntu/datafiles/quickumls', accepted_semtypes=['T200', 'T203', 'T109', 'T121', 'T114', 'T116', 'T125', 'T195', 'T123'])
#       tests_matchers = QuickUMLS(quickumls_fp='/home/ubuntu/datafiles/quickumls', accepted_semtypes=['T060', 'T059', 'T061', 'T034', 'T033'])
#       diseases_matchers = QuickUMLS(quickumls_fp='/home/ubuntu/datafiles/quickumls', accepted_semtypes=['T047','T050','T191','T046','T048','T037','T093'])  
#     elif fetched_fastapi_environment == "PROD":
#       symptom_matcher = QuickUMLS(quickumls_fp='/pyapp/datafiles/quickumls', accepted_semtypes=['T184', 'T033', 'T046'])
#       anatomy_matcher = QuickUMLS(quickumls_fp='/pyapp/datafiles/quickumls', accepted_semtypes=["T029", "T023", "T030", "T031", "T022", "T021", "T190", "T017", "T185"])
#       medication_matcher = QuickUMLS(quickumls_fp='/pyapp/datafiles/quickumls', accepted_semtypes=['T200', 'T203', 'T109', 'T121', 'T114', 'T116', 'T125', 'T195', 'T123'])
#       tests_matchers = QuickUMLS(quickumls_fp='/pyapp/datafiles/quickumls', accepted_semtypes=['T060', 'T059', 'T061', 'T034', 'T033'])
#       diseases_matchers = QuickUMLS(quickumls_fp='/pyapp/datafiles/quickumls', accepted_semtypes=['T047','T050','T191','T046','T048','T037','T093'])
#     elif fetched_fastapi_environment == "ZARDAR_LOCAL":
#       symptom_matcher = QuickUMLS(quickumls_fp='/Users/zarda/datafiles/quickumls', accepted_semtypes=['T184', 'T033', 'T046'])
#       anatomy_matcher = QuickUMLS(quickumls_fp='/Users/zarda/datafiles/quickumls', accepted_semtypes=["T029", "T023", "T030", "T031", "T022", "T021", "T190", "T017", "T185"])
#       medication_matcher = QuickUMLS(quickumls_fp='/Users/zarda/datafiles/quickumls', accepted_semtypes=['T200', 'T203', 'T109', 'T121', 'T114', 'T116', 'T125', 'T195', 'T123'])
#       tests_matchers = QuickUMLS(quickumls_fp='/Users/zarda/datafiles/quickumls', accepted_semtypes=['T060', 'T059', 'T061', 'T034', 'T033'])
#       diseases_matchers = QuickUMLS(quickumls_fp='/Users/zarda/datafiles/quickumls', accepted_semtypes=['T047','T050','T191','T046','T048','T037','T093'])        
#     elif fetched_fastapi_environment == "MADHAVI_LOCAL":
#       symptom_matcher = QuickUMLS(quickumls_fp='/Users/phanisingaraju/quickumls/', accepted_semtypes=['T184', 'T033', 'T046'])
#       anatomy_matcher = QuickUMLS(quickumls_fp='/Users/phanisingaraju/quickumls/', accepted_semtypes=["T029", "T023", "T030", "T031", "T022", "T021", "T190", "T017", "T185"])
#       medication_matcher = QuickUMLS(quickumls_fp='/Users/phanisingaraju/quickumls/', accepted_semtypes=['T200', 'T203', 'T109', 'T121', 'T114', 'T116', 'T125', 'T195', 'T123'])
#       tests_matchers = QuickUMLS(quickumls_fp='/Users/phanisingaraju/quickumls/', accepted_semtypes=['T060', 'T059', 'T061', 'T034', 'T033'])
#       diseases_matchers = QuickUMLS(quickumls_fp='/Users/phanisingaraju/quickumls/', accepted_semtypes=['T047','T050','T191','T046','T048','T037','T093'])
# except KeyError as e:
#     print(f"\nERROR: No such env variable FASTAPI_ENV= {fetched_fastapi_environment}, using relevant quickumls directory path")
#     print(f"\nERROR: No such env variable for FASTAPI_ENV...\n")

# def intersection_of_lists(list1, list2):
#   return [word for word in list1 if word in list2]

# def calculate_label_score_post_umls(stanza_entity, umls_matches):
#   temp_scores = []
#   #shared_words = []
#   stanza_entity_split = stanza_entity.split()
#   for entry in umls_matches:
#     entry_split = entry['term'].split()
#     #print('# of words in stanza_entity: ',stanza_entity_split, len(stanza_entity_split))
#     #print('# of words in umls_matches: ', entry_split, len(entry_split))
#     #Right now, we're not going to look at shared_words
#     #shared_words = intersection_of_lists(stanza_entity_split, entry_split)
#     ##print('shared_words: ',shared_words, len(shared_words))
#     entry_score = (min(len(entry_split), len(stanza_entity_split)) / max(len(entry_split), len(stanza_entity_split))) * float(entry['similarity']) 
#     temp_scores.append(entry_score) 
#   return max(temp_scores)
    
# def labelscore_with_quickumls(input_list, type):
#   if type == 'symptoms':
#     new_symptom_entities_postumls = []
#     for symptom in input_list:
#       temp_keyword = symptom['keyword']
#       temp_original_keyword = symptom['original_keyword']

#       temp_umls_matches_of_symptom = []
#       #print('looking for following symptom in quickumls: ',temp_keyword)
#       umls_match = symptom_matcher.match(temp_keyword, best_match=True, ignore_syntax=False)
#       if umls_match:
#         for entity in umls_match:
#             for entry in entity:
#                 if entry['similarity'] >= 0.8:
#                     temp_umls_matches_of_symptom.append({'term': entry['term'], 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
#                     #print('found :', entry['term'], 'with similarity: ', entry['similarity'])
#                 else:
#                     print('no similarity >= 0.8:', entry['term'], 'similarity:', entry['similarity'])
#         if temp_umls_matches_of_symptom:
#           calculated_label_score = calculate_label_score_post_umls(temp_keyword, temp_umls_matches_of_symptom)
#           returning_symptom = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score':calculated_label_score, 'properties': {'umls': temp_umls_matches_of_symptom}}
#           new_symptom_entities_postumls.append(returning_symptom)
#         else:
#           returning_symptom = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score':0.0, 'properties': {'umls': []}}
#           new_symptom_entities_postumls.append(returning_symptom)
#       else:
#         returning_symptom = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score':0.0, 'properties': {'umls': []}}
#         new_symptom_entities_postumls.append(returning_symptom)
  
#     return new_symptom_entities_postumls
 
#   if type == 'anatomy': #{"keyword": temp_entity, "original_keyword": entity['original_keyword']}
#     new_anatomy_entities_post_umls = []
#     for ana_entity in input_list:
#       temp_keyword = ana_entity['keyword']
#       temp_original_keyword = ana_entity['original_keyword']

#       temp_umls_matches_of_anatomy = []
#       #print('looking for following anatomy in quickumls: ',temp_keyword)
#       umls_match = anatomy_matcher.match(temp_keyword, best_match=True, ignore_syntax=False)
#       if umls_match:
#         for entity in umls_match:
#           for entry in entity:
#             if entry['similarity'] >= 0.8:
#               temp_umls_matches_of_anatomy.append({'term': entry['term'], 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
#               #{'term': entry['term'].lower(), 'semtype': str(entry['semtypes'])}
#               #print('found :', entry['term'] , 'with similarity: ', entry['similarity'])
#             else:
#               print('not similarity >= 0.8:', entry['term'], entry['similarity'])
#         if temp_umls_matches_of_anatomy:
#           calculated_label_score = calculate_label_score_post_umls(temp_keyword, temp_umls_matches_of_anatomy)
#           returning_anatomy = {'keyword': temp_keyword, 'original_keyword':temp_original_keyword, 'label_score':calculated_label_score, 'properties': {'umls': temp_umls_matches_of_anatomy}}
#           new_anatomy_entities_post_umls.append(returning_anatomy)
#         else:
#           returning_anatomy = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score':0.0, 'properties': {'umls': []}}
#           new_anatomy_entities_post_umls.append(returning_anatomy)
#       else:
#         returning_anatomy = {'keyword': temp_keyword, 'original_keyword':temp_original_keyword, 'label_score': 0.0, 'properties': {'umls': []}}
#         new_anatomy_entities_post_umls.append(returning_anatomy)
    
#     return new_anatomy_entities_post_umls
  
#   if type == 'medication':
#     new_medication_entities_postumls = []
#     for entity in input_list:
#       temp_keyword = entity['keyword']
#       temp_original_keyword = entity['original_keyword']

#       temp_umls_matches_of_medication = []
#       #print('looking for following medication in quickumls: ',temp_keyword)
#       umls_match = medication_matcher.match(temp_keyword, best_match=True, ignore_syntax=False)
#       if umls_match:
#         for item in umls_match:
#           for entry in item:
#             if entry['similarity'] >= 0.8:
#               temp_umls_matches_of_medication.append({'term': entry['term'], 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
#               #print('found :', entry['term'], 'with similarity: ', entry['similarity'])
#             else:
#               print('not similarity >= 0.8:', entry['term'], entry['similarity'])
#         if temp_umls_matches_of_medication:
#           calculated_label_score = calculate_label_score_post_umls(temp_keyword, temp_umls_matches_of_medication)
#           returning_medication = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword,'label_score': calculated_label_score, 'properties': {'umls': temp_umls_matches_of_medication}}
#           new_medication_entities_postumls.append(returning_medication)
#         else:
#           returning_medication = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score':0.0, 'properties': {'umls': []}}
#           new_medication_entities_postumls.append(returning_medication)
#       else:
#         returning_medication = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score': 0.0, 'properties': {'umls': []}}
#         new_medication_entities_postumls.append(returning_medication)
    
#     return new_medication_entities_postumls
  
#   if type == 'tests':
#     new_tests_entities_postumls = []
#     for entity in input_list:
#       temp_keyword = entity['keyword']
#       temp_original_keyword = entity['original_keyword']

#       temp_umls_matches_of_tests = []
#       #print('looking for following tests in quickumls: ',temp_keyword)
#       umls_match = tests_matchers.match(temp_keyword, best_match=True, ignore_syntax=False)
#       if umls_match:
#         for item in umls_match:
#           for entry in item:
#             if entry['similarity'] >= 0.8:
#               temp_umls_matches_of_tests.append({'term': entry['term'], 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
#               #print('found :', entry['term'], 'with similarity: ', entry['similarity'])
#             else:
#               print('not similarity >= 0.8:', entry['term'], entry['similarity'])
#         if temp_umls_matches_of_tests:
#           calculated_label_score = calculate_label_score_post_umls(temp_keyword, temp_umls_matches_of_tests)
#           returning_tests = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score': calculated_label_score, 'properties': {'umls': temp_umls_matches_of_tests}}
#           new_tests_entities_postumls.append(returning_tests)
#         else:
#           returning_tests = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score':0.0, 'properties': {'umls': []}}
#           new_tests_entities_postumls.append(returning_tests)
#       else:
#         returning_tests = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score': 0.0, 'properties': {'umls': []}}
#         new_tests_entities_postumls.append(returning_tests)
    
#     #print('final list by quickumls of tests', new_tests_entities_postumls)
#     return new_tests_entities_postumls    
  
#   if type == 'diseases':
#     new_diseases_entities_postumls = []
#     for entity in input_list:
#       temp_keyword = entity['keyword']
#       temp_original_keyword = entity['original_keyword']

#       temp_umls_matches_of_diseases = []
#       #print('looking for following diseases in quickumls: ',temp_keyword)
#       umls_match = diseases_matchers.match(temp_keyword, best_match=True, ignore_syntax=False)
#       if umls_match:
#         for item in umls_match:
#           for entry in item:
#             if entry['similarity'] >= 0.8:
#               temp_umls_matches_of_diseases.append({'term': entry['term'], 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
#               #print('found :', entry['term'], 'with similarity: ', entry['similarity'])
#             else:
#               print('not similarity >= 0.8:', entry['term'], entry['similarity'])
#         if temp_umls_matches_of_diseases:
#           calculated_label_score = calculate_label_score_post_umls(temp_keyword, temp_umls_matches_of_diseases)
#           returning_diseases = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score': calculated_label_score, 'properties': {'umls': temp_umls_matches_of_diseases}}
#           new_diseases_entities_postumls.append(returning_diseases)
#         else:
#           returning_diseases = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score':0.0, 'properties': {'umls': []}}
#           new_diseases_entities_postumls.append(returning_diseases)
#       else:
#         returning_diseases = {'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'label_score': 0.0, 'properties': {'umls': []}}
#         new_diseases_entities_postumls.append(returning_diseases)
    
#     #print('final list by quickumls of diseases', new_diseases_entities_postumls)
#     return new_diseases_entities_postumls
#   return []

# def find_unique_entities(entities): #{"keyword": temp_entity, "original_keyword": entity['original_keyword']}
#   unique_entities = []

#   for ent in entities:
#     if not any((ent['keyword'] is element['keyword'] and ent['original_keyword'] is element['original_keyword']) or (ent['keyword'] == element['keyword'] and ent['original_keyword'] == element['original_keyword']) for element in unique_entities):
#       unique_entities.append(ent)
  
#   return unique_entities

# def remove_clinical_stopwords_from_phrase(entity):
#   #lowercase & expand contractions
#   temp_entity = entity["original_keyword"].lower()
#   temp_entity = contractions.fix(temp_entity)
#   #stop word removal
#   word_tokenized_entity_keyword = word_tweet_tokenizer.tokenize(temp_entity)
#   keyword_without_stopwords = [word for word in word_tokenized_entity_keyword if word not in clinical_stop_words]
#   temp_entity = (" ").join(keyword_without_stopwords)
  
#   return {"keyword": temp_entity, "original_keyword": entity['original_keyword']}

# def find_anatomy_entities(query: str) -> List[Body_Structure]:
#     if anat_pipeline:
#       anatomy_entities = []
#       anatomy_entities_unique = []
#       anatomy_entities_unique_umls = List[Body_Structure]

#       temp_doc = anat_pipeline(query)
#       ###print(temp_doc.entities)
#       if temp_doc.entities:
#         #print('Anatomy pipeline found {} entities'.format(len(temp_doc.entities)))
        
#         for entity in temp_doc.entities:
#           anatomy_entities.append({"original_keyword": entity.text.rstrip(punctuation)})
#         # for entity in temp_doc.entities:
#         #   anatomy.append(entity.text)

#         #remove clinical stop words and return unique
#         for entity in anatomy_entities:
#           anatomy_entities_unique.append(remove_clinical_stopwords_from_phrase(entity))
        
#         anatomy_entities_unique = find_unique_entities(anatomy_entities_unique)
        
#       else:
#         anatomy_entities_unique = anatomy_entities

#       #filter through quickumls
#       anatomy_entities_unique_umls = labelscore_with_quickumls(anatomy_entities_unique, 'anatomy')
#       #print('following label_scoring with quickumls: ', anatomy_entities_unique_umls)
      
#       ##print('unique anatomy',unique_anatomy)
#       return anatomy_entities_unique_umls
#     else:
#       #print('no anatomy pipeline...')
#       return []

# def find_i2b2_entities(query: str):
#     symptom_entities = []
#     symptom_entities_unique = []
#     symptom_entities_unique_umls = List[Symptom]

#     medication_entities = []
#     medication_entities_unique = []
#     medication_entities_unique_umls = List[Medication]
    
#     procedure_entities = []
#     procedure_entities_unique = []
#     procedure_entities_unique_umls = List[Procedure]

#     if i2b2_pipeline:
#       temp_doc = i2b2_pipeline(query)
      
#       for entity in temp_doc.entities:
#         #print("i2b2 pipeline found {} entities".format(len(temp_doc.entities)))
#         #need to return this as an object
#         if entity.type == 'PROBLEM':
#             #problems.append({'keyword': remove_clinical_stopwords_from_phrase(entity.text), 'original_keyword': entity.text})
#             symptom_entities.append({"original_keyword": entity.text.rstrip(punctuation)})
#         elif entity.type == 'TREATMENT':
#           #mapped to medicationsDto
#             #treatments.append({'keyword': remove_clinical_stopwords_from_phrase(entity.text), 'original_keyword': entity.text})
#             medication_entities.append({"original_keyword": entity.text.rstrip(punctuation)})
#             ####print('Treatment found with i2b2',entity.text)
#         elif entity.type == 'TEST':
#           #mapped to proceduresDto
#             #tests.append({'keyword': remove_clinical_stopwords_from_phrase(entity.text), 'original_keyword': entity.text})
#             ###print(entity.text,'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Test found by i2b2')
#             procedure_entities.append({"original_keyword": entity.text.rstrip(punctuation)})

#       #From entities, remove stop words and find unique
#       for problem in symptom_entities:
#         symptom_entities_unique.append(remove_clinical_stopwords_from_phrase(problem))
#         print('>>> symptom_entities_unique',symptom_entities_unique, '\n')
      
#       for treatment in medication_entities:
#         medication_entities_unique.append(remove_clinical_stopwords_from_phrase(treatment))

#       for test in procedure_entities:
#         procedure_entities_unique.append(remove_clinical_stopwords_from_phrase(test))
      
#       #find unique entities:
#       symptom_entities_unique = find_unique_entities(symptom_entities_unique)
#       print('>>> symptom_entities_unique',symptom_entities_unique, '\n')
#       medication_entities_unique = find_unique_entities(medication_entities_unique)
#       procedure_entities_unique = find_unique_entities(procedure_entities_unique)

#       #filter through quickumls
#       symptom_entities_unique_umls = labelscore_with_quickumls(symptom_entities_unique, 'symptoms')
#       medication_entities_unique_umls = labelscore_with_quickumls(medication_entities_unique, 'medication')
#       procedure_entities_unique_umls = labelscore_with_quickumls(procedure_entities_unique, 'tests')
      
#       #print('symptoms following label_scoring with quickumls: ', symptom_entities_unique_umls)
#       #print("medication following label_scoring with quickumls: ", medication_entities_unique_umls)
#       #print("tests following label_scoring with quickumls: ", procedure_entities_unique_umls)

#       return symptom_entities_unique_umls, medication_entities_unique_umls, procedure_entities_unique_umls
    
#     else:
#       #print('no i2b2 pipeline...')
#       return [], [], []

# def find_ncbi_disease_entities(query: str) -> List[Condition]:
#     if ncbi_disease_pipeline:
#       disease_entities = []
#       disease_entities_unique = []
#       disease_entities_unique_umls = []

#       temp_doc = ncbi_disease_pipeline(query)

#       for entity in temp_doc.entities:
#         #mapped to conditionsDto
#           disease_entities.append({'original_keyword':entity.text.rstrip(punctuation)})

#       #remove stop words    
#       for disease in disease_entities:
#         disease_entities_unique.append(remove_clinical_stopwords_from_phrase(disease))

#       #find unique diseases
#       disease_entities_unique = find_unique_entities(disease_entities_unique)
      
#       #filter through quickumls
#       disease_entities_unique_umls = labelscore_with_quickumls(disease_entities_unique, 'diseases')

#       return disease_entities_unique_umls
#     else:
#       #print('no ncbi pipeline...')
#       return []

# def find_timebased_entities(keyword: str, segment: str, type_of_entity: str):
#   if type_of_entity == 'symptom':
#     time_entities = []
#     macaw_timebased_entities = {}

#     temp_doc = date_ner_pipeline(segment)
    
#     for entity in temp_doc.entities:
#       if entity.type == 'DATE' or entity.type == 'TIME':
#         time_entities.append(entity.text)
    
#     macaw_timebased_entities = extract_timebased_entities_macaw(keyword, segment, time_entities, type_of_entity)
#     return macaw_timebased_entities
  
#   #{"time_of_onset": time_of_onset, "frequency": frequency, "duration": duration, "duration_flareup": duration_flareup}
#   if type_of_entity == 'condition':
#     time_entities = []
#     macaw_timebased_entities = {}

#     temp_doc = date_ner_pipeline(segment)

#     for entity in temp_doc.entities:
#       if entity.type == 'DATE' or entity.type == 'TIME':
#         time_entities.append(entity.text)
    
#     macaw_timebased_entities = extract_timebased_entities_macaw(keyword, segment, time_entities, type_of_entity)
#     return macaw_timebased_entities

#   if type_of_entity == 'procedure':
#     time_entities = []
#     macaw_timebased_entities = {}

#     temp_doc = date_ner_pipeline(segment)

#     for entity in temp_doc.entities:
#       if entity.type == 'DATE' or entity.type == 'TIME':
#         time_entities.append(entity.text)
    
#     macaw_timebased_entities = extract_timebased_entities_macaw(keyword, segment, time_entities, type_of_entity)
#     return macaw_timebased_entities
  
#   if type_of_entity == 'medication':
#     time_entities = []
#     macaw_timebased_entities = {}

#     temp_doc = date_ner_pipeline(segment)

#     for entity in temp_doc.entities:
#       if entity.type == 'DATE' or entity.type == 'TIME':
#         time_entities.append(entity.text)
    
#     macaw_timebased_entities = extract_timebased_entities_macaw(keyword, segment, time_entities, type_of_entity)
#     return macaw_timebased_entities

# # def extract_symptom_status_mnli(sentence_segment, symptom):
# #   result = classifier(sentence_segment + '.' + 'Patient has ' + symptom)
# #   ####print(str(result))
# #   if result[0]['label'] == 'ENTAILMENT':
# #     return [{'label': 'Experienced', 'value': result[0]['score']}]
# #   elif result[0]['label'] == 'CONTRADICTION':
# #     return [{'label': 'Not Experienced', 'value': result[0]['score']}]
# #   elif result[0]['label'] == 'NEUTRAL':
# #     return [{'label': 'Uncertain', 'value': result[0]['score']}]  

# def extract_symptom_details(text: str, symptoms: List[Symptom]) -> List[Symptom]:
#   sentences = return_text_as_sent(text)
#   temp_symptoms = []
#   #Will go through each sentence and find instances where symptom is mentioned
#   #Then will append sentence +/- 1 surrounding sentence as a 'sentence segment'
#   #Note: the +/- 1 is hard coded but could be configured to accept an int value for setting window
#   for idx, sentence in enumerate(sentences):
#     for symptom in symptoms:
#       temp_keyword = symptom['keyword']
#       temp_original_keyword = symptom['original_keyword']
#       temp_umls_properties = symptom['properties']['umls']
#       temp_label_score = symptom['label_score']
#       temp_source_sentence = str(sentences[idx])

#       if re.search(r'\b'+temp_original_keyword.lower()+r'\b', sentence.text.lower()):#has to be an exact match?
#         #check if index is out of range. Note: this is dependant on how many sentence's we're grabbing from each side
#         if len(sentences) > 2:
#           if idx > 0 and idx < (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-1]) + ' ' + str(sentences[idx]) + ' ' + str(sentences[idx+1])
#           elif idx == 0:
#             temp_sentence_segment = str(sentences[idx]) + ' ' + str(sentences[idx+1]) + ' ' + str(sentences[idx+2])
#           elif idx == (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-2]) + ' ' + str(sentences[idx-1]) + ' ' + str(sentences[idx])
#         else:
#           temp_sentence_segment = str(sentences[idx])
#         #Perform symptom attribute extraction tasks
#         #symptom_status = extract_symptom_status_mnli(temp_sentence_segment, keyword)
#         ##print('symptom_status returned: ', symptom_status)
#         #Just testing right now!
#         symptom_status = extract_symptom_status_macaw(text, temp_keyword)
#         symptom_change_status = extract_symptom_change_status_macaw(text, temp_keyword)
#         #if (value := find_date_ner(temp_sentence_segment)) is not None:
#         macaw_timebased_entities = find_timebased_entities(temp_keyword, text, "symptom")
#         #build our returning symptoms object
#         temp_symptoms.append({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'status':symptom_status, 'properties': {"duration_alltime":macaw_timebased_entities['duration_alltime'], "duration_episodic":macaw_timebased_entities['duration_episodic'], "time_of_onset":macaw_timebased_entities['time_of_onset'], "frequency_tempo": macaw_timebased_entities['frequency_tempo'],'umls':temp_umls_properties, "change_status": symptom_change_status}})
#         #For debugging output
#         #print({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'status':symptom_status, 'properties': {"duration_alltime":macaw_timebased_entities['duration_alltime'], "duration_episodic":macaw_timebased_entities['duration_episodic'], "time_of_onset":macaw_timebased_entities['time_of_onset'], "frequency_tempo": macaw_timebased_entities['frequency_tempo'],'umls':temp_umls_properties, "change_status": symptom_change_status}})

#   return temp_symptoms

# def extract_treatment_status_mnli(sentence_segment, treatment):
#   result = classifier(sentence_segment + '.' + 'Patient is taking ' + treatment)
#   ####print(str(result))
#   if result[0]['label'] == 'ENTAILMENT':
#     return [{'label': 'Experienced', 'value': result[0]['score']}]
#   elif result[0]['label'] == 'CONTRADICTION':
#     return [{'label': 'Not Experienced', 'value': result[0]['score']}]
#   elif result[0]['label'] == 'NEUTRAL':
#     return [{'label': 'Uncertain', 'value': result[0]['score']}]

# #Mapped to MedicationsDto
# def extract_treatment_details(text, treatments: List[Medication]) -> List[Medication]:
#   ###print("Extracting treatment details from the following query: ", text[0:50], "...")
#   print('\ninside extract_treatment_details\n', treatments)
#   sentences = return_text_as_sent(text)
#   temp_treatments = []
#   #Will go through each sentence and find instances where symptom is mentioned
#   #Then will append sentence +/- 1 surrounding sentence as a 'sentence segment'
#   #Note: the +/- 1 is hard coded but could be configured to accept an int value for setting window
#   for idx, sentence in enumerate(sentences):
#     for treatment in treatments:
#       temp_keyword = treatment['keyword']
#       temp_original_keyword = treatment['original_keyword']
#       temp_umls_properties = treatment['properties']['umls']
#       temp_label_score = treatment['label_score']
#       temp_source_sentence = str(sentences[idx])

#       if re.search(r'\b'+temp_original_keyword.lower()+r'\b', sentence.text.lower()):
#         #check if index is out of range. Note: this is dependant on how many sentence's we're grabbing from each side
#         if len(sentences) > 2:
#           if idx > 0 and idx < (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-1]) + ' ' + str(sentences[idx]) + ' ' + str(sentences[idx+1])
#           elif idx == 0:
#             temp_sentence_segment = str(sentences[idx]) + ' ' + str(sentences[idx+1]) + ' ' + str(sentences[idx+2])
#           elif idx == (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-2]) + ' ' + str(sentences[idx-1]) + ' ' + str(sentences[idx])
#         else:
#           temp_sentence_segment = str(sentences[idx])
        
#         treatment_status = extract_treatment_status_mnli(temp_sentence_segment, temp_keyword)
#         ###print('treatment status :', treatment_status)
#         #This needs to be rewritten for medications
#         macaw_timebased_entities = find_timebased_entities(temp_keyword, text, "medication")
#         #build our returning symptoms object
#         temp_treatments.append({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'status': treatment_status, 'properties': {"umls": temp_umls_properties, "frequency": macaw_timebased_entities['frequency'], "duration_how_long_pt_taking": macaw_timebased_entities['duration_how_long_pt_taking'], "duration_as_per_physician_instructions": macaw_timebased_entities['duration_as_per_physician_instructions']}})
#         #For debugging output
#         #print({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'status': treatment_status, 'properties': {"umls": temp_umls_properties}})

#   return temp_treatments

# def extract_test_status_mnli(sentence_segment, treatment):
#   results = []

#   #Was this test ordered or requested
#   was_ordered = classifier(sentence_segment + '.' + treatment + ' has been ordered or requested')
#   ###print(str(result))

#   if was_ordered[0]['label'] == 'ENTAILMENT':
#     results.append({'label': ProcedureStatus.ORDERED.value, 'value': was_ordered[0]['score']})
#   elif was_ordered[0]['label'] == 'CONTRADICTION':
#     results.append({'label': ProcedureStatus.NOT_ORDERED.value, 'value': was_ordered[0]['score']})
#   elif was_ordered[0]['label'] == 'NEUTRAL':
#     results.append({'label': ProcedureStatus.ORDER_STATUS_UNK.value, 'value': was_ordered[0]['score']})
  
#   #Was this test completed or performed
#   was_completed = classifier(sentence_segment + '.' + treatment + ' has been completed or performed')
#   ###print(str(result))
#   if was_completed[0]['label'] == 'ENTAILMENT':
#     results.append({'label': ProcedureStatus.COMPLETED.value, 'value': was_completed[0]['score']})
#   elif was_completed[0]['label'] == 'CONTRADICTION':
#     results.append({'label': ProcedureStatus.INCOMPLETE.value, 'value': was_completed[0]['score']})
#   elif was_completed[0]['label'] == 'NEUTRAL':
#     results.append({'label': ProcedureStatus.COMPLETION_UNK.value, 'value': was_completed[0]['score']})
   
#   return results

# #mapped to ProceduresDto
# def extract_test_details(text, tests: List[Procedure]) -> List[Procedure]:
#   sentences = return_text_as_sent(text)
#   temp_tests = []
#   #Will go through each sentence and find instances where symptom is mentioned
#   #Then will append sentence +/- 1 surrounding sentence as a 'sentence segment'
#   #Note: the +/- 1 is hard coded but could be configured to accept an int value for setting window
#   for idx, sentence in enumerate(sentences):
#     for test in tests:
#       temp_keyword = test['keyword']
#       temp_original_keyword = test['original_keyword']
#       temp_umls_properties = test['properties']['umls']
#       temp_label_score = test['label_score']
#       temp_source_sentence = str(sentences[idx])

#       if re.search(r'\b'+temp_original_keyword.lower()+r'\b', sentence.text.lower()):
#         #check if index is out of range. Note: this is dependant on how many sentence's we're grabbing from each side
#         if len(sentences) > 2:
#           if idx > 0 and idx < (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-1]) + ' ' + str(sentences[idx]) + ' ' + str(sentences[idx+1])
#           elif idx == 0:
#             temp_sentence_segment = str(sentences[idx]) + ' ' + str(sentences[idx+1]) + ' ' + str(sentences[idx+2])
#           elif idx == (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-2]) + ' ' + str(sentences[idx-1]) + ' ' + str(sentences[idx])
#         else:
#           temp_sentence_segment = str(sentences[idx])

#         #Perform symptom attribute extraction tasks
#         test_status = extract_test_status_mnli(temp_sentence_segment, temp_keyword)
#         macaw_extract_value_and_findings = extract_values_and_findings_tests_macaw(text, temp_keyword)
#         macaw_timebased_entities = find_timebased_entities(temp_keyword, text, "procedure")
#         #faux_duration = find_timebased_entities(temp_sentence_segment)
#         #build our returning symptoms object
#         temp_tests.append({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'status': test_status, 'properties': {'umls':temp_umls_properties, "frequency": macaw_timebased_entities["frequency"], "findings": macaw_extract_value_and_findings, "time_of_study": macaw_timebased_entities["time_of_study"]}})
#         #For debugging output
#         #print({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'status': test_status, 'properties': {'umls':temp_umls_properties, "frequency": macaw_timebased_entities["frequency"], "time_of_study": macaw_timebased_entities["time_of_study"]}})

#   return temp_tests

# #Mapped to conditions
# def extract_disease_details(text, diseases:List[Condition]) -> List[Condition]:
#   sentences = return_text_as_sent(text)
#   temp_diseases = []
#   #Will go through each sentence and find instances where symptom is mentioned
#   #Then will append sentence +/- 1 surrounding sentence as a 'sentence segment'
#   #Note: the +/- 1 is hard coded but could be configured to accept an int value for setting window
#   for idx, sentence in enumerate(sentences):
#     for disease in diseases:
#       temp_keyword = disease['keyword']
#       temp_original_keyword = disease['original_keyword']
#       temp_umls_properties = disease['properties']['umls']
#       temp_label_score = disease['label_score']
#       temp_source_sentence = str(sentences[idx])

#       if re.search(r'\b'+temp_original_keyword.lower()+r'\b', sentence.text.lower()):
#         #check if index is out of range. Note: this is dependant on how many sentence's we're grabbing from each side
#         if len(sentences) > 2:
#           if idx > 0 and idx < (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-1]) + ' ' + str(sentences[idx]) + ' ' + str(sentences[idx+1])
#           elif idx == 0:
#             temp_sentence_segment = str(sentences[idx]) + ' ' + str(sentences[idx+1]) + ' ' + str(sentences[idx+2])
#           elif idx == (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-2]) + ' ' + str(sentences[idx-1]) + ' ' + str(sentences[idx])
#         else:
#           temp_sentence_segment = str(sentences[idx])
#         #Right now we're using symptom's mnli method because the probing question for symptom is similar to one for condition
#         disease_status = extract_condition_status_macaw(text, temp_keyword)
#         #disease_status = extract_symptom_status_mnli(temp_sentence_segment, keyword)
#         disease_change_status = extract_condition_change_status_macaw(text, temp_keyword)
#         macaw_timebased_entities = find_timebased_entities(temp_keyword, text, "condition")
#         #faux_duration = find_timebased_entities(temp_sentence_segment)
#         #build our returning symptoms object
#         temp_diseases.append({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword':temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score':temp_label_score, 'status':disease_status, 'properties': {"umls": temp_umls_properties, "frequency": macaw_timebased_entities['frequency'], "duration": macaw_timebased_entities['duration'], "duration_flareup":macaw_timebased_entities['duration_flareup'], "time_of_onset": macaw_timebased_entities['time_of_onset'], "change_status": disease_change_status}})
#         #For debugging output
#         #print({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword':temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score':temp_label_score, 'status':disease_status, 'properties': {"umls": temp_umls_properties, "frequency": macaw_timebased_entities['frequency'], "duration": macaw_timebased_entities['duration'], "duration_flareup":macaw_timebased_entities['duration_flareup'], "time_of_onset": macaw_timebased_entities['time_of_onset'], "change_status": disease_change_status}})

#   return temp_diseases

# def extract_anatomy_details(text, anatomy: List[Body_Structure]) -> List[Body_Structure]:
#   ##print("Extracting anatomy details from the following query: ", text[0:50], "...")
#   ###print(anatomy)
#   sentences = return_text_as_sent(text)
#   #print('sentences:', sentences, len(sentences))
#   temp_anatomy = []
#   #Will go through each sentence and find instances where symptom is mentioned
#   #Then will append sentence +/- 1 surrounding sentence as a 'sentence segment'
#   #Note: the +/- 1 is hard coded but could be configured to accept an int value for setting window
#   for idx, sentence in enumerate(sentences):
#     for entity in anatomy:
#       temp_original_keyword = entity['original_keyword']
#       temp_keyword = entity['keyword']
#       temp_properties = entity['properties']
#       temp_label_score = entity['label_score']
#       temp_source_sentence = str(sentences[idx])

#       if re.search(r'\b'+temp_original_keyword.lower()+r'\b', sentence.text.lower()):
#         # keyword = entity['keyword']
#         # source_sentence = str(sentences[idx])
#         # properties = entity['properties']
#         # label_score = entity['label_score']
#         # #check if index is out of range. Note: this is dependant on how many sentence's we're grabbing from each side
#         if len(sentences) > 2:#hard coded for now to avoid list index out of range
#           if idx > 0 and idx < (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-1]) + ' ' + str(sentences[idx]) + ' ' + str(sentences[idx+1])
#           elif idx == 0:
#             #print(str(sentences[idx]), str(sentences[idx+1]), str(sentences[idx+2]))
#             temp_sentence_segment = str(sentences[idx]) + ' ' + str(sentences[idx+1]) + ' ' + str(sentences[idx+2])
#           elif idx == (len(sentences) - 1):
#             temp_sentence_segment = str(sentences[idx-2]) + ' ' + str(sentences[idx-1]) + ' ' + str(sentences[idx])
#         else:
#           temp_sentence_segment = str(sentences[idx])

#         temp_anatomy.append({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'original_keyword': temp_original_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'properties': temp_properties})
#         #For debugging output
#         #print(temp_anatomy.append({'sent_segment': temp_sentence_segment,'keyword': temp_keyword, 'source_sentence': temp_source_sentence, 'label_score': temp_label_score, 'properties': temp_properties}))  
#   return temp_anatomy

# def calculate_levenshtein_score(string1, string2):
#     return (1 - levenshtein(string1, string2)/max(len(string1), len(string2)))

# def search_local_csv(keyword, ent_type):
#   temp_matches = []

#   base_dir = os.path.abspath(os.path.dirname(__file__))
#   conditions_csv = os.path.join(base_dir, 'resources/conditions.csv')
#   symptoms_csv = os.path.join(base_dir, 'resources/symptoms.csv')
#   anatomy_csv = os.path.join(base_dir, 'resources/anatomy.csv')
#   medications_csv = os.path.join(base_dir, 'resources/medications.csv')
#   procedures_csv = os.path.join(base_dir, 'resources/procedures.csv')
  
#   if ent_type == 'condition':
#     csv_file = conditions_csv
#     with open(csv_file, 'r') as csvfile:
#       reader = csv.reader(csvfile, delimiter=',' , quotechar='|')
#       for row in reader:
#           for column in row:
#             score = calculate_levenshtein_score(keyword.lower(), column.lower())
#             if score > 0.6:
#               temp_matches.append({'term': column, 'similarity': score})
#   elif ent_type == 'symptom':
#     csv_file = symptoms_csv
#     with open(csv_file, 'r') as csvfile:
#       reader = csv.reader(csvfile, delimiter=',' , quotechar='|')
#       for row in reader:
#           for column in row:
#             score = calculate_levenshtein_score(keyword.lower(), column.lower())
#             if score > 0.6:
#               temp_matches.append({'term': column, 'similarity': score})
#   elif ent_type == 'anatomy':
#     csv_file = anatomy_csv
#     with open(csv_file, 'r') as csvfile:
#       reader = csv.reader(csvfile, delimiter=',' , quotechar='|')
#       for row in reader:
#           for column in row:
#             score = calculate_levenshtein_score(keyword.lower(), column.lower())
#             if score > 0.6:
#               temp_matches.append({'term': column, 'similarity': score})
#   elif ent_type == 'medication':
#     csv_file = medications_csv
#     with open(csv_file, 'r') as csvfile:
#       reader = csv.reader(csvfile, delimiter=',' , quotechar='|')
#       for row in reader:
#           for column in row:
#             score = calculate_levenshtein_score(keyword.lower(), column.lower())
#             if score > 0.6:
#               temp_matches.append({'term': column, 'similarity': score})
#   elif ent_type == 'test':
#     csv_file = procedures_csv
#     with open(csv_file, 'r') as csvfile:
#       reader = csv.reader(csvfile, delimiter=',' , quotechar='|')
#       for row in reader:
#           for column in row:
#             score = calculate_levenshtein_score(keyword.lower(), column.lower())
#             if score > 0.6:
#               temp_matches.append({'term': column, 'similarity': score})
#   elif ent_type == 'extra':
#     csv_files = [conditions_csv, symptoms_csv, anatomy_csv, medications_csv, procedures_csv]
#     for csv_file in csv_files:
#       with open(csv_file, 'r') as csvfile:
#           reader = csv.reader(csvfile, delimiter=',' , quotechar='|')
#           for row in reader:
#               for column in row:
#                 score = calculate_levenshtein_score(keyword.lower(), column.lower())
#                 if score > 0.6:
#                   temp_matches.append({'term': column, 'similarity': score})

#   if temp_matches:
#     calculated_label_score = calculate_label_score_post_umls(keyword, temp_matches)
#     print({'keyword': keyword, 'label_score': calculated_label_score, 'properties': {'csv': temp_matches}})
#     return {'keyword': keyword, 'label_score': calculated_label_score, 'properties': {'csv': temp_matches}}
#   else:
#     print({'keyword': keyword, 'label_score': 0, 'properties': {'csv': temp_matches}})
#     return {'keyword': keyword, 'label_score': 0, 'properties': {'csv': temp_matches}}
#   #handle return either null or matches, add new property called csv

# def pass_filter_csv_stoplist(anatomy, problems, treatments, tests, diseases):
#   #if label_score is 0, pass through our csv search - will match & if it matches will update score
#   #then pass through stop list, if it does not match, drop the score to 0  
#   for entity in anatomy:
#     if entity['label_score'] == 0:
#       #temp_ent = entity
#       new_entity = search_local_csv(entity['keyword'], 'anatomy')
#       entity['keyword'] = new_entity['keyword']
#       entity['label_score'] = new_entity['label_score']
#       entity['properties']['csv'] = new_entity['properties']['csv']

#     if entity['keyword'].lower() in anatomy_stop_words:
#       #print('following anatomy found in stop_words: ', entity['keyword'])
#       entity['label_score'] = 0
#   #handling symptoms
#   for symptom in problems:
#     #print('symptom inside csv fxn: ', symptom)
#     if symptom['label_score'] == 0:
#       new_symptom = search_local_csv(symptom['keyword'], 'symptom')
#       symptom['keyword'] = new_symptom['keyword']
#       symptom['label_score'] = new_symptom['label_score']
#       symptom['properties']['csv'] = new_symptom['properties']['csv']
    
#     if symptom['keyword'].lower() in symptom_stop_words:
#       #print('following symptom: {}, is in the stop list'.format(symptom['keyword']))
#       symptom['label_score'] = 0
#   #handling medications
#   for medication in treatments:
#     if medication['label_score'] == 0:
#       new_medication = search_local_csv(medication['keyword'], 'medication')
#       medication['keyword'] = new_medication['keyword']
#       medication['label_score'] = new_medication['label_score']
#       medication['properties']['csv'] = new_medication['properties']['csv']
    
#     if medication['keyword'].lower() in medication_stop_words:
#       #print('following medication: {}, is in the stop list'.format(medication['keyword']))
#       medication['label_score'] = 0
#   #handling tests
#   for test in tests:
#     if test['label_score'] == 0:
#       new_test = search_local_csv(test['keyword'], 'test')
#       test['keyword'] = new_test['keyword']
#       test['label_score'] = new_test['label_score']
#       test['properties']['csv'] = new_test['properties']['csv']
    
#     if test['keyword'].lower() in procedure_stop_words:
#       #print('following procedure/test: {}, is in the stop list'.format(test['keyword']))
#       test['label_score'] = 0
#   #handling diseases
#   for disease in diseases:
#     if disease['label_score'] == 0:
#       new_disease = search_local_csv(disease['keyword'], 'condition')
#       new_test = search_local_csv(disease['keyword'], 'test')
#       disease['keyword'] = new_disease['keyword']
#       disease['label_score'] = new_disease['label_score']
#       disease['properties']['csv'] = new_disease['properties']['csv']
    
#     if disease['keyword'].lower() in condition_stop_words:
#       #print('following disease: {}, is in the stop list'.format(disease['keyword']))
#       disease['label_score'] = 0

#   return anatomy, problems, treatments, tests, diseases

# ################################################################################################################################################
# #Stanza API Code################################################################################################################################
# ################################################################################################################################################
# def stanza_remove_clinical_stopwords(current_list):
#   #We will first create a sentence from our list, word tokenize it, remove stop words
#   new_list = []
#   for entity in current_list:
#     #lowercase & expand contractions
#     entity = contractions.fix(entity.lower())
#     #stop word remoal
#     temp_word_tokenized_entity = word_tweet_tokenizer.tokenize(entity)
#     entity_wo_stopword = [word for word in temp_word_tokenized_entity if word not in clinical_stop_words]
#     temp_entity = (" ").join(entity_wo_stopword)
#     new_list.append(temp_entity)

#   return list(OrderedDict.fromkeys(new_list))

# def stanza_find_anatomy_entities(query):
#   ##print("Extracting anatomy entities (/stanza) from the following query: ", query[0:50], '\n')
#   if anat_pipeline:
#       temp_doc = anat_pipeline(query)

#       anatomy = []

#       for entity in temp_doc.entities:
#           anatomy.append(entity.text)

#       #we will be returning entities in their raw format for stanza
#       # if anatomy:
#       #     anatomy = stanza_remove_clinical_stopwords(anatomy)
#           #anatomy = list(OrderedDict.fromkeys(anatomyWoStopWords))
      
#       ##print("anatomy entities (/stanza) found: ", anatomy)
#       return anatomy

# def stanza_find_i2b2_entities(query):
#   ##print("Extracting i2b2 entities (/stanza) from the following query: ", query[0:50],'...\n')
#   if i2b2_pipeline:
#       temp_doc = i2b2_pipeline(query)
#       problems = []
#       treatments = []
#       tests = []
      
#       for entity in temp_doc.entities:
#           if entity.type == 'PROBLEM':
#               problems.append(entity.text)
#           elif entity.type == 'TREATMENT':
#               treatments.append(entity.text)
#           elif entity.type == 'TEST':
#               tests.append(entity.text)
      
#       #we will be returning entities in their raw format for stanza
#       #using set() to remove obvious duplicates
#       # if problems:
#       #     problems = stanza_remove_clinical_stopwords(problems)
#       #     #problems = list(OrderedDict.fromkeys(problemsWoStopWords))
#       # if treatments:
#       #     treatments = stanza_remove_clinical_stopwords(treatments)
#       #     #treatments = list(OrderedDict.fromkeys(treatmentsWoStopWords))
#       # if tests:
#       #     tests = stanza_remove_clinical_stopwords(tests)
#       #     #tests = list(OrderedDict.fromkeys(testsWoStopWords))

#       ##print("problem / symptoms entities (/stanza) found: ", problems)
#       ##print("treatments / mediations, operations etc. entities (/stanza) found: ", treatments)
#       ##print("tests / procedures entities (/stanza) found: ", tests)
#       return problems, treatments, tests

# def stanza_find_ncbi_diseases(query):
#   ##print("Extracting ncbi disease entities (/stanza) from the following query: ", query[0:50], '...\n')
#   if ncbi_disease_pipeline:
#       temp_doc = ncbi_disease_pipeline(query)
#       diseases = []

#       for entity in temp_doc.entities:
#           diseases.append(entity.text)

#       #we will be returning entities in their raw format for stanza
#       #using set() to remove obvious duplicates
#       # if diseases:
#       #     diseases = stanza_remove_clinical_stopwords(diseases) 
#       #     #diseases = list(OrderedDict.fromkeys(diseasesWoStopWords))

#       ##print("disease entities (/stanza) found: ", diseases)
#       return diseases

# def stanza_find_medical_entities(query: str):
#   #time_of_req = time.time()
#   ##print('incoming request to /stanza fxn: ', query[0:50], ' at ', time_of_req)
  
#   anatomy = []
#   problems = []
#   treatments = []
#   tests = []
#   diseases = []
#   #default error state
#   error_messages = []
#   error_code = 1
  
#   #Try to find varying entities
#   try:
#       anatomy = stanza_find_anatomy_entities(query)
#   except Exception as e:
#       errorString = repr(e)
#       error_messages.append('Error NER (anatem): '+errorString)
#       #print('Error NER (anatem): '+errorString+' see traceback:')
#       #print(traceback.format_exc())

#   try: 
#       problems, treatments, tests = stanza_find_i2b2_entities(query)
#   except Exception as e:
#       errorString = repr(e)
#       error_messages.append('Error NER (i2b2): '+errorString)
#       #print('Error NER (i2b2): '+errorString+' see traceback:')
#       #print(traceback.format_exc())


#   try:
#       diseases = stanza_find_ncbi_diseases(query)
#       #print('\nDiseases found by ncbi model: \n', diseases)

#   except Exception as e:
#       errorString = repr(e)
#       error_messages.append('Error NER (ncbi_disease): '+errorString)
#       #print('Error NER (ncbi_disease): '+errorString+' see traceback:')
#       #print(traceback.format_exc()) 

#   #check if errorMessages is still null then change errorCode to 0
#   if len(error_messages) == 0:
#       error_code = 0

#   # #structure resp:
#   # symptoms = problems
#   # body_structures = anatomy
#   # medications = treatments
#   # conditions = diseases
#   # procedures = tests
#   # api_version = 1.0
#   # source = 'stanza'
#   run_time_in_secs =  0.0
  
#   structured_resp = StanzaEntities(query=query, symptoms=problems, body_structures=anatomy, medications=treatments, conditions=diseases, procedures=tests, error_code=error_code, error_messages=error_messages, run_time_in_secs=run_time_in_secs, api_version=1.0, source='stanza')

#   return structured_resp
#   #return {'query': query, 'symptoms': problems, 'body_structures' : anatomy, 'medications': treatments, 'conditions' : diseases, 'procedures' : tests, 'error_code' : error_code, 'error_messages' : error_messages, 'run_time_in_secs': run_time_in_secs, 'api_version': 1.0, 'source': 'stanza'}

# ################################################################################################################################################
# #AssemblyAI API Code################################################################################################################################
# ################################################################################################################################################

# def pass_filter_csv_stoplist_assemblyai(symptoms, procedures, extra):
#   #handling extra
#   for entity in extra:
#     new_extra = search_local_csv(entity['keyword'], 'extra')
#     entity['keyword'] = new_extra['keyword']
#     entity['label_score'] = new_extra['label_score']
#     entity['properties']['csv'] = new_extra['properties']['csv']

#   #anatomy_stop_words, symptom_stop_words, condition_stop_words, medication_stop_words, procedure_stop_words
#     if entity['keyword'].lower() in anatomy_stop_words or entity['keyword'].lower() in symptom_stop_words or entity['keyword'].lower() in condition_stop_words or entity['keyword'].lower() in medication_stop_words or entity['keyword'].lower() in procedure_stop_words:
#       #print("following entity found in stop_words: ", entity['keyword'])
#       entity['label_score'] = 0  
#     # if entity['label_score'] == 0:
#     #   #temp_ent = entity
#     #   temp_keyword = search_local_csv(entity['keyword'], 'extra')
#     #   if temp_keyword != 'Not Found':
#     #     entity['keyword'] = temp_keyword
#     #     entity['label_score'] = 1

#     if entity['keyword'].lower() in anatomy_stop_words or entity['keyword'].lower() in medication_stop_words or entity['keyword'].lower() in condition_stop_words or entity['keyword'].lower() in procedure_stop_words or entity['keyword'].lower() in symptom_stop_words:
#       #print('following entity found in stop_words: ', entity['keyword'])
#       entity['label_score'] = 0
  
#   #handling symptoms
#   for symptom in symptoms:
#     new_symptom = search_local_csv(symptom['keyword'], 'symptom')
#     symptom['keyword'] = new_symptom['keyword']
#     symptom['label_score'] = new_symptom['label_score']
#     symptom['properties']['csv'] = new_symptom['properties']['csv']
    
#     if symptom['keyword'].lower() in symptom_stop_words:
#       #print('following symptom: {}, is in the stop list'.format(symptom['keyword']))
#       symptom['label_score'] = 0

#   #handling tests
#   for test in procedures:
#     new_test = search_local_csv(test['keyword'], 'test')
#     test['keyword'] = new_test['keyword']
#     test['label_score'] = new_test['label_score']
#     test['properties']['csv'] = new_test['properties']['csv']
    
#     if test['keyword'].lower() in procedure_stop_words:
#       #print('following procedure/test: {}, is in the stop list'.format(test['keyword']))
#       test['label_score'] = 0

#   return symptoms, procedures, extra

# # def labelscore_with_quickumls_assemblyai(input_list, type):
# #   if type == 'extra':
# #     new_extra_entities_postumls = []
# #     for entity in input_list:
# #       temp_umls_matches_of_extras = []
# #       #print('looking for following extra in quickumls: ',entity['keyword'])
# #       umls_match = extra_matchers.match(entity['keyword'], best_match=True, ignore_syntax=False)
# #       if umls_match:
# #         for item in umls_match:
# #           for entry in item:
# #             if entry['similarity'] >= 0.8:
# #               temp_umls_matches_of_extras.append({'term': entry['term'].lower(), 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
# #               #print('found :', entry['term'].lower(), 'with similarity: ', entry['similarity'])
# #             else:
# #               #print('not similarity >= 0.8:', entry['term'].lower(), entry['similarity'])
# #         if temp_umls_matches_of_extras:
# #           calculated_label_score = calculate_label_score_post_umls(entity['keyword'], temp_umls_matches_of_extras)
# #           returning_extra = {'keyword': entity['keyword'], 'original_keyword': entity['original_keyword'], 'source_sentence':entity['source_sentence'], 'label_score': calculated_label_score, 'properties': {'speaker_id': entity['properties']['speaker_id'],'umls': temp_umls_matches_of_extras}}
# #           new_extra_entities_postumls.append(returning_extra)
# #       else:
# #         returning_extra = {'keyword': entity['keyword'], 'original_keyword': entity['original_keyword'], 'source_sentence':entity['source_sentence'], 'label_score': 0.0, 'properties': {'speaker_id': entity['properties']['speaker_id'],'umls': []}}
# #         new_extra_entities_postumls.append(returning_extra)
# #     #print("final list by quickumls extras", new_extra_entities_postumls)
# #     return new_extra_entities_postumls

# #   if type == 'symptoms':
# #     new_symptom_entities_postumls = []
# #     for symptom in input_list:
# #       temp_umls_matches_of_symptom = []
# #       #print('looking for following symptom in quickumls: ',symptom['keyword'])
# #       umls_match = symptom_matcher.match(symptom['keyword'], best_match=True, ignore_syntax=False)
# #       if umls_match:
# #         for entity in umls_match:
# #             for entry in entity:
# #                 if entry['similarity'] >= 0.8:
# #                     temp_umls_matches_of_symptom.append({'term': entry['term'].lower(), 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
# #                     #print('found :', entry['term'].lower(), 'with similarity: ', entry['similarity'])
# #                 else:
# #                     #print('no similarity >= 0.8:', entry['term'].lower(), 'similarity:', entry['similarity'])
# #         if temp_umls_matches_of_symptom:
# #           calculated_label_score = calculate_label_score_post_umls(symptom['keyword'], temp_umls_matches_of_symptom)
# #           returning_symptom = {'keyword': symptom['keyword'], 'original_keyword': symptom['original_keyword'],'source_sentence': symptom['source_sentence'], 'label_score':calculated_label_score, 'properties': {'umls': temp_umls_matches_of_symptom, 'speaker_id': symptom['properties']['speaker_id']}}
# #           new_symptom_entities_postumls.append(returning_symptom)
# #       else:
# #         returning_symptom = {'keyword': symptom['keyword'], 'original_keyword': symptom['original_keyword'],'source_sentence': symptom['source_sentence'], 'label_score':0.0, 'properties': {'umls': [], 'speaker_id': symptom['properties']['speaker_id']}}
# #         new_symptom_entities_postumls.append(returning_symptom)
          
# #     #print('final list by quickumls, ', new_symptom_entities_postumls)
# #     return new_symptom_entities_postumls
  
# #   if type == 'tests':
# #     new_tests_entities_postumls = []
# #     for entity in input_list:
# #       temp_umls_matches_of_tests = []
# #       #print('looking for following test in quickumls: ',entity['keyword'])
# #       umls_match = tests_matchers.match(entity['keyword'], best_match=True, ignore_syntax=False)
# #       if umls_match:
# #         for item in umls_match:
# #             for entry in item:
# #                 if entry['similarity'] >= 0.8:
# #                     temp_umls_matches_of_tests.append({'term': entry['term'].lower(), 'semtype': str(entry['semtypes']), 'similarity': entry['similarity']})
# #                     #print('found :', entry['term'].lower(), 'with similarity: ', entry['similarity'])
# #                 else:
# #                     #print('no similarity >= 0.8:', entry['term'].lower(), 'similarity:', entry['similarity'])
# #         if temp_umls_matches_of_tests:
# #           calculated_label_score = calculate_label_score_post_umls(entity['keyword'], temp_umls_matches_of_tests)
# #           returning_test = {'keyword': entity['keyword'], 'original_keyword': entity['original_keyword'],'source_sentence': entity['source_sentence'], 'label_score':calculated_label_score, 'properties': {'umls': temp_umls_matches_of_tests}}
# #           new_tests_entities_postumls.append(returning_test)
# #       else:
# #         returning_tests = {'keyword': entity['keyword'], 'original_keyword': entity['original_keyword'],'source_sentence': entity['source_sentence'], 'label_score':0.0, 'properties': {'umls': []}}
# #         new_tests_entities_postumls.append(returning_tests)

# #     #print('final list by quickumls, ', new_tests_entities_postumls)
# #     return new_tests_entities_postumls

#   # return []